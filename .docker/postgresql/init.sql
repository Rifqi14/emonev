--
-- PostgreSQL database dump
--

-- Dumped from database version 12.17
-- Dumped by pg_dump version 15.3

-- Started on 2024-01-17 22:37:12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA IF NOT EXISTS public;


--
-- TOC entry 4187 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 223 (class 1259 OID 64051)
-- Name: activity; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.activity (
    id bigint NOT NULL,
    title character varying NOT NULL,
    code character varying NOT NULL,
    program_id bigint,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    sub_activity character varying
);


--
-- TOC entry 222 (class 1259 OID 64049)
-- Name: activity_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.activity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4188 (class 0 OID 0)
-- Dependencies: 222
-- Name: activity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.activity_id_seq OWNED BY public.activity.id;


--
-- TOC entry 235 (class 1259 OID 64339)
-- Name: data_triwulan; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.data_triwulan (
    id bigint NOT NULL,
    activity_name character varying NOT NULL,
    activity_location character varying NOT NULL,
    fund_source_id bigint,
    fund_ceiling numeric(16,2) DEFAULT '0'::numeric NOT NULL,
    management_organization character varying,
    pptk_name character varying,
    contract_number_date character varying,
    contractor_name character varying,
    implementation_period character varying,
    contract_value numeric(16,2) DEFAULT '0'::numeric NOT NULL,
    physical_realization numeric(16,2) DEFAULT 0 NOT NULL,
    fund_realization numeric(16,2) DEFAULT 0 NOT NULL,
    activity_volume character varying,
    activity_output character varying,
    direct_target_group character varying,
    indirect_target_group character varying,
    local_workforce numeric(16,2) DEFAULT 0 NOT NULL,
    non_local_workforce numeric(16,2) DEFAULT 0 NOT NULL,
    problems character varying,
    solution character varying,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    file text,
    user_id bigint,
    procurement_type character varying,
    procurement_method character varying,
    activity_id bigint,
    activity_form character varying,
    contract_date date,
    pic_name character varying,
    leader_name character varying,
    optional character varying,
    reason text
);


--
-- TOC entry 234 (class 1259 OID 64337)
-- Name: data_triwulan_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.data_triwulan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4189 (class 0 OID 0)
-- Dependencies: 234
-- Name: data_triwulan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.data_triwulan_id_seq OWNED BY public.data_triwulan.id;


--
-- TOC entry 229 (class 1259 OID 64291)
-- Name: fund_sources; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fund_sources (
    id bigint NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    fund_source_total numeric(20,2) DEFAULT '0'::numeric
);


--
-- TOC entry 228 (class 1259 OID 64289)
-- Name: fund_sources_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fund_sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4190 (class 0 OID 0)
-- Dependencies: 228
-- Name: fund_sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fund_sources_id_seq OWNED BY public.fund_sources.id;


--
-- TOC entry 227 (class 1259 OID 64085)
-- Name: master; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.master (
    id bigint NOT NULL,
    description text,
    triwulan_id bigint,
    organization_id integer,
    purpose_id bigint,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    user_id bigint
);


--
-- TOC entry 226 (class 1259 OID 64083)
-- Name: master_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.master_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4191 (class 0 OID 0)
-- Dependencies: 226
-- Name: master_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.master_id_seq OWNED BY public.master.id;


--
-- TOC entry 225 (class 1259 OID 64070)
-- Name: master_occassion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.master_occassion (
    id bigint NOT NULL,
    master_id bigint,
    occassion_id bigint,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 224 (class 1259 OID 64068)
-- Name: master_occassion_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.master_occassion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4192 (class 0 OID 0)
-- Dependencies: 224
-- Name: master_occassion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.master_occassion_id_seq OWNED BY public.master_occassion.id;


--
-- TOC entry 203 (class 1259 OID 63852)
-- Name: migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    "timestamp" bigint NOT NULL,
    name character varying NOT NULL
);


--
-- TOC entry 202 (class 1259 OID 63850)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4193 (class 0 OID 0)
-- Dependencies: 202
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 219 (class 1259 OID 63992)
-- Name: occassion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.occassion (
    id bigint NOT NULL,
    code character varying NOT NULL,
    title character varying NOT NULL,
    status_id integer,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 218 (class 1259 OID 63990)
-- Name: occassion_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.occassion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4194 (class 0 OID 0)
-- Dependencies: 218
-- Name: occassion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.occassion_id_seq OWNED BY public.occassion.id;


--
-- TOC entry 209 (class 1259 OID 63889)
-- Name: organization; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.organization (
    id integer NOT NULL,
    code character varying NOT NULL,
    title character varying NOT NULL,
    status_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 208 (class 1259 OID 63887)
-- Name: organization_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.organization_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4195 (class 0 OID 0)
-- Dependencies: 208
-- Name: organization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.organization_id_seq OWNED BY public.organization.id;


--
-- TOC entry 233 (class 1259 OID 64317)
-- Name: procurement_method; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.procurement_method (
    id bigint NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 232 (class 1259 OID 64315)
-- Name: procurement_method_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.procurement_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4196 (class 0 OID 0)
-- Dependencies: 232
-- Name: procurement_method_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.procurement_method_id_seq OWNED BY public.procurement_method.id;


--
-- TOC entry 231 (class 1259 OID 64304)
-- Name: procurement_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.procurement_type (
    id bigint NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 230 (class 1259 OID 64302)
-- Name: procurement_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.procurement_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4197 (class 0 OID 0)
-- Dependencies: 230
-- Name: procurement_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.procurement_type_id_seq OWNED BY public.procurement_type.id;


--
-- TOC entry 217 (class 1259 OID 63974)
-- Name: program; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.program (
    id bigint NOT NULL,
    title character varying NOT NULL,
    code character varying NOT NULL,
    status_id integer,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    occassion_id bigint
);


--
-- TOC entry 216 (class 1259 OID 63972)
-- Name: program_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.program_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4198 (class 0 OID 0)
-- Dependencies: 216
-- Name: program_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.program_id_seq OWNED BY public.program.id;


--
-- TOC entry 215 (class 1259 OID 63961)
-- Name: purpose; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.purpose (
    id bigint NOT NULL,
    title character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 214 (class 1259 OID 63959)
-- Name: purpose_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.purpose_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4199 (class 0 OID 0)
-- Dependencies: 214
-- Name: purpose_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.purpose_id_seq OWNED BY public.purpose.id;


--
-- TOC entry 221 (class 1259 OID 64017)
-- Name: report; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.report (
    id bigint NOT NULL,
    description text,
    program_description text,
    triwulan_id bigint,
    organization_id integer,
    occassion_id bigint,
    program_id bigint,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 220 (class 1259 OID 64015)
-- Name: report_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4200 (class 0 OID 0)
-- Dependencies: 220
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.report_id_seq OWNED BY public.report.id;


--
-- TOC entry 207 (class 1259 OID 63876)
-- Name: role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.role (
    id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 206 (class 1259 OID 63874)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4201 (class 0 OID 0)
-- Dependencies: 206
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- TOC entry 237 (class 1259 OID 64501)
-- Name: setting; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.setting (
    id bigint NOT NULL,
    key character varying NOT NULL,
    value text NOT NULL,
    data_type character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 236 (class 1259 OID 64499)
-- Name: setting_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4202 (class 0 OID 0)
-- Dependencies: 236
-- Name: setting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.setting_id_seq OWNED BY public.setting.id;


--
-- TOC entry 205 (class 1259 OID 63863)
-- Name: status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.status (
    id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 204 (class 1259 OID 63861)
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4203 (class 0 OID 0)
-- Dependencies: 204
-- Name: status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.status_id_seq OWNED BY public.status.id;


--
-- TOC entry 213 (class 1259 OID 63948)
-- Name: triwulan; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.triwulan (
    id bigint NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 212 (class 1259 OID 63946)
-- Name: triwulan_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.triwulan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4204 (class 0 OID 0)
-- Dependencies: 212
-- Name: triwulan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.triwulan_id_seq OWNED BY public.triwulan.id;


--
-- TOC entry 211 (class 1259 OID 63908)
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."user" (
    id bigint NOT NULL,
    username character varying NOT NULL,
    password character varying NOT NULL,
    name character varying,
    organization_id integer,
    role_id integer NOT NULL,
    status_id integer,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    email character varying,
    created_by character varying
);


--
-- TOC entry 210 (class 1259 OID 63906)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 4205 (class 0 OID 0)
-- Dependencies: 210
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- TOC entry 238 (class 1259 OID 64558)
-- Name: user_organization; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_organization (
    user_id bigint NOT NULL,
    organization_id integer NOT NULL
);


--
-- TOC entry 3920 (class 2604 OID 64054)
-- Name: activity id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.activity ALTER COLUMN id SET DEFAULT nextval('public.activity_id_seq'::regclass);


--
-- TOC entry 3939 (class 2604 OID 64342)
-- Name: data_triwulan id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_triwulan ALTER COLUMN id SET DEFAULT nextval('public.data_triwulan_id_seq'::regclass);


--
-- TOC entry 3929 (class 2604 OID 64294)
-- Name: fund_sources id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fund_sources ALTER COLUMN id SET DEFAULT nextval('public.fund_sources_id_seq'::regclass);


--
-- TOC entry 3926 (class 2604 OID 64088)
-- Name: master id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master ALTER COLUMN id SET DEFAULT nextval('public.master_id_seq'::regclass);


--
-- TOC entry 3923 (class 2604 OID 64073)
-- Name: master_occassion id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master_occassion ALTER COLUMN id SET DEFAULT nextval('public.master_occassion_id_seq'::regclass);


--
-- TOC entry 3892 (class 2604 OID 63855)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 3914 (class 2604 OID 63995)
-- Name: occassion id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.occassion ALTER COLUMN id SET DEFAULT nextval('public.occassion_id_seq'::regclass);


--
-- TOC entry 3899 (class 2604 OID 63892)
-- Name: organization id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.organization ALTER COLUMN id SET DEFAULT nextval('public.organization_id_seq'::regclass);


--
-- TOC entry 3936 (class 2604 OID 64320)
-- Name: procurement_method id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procurement_method ALTER COLUMN id SET DEFAULT nextval('public.procurement_method_id_seq'::regclass);


--
-- TOC entry 3933 (class 2604 OID 64307)
-- Name: procurement_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procurement_type ALTER COLUMN id SET DEFAULT nextval('public.procurement_type_id_seq'::regclass);


--
-- TOC entry 3911 (class 2604 OID 63977)
-- Name: program id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.program ALTER COLUMN id SET DEFAULT nextval('public.program_id_seq'::regclass);


--
-- TOC entry 3908 (class 2604 OID 63964)
-- Name: purpose id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.purpose ALTER COLUMN id SET DEFAULT nextval('public.purpose_id_seq'::regclass);


--
-- TOC entry 3917 (class 2604 OID 64020)
-- Name: report id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.report ALTER COLUMN id SET DEFAULT nextval('public.report_id_seq'::regclass);


--
-- TOC entry 3896 (class 2604 OID 63879)
-- Name: role id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- TOC entry 3948 (class 2604 OID 64504)
-- Name: setting id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.setting ALTER COLUMN id SET DEFAULT nextval('public.setting_id_seq'::regclass);


--
-- TOC entry 3893 (class 2604 OID 63866)
-- Name: status id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.status ALTER COLUMN id SET DEFAULT nextval('public.status_id_seq'::regclass);


--
-- TOC entry 3905 (class 2604 OID 63951)
-- Name: triwulan id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.triwulan ALTER COLUMN id SET DEFAULT nextval('public.triwulan_id_seq'::regclass);


--
-- TOC entry 3902 (class 2604 OID 63911)
-- Name: user id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- TOC entry 4166 (class 0 OID 64051)
-- Dependencies: 223
-- Data for Name: activity; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.activity VALUES (29, 'Tes', 'A9247929', 16, '2023-10-07 23:52:45.92821', '2023-10-07 23:52:45.92821', 'Tes');


--
-- TOC entry 4178 (class 0 OID 64339)
-- Dependencies: 235
-- Data for Name: data_triwulan; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.data_triwulan VALUES (13, 'test', 'testlocation', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 22:00:45.068202', '2023-09-28 22:00:45.068202', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (14, 'asd', 'asd', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 22:03:30.08023', '2023-09-28 22:03:30.08023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (15, 'das', 'asd', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 22:07:47.765525', '2023-09-28 22:07:47.765525', 'public/upload/data-triwulan/1695913667695_data-triwulan.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (17, 'asd', 'asdas', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 22:18:37.181253', '2023-09-28 22:18:37.181253', 'public/upload/data-triwulan/1695914317139_data-triwulan.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (16, 'aSd', 'asasd', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 22:10:30.127041', '2023-09-28 22:10:30.127041', 'public/upload/data-triwulan/1695913830016_data-triwulan.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (23, 'Testing', 'Testing', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-10-08 17:43:11.752527', '2023-10-08 17:43:11.752527', NULL, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (24, 'Test', 'RTest', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-10-08 17:51:56.898371', '2023-10-08 17:51:56.898371', NULL, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (25, 'Tes1', 'Tes1', 16, 123.00, 'Tes1', 'Tes1', '123123', 'Tes1', 'Tes1', 321.00, 2131.00, 2341.00, 'Tes1', 'Tes1', 'Tes1', 'Tes1', 213.00, 2131.00, 'Tes1', 'Tes1', '2023-10-08 19:43:16.699276', '2023-10-08 19:43:16.699276', 'public/upload/data-triwulan/1696768996651_data-triwulan.jpeg', 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (26, 'Koordinasi Pelaksanaan Sinergitas dan Harmonisasi Perencanaan Pembangunan Daerah Bidang Pembangunan Manusia', 'Kabupaten Sorong', 19, 99999999.00, 'Baperlitbang Kabupaten Sorong', 'Philipus E.R.Paraibabo,SE.,M.Ec.Dev', NULL, NULL, 'Januari-Desember', 0.00, 50.00, 59870000.00, '1', 'Jumlah Laporan Hasil Sinkronisasi Renstra/Renja dengan RKPD/RPJMD pada Bidang Pembangunan Manusia', 'Terlaksananya Koordinasi Bidang Pembangunan Manusia', NULL, 3.00, 0.00, NULL, NULL, '2023-10-10 16:07:50.722243', '2023-10-10 16:07:50.722243', NULL, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (27, 'Koordinasi Pelaksanaan Sinergitas dan Harmonisasi Perencanaan Pembangunan Daerah Bidang Pembangunan Manusia', 'Kabupaten Sorong', 19, 99999999.00, 'Baperlitbang Kabupaten Sorong', 'Philipus E.R.Paraibabo,SE.,M.Ec.Dev', NULL, NULL, 'Januari-Desember', 99999999.00, 50.00, 59870000.00, '1', 'Jumlah Laporan Hasil Sinkronisasi Renstra/Renja dengan RKPD/RPJMD pada Bidang Pembangunan Manusia', 'Terlaksananya Koordinasi Bidang Pembangunan Manusia', NULL, 3.00, 0.00, NULL, NULL, '2023-10-10 16:12:48.028286', '2023-10-10 16:12:48.028286', NULL, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (28, 'Koordinasi Pelaksanaan Sinergitas dan Harmonisasi Perencanaan Pembangunan Daerah Bidang Pembangunan Manusia', 'Kabupaten Sorong', 20, 250.00, 'Baperlitbang Kabupaten Sorong', 'Philipus E.R.Paraibabo,SE.,M.Ec.Dev', '1112023', NULL, 'Januari-Desember', 250.00, 0.00, 0.00, '1', NULL, 'Telaksananya Monitoring Kegiatan bersumber dana Otsus', NULL, 20.00, 0.00, NULL, NULL, '2023-10-10 16:26:28.506131', '2023-10-10 16:26:28.506131', NULL, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (29, 'Asistensi Penyusunan Dokumen Perencanaan Pembangunan Perangkat Daerah Bidang Pemerintahan', 'Kabupaten Sorong', 20, 250.00, 'Baperlitbang Kabupaten Sorong', 'Philipus E.R.Paraibabo,SE.,M.Ec.Dev', '5052023', NULL, 'Januari-Desember', 250.00, 250.00, 250.00, '1', NULL, 'terlaksananya Asistensi Penyusunan Dokumen RAP OTSUS', NULL, 2.00, 0.00, NULL, NULL, '2023-10-10 16:31:00.412564', '2023-10-10 16:31:00.412564', 'public/upload/data-triwulan/1696930260362_data-triwulan.02', 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (30, 'Pengelolaan Data dalam Sistem Informasi Pemerintahan Daerah di Bidang Pembangunan Daerah', 'Kabupaten Sorong', 20, 450.00, 'Baperlitbang Kabupaten Sorong', 'Philipus E.R.Paraibabo,SE.,M.Ec.Dev', '20102023', NULL, 'Juni-Desember', 450.00, 70.00, 450.00, '1', NULL, 'Terlaksnanya koordinasi Kader Kampung dan kelurahan SAIK+ Kabupaten Sorong', NULL, 5.00, 252.00, NULL, NULL, '2023-10-10 16:35:15.041365', '2023-10-10 16:35:15.041365', 'public/upload/data-triwulan/1696930514999_data-triwulan.44', 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (31, 'Asistensi Penyusunan Dokumen Perencanaan Pembangunan Perangkat Daerah Bidang Pembangunan Manusia', 'Kabupaten Sorong', 20, 200.00, 'Baperlitbang Kabupaten Sorong', 'Philipus E.R.Paraibabo,SE.,M.Ec.Dev', '23092023', NULL, 'Juni-Desember', 200.00, 60.00, 200.00, '1', NULL, 'terlaksananya Asistensi Pendataan SAIK+', NULL, 5.00, 0.00, NULL, NULL, '2023-10-10 16:42:30.501592', '2023-10-10 16:42:30.501592', 'public/upload/data-triwulan/1696930950468_data-triwulan.27', 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (32, 'Fasilitasi, Pelaksanaan dan Evaluasi Penelitian dan Pengembangan Bidang Penyelenggaraan Otonomi Daerah', 'Kabupaten Sorong', 20, 371916.00, 'Baperlitbang Kabupaten Sorong', 'Philipus E.R.Paraibabo,SE.,M.Ec.Dev', '30112023', NULL, 'november-Desember', 371916.00, 0.00, 0.00, NULL, NULL, 'Telaksananya Bimtek RAP OTSUS', NULL, 20.00, 0.00, NULL, NULL, '2023-10-10 16:49:54.277382', '2023-10-10 16:49:54.277382', NULL, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (33, 'Seminar Perekonomian', 'Kabupaten Sorong', 22, 50000000.00, 'Baperlitbang', 'Agata F Tenau', NULL, '-', 'oktober-desember', 50000000.00, 80.00, 30000000.00, NULL, NULL, 'UMKM dan BUMDes', NULL, 10.00, 3.00, NULL, NULL, '2023-10-10 18:48:19.324704', '2023-10-10 18:48:19.324704', NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (34, 'seminar perekonomian', 'kabupaten sorong', 21, 50000000.00, 'BAPERLITBANG', 'AGATHA TENAU', NULL, '-', 'OKTOBER-DESEMBER', 50000000.00, 80.00, 30000000.00, NULL, NULL, 'UMKM DAN BUMDES', NULL, 10.00, 3.00, NULL, NULL, '2023-10-10 18:48:24.230987', '2023-10-10 18:48:24.230987', NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (7, 'Testing', 'Testing', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 16:37:02.835807', '2023-09-28 16:37:02.835807', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (37, 'Sgsg', 'fwqfeqg', 21, 20000000000.00, 'Sgwg', 'gsgw', '324', 'Dbhd', 'her', 41.00, 234.00, 23423.00, '532', 'sgfew', 'wgtw', 'wetw', 345.00, 25.00, 'ereryh', 'we4y4y', '2023-10-25 15:43:26.249875', '2023-10-27 19:33:53.634081', NULL, 27, 'Pekerjaan Konstruksi', 'Penunjukan Langsung', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (35, 'Test', 'Test', NULL, 200000000.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, 'Tes hambata', 'Tes solusi', '2023-10-24 20:15:56.799409', '2023-10-27 19:34:10.590283', NULL, 22, 'Jasa Konsultasi', 'Swakelola', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (2, 'aaasd', 'asdds', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 08:42:46.698376', '2023-09-28 08:42:46.698376', NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (3, '12', '12', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 08:43:11.829814', '2023-09-28 08:43:11.829814', NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (4, 'nama', 'lokasi', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 08:44:59.207012', '2023-09-28 08:44:59.207012', NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (5, '111', '11', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 08:45:31.965552', '2023-09-28 08:45:31.965552', NULL, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (6, 'asd', 'asd', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 08:51:07.804094', '2023-09-28 08:51:07.804094', NULL, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (12, 'test', 'testlocation', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 21:59:06.865979', '2023-09-28 21:59:06.865979', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (38, 'Tes output sub kegiatan 2', 'Tes lokasi kegiatan. 2', 23, 1000.00, 'Tes op pengelola', 'Tes pptk', '2141', 'Tes nama kontraktor', 'Tes jangka waktu', 1000.00, 1000.00, 1000.00, 'Tes volume', 'Tes output', 'Tes manfaat', 'Tes manfaat tdk', 10.00, 12.00, 'Tes hambata', 'Tes solusi', '2023-10-26 12:17:41.135981', '2023-11-08 12:08:38.390676', 'public/upload/data-triwulan/1699420044076_data-triwulan.jpeg', 27, 'Barang', 'Swakelola', 29, '1', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (39, 'Jdnndd', 'Nendn', 17, 1000000.00, 'Jdndnd', 'Jdndnd', '727272', 'Ndndn', 'Ndnnd', 73733.00, 500000.00, 73773.00, 'Jdnd', 'Jdndjd', 'Ndndnd', 'Ndndn', 73.00, 83.00, 'Jdndn', 'Jendnd', '2023-11-06 19:36:12.307475', '2023-11-09 20:06:58.996202', 'public/upload/data-triwulan/1699535218919_data-triwulan.jpeg', 27, 'Jasa Kelola', 'Penunjukan Langsung', 29, '2', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (9, 'Testing', 'Testing', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-09-28 17:30:17.585675', '2023-09-28 17:30:17.585675', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (40, 'Normalisasi Sungai Makotyamsa', 'Kelurahan Makotyamsa', 16, 1000000000.00, 'Dinas PU', 'Simon ', '21', 'CV. Mutiara', '10 Minggu', 90000000.00, 30.00, 20.00, '100 m', 'Normalisasi Sungai Makotyamsa', 'Masyarakat Makotyamsa', 'Masyarakat Kabupaten Sorong', 3.00, 2.00, '-', '-', '2023-11-30 12:19:04.997232', '2023-11-30 12:19:04.997232', NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (41, 'Bimtek E-Montir Pemda', 'Kabupaten Sorong', 20, 1000000000.00, 'Dinas Pendidikan ', 'Imam Ansori, M.Pd', '129', NULL, '12 Juni 2023 - 16 Desember 2023', 1800000000.00, 30.00, 30.00, '1', 'Terbangunnya Rumah Dinas guru SD 28 Klamono', 'SD di Distrik Klamono', 'Tertambahnya Rumah Dinas Guru di SD Klamono', 3.00, 3.00, '-', '-', '2023-11-30 12:19:38.874131', '2023-11-30 12:19:38.874131', NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (42, 'penyelanggaraan', 'tes', 26, 4554544545.00, 'tes', 'tes', '4555545555455', 'tes', '3 bulan', 15555421999.00, 45.00, 54545555545.00, 'tes', 'tes', 'tes', 'tes', 222222.00, 1111.00, 'tes', 'tes', '2023-11-30 12:21:43.210713', '2023-11-30 12:21:43.210713', NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (43, 'Test', 'Test', NULL, 0.00, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, '2023-12-02 04:58:44.41235', '2023-12-02 04:58:44.41235', NULL, 2, NULL, NULL, NULL, NULL, '2024-12-14', 'John Doe', 'John Doe', 'Forst Major', 'lorem ipsum');
INSERT INTO public.data_triwulan VALUES (45, '123', '123', NULL, 12321.00, NULL, NULL, NULL, NULL, NULL, 0.00, 12323.00, 12323.00, NULL, NULL, NULL, NULL, 123.00, 123.00, NULL, NULL, '2023-12-05 12:09:49.393562', '2023-12-05 12:09:49.393562', NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.data_triwulan VALUES (44, 'asdsad', 'asdasd', 23, 12312312.00, NULL, NULL, NULL, NULL, NULL, 0.00, 123.00, 123123123.00, NULL, NULL, NULL, NULL, 123123.00, 21312132.00, NULL, NULL, '2023-12-05 11:56:31.541139', '2023-12-05 15:05:57.440666', NULL, 25, NULL, NULL, NULL, NULL, '2023-12-21', NULL, 'pimpinan', 'Perubahan Kebijakan Adendum', 'alasan');
INSERT INTO public.data_triwulan VALUES (46, 'asdasd', 'asdasdasdasd', 22, 123123.00, 'asdasd', 'asdasd', 'asdads', 'asdsad', 'asdasdas', 0.00, 132312.00, 123123.00, 'asdasd', 'asdasd', 'asdasd', 'adasda', 123123.00, 123312.00, 'asdad', 'asddas', '2023-12-05 12:10:52.587543', '2023-12-05 15:07:06.029001', 'public/upload/data-triwulan/1701753052496_data-triwulan.29', 25, 'Jasa Kelola', 'Tender', 29, '2', '2023-12-21', 'asdasd', 'asdasdsa', 'Perubahan Kebijakan Adendum', 'asdasd');


--
-- TOC entry 4172 (class 0 OID 64291)
-- Dependencies: 229
-- Data for Name: fund_sources; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.fund_sources VALUES (1, 'string 1', '2023-09-21 22:53:23.02605', '2023-09-21 22:53:23.02605', 0.00);
INSERT INTO public.fund_sources VALUES (2, 'string 2', '2023-09-21 22:53:37.554372', '2023-09-21 22:53:37.554372', 0.00);
INSERT INTO public.fund_sources VALUES (3, 'string 3', '2023-09-21 22:53:40.919365', '2023-09-21 22:53:40.919365', 0.00);
INSERT INTO public.fund_sources VALUES (4, 'string 3', '2023-09-21 22:53:41.803385', '2023-09-21 22:53:41.803385', 0.00);
INSERT INTO public.fund_sources VALUES (5, 'string 3', '2023-09-21 22:53:42.728424', '2023-09-21 22:53:42.728424', 0.00);
INSERT INTO public.fund_sources VALUES (6, 'string 3', '2023-09-21 22:53:43.364962', '2023-09-21 22:53:43.364962', 0.00);
INSERT INTO public.fund_sources VALUES (7, 'string 3', '2023-09-21 22:53:43.687579', '2023-09-21 22:53:43.687579', 0.00);
INSERT INTO public.fund_sources VALUES (8, 'string 3', '2023-09-21 22:53:44.207151', '2023-09-21 22:53:44.207151', 0.00);
INSERT INTO public.fund_sources VALUES (9, 'string 3', '2023-09-21 22:53:45.213166', '2023-09-21 22:53:45.213166', 0.00);
INSERT INTO public.fund_sources VALUES (10, 'string 3', '2023-09-21 22:54:41.947835', '2023-09-21 22:54:41.947835', 0.00);
INSERT INTO public.fund_sources VALUES (11, 'string 3', '2023-09-21 22:54:42.355106', '2023-09-21 22:54:42.355106', 0.00);
INSERT INTO public.fund_sources VALUES (12, 'string 3', '2023-09-21 22:54:43.161189', '2023-09-21 22:54:43.161189', 0.00);
INSERT INTO public.fund_sources VALUES (15, 'test', '2023-10-08 08:40:51.545256', '2023-10-08 08:40:51.545256', 0.00);
INSERT INTO public.fund_sources VALUES (16, 'test2', '2023-10-08 17:00:00.064701', '2023-10-08 17:00:00.064701', 0.00);
INSERT INTO public.fund_sources VALUES (18, 'PAD', '2023-10-09 09:35:33.983331', '2023-10-09 09:35:33.983331', 0.00);
INSERT INTO public.fund_sources VALUES (19, 'Sisa Belanja Lainnya', '2023-10-10 15:54:48.706953', '2023-10-10 15:54:48.706953', 0.00);
INSERT INTO public.fund_sources VALUES (20, 'Dana Otonomi Khusus', '2023-10-10 16:23:11.232387', '2023-10-10 16:23:11.232387', 0.00);
INSERT INTO public.fund_sources VALUES (22, 'DBH', '2023-10-10 18:29:29.379556', '2023-10-10 18:29:29.379556', 0.00);
INSERT INTO public.fund_sources VALUES (23, 'kayu', '2023-10-10 19:41:55.609449', '2023-10-10 19:41:55.609449', 0.00);
INSERT INTO public.fund_sources VALUES (26, 'bot', '2023-10-25 20:58:31.126903', '2023-10-25 21:09:20.215194', 90000000.00);
INSERT INTO public.fund_sources VALUES (21, 'DBH', '2023-10-10 18:29:29.378505', '2023-10-10 18:29:29.378505', 90000000.00);
INSERT INTO public.fund_sources VALUES (17, 'TESLAGI', '2023-10-08 17:07:48.370934', '2023-11-06 19:28:09.865499', 10000000.00);


--
-- TOC entry 4170 (class 0 OID 64085)
-- Dependencies: 227
-- Data for Name: master; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.master VALUES (6, NULL, 1, 6, 2, '2023-09-28 11:30:51.016179', '2023-09-28 11:30:51.016179', NULL);
INSERT INTO public.master VALUES (7, NULL, 1, 10, 2, '2023-09-28 20:57:10.488928', '2023-09-28 20:57:10.488928', NULL);
INSERT INTO public.master VALUES (8, NULL, 1, 6, 3, '2023-09-29 07:02:40.981885', '2023-09-29 07:02:40.981885', NULL);
INSERT INTO public.master VALUES (10, NULL, 1, 12, 2, '2023-10-03 20:43:41.409813', '2023-10-03 20:43:41.409813', 25);
INSERT INTO public.master VALUES (11, NULL, 1, 10, 2, '2023-10-03 23:00:11.985145', '2023-10-03 23:00:11.985145', 27);
INSERT INTO public.master VALUES (12, NULL, 1, 12, 2, '2023-10-03 23:00:30.723813', '2023-10-03 23:00:30.723813', 27);
INSERT INTO public.master VALUES (13, NULL, 1, 13, 6, '2023-10-05 16:24:59.625512', '2023-10-05 16:24:59.625512', 27);
INSERT INTO public.master VALUES (14, NULL, 1, 12, 3, '2023-10-07 09:55:15.102327', '2023-10-07 09:55:15.102327', 24);
INSERT INTO public.master VALUES (9, NULL, 2, 16, 3, '2023-10-02 13:14:57.669116', '2023-10-07 09:57:13.354669', NULL);
INSERT INTO public.master VALUES (15, NULL, 1, 13, 3, '2023-10-07 11:03:01.587564', '2023-10-07 11:03:01.587564', 25);
INSERT INTO public.master VALUES (16, NULL, 1, 16, 3, '2023-10-07 12:01:28.197345', '2023-10-07 12:01:28.197345', 27);
INSERT INTO public.master VALUES (17, NULL, 1, 29, 6, '2023-10-08 00:17:00.242261', '2023-10-08 00:17:00.242261', 27);
INSERT INTO public.master VALUES (18, NULL, 1, 33, 1, '2023-10-08 00:19:00.189375', '2023-10-08 00:19:00.189375', 27);
INSERT INTO public.master VALUES (19, NULL, 1, 31, 6, '2023-10-08 19:41:50.151037', '2023-10-08 19:41:50.151037', 27);
INSERT INTO public.master VALUES (20, NULL, 3, 46, 6, '2023-10-10 16:54:13.077541', '2023-10-10 16:54:13.077541', 47);
INSERT INTO public.master VALUES (21, NULL, 4, 48, 3, '2023-10-10 18:52:38.866238', '2023-10-10 18:52:38.866238', 27);
INSERT INTO public.master VALUES (22, NULL, 4, 46, 6, '2023-10-10 18:53:26.705254', '2023-10-10 18:53:26.705254', 27);
INSERT INTO public.master VALUES (23, NULL, 4, 46, 6, '2023-10-10 18:53:39.130006', '2023-10-10 18:53:39.130006', 27);
INSERT INTO public.master VALUES (24, NULL, 3, 46, 2, '2023-10-24 20:07:55.244305', '2023-10-24 20:07:55.244305', 27);
INSERT INTO public.master VALUES (25, 'asdsd', 3, 46, 3, '2023-10-25 16:15:17.625438', '2023-10-25 16:15:17.625438', 25);
INSERT INTO public.master VALUES (26, 'inilah indikator', 2, 46, 3, '2023-10-25 16:16:15.226005', '2023-10-25 16:16:15.226005', 25);
INSERT INTO public.master VALUES (28, 'Kekkkr', 2, 45, 4, '2023-10-25 22:27:19.920796', '2023-10-27 19:06:34.441867', 27);
INSERT INTO public.master VALUES (27, 'asd', 2, 46, 1, '2023-10-25 16:25:50.170497', '2023-10-27 19:07:24.527533', 25);
INSERT INTO public.master VALUES (2, 'description data master', 1, 10, 4, '2023-09-17 16:20:19.445238', '2023-10-07 09:56:28.959435', 2);
INSERT INTO public.master VALUES (4, 'description data master', 1, 1, 1, '2023-09-17 16:20:48.500124', '2023-09-17 16:20:48.500124', 2);
INSERT INTO public.master VALUES (5, NULL, 1, 6, 1, '2023-09-28 11:29:54.167329', '2023-09-28 11:29:54.167329', 2);


--
-- TOC entry 4168 (class 0 OID 64070)
-- Dependencies: 225
-- Data for Name: master_occassion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.master_occassion VALUES (5, 4, 1, '2023-09-17 16:20:48.500124', '2023-09-17 16:20:48.500124');
INSERT INTO public.master_occassion VALUES (6, 4, 5, '2023-09-17 16:20:48.500124', '2023-09-17 16:20:48.500124');
INSERT INTO public.master_occassion VALUES (7, 4, 7, '2023-09-17 16:20:48.500124', '2023-09-17 16:20:48.500124');
INSERT INTO public.master_occassion VALUES (16, 5, 16, '2023-09-28 11:29:54.167329', '2023-09-28 11:29:54.167329');
INSERT INTO public.master_occassion VALUES (17, 5, 17, '2023-09-28 11:29:54.167329', '2023-09-28 11:29:54.167329');
INSERT INTO public.master_occassion VALUES (18, 6, 17, '2023-09-28 11:30:51.016179', '2023-09-28 11:30:51.016179');
INSERT INTO public.master_occassion VALUES (19, 6, 17, '2023-09-28 11:30:51.016179', '2023-09-28 11:30:51.016179');
INSERT INTO public.master_occassion VALUES (20, 6, 16, '2023-09-28 11:30:51.016179', '2023-09-28 11:30:51.016179');
INSERT INTO public.master_occassion VALUES (21, 7, 17, '2023-09-28 20:57:10.488928', '2023-09-28 20:57:10.488928');
INSERT INTO public.master_occassion VALUES (22, 7, 16, '2023-09-28 20:57:10.488928', '2023-09-28 20:57:10.488928');
INSERT INTO public.master_occassion VALUES (23, 8, 16, '2023-09-29 07:02:40.981885', '2023-09-29 07:02:40.981885');
INSERT INTO public.master_occassion VALUES (24, 8, 17, '2023-09-29 07:02:40.981885', '2023-09-29 07:02:40.981885');
INSERT INTO public.master_occassion VALUES (27, 10, 18, '2023-10-03 20:43:41.409813', '2023-10-03 20:43:41.409813');
INSERT INTO public.master_occassion VALUES (28, 11, 16, '2023-10-03 23:00:11.985145', '2023-10-03 23:00:11.985145');
INSERT INTO public.master_occassion VALUES (29, 12, 18, '2023-10-03 23:00:30.723813', '2023-10-03 23:00:30.723813');
INSERT INTO public.master_occassion VALUES (30, 13, 18, '2023-10-05 16:24:59.625512', '2023-10-05 16:24:59.625512');
INSERT INTO public.master_occassion VALUES (31, 14, 17, '2023-10-07 09:55:15.102327', '2023-10-07 09:55:15.102327');
INSERT INTO public.master_occassion VALUES (32, 14, 16, '2023-10-07 09:55:15.102327', '2023-10-07 09:55:15.102327');
INSERT INTO public.master_occassion VALUES (33, 2, 17, '2023-10-07 09:56:28.959435', '2023-10-07 09:56:28.959435');
INSERT INTO public.master_occassion VALUES (14, NULL, 5, '2023-09-17 16:37:03.780514', '2023-10-07 09:56:28.959435');
INSERT INTO public.master_occassion VALUES (15, NULL, 7, '2023-09-17 16:37:03.780514', '2023-10-07 09:56:28.959435');
INSERT INTO public.master_occassion VALUES (34, 9, 17, '2023-10-07 09:57:13.354669', '2023-10-07 09:57:13.354669');
INSERT INTO public.master_occassion VALUES (35, 15, 17, '2023-10-07 11:03:01.587564', '2023-10-07 11:03:01.587564');
INSERT INTO public.master_occassion VALUES (36, 15, 16, '2023-10-07 11:03:01.587564', '2023-10-07 11:03:01.587564');
INSERT INTO public.master_occassion VALUES (37, 16, 17, '2023-10-07 12:01:28.197345', '2023-10-07 12:01:28.197345');
INSERT INTO public.master_occassion VALUES (38, 17, 17, '2023-10-08 00:17:00.242261', '2023-10-08 00:17:00.242261');
INSERT INTO public.master_occassion VALUES (39, 18, 17, '2023-10-08 00:19:00.189375', '2023-10-08 00:19:00.189375');
INSERT INTO public.master_occassion VALUES (40, 18, 17, '2023-10-08 00:19:00.189375', '2023-10-08 00:19:00.189375');
INSERT INTO public.master_occassion VALUES (41, 19, 17, '2023-10-08 19:41:50.151037', '2023-10-08 19:41:50.151037');
INSERT INTO public.master_occassion VALUES (42, 20, 17, '2023-10-10 16:54:13.077541', '2023-10-10 16:54:13.077541');
INSERT INTO public.master_occassion VALUES (43, 21, 17, '2023-10-10 18:52:38.866238', '2023-10-10 18:52:38.866238');
INSERT INTO public.master_occassion VALUES (44, 21, 17, '2023-10-10 18:52:38.866238', '2023-10-10 18:52:38.866238');
INSERT INTO public.master_occassion VALUES (45, 21, 17, '2023-10-10 18:52:38.866238', '2023-10-10 18:52:38.866238');
INSERT INTO public.master_occassion VALUES (46, 22, 17, '2023-10-10 18:53:26.705254', '2023-10-10 18:53:26.705254');
INSERT INTO public.master_occassion VALUES (47, 23, 17, '2023-10-10 18:53:39.130006', '2023-10-10 18:53:39.130006');
INSERT INTO public.master_occassion VALUES (48, 24, 17, '2023-10-24 20:07:55.244305', '2023-10-24 20:07:55.244305');
INSERT INTO public.master_occassion VALUES (49, 25, 17, '2023-10-25 16:15:17.625438', '2023-10-25 16:15:17.625438');
INSERT INTO public.master_occassion VALUES (50, 26, 17, '2023-10-25 16:16:15.226005', '2023-10-25 16:16:15.226005');
INSERT INTO public.master_occassion VALUES (53, 28, 17, '2023-10-27 19:06:34.441867', '2023-10-27 19:06:34.441867');
INSERT INTO public.master_occassion VALUES (54, 27, 17, '2023-10-27 19:07:24.527533', '2023-10-27 19:07:24.527533');


--
-- TOC entry 4146 (class 0 OID 63852)
-- Dependencies: 203
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.migrations VALUES (1, 1694590510579, 'CreateStatusTable1694590510579');
INSERT INTO public.migrations VALUES (2, 1694617845458, 'CreateRoleTable1694617845458');
INSERT INTO public.migrations VALUES (3, 1694618266524, 'CreateOrganizationTable1694618266524');
INSERT INTO public.migrations VALUES (4, 1694619156385, 'CreateUserTable1694619156385');
INSERT INTO public.migrations VALUES (5, 1694747371384, 'CreateTriwulanTable1694747371384');
INSERT INTO public.migrations VALUES (6, 1694748126317, 'CreatePurposeTable1694748126317');
INSERT INTO public.migrations VALUES (7, 1694748593805, 'CreateProgramTable1694748593805');
INSERT INTO public.migrations VALUES (8, 1694749207413, 'CreateOccassionTable1694749207413');
INSERT INTO public.migrations VALUES (9, 1694749433597, 'UpdateProgramTable1694749433597');
INSERT INTO public.migrations VALUES (10, 1694750123030, 'CreateReportTable1694750123030');
INSERT INTO public.migrations VALUES (11, 1694750655842, 'CreateActivityTable1694750655842');
INSERT INTO public.migrations VALUES (12, 1694751050540, 'CreateMasterOccassionTable1694751050540');
INSERT INTO public.migrations VALUES (13, 1694751591203, 'CreateMasterTable1694751591203');
INSERT INTO public.migrations VALUES (14, 1694751729616, 'UpdateMasterOccassionTable1694751729616');
INSERT INTO public.migrations VALUES (15, 1695075506054, 'AddEmailColumnToUserTable1695075506054');
INSERT INTO public.migrations VALUES (16, 1695218576411, 'AddRelationInProgramToOccassionTable1695218576411');
INSERT INTO public.migrations VALUES (17, 1695310615977, 'CreateFundSourcesTable1695310615977');
INSERT INTO public.migrations VALUES (18, 1695312018827, 'CreateProcurementTypeTable1695312018827');
INSERT INTO public.migrations VALUES (19, 1695312979618, 'CreateProcurementMethodTable1695312979618');
INSERT INTO public.migrations VALUES (20, 1695372852866, 'CreateDataTriwulanTable1695372852866');
INSERT INTO public.migrations VALUES (21, 1695393107298, 'AddFileColumnToDataTriwulanTable1695393107298');
INSERT INTO public.migrations VALUES (22, 1695886981496, 'AddSubActivityToActivityTable1695886981496');
INSERT INTO public.migrations VALUES (23, 1695889510870, 'CreateSettingTable1695889510870');
INSERT INTO public.migrations VALUES (24, 1696044050969, 'CreateUserOrganizationTable1696044050969');
INSERT INTO public.migrations VALUES (25, 1696325172335, 'AddCreatedByToMasterTable1696325172335');
INSERT INTO public.migrations VALUES (26, 1696325519475, 'AddUserIdToDataTriwulanTable1696325519475');
INSERT INTO public.migrations VALUES (27, 1696326526279, 'RemoveRelationInDataTriwulanTable1696326526279');
INSERT INTO public.migrations VALUES (28, 1698160493368, 'UpdateFundSourceTable1698160493368');
INSERT INTO public.migrations VALUES (29, 1698162234580, 'UpdateDataTriwulanTable1698162234580');
INSERT INTO public.migrations VALUES (30, 1699997699212, 'AddCreatedByInUserTable1699997699212');
INSERT INTO public.migrations VALUES (31, 1699997897813, 'ChangeCreatedByInUserTable1699997897813');
INSERT INTO public.migrations VALUES (32, 1701467139671, 'UpdateDataTriwulanTable1701467139671');


--
-- TOC entry 4162 (class 0 OID 63992)
-- Dependencies: 219
-- Data for Name: occassion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.occassion VALUES (9, '1283162', 'judul a', 3, '2023-09-15 15:08:31.999073', '2023-09-15 15:12:17.784124');
INSERT INTO public.occassion VALUES (1, '0123811', 'kodok', 3, '2023-09-15 15:07:52.189064', '2023-09-26 16:24:53.00797');
INSERT INTO public.occassion VALUES (5, 'O2721582', 'kodok', 3, '2023-09-15 15:08:17.053611', '2023-09-26 16:25:02.276495');
INSERT INTO public.occassion VALUES (6, 'O5541586', 'kodok', 3, '2023-09-15 15:08:19.380635', '2023-09-26 16:25:09.179035');
INSERT INTO public.occassion VALUES (7, 'O7331587', 'kodok', 3, '2023-09-15 15:08:21.524608', '2023-09-26 16:26:30.70082');
INSERT INTO public.occassion VALUES (8, 'O1241588', 'kodok', 3, '2023-09-15 15:08:26.916883', '2023-09-26 16:27:26.200009');
INSERT INTO public.occassion VALUES (11, 'O66626810', 'test', 3, '2023-09-26 16:24:11.701139', '2023-09-26 16:27:48.105479');
INSERT INTO public.occassion VALUES (13, 'O59326813', 'test3', 3, '2023-09-26 16:27:38.604586', '2023-09-26 16:27:52.936717');
INSERT INTO public.occassion VALUES (15, 'O21926815', 'asd', 3, '2023-09-26 17:18:04.260526', '2023-09-26 17:19:38.037411');
INSERT INTO public.occassion VALUES (12, 'O90226812', 'test3', 3, '2023-09-26 16:27:34.911931', '2023-09-26 17:19:42.542648');
INSERT INTO public.occassion VALUES (14, 'O54126814', 'kodok', 3, '2023-09-26 16:27:41.550862', '2023-09-26 19:10:03.36771');
INSERT INTO public.occassion VALUES (18, 'O4362918', 'Tess', 3, '2023-10-02 13:05:43.468192', '2023-10-05 18:10:58.166659');
INSERT INTO public.occassion VALUES (19, 'O8147919', 'x2', 3, '2023-10-07 11:33:54.837724', '2023-10-07 11:34:03.724355');
INSERT INTO public.occassion VALUES (16, 'O66526816', 'asd', 3, '2023-09-26 17:20:18.682419', '2023-10-07 23:51:45.192457');
INSERT INTO public.occassion VALUES (20, 'O90110920', 'TES', 3, '2023-10-10 18:13:35.93789', '2023-10-10 18:13:45.474004');
INSERT INTO public.occassion VALUES (17, 'O16326817', 'Testes', 1, '2023-09-26 19:09:52.183323', '2023-10-11 12:21:48.332871');
INSERT INTO public.occassion VALUES (21, 'O7011921', 'Setyo', 3, '2023-10-11 12:22:03.123681', '2023-10-11 12:22:14.395787');
INSERT INTO public.occassion VALUES (22, 'O87928922', 'Aku5', 3, '2023-10-28 19:17:49.910081', '2023-10-28 19:17:59.552178');


--
-- TOC entry 4152 (class 0 OID 63889)
-- Dependencies: 209
-- Data for Name: organization; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.organization VALUES (1, '123122', 'judul ea', 3, '2023-09-14 23:24:42.271124', '2023-09-14 23:25:57.635055');
INSERT INTO public.organization VALUES (7, 'Org2542687', 'asd', 3, '2023-09-26 17:20:40.27117', '2023-09-26 17:20:54.532987');
INSERT INTO public.organization VALUES (8, 'Org9702688', 'adc', 3, '2023-09-26 17:20:57.986989', '2023-09-26 17:21:00.661978');
INSERT INTO public.organization VALUES (51, 'Org23311951', 'setyo', 3, '2023-10-11 12:23:28.254363', '2023-10-11 12:23:39.324264');
INSERT INTO public.organization VALUES (9, 'Org112689', 'asdasdasd12', 3, '2023-09-26 17:23:40.028227', '2023-09-26 17:23:48.115972');
INSERT INTO public.organization VALUES (11, 'Org8030811', 'asd 2', 3, '2023-09-30 13:34:07.111968', '2023-09-30 13:36:15.362824');
INSERT INTO public.organization VALUES (12, 'Org5552912', 'Tesse', 3, '2023-10-02 13:05:50.566303', '2023-10-05 18:11:12.979269');
INSERT INTO public.organization VALUES (14, 'Org4015914', 'ddasd asd asd asd asd asd asd asd asd asd asd asdas dasd asd asd asd asd asd asd asd asd asd asd asd asda sd asd asd asd asd asd asd asd', 3, '2023-10-05 18:59:04.426069', '2023-10-05 18:59:51.756758');
INSERT INTO public.organization VALUES (17, 'Org3647917', 'x2', 3, '2023-10-07 11:34:11.385795', '2023-10-07 11:34:18.421496');
INSERT INTO public.organization VALUES (3, 'Org2821481', 'divisi olahraga', 3, '2023-09-14 23:30:21.393215', '2023-10-07 23:48:03.337163');
INSERT INTO public.organization VALUES (4, 'Org6971483', 'divisi olahraga', 3, '2023-09-14 23:30:23.758401', '2023-10-07 23:48:06.744254');
INSERT INTO public.organization VALUES (16, 'Org7535916', 'Sekretariat Kabupaten Sorong', 1, '2023-10-05 20:50:53.773652', '2023-10-07 23:55:47.982487');
INSERT INTO public.organization VALUES (15, 'Org9945915', 'Badan kepegawaian, pendidikan, dan pelatihan Kabupaten Sorong', 1, '2023-10-05 18:59:49.014812', '2023-10-07 23:56:11.590337');
INSERT INTO public.organization VALUES (13, 'Org395913', 'Badan pengelola Keuangan dan Aset Daerah Kabupaten Sorong', 1, '2023-10-05 16:14:50.05788', '2023-10-07 23:56:32.496852');
INSERT INTO public.organization VALUES (10, 'Org13826810', 'Dinas Pemberdayaan Masyarakat Kampung Kabupaten Sorong', 1, '2023-09-26 19:09:59.150681', '2023-10-07 23:56:52.664924');
INSERT INTO public.organization VALUES (6, 'Org7141485', 'Dinas Tenaga kerja dan Transmigrasi Kabupaten Sorong', 1, '2023-09-14 23:30:27.765604', '2023-10-07 23:57:16.490769');
INSERT INTO public.organization VALUES (5, 'Org611484', 'Dinas Perpustakaan Kabupaten Sorong', 1, '2023-09-14 23:30:26.114881', '2023-10-07 23:57:32.788104');
INSERT INTO public.organization VALUES (18, 'Org4897918', 'Dinas Kesehatan Kabupaten Sorong', 1, '2023-10-07 23:57:47.50899', '2023-10-07 23:57:47.50899');
INSERT INTO public.organization VALUES (19, 'Org2077919', 'Dinas Pengendalian Penduduk, Kelurga Berencana, Pemberdayaan perempuan, dan perlindungan Anak Kabupaten Sorong', 1, '2023-10-07 23:58:47.227558', '2023-10-07 23:58:47.227558');
INSERT INTO public.organization VALUES (20, 'Org777920', 'Dinas sosial Kabupaten Sorong', 1, '2023-10-07 23:59:00.101774', '2023-10-07 23:59:00.101774');
INSERT INTO public.organization VALUES (21, 'Org3757921', 'Dinas pertanian, tanaman pangan, holtikultura, dan perkebunan Kabupaten Sorong', 1, '2023-10-07 23:59:28.400748', '2023-10-07 23:59:28.400748');
INSERT INTO public.organization VALUES (22, 'Org8007922', 'Dinas pertenakan dan kesehatan hewan Kabupaten Sorong', 1, '2023-10-07 23:59:40.814733', '2023-10-08 00:00:08.864951');
INSERT INTO public.organization VALUES (23, 'Org5728923', 'Dinas perikanan Kabupaten Sorong', 1, '2023-10-08 00:00:20.600302', '2023-10-08 00:00:20.600302');
INSERT INTO public.organization VALUES (24, 'Org7618924', 'Dinas pariwisata, pemud, dan olahraga Kabupaten Sorong', 1, '2023-10-08 00:01:24.788365', '2023-10-08 00:01:24.788365');
INSERT INTO public.organization VALUES (25, 'Org4188925', 'Dinas pekerjaan umum dan penataan ruang Kabupaten Sorong', 1, '2023-10-08 00:01:44.444051', '2023-10-08 00:01:44.444051');
INSERT INTO public.organization VALUES (26, 'Org1558926', 'Dinas pedagangan, koperasi, usaha kecil, dan menengah Kabupaten Sorong', 1, '2023-10-08 00:02:11.190734', '2023-10-08 00:02:11.190734');
INSERT INTO public.organization VALUES (27, 'Org4268927', 'Dinas penanaman modal pelayanan terpadu satu pintu', 1, '2023-10-08 00:02:34.44723', '2023-10-08 00:02:34.44723');
INSERT INTO public.organization VALUES (28, 'Org6158928', 'Dinas komunikasi, informatika, statistik dan persandian', 1, '2023-10-08 00:02:57.637603', '2023-10-08 00:02:57.637603');
INSERT INTO public.organization VALUES (29, 'Org8208929', 'samsat Kabupaten Sorong', 1, '2023-10-08 00:03:07.840453', '2023-10-08 00:03:07.840453');
INSERT INTO public.organization VALUES (30, 'Org1758930', 'Kantor pertanahan Kabupaten Sorong', 1, '2023-10-08 00:03:24.195856', '2023-10-08 00:03:24.195856');
INSERT INTO public.organization VALUES (31, 'Org5538931', 'Kantor kementrian agama Kabupaten Sorong', 1, '2023-10-08 00:03:35.572679', '2023-10-08 00:03:35.572679');
INSERT INTO public.organization VALUES (32, 'Org2838932', 'Kepolisian resor sorong', 1, '2023-10-08 00:03:49.323928', '2023-10-08 00:03:49.323928');
INSERT INTO public.organization VALUES (33, 'Org4798933', 'Pln ulp aimas', 1, '2023-10-08 00:04:00.502815', '2023-10-08 00:04:00.502815');
INSERT INTO public.organization VALUES (34, 'Org9258934', 'Pt andriyni jaya abadi', 1, '2023-10-08 00:04:13.948382', '2023-10-08 00:04:13.948382');
INSERT INTO public.organization VALUES (35, 'Org5218935', 'Stasiun geometeorologi dan geofisika sorong', 1, '2023-10-08 00:04:40.540449', '2023-10-08 00:04:40.540449');
INSERT INTO public.organization VALUES (36, 'Org39936', 'Sekretariat Daerah Kabupaten Sorong', 1, '2023-10-09 21:44:49.039602', '2023-10-09 21:44:49.039602');
INSERT INTO public.organization VALUES (38, 'Org4519938', 'Dinas pendidikan dan Kebudayaan', 1, '2023-10-09 21:45:11.459482', '2023-10-09 21:45:11.459482');
INSERT INTO public.organization VALUES (39, 'Org6099939', 'Dinas perumahan dan kawasan pemukiman', 1, '2023-10-09 21:45:20.617726', '2023-10-09 21:45:20.617726');
INSERT INTO public.organization VALUES (40, 'Org809940', 'Dinas perumahan dan kawasan pemukiman', 3, '2023-10-09 21:45:34.098749', '2023-10-09 21:45:42.616239');
INSERT INTO public.organization VALUES (37, 'Org4679937', 'Inspektorat Daerah Kabupaten Sorong', 1, '2023-10-09 21:45:01.486127', '2023-10-09 21:46:15.77296');
INSERT INTO public.organization VALUES (41, 'Org5579941', 'Satuan polisi pamong praja', 1, '2023-10-09 21:45:52.564781', '2023-10-09 21:46:45.874842');
INSERT INTO public.organization VALUES (42, 'Org1819942', 'Dinas lingkungan hidup', 1, '2023-10-09 21:46:57.207905', '2023-10-09 21:46:57.207905');
INSERT INTO public.organization VALUES (43, 'Org7549943', 'Dinas kependudukan dan pencacatan sipil', 1, '2023-10-09 21:47:06.768075', '2023-10-09 21:47:06.768075');
INSERT INTO public.organization VALUES (44, 'Org7799944', 'Dinas Pariwisata, pemuda dan olahraga', 1, '2023-10-09 21:47:24.79991', '2023-10-09 21:47:24.79991');
INSERT INTO public.organization VALUES (45, 'Org8019945', '	Dinas perpustakaan dan kearsipan', 1, '2023-10-09 21:47:35.818384', '2023-10-09 21:47:35.818384');
INSERT INTO public.organization VALUES (46, 'Org3939946', 'Badan perencanaan, penelitian dan pengembangan', 1, '2023-10-09 21:47:46.411222', '2023-10-09 21:47:46.411222');
INSERT INTO public.organization VALUES (47, 'Org2999947', 'Badan pengelola keuangan dan aset daerah', 1, '2023-10-09 21:48:01.323859', '2023-10-09 21:48:01.323859');
INSERT INTO public.organization VALUES (48, 'Org3939948', 'Badan pengelola pajak dan restribusi daerah', 1, '2023-10-09 21:48:11.4278', '2023-10-09 21:48:11.4278');
INSERT INTO public.organization VALUES (49, 'Org4329949', 'Badan kepegawaian, pendidikan, dan pelatihan daerah', 1, '2023-10-09 21:48:22.450958', '2023-10-09 21:48:22.450958');
INSERT INTO public.organization VALUES (50, 'Org6589950', 'Badan penanggulangan bencana daerah kabupaten sorong', 1, '2023-10-09 21:48:32.675569', '2023-10-09 21:48:32.675569');


--
-- TOC entry 4176 (class 0 OID 64317)
-- Dependencies: 233
-- Data for Name: procurement_method; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.procurement_method VALUES (1, 'string 1', '2023-09-21 23:26:54.849942', '2023-09-21 23:26:54.849942');
INSERT INTO public.procurement_method VALUES (2, 'string 2', '2023-09-21 23:27:02.837051', '2023-09-21 23:27:02.837051');
INSERT INTO public.procurement_method VALUES (3, 'string 3', '2023-09-21 23:27:07.379706', '2023-09-21 23:27:07.379706');
INSERT INTO public.procurement_method VALUES (4, 'string 4', '2023-09-21 23:27:16.846345', '2023-09-21 23:27:16.846345');
INSERT INTO public.procurement_method VALUES (5, 'string 4', '2023-09-21 23:27:18.024275', '2023-09-21 23:27:18.024275');
INSERT INTO public.procurement_method VALUES (6, 'string 4', '2023-09-21 23:27:19.196992', '2023-09-21 23:27:19.196992');
INSERT INTO public.procurement_method VALUES (7, 'string 4', '2023-09-21 23:27:20.430425', '2023-09-21 23:27:20.430425');
INSERT INTO public.procurement_method VALUES (8, 'string 4', '2023-09-21 23:27:20.6987', '2023-09-21 23:27:20.6987');
INSERT INTO public.procurement_method VALUES (9, 'string 4', '2023-09-21 23:27:20.993975', '2023-09-21 23:27:20.993975');
INSERT INTO public.procurement_method VALUES (10, 'string 4', '2023-09-21 23:27:21.413641', '2023-09-21 23:27:21.413641');
INSERT INTO public.procurement_method VALUES (11, 'string 4', '2023-09-21 23:27:21.813692', '2023-09-21 23:27:21.813692');
INSERT INTO public.procurement_method VALUES (12, 'string 4', '2023-09-21 23:27:22.049881', '2023-09-21 23:27:22.049881');
INSERT INTO public.procurement_method VALUES (13, 'string 4', '2023-09-21 23:27:22.517669', '2023-09-21 23:27:22.517669');
INSERT INTO public.procurement_method VALUES (14, 'string 4', '2023-09-21 23:27:22.766763', '2023-09-21 23:27:22.766763');
INSERT INTO public.procurement_method VALUES (15, 'string 4', '2023-09-21 23:27:23.158712', '2023-09-21 23:27:23.158712');
INSERT INTO public.procurement_method VALUES (16, 'string 4', '2023-09-21 23:27:23.47364', '2023-09-21 23:27:23.47364');


--
-- TOC entry 4174 (class 0 OID 64304)
-- Dependencies: 231
-- Data for Name: procurement_type; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.procurement_type VALUES (1, 'string', '2023-09-21 23:11:49.905367', '2023-09-21 23:11:49.905367');
INSERT INTO public.procurement_type VALUES (2, 'string 2', '2023-09-21 23:11:52.939987', '2023-09-21 23:11:52.939987');
INSERT INTO public.procurement_type VALUES (3, 'string 3', '2023-09-21 23:11:56.819992', '2023-09-21 23:11:56.819992');
INSERT INTO public.procurement_type VALUES (4, 'string 4', '2023-09-21 23:12:01.027922', '2023-09-21 23:12:01.027922');
INSERT INTO public.procurement_type VALUES (5, 'string 4', '2023-09-21 23:12:05.200155', '2023-09-21 23:12:05.200155');
INSERT INTO public.procurement_type VALUES (6, 'string 4', '2023-09-21 23:12:05.787042', '2023-09-21 23:12:05.787042');
INSERT INTO public.procurement_type VALUES (7, 'string 4', '2023-09-21 23:12:06.455003', '2023-09-21 23:12:06.455003');
INSERT INTO public.procurement_type VALUES (8, 'string 4', '2023-09-21 23:12:07.07522', '2023-09-21 23:12:07.07522');
INSERT INTO public.procurement_type VALUES (9, 'string 4', '2023-09-21 23:12:07.664295', '2023-09-21 23:12:07.664295');
INSERT INTO public.procurement_type VALUES (10, 'string 4', '2023-09-21 23:12:08.296215', '2023-09-21 23:12:08.296215');
INSERT INTO public.procurement_type VALUES (11, 'string 4', '2023-09-21 23:12:08.902044', '2023-09-21 23:12:08.902044');
INSERT INTO public.procurement_type VALUES (12, 'string 4', '2023-09-21 23:12:09.570052', '2023-09-21 23:12:09.570052');
INSERT INTO public.procurement_type VALUES (13, 'string 4', '2023-09-21 23:12:10.298878', '2023-09-21 23:12:10.298878');
INSERT INTO public.procurement_type VALUES (14, 'string 4', '2023-09-21 23:12:10.932709', '2023-09-21 23:12:10.932709');


--
-- TOC entry 4160 (class 0 OID 63974)
-- Dependencies: 217
-- Data for Name: program; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.program VALUES (1, 'judul', '129372', 3, '2023-09-15 14:41:08.851448', '2023-09-15 14:48:40.821393', NULL);
INSERT INTO public.program VALUES (2, 'judul', 'P131581', 3, '2023-09-15 14:41:46.889456', '2023-09-30 03:45:33.389608', NULL);
INSERT INTO public.program VALUES (3, 'judul', 'P71582', 3, '2023-09-15 14:41:48.783282', '2023-09-30 03:45:55.317898', NULL);
INSERT INTO public.program VALUES (4, 'judul', 'P4841583', 3, '2023-09-15 14:41:51.25847', '2023-09-30 03:46:14.885214', NULL);
INSERT INTO public.program VALUES (5, 'judul', 'P90315841', 3, '2023-09-15 14:42:54.70426', '2023-09-30 03:58:23.079592', NULL);
INSERT INTO public.program VALUES (13, 'asd', 'P35930813', 3, '2023-09-30 13:39:09.384008', '2023-09-30 13:42:20.612105', 9);
INSERT INTO public.program VALUES (10, 'Test relasi', 'P4020810', 3, '2023-09-20 21:15:07.133014', '2023-10-03 20:29:21.623449', 8);
INSERT INTO public.program VALUES (16, 'Tesse', 'P1773916', 1, '2023-10-03 23:02:40.201249', '2023-10-03 23:02:40.201249', 18);
INSERT INTO public.program VALUES (17, 'Sas', 'P4045917', 3, '2023-10-05 16:13:50.449711', '2023-10-05 18:11:19.814364', 18);
INSERT INTO public.program VALUES (18, 'x2', 'P4737918', 3, '2023-10-07 11:34:24.495234', '2023-10-07 11:34:33.550617', 9);
INSERT INTO public.program VALUES (6, 'judul', 'P90715851', 3, '2023-09-15 14:42:56.680075', '2023-10-07 23:48:52.972908', NULL);
INSERT INTO public.program VALUES (7, 'judul', 'P8821587', 3, '2023-09-15 14:43:32.645953', '2023-10-07 23:48:55.261983', NULL);
INSERT INTO public.program VALUES (8, 'judul', 'P9701588', 3, '2023-09-15 14:43:34.73322', '2023-10-07 23:48:59.506848', NULL);
INSERT INTO public.program VALUES (9, 'judul', 'P9411589', 3, '2023-09-15 14:43:36.712121', '2023-10-07 23:49:01.413087', NULL);
INSERT INTO public.program VALUES (11, 'asd', 'P21530811', 3, '2023-09-30 03:57:56.243624', '2023-10-07 23:49:03.356499', 9);
INSERT INTO public.program VALUES (12, 'program gob', 'P45230812', 3, '2023-09-30 03:58:05.475729', '2023-10-07 23:49:24.162528', 9);
INSERT INTO public.program VALUES (14, 'Aku', 'P3022914', 3, '2023-10-02 13:06:00.325399', '2023-10-07 23:49:25.711111', 18);
INSERT INTO public.program VALUES (15, 'test sekarang', 'P2173915', 3, '2023-10-03 20:29:37.24762', '2023-10-07 23:49:27.557464', 18);


--
-- TOC entry 4158 (class 0 OID 63961)
-- Dependencies: 215
-- Data for Name: purpose; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.purpose VALUES (6, 'Tes', '2023-09-15 13:53:17.394621', '2023-10-07 23:50:31.501132');
INSERT INTO public.purpose VALUES (4, 'TES2', '2023-09-15 13:53:13.831549', '2023-10-07 23:51:57.440362');
INSERT INTO public.purpose VALUES (3, 'TES3', '2023-09-15 13:53:11.077934', '2023-10-07 23:52:09.278709');
INSERT INTO public.purpose VALUES (2, 'TES4', '2023-09-15 13:53:08.503448', '2023-10-07 23:52:18.148231');
INSERT INTO public.purpose VALUES (1, 'TES5', '2023-09-15 13:53:03.97705', '2023-10-07 23:52:25.352478');


--
-- TOC entry 4164 (class 0 OID 64017)
-- Dependencies: 221
-- Data for Name: report; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 4150 (class 0 OID 63876)
-- Dependencies: 207
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.role VALUES (1, 'Superadmin', '2023-09-14 10:46:13.705463', '2023-09-14 10:46:13.705463');
INSERT INTO public.role VALUES (2, 'OPD', '2023-09-14 10:46:13.707644', '2023-09-14 10:46:13.707644');
INSERT INTO public.role VALUES (3, 'Admin Bidang', '2023-09-30 09:55:40.771981', '2023-09-30 09:55:40.771981');
INSERT INTO public.role VALUES (4, 'Atasan Daerah', '2023-10-24 22:33:40.367685', '2023-10-24 22:33:40.367685');


--
-- TOC entry 4180 (class 0 OID 64501)
-- Dependencies: 237
-- Data for Name: setting; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.setting VALUES (2, 'TRIWULAN_STARTED', '2023-12-01', 'DATE', '2023-09-28 16:07:01.982276', '2023-12-04 19:49:33.843289');
INSERT INTO public.setting VALUES (1, 'TRIWULAN_ENDED', '2023-12-22', 'DATE', '2023-09-28 16:07:01.681066', '2023-12-04 19:49:33.845279');


--
-- TOC entry 4148 (class 0 OID 63863)
-- Dependencies: 205
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.status VALUES (1, 'Active', '2023-09-14 10:46:13.499683', '2023-09-14 10:46:13.499683');
INSERT INTO public.status VALUES (2, 'Inactive', '2023-09-14 10:46:13.693969', '2023-09-14 10:46:13.693969');
INSERT INTO public.status VALUES (3, 'Deleted', '2023-09-14 10:46:13.694026', '2023-09-14 10:46:13.694026');


--
-- TOC entry 4156 (class 0 OID 63948)
-- Dependencies: 213
-- Data for Name: triwulan; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.triwulan VALUES (1, 'Triwulan 1', '2023-09-15 15:43:33.022481', '2023-09-15 15:43:33.022481');
INSERT INTO public.triwulan VALUES (2, 'Triwulan 2', '2023-09-15 15:43:33.190685', '2023-09-15 15:43:33.190685');
INSERT INTO public.triwulan VALUES (3, 'Triwulan 3', '2023-09-15 15:43:33.210252', '2023-09-15 15:43:33.210252');
INSERT INTO public.triwulan VALUES (4, 'Triwulan 4', '2023-09-15 15:43:33.261014', '2023-09-15 15:43:33.261014');


--
-- TOC entry 4154 (class 0 OID 63908)
-- Dependencies: 211
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public."user" VALUES (2, 'adminbaru', '$2b$10$zQJ8jIQqIsdXB1WniaI06u35pafhudoGXOjd0lDNewCP1kadtW5D6', 'Superadmin', NULL, 1, 1, '2023-09-14 10:50:15.4067', '2023-09-26 09:52:13.75954', 'admin@example.com', NULL);
INSERT INTO public."user" VALUES (25, 'user', '$2b$10$0WCmtXCW1s4yjn.1WoTocuie8xquUJmRj48qDBMfTB9wcy3utkhLC', 'user', NULL, 2, 3, '2023-09-26 18:41:29.902744', '2023-10-05 20:52:00.728397', NULL, 'adminbaru');
INSERT INTO public."user" VALUES (21, 'opd2', '', 'opd', NULL, 2, 3, '2023-09-26 04:23:17.569254', '2023-09-26 18:39:22.590369', NULL, 'adminbaru');
INSERT INTO public."user" VALUES (28, 'daftar_rifqi', '$2b$10$FllTgxk4ELJ3jQke7Bywl.AuePm9NeplhqgYKPgSxSDrGlKy.cxFm', 'Muhammad Rifqi', NULL, 2, 1, '2023-09-30 11:17:46.509971', '2023-09-30 11:17:46.509971', 'm.rifqi@rnd.ts.co.id', 'adminbaru');
INSERT INTO public."user" VALUES (29, 'user_baru', '$2b$10$Cv6zuhRGj3bq9WLvKhsIg.fHCeMK6KYfB4NU86dMwNGzGom4pvpX.', 'User Baru', NULL, 3, 1, '2023-09-30 11:23:26.101341', '2023-09-30 11:23:26.101341', 'user.baru@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (12, 'rifqi14', '$2b$10$eIA/swhwH779X9c/sXEoc.J9vtRR.xuWdtXe/GZu3Ls6di33rx6MK', 'rifqi', NULL, 1, 1, '2023-09-14 15:20:50.459865', '2023-09-20 21:33:08.652094', 'rifqi.persie@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (13, 'rifqi1423', '$2b$10$04dClcDqV/F3LBWSzHIuX.OFsO8aobNf0dzZs3pfy34QC5rCNnGeu', 'rifqi', NULL, 1, 3, '2023-09-14 15:20:50.459865', '2023-09-25 02:27:14.083371', 'mrifqi.techtime@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (14, 'rifqi142', '$2b$10$04dClcDqV/F3LBWSzHIuX.OFsO8aobNf0dzZs3pfy34QC5rCNnGeu', 'rifqi', NULL, 1, 3, '2023-09-14 15:20:50.459865', '2023-09-25 02:29:51.670042', 'iki.mrifqi@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (44, 'user222', '$2b$10$k3MCw8ZXrRFL3xYWcEpC2OknU5t4ohi1.5DmggSyi/Ny9PJvsZI0K', 'user222', 32, 2, 1, '2023-10-09 08:41:31.099501', '2023-10-09 08:41:31.099501', 'user222@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (45, 'user333', '$2b$10$v7PTuznrVdw.P/Xp.jH2SO71sjcAIwiHNJYF.9y/PPdqcWykakv0G', 'user333', 33, 2, 1, '2023-10-09 08:47:42.866259', '2023-10-09 08:47:42.866259', 'user333@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (46, 'user444', '$2b$10$P5F0DLWEFoCeF5PRSgAmpO3Dc0UC.OYIof8/v8n/gZ3.azXZBSCBi', 'user444', 30, 2, 1, '2023-10-09 08:48:15.947779', '2023-10-09 08:48:15.947779', 'user444@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (31, 'adminbidang', '$2b$10$aq.Zhbrl2aQRdmkpi8gPX.PkByl2eiOOAnphrl2zp9zDgm4Xbxwde', 'admin bidang', NULL, 3, 1, '2023-10-04 11:45:08.749966', '2023-10-04 11:45:08.749966', 'adminbidang@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (33, 'adminbidang2', '$2b$10$c/dUijeV1.Q3HvPLbV5Z0eN7iZnmX2nAHwZ2tIS3J4fVd4FBWhIwW', 'adminbidang2', 12, 3, 1, '2023-10-05 09:10:10.136806', '2023-10-05 09:17:09.022155', 'adminbidang2@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (30, 'test', '$2b$10$Bmp3OnsG9UEAl4526YfP7OEu.maITi0ivSG0Iy8DxM3nvPwHu/6zq', 'asd', NULL, 2, 3, '2023-09-30 13:05:30.612673', '2023-10-05 18:10:01.879713', NULL, 'adminbaru');
INSERT INTO public."user" VALUES (23, 'rizky', '$2b$10$6gwyj4UrBVsoa5M4EaiGueV44eiZ7p7gcO2X15Fz1p4VH59NV9kmW', 'Rizky', NULL, 2, 3, '2023-09-26 16:07:08.830569', '2023-10-05 18:10:12.412512', NULL, 'adminbaru');
INSERT INTO public."user" VALUES (27, 'user22', '$2b$10$tzPIozBtq3TVtQ6PI8DMF.ZOCOhNx3HA/kPvdXX3Lde9asYmEqaqm', 'user', NULL, 2, 3, '2023-09-26 19:11:50.115419', '2023-10-05 18:10:29.646788', NULL, 'adminbaru');
INSERT INTO public."user" VALUES (19, 'admin2', '$2b$10$s5QRv0hezAPcOk8OfPfZsu6PCE7kHiJTY.PjB.l2CJFx6HD.Optea', 'admin2', NULL, 1, 3, '2023-09-26 04:22:39.013766', '2023-10-05 18:12:41.73546', NULL, 'adminbaru');
INSERT INTO public."user" VALUES (36, 'asdasd', '$2b$10$Mv1M0KgoCwQ9u1DinxNWh.a8bEpwmtztGyzorqj.WJTnK94omOkp2', 'asd', 13, 1, 3, '2023-10-05 18:13:51.961454', '2023-10-05 18:14:03.52435', 'asasd@mgial.com', 'adminbaru');
INSERT INTO public."user" VALUES (37, 'asd', '$2b$10$2v/TCeeS5AG.MVuCIXj/uOZ6A7uIEaWiLfvLICYeeamZMnYdTQNvG', 'asd', 6, 1, 3, '2023-10-05 18:16:44.447213', '2023-10-05 18:16:50.502619', 'asd@d.com', 'adminbaru');
INSERT INTO public."user" VALUES (47, 'user555', '$2b$10$mCk7LYLsnTeY2zYziZtXrOd9BzC6GMTjxJA3vLONYqyJQeM1LU24a', 'user555', 29, 2, 1, '2023-10-09 08:48:54.906791', '2023-10-09 08:48:54.906791', 'user555@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (35, 'xxxx', '$2b$10$65HMpybht37xvFp.BwYN/OQBWQlBu8McvX3mpbc.izv9lerOj5Chi', 'namaberubah', 10, 2, 1, '2023-10-05 16:17:47.841605', '2023-10-05 18:21:20.728651', 'user22@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (48, 'Tesatasan', '$2b$10$kZ9D8s/Gamn8ChS9QmPnduPe.nSGccLJajfwoVqH688KiDiEyZ8QG', 'Tesatasan', 42, 4, 1, '2023-10-25 15:12:14.26349', '2023-10-25 15:12:14.26349', 'Tesatasan@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (49, 'atasandaerah', '$2b$10$d.//G43kHVOFP2FHtzKaZOMtqQc3Nu19sA6JUBlN7uqXDcuWG/Y.a', 'atasandaerah', 49, 4, 1, '2023-10-25 20:22:54.589733', '2023-10-25 20:22:54.589733', 'atasandaerah@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (38, 'Bidang22', '$2b$10$n0BTT/kygoRP2UzQPdFqc.C2kRZC4qaITmCqCVqcDFHT9PurtELCG', 'Bidang22', 13, 3, 1, '2023-10-05 20:55:31.142039', '2023-10-05 20:55:31.142039', 'Bidang22@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (39, 'superadmin3', '$2b$10$1pNTg1Mq05F00NSwvzGW2O0JpAZKYMOXlwsSl/hH4DU.VvNsdRTli', 'superadmin3', 16, 2, 3, '2023-10-07 11:32:56.413258', '2023-10-07 11:33:49.624177', 'superadmin3@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (34, 'Perikanan222', '$2b$10$O3B24UwED4ldoKguXFuWb.Fizpb2vNFj0Fni0YO3XGtjpdMezOFCm', 'Dinas perikanan 2', 13, 3, 1, '2023-10-05 16:16:10.612541', '2023-10-07 22:21:05.215827', 'suryadisetyo45@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (40, 'user11', '$2b$10$0Ptfn19MzQaDCwdzEO517uLVvUm1o/oCZ3H14oImkcDsIiqBPoEXe', 'User11', 32, 2, 1, '2023-10-09 08:40:14.975089', '2023-10-09 08:40:14.975089', 'user11@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (41, 'testopd1', '$2b$10$6uRg4WsNxvaI1E0tciZWCus8t5KJftEmjHyRljsYyUNM.LLetpxVm', 'testopd', 35, 2, 1, '2023-10-09 08:40:45.070728', '2023-10-09 08:40:45.070728', 'testopd1@gmail.com', 'adminbaru');
INSERT INTO public."user" VALUES (24, 'superadmin', '$2b$10$vfxg8j7TBpUIuPtaJIKNAu6TeUzUb0VyNCy00UIqdp356U/KEEd4y', 'superadmin', 29, 1, 1, '2023-09-26 18:39:04.608129', '2023-10-07 11:32:08.195518', NULL, 'adminbaru');
INSERT INTO public."user" VALUES (22, 'admin', '$2b$10$6pyCSYAdEHHZ5Y5N9ITJveO31BCFHV5/lIflRkpP5nNmPlTbsaPoe', 'Superadmin', 29, 1, 3, '2023-09-26 16:01:10.055049', '2023-10-05 20:52:08.320516', NULL, 'adminbaru');


--
-- TOC entry 4181 (class 0 OID 64558)
-- Dependencies: 238
-- Data for Name: user_organization; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.user_organization VALUES (29, 5);
INSERT INTO public.user_organization VALUES (29, 6);
INSERT INTO public.user_organization VALUES (29, 10);
INSERT INTO public.user_organization VALUES (33, 6);
INSERT INTO public.user_organization VALUES (33, 10);
INSERT INTO public.user_organization VALUES (33, 12);
INSERT INTO public.user_organization VALUES (35, 10);
INSERT INTO public.user_organization VALUES (35, 12);
INSERT INTO public.user_organization VALUES (38, 10);
INSERT INTO public.user_organization VALUES (38, 13);
INSERT INTO public.user_organization VALUES (24, 29);
INSERT INTO public.user_organization VALUES (22, 29);


--
-- TOC entry 4206 (class 0 OID 0)
-- Dependencies: 222
-- Name: activity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.activity_id_seq', 30, true);


--
-- TOC entry 4207 (class 0 OID 0)
-- Dependencies: 234
-- Name: data_triwulan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.data_triwulan_id_seq', 46, true);


--
-- TOC entry 4208 (class 0 OID 0)
-- Dependencies: 228
-- Name: fund_sources_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.fund_sources_id_seq', 27, true);


--
-- TOC entry 4209 (class 0 OID 0)
-- Dependencies: 226
-- Name: master_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.master_id_seq', 28, true);


--
-- TOC entry 4210 (class 0 OID 0)
-- Dependencies: 224
-- Name: master_occassion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.master_occassion_id_seq', 54, true);


--
-- TOC entry 4211 (class 0 OID 0)
-- Dependencies: 202
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.migrations_id_seq', 32, true);


--
-- TOC entry 4212 (class 0 OID 0)
-- Dependencies: 218
-- Name: occassion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.occassion_id_seq', 22, true);


--
-- TOC entry 4213 (class 0 OID 0)
-- Dependencies: 208
-- Name: organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.organization_id_seq', 51, true);


--
-- TOC entry 4214 (class 0 OID 0)
-- Dependencies: 232
-- Name: procurement_method_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.procurement_method_id_seq', 16, true);


--
-- TOC entry 4215 (class 0 OID 0)
-- Dependencies: 230
-- Name: procurement_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.procurement_type_id_seq', 14, true);


--
-- TOC entry 4216 (class 0 OID 0)
-- Dependencies: 216
-- Name: program_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.program_id_seq', 18, true);


--
-- TOC entry 4217 (class 0 OID 0)
-- Dependencies: 214
-- Name: purpose_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.purpose_id_seq', 12, true);


--
-- TOC entry 4218 (class 0 OID 0)
-- Dependencies: 220
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.report_id_seq', 11, true);


--
-- TOC entry 4219 (class 0 OID 0)
-- Dependencies: 206
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.role_id_seq', 4, true);


--
-- TOC entry 4220 (class 0 OID 0)
-- Dependencies: 236
-- Name: setting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.setting_id_seq', 2, true);


--
-- TOC entry 4221 (class 0 OID 0)
-- Dependencies: 204
-- Name: status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.status_id_seq', 3, true);


--
-- TOC entry 4222 (class 0 OID 0)
-- Dependencies: 212
-- Name: triwulan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.triwulan_id_seq', 4, true);


--
-- TOC entry 4223 (class 0 OID 0)
-- Dependencies: 210
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_id_seq', 49, true);


--
-- TOC entry 3952 (class 2606 OID 63860)
-- Name: migrations PK_8c82d7f526340ab734260ea46be; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT "PK_8c82d7f526340ab734260ea46be" PRIMARY KEY (id);


--
-- TOC entry 3978 (class 2606 OID 64061)
-- Name: activity pk_activity; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT pk_activity PRIMARY KEY (id);


--
-- TOC entry 3990 (class 2606 OID 64355)
-- Name: data_triwulan pk_data_triwulan; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_triwulan
    ADD CONSTRAINT pk_data_triwulan PRIMARY KEY (id);


--
-- TOC entry 3984 (class 2606 OID 64301)
-- Name: fund_sources pk_fund_source; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fund_sources
    ADD CONSTRAINT pk_fund_source PRIMARY KEY (id);


--
-- TOC entry 3982 (class 2606 OID 64095)
-- Name: master pk_master; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master
    ADD CONSTRAINT pk_master PRIMARY KEY (id);


--
-- TOC entry 3980 (class 2606 OID 64077)
-- Name: master_occassion pk_master_occassion; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master_occassion
    ADD CONSTRAINT pk_master_occassion PRIMARY KEY (id);


--
-- TOC entry 3973 (class 2606 OID 64002)
-- Name: occassion pk_occassion; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.occassion
    ADD CONSTRAINT pk_occassion PRIMARY KEY (id);


--
-- TOC entry 3959 (class 2606 OID 63899)
-- Name: organization pk_organization; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.organization
    ADD CONSTRAINT pk_organization PRIMARY KEY (id);


--
-- TOC entry 3988 (class 2606 OID 64327)
-- Name: procurement_method pk_procurement_method; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procurement_method
    ADD CONSTRAINT pk_procurement_method PRIMARY KEY (id);


--
-- TOC entry 3986 (class 2606 OID 64314)
-- Name: procurement_type pk_procurement_type; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.procurement_type
    ADD CONSTRAINT pk_procurement_type PRIMARY KEY (id);


--
-- TOC entry 3970 (class 2606 OID 63984)
-- Name: program pk_program; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.program
    ADD CONSTRAINT pk_program PRIMARY KEY (id);


--
-- TOC entry 3967 (class 2606 OID 63971)
-- Name: purpose pk_purpose; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.purpose
    ADD CONSTRAINT pk_purpose PRIMARY KEY (id);


--
-- TOC entry 3975 (class 2606 OID 64027)
-- Name: report pk_report; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT pk_report PRIMARY KEY (id);


--
-- TOC entry 3956 (class 2606 OID 63886)
-- Name: role pk_role; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT pk_role PRIMARY KEY (id);


--
-- TOC entry 3992 (class 2606 OID 64511)
-- Name: setting pk_setting; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.setting
    ADD CONSTRAINT pk_setting PRIMARY KEY (id);


--
-- TOC entry 3954 (class 2606 OID 63873)
-- Name: status pk_status; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT pk_status PRIMARY KEY (id);


--
-- TOC entry 3965 (class 2606 OID 63958)
-- Name: triwulan pk_triwulan; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.triwulan
    ADD CONSTRAINT pk_triwulan PRIMARY KEY (id);


--
-- TOC entry 3963 (class 2606 OID 63918)
-- Name: user pk_user; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT pk_user PRIMARY KEY (id);


--
-- TOC entry 3994 (class 2606 OID 64562)
-- Name: user_organization pk_user_organization; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_organization
    ADD CONSTRAINT pk_user_organization PRIMARY KEY (user_id, organization_id);


--
-- TOC entry 3957 (class 1259 OID 63900)
-- Name: idx_code_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_code_unique ON public.organization USING btree (code);


--
-- TOC entry 3976 (class 1259 OID 64062)
-- Name: idx_code_unique_activity; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_code_unique_activity ON public.activity USING btree (code);


--
-- TOC entry 3971 (class 1259 OID 64003)
-- Name: idx_code_unique_occassion; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_code_unique_occassion ON public.occassion USING btree (code);


--
-- TOC entry 3968 (class 1259 OID 64009)
-- Name: idx_code_unique_program; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_code_unique_program ON public.program USING btree (code);


--
-- TOC entry 3960 (class 1259 OID 64154)
-- Name: idx_email_unique_user; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_email_unique_user ON public."user" USING btree (email);


--
-- TOC entry 3961 (class 1259 OID 63919)
-- Name: idx_username_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_username_unique ON public."user" USING btree (username);


--
-- TOC entry 4014 (class 2606 OID 65163)
-- Name: data_triwulan fk_activity_id_data_triwulan; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_triwulan
    ADD CONSTRAINT fk_activity_id_data_triwulan FOREIGN KEY (activity_id) REFERENCES public.activity(id);


--
-- TOC entry 3996 (class 2606 OID 101046)
-- Name: user fk_created_by_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_created_by_user FOREIGN KEY (created_by) REFERENCES public."user"(username);


--
-- TOC entry 4015 (class 2606 OID 64356)
-- Name: data_triwulan fk_fund_source_id_data_triwulan; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_triwulan
    ADD CONSTRAINT fk_fund_source_id_data_triwulan FOREIGN KEY (fund_source_id) REFERENCES public.fund_sources(id);


--
-- TOC entry 4008 (class 2606 OID 64111)
-- Name: master_occassion fk_master_id_master_occassion; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master_occassion
    ADD CONSTRAINT fk_master_id_master_occassion FOREIGN KEY (master_id) REFERENCES public.master(id);


--
-- TOC entry 4009 (class 2606 OID 64078)
-- Name: master_occassion fk_occassion_id_master_occassion; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master_occassion
    ADD CONSTRAINT fk_occassion_id_master_occassion FOREIGN KEY (occassion_id) REFERENCES public.occassion(id);


--
-- TOC entry 4000 (class 2606 OID 64181)
-- Name: program fk_occassion_id_program; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.program
    ADD CONSTRAINT fk_occassion_id_program FOREIGN KEY (occassion_id) REFERENCES public.occassion(id);


--
-- TOC entry 4003 (class 2606 OID 64038)
-- Name: report fk_occassion_id_report; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT fk_occassion_id_report FOREIGN KEY (occassion_id) REFERENCES public.occassion(id);


--
-- TOC entry 4010 (class 2606 OID 64106)
-- Name: master fk_organization_id_master; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master
    ADD CONSTRAINT fk_organization_id_master FOREIGN KEY (organization_id) REFERENCES public.organization(id);


--
-- TOC entry 4004 (class 2606 OID 64033)
-- Name: report fk_organization_id_report; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT fk_organization_id_report FOREIGN KEY (organization_id) REFERENCES public.organization(id);


--
-- TOC entry 3997 (class 2606 OID 63920)
-- Name: user fk_organization_id_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_organization_id_user FOREIGN KEY (organization_id) REFERENCES public.organization(id);


--
-- TOC entry 4017 (class 2606 OID 64563)
-- Name: user_organization fk_organization_id_user_organization; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_organization
    ADD CONSTRAINT fk_organization_id_user_organization FOREIGN KEY (organization_id) REFERENCES public.organization(id);


--
-- TOC entry 4007 (class 2606 OID 64063)
-- Name: activity fk_program_id_activity; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.activity
    ADD CONSTRAINT fk_program_id_activity FOREIGN KEY (program_id) REFERENCES public.program(id);


--
-- TOC entry 4005 (class 2606 OID 64043)
-- Name: report fk_program_id_report; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT fk_program_id_report FOREIGN KEY (program_id) REFERENCES public.program(id);


--
-- TOC entry 4011 (class 2606 OID 64101)
-- Name: master fk_purpose_id_master; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master
    ADD CONSTRAINT fk_purpose_id_master FOREIGN KEY (purpose_id) REFERENCES public.purpose(id);


--
-- TOC entry 3998 (class 2606 OID 63925)
-- Name: user fk_role_id_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_role_id_user FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- TOC entry 4002 (class 2606 OID 64004)
-- Name: occassion fk_status_id_occassion; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.occassion
    ADD CONSTRAINT fk_status_id_occassion FOREIGN KEY (status_id) REFERENCES public.status(id);


--
-- TOC entry 3995 (class 2606 OID 63901)
-- Name: organization fk_status_id_organization; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.organization
    ADD CONSTRAINT fk_status_id_organization FOREIGN KEY (status_id) REFERENCES public.status(id);


--
-- TOC entry 4001 (class 2606 OID 64010)
-- Name: program fk_status_id_program; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.program
    ADD CONSTRAINT fk_status_id_program FOREIGN KEY (status_id) REFERENCES public.status(id);


--
-- TOC entry 3999 (class 2606 OID 63930)
-- Name: user fk_status_id_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_status_id_user FOREIGN KEY (status_id) REFERENCES public.status(id);


--
-- TOC entry 4012 (class 2606 OID 64096)
-- Name: master fk_triwulan_id_master; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master
    ADD CONSTRAINT fk_triwulan_id_master FOREIGN KEY (triwulan_id) REFERENCES public.triwulan(id);


--
-- TOC entry 4006 (class 2606 OID 64028)
-- Name: report fk_triwulan_id_report; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT fk_triwulan_id_report FOREIGN KEY (triwulan_id) REFERENCES public.triwulan(id);


--
-- TOC entry 4016 (class 2606 OID 64622)
-- Name: data_triwulan fk_user_id_data_triwulan; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_triwulan
    ADD CONSTRAINT fk_user_id_data_triwulan FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- TOC entry 4013 (class 2606 OID 64617)
-- Name: master fk_user_id_master; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.master
    ADD CONSTRAINT fk_user_id_master FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- TOC entry 4018 (class 2606 OID 64568)
-- Name: user_organization fk_user_id_user_organization; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_organization
    ADD CONSTRAINT fk_user_id_user_organization FOREIGN KEY (user_id) REFERENCES public."user"(id);


-- Completed on 2024-01-17 22:37:17

--
-- PostgreSQL database dump complete
--

