import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { Master } from 'src/database/models/master.entity';
import { Report } from 'src/database/models/report.entity';
import { ListBaseRequest } from 'src/utils/helper';
export declare class CreateReportRequest {
    description?: string;
    program_description?: string;
    triwulan_id?: number;
    organization_id?: number;
    occassion_id?: number;
    program_id?: number;
}
export declare class CreateReportResponse extends BaseApiResponse {
    data: Report;
}
declare const UpdateReportRequest_base: import("@nestjs/common").Type<Partial<CreateReportRequest>>;
export declare class UpdateReportRequest extends UpdateReportRequest_base {
    data_report_id: number;
}
export declare class UpdateReportResponse extends CreateReportResponse {
}
export declare class DeleteReportRequest {
    data_report_id: number;
}
export declare class DeleteReportResponse extends BaseApiResponse {
}
export declare class ListReportRequest extends ListBaseRequest {
    month?: string;
    year?: string;
    triwulan_id?: string;
}
export declare class ListReportResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<Report>>;
}
export declare class DataDetailResponse {
    result: Report;
}
export declare class DetailReportResponse extends BaseApiResponse {
    data: DataDetailResponse;
}
export declare class ListReportUserRequest extends ListBaseRequest {
    month?: string;
    year?: string;
    triwulan_id?: number;
    fund_source_id?: number;
}
declare const ListReportTriwulanRequest_base: import("@nestjs/common").Type<Omit<ListReportUserRequest, "triwulan_id">>;
export declare class ListReportTriwulanRequest extends ListReportTriwulanRequest_base {
}
declare const ListReportMasterRequest_base: import("@nestjs/common").Type<Omit<ListReportUserRequest, "fund_source_id">>;
export declare class ListReportMasterRequest extends ListReportMasterRequest_base {
}
declare const DownloadListReportTriwulanRequest_base: import("@nestjs/common").Type<Omit<ListReportUserRequest, "page" | "limit" | "triwulan_id">>;
export declare class DownloadListReportTriwulanRequest extends DownloadListReportTriwulanRequest_base {
}
declare const DownloadListReportMasterRequest_base: import("@nestjs/common").Type<Omit<ListReportUserRequest, "page" | "limit" | "fund_source_id">>;
export declare class DownloadListReportMasterRequest extends DownloadListReportMasterRequest_base {
}
export declare class ListReportMasterUserResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<Master>>;
}
export declare class ListReportTriwulanUserResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<DataTriwulan>>;
}
export declare class ListReportMasterUserDownload {
    no: number;
    tanggal: string;
    urusan: string;
    sasaran: string;
    organisasi: string;
    indikator_kegiatan: string;
}
export declare class ListReportTriwulanUserDownload {
    no: number;
    nama_kegiatan: string;
    lokasi_kegiatan: string;
    sumber_dana: string;
    pagu_dana: string;
    opd_organisasi: string;
    nama_pptk: string;
    nomor_dan_tanggal_kontrak: string;
    nama_kontraktor: string;
    jangka_waktu: string;
    nilai_kontrak: string;
    realisasi_fisik: string;
    realisasi_keuangan: string;
    volume_kegiatan: string;
    output_kegiatan: string;
    manfaat_kegiatan_kelompok_sasaran_langsung: string;
    manfaat_kegiatan_kelompok_sasaran_tidak_langsung: string;
    jumlah_tenaga_kerja_lokal: string;
    jumlah_tenaga_kerja_non_lokal: string;
    hambatan_dan_permasalahan: string;
    solusi_permasalahan: string;
    jenis_pengadaan: string;
    cara_pengadaan: string;
    tanggal_kontrak: string;
    nama_penanggung_jawab: string;
    nama_pimpinan: string;
    opsi: string;
    alasan: string;
}
export {};
