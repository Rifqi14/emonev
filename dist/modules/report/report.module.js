"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportModule = void 0;
const common_1 = require("@nestjs/common");
const report_service_1 = require("./report.service");
const report_controller_1 = require("./report.controller");
const report_providers_1 = require("./report.providers");
const database_module_1 = require("../../database/database.module");
const master_providers_1 = require("../master/master.providers");
const data_triwulan_providers_1 = require("../data-triwulan/data-triwulan.providers");
const excel_service_1 = require("../../common/excel.service");
let ReportModule = class ReportModule {
};
ReportModule = __decorate([
    (0, common_1.Module)({
        imports: [database_module_1.DatabaseModule],
        controllers: [report_controller_1.ReportController],
        providers: [
            report_service_1.ReportService,
            excel_service_1.ExcelService,
            ...report_providers_1.reportProviders,
            ...master_providers_1.masterProviders,
            ...data_triwulan_providers_1.dataTriwulanProviders,
        ],
        exports: [report_service_1.ReportService],
    })
], ReportModule);
exports.ReportModule = ReportModule;
//# sourceMappingURL=report.module.js.map