import { Request } from 'express';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
import { ExcelService } from 'src/common/excel.service';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { Master } from 'src/database/models/master.entity';
import { Report } from 'src/database/models/report.entity';
import { Repository } from 'typeorm';
import { PayloadClientResponse } from '../authentication/authentication.dto';
import { CreateReportRequest, DownloadListReportMasterRequest, DownloadListReportTriwulanRequest, ListReportMasterUserDownload, ListReportRequest, ListReportTriwulanUserDownload, ListReportUserRequest, UpdateReportRequest } from './report.dto';
export declare class ReportService {
    private repository;
    private masterRepository;
    private dataTriwulanRepository;
    private request;
    private excelService;
    constructor(repository: Repository<Report>, masterRepository: Repository<Master>, dataTriwulanRepository: Repository<DataTriwulan>, request: Request & {
        user: PayloadClientResponse;
        is_admin_bidang: boolean;
    }, excelService: ExcelService);
    create(data: CreateReportRequest): Promise<Report>;
    detail(id: number): Promise<Report>;
    update(data: UpdateReportRequest): Promise<Report>;
    delete(data_report_id: number): Promise<void>;
    list(query: ListReportRequest): Promise<DataWithPaginationApiResponse<Array<Report>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): string;
    listMaster(query: Omit<ListReportUserRequest, 'fund_source_id'>): Promise<DataWithPaginationApiResponse<Array<Master>>>;
    listDataTriwulan(query: Omit<ListReportUserRequest, 'triwulan_id'>): Promise<DataWithPaginationApiResponse<Array<DataTriwulan>>>;
    downloadListMaster(query: DownloadListReportMasterRequest): Promise<[string, ListReportMasterUserDownload[]]>;
    downloadSingleDataTriwulan(id: number): Promise<[string, ListReportTriwulanUserDownload]>;
    downloadListDataTriwulan(query: DownloadListReportTriwulanRequest): Promise<[string, ListReportTriwulanUserDownload[]]>;
    downloadPdfListMaster(query: DownloadListReportMasterRequest): Promise<string>;
    downloadPdfListTriwulan(query: DownloadListReportTriwulanRequest): Promise<string>;
}
