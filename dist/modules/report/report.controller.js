"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const openapi_1 = require("../../common/openapi");
const data_triwulan_entity_1 = require("../../database/models/data-triwulan.entity");
const master_entity_1 = require("../../database/models/master.entity");
const report_dto_1 = require("./report.dto");
const report_service_1 = require("./report.service");
const fs_1 = require("fs");
const authentication_guard_1 = require("../authentication/authentication.guard");
const dayjs_1 = __importDefault(require("dayjs"));
let ReportController = class ReportController {
    constructor(reportService) {
        this.reportService = reportService;
    }
    async detail(id) {
        return {
            statusCode: 200,
            message: `Pengambilan data berhasil`,
            data: {
                result: await this.reportService.detail(id),
            },
        };
    }
    async list(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.reportService.list(query),
        };
    }
    async create(data) {
        return {
            statusCode: 200,
            message: `Pembuatan data berhasil`,
            data: await this.reportService.create(data),
        };
    }
    async update(data) {
        return {
            statusCode: 200,
            message: 'Pengkinian data berhasil',
            data: await this.reportService.update(data),
        };
    }
    async delete(data) {
        await this.reportService.delete(data.data_report_id);
        return {
            statusCode: 200,
            message: 'Penghapusan data berhasil',
        };
    }
    async listMaster(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.reportService.listMaster(query),
        };
    }
    async listDataTriwulan(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.reportService.listDataTriwulan(query),
        };
    }
    async downloadExcelListMaster(query, res) {
        const [path, data] = await this.reportService.downloadListMaster(query);
        if (!data || data.length === 0) {
            res.status(404).json({
                statusCode: 404,
                message: 'Data tidak ditemukan',
            });
            return;
        }
        res.download(path, (err) => {
            if (!err) {
                (0, fs_1.unlink)(path, (err) => {
                    if (err)
                        throw err;
                });
            }
        });
    }
    async downloadExcelListDataTriwulan(query, res) {
        const [path, data] = await this.reportService.downloadListDataTriwulan(query);
        if (!data || data.length === 0) {
            res.status(404).json({
                statusCode: 404,
                message: 'Data tidak ditemukan',
            });
            return;
        }
        res.download(path, (err) => {
            if (!err) {
                (0, fs_1.unlink)(path, (err) => {
                    if (err)
                        throw err;
                });
            }
        });
    }
    async downloadExcelSingleDataTriwulan(id, res) {
        const [path, data] = await this.reportService.downloadSingleDataTriwulan(id);
        if (!data) {
            res.status(404).json({
                statusCode: 404,
                message: 'Data tidak ditemukan',
            });
            return;
        }
        res.download(path, (err) => {
            if (!err) {
                (0, fs_1.unlink)(path, (err) => {
                    if (err)
                        throw err;
                });
            }
        });
    }
    async downloadPdfListMaster(query, res) {
        const path = await this.reportService.downloadPdfListMaster(query);
        res.download(path, (err) => {
            if (!err) {
                (0, fs_1.unlink)(path, (err) => {
                    if (err)
                        throw err;
                });
            }
        });
    }
    async downloadPdfListTriwulan(query, res) {
        const path = await this.reportService.downloadPdfListTriwulan(query);
        res.download(path, (err) => {
            if (!err) {
                (0, fs_1.unlink)(path, (err) => {
                    if (err)
                        throw err;
                });
            }
        });
    }
    async templateDataMaster(res, query) {
        const [, data] = await this.reportService.downloadListMaster(query);
        return res.render('data-triwulan', {
            title: 'Laporan Master',
            headers: Object.keys(data[0]).map((key) => key
                .split('_')
                .map((column) => column.charAt(0).toUpperCase() + column.slice(1))
                .join(' ')),
            data: data,
            date: (0, dayjs_1.default)().format('DD MMMM YYYY'),
        });
    }
    async templateDataTriwulan(res, query) {
        const [, data] = await this.reportService.downloadListDataTriwulan(query);
        return res.render('data-triwulan', {
            title: 'Laporan Triwulan',
            headers: Object.keys(data[0])
                .filter((v) => v != 'id')
                .map((key) => key
                .split('_')
                .map((column) => column.charAt(0).toUpperCase() + column.slice(1))
                .join(' ')),
            data: data,
            date: (0, dayjs_1.default)().format('DD MMMM YYYY'),
        });
    }
};
__decorate([
    (0, openapi_1.SwaggerOkResponse)(report_dto_1.DetailReportResponse),
    (0, common_1.Get)('detail/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "detail", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(report_dto_1.ListReportResponse),
    (0, common_1.Get)('list'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.ListReportRequest]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "list", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(report_dto_1.CreateReportResponse),
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.CreateReportRequest]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "create", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(report_dto_1.UpdateReportResponse),
    (0, common_1.Patch)('update'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.UpdateReportRequest]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "update", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(report_dto_1.DeleteReportResponse),
    (0, common_1.Patch)('delete'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.DeleteReportRequest]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "delete", null);
__decorate([
    (0, swagger_1.ApiTags)('User | Data Report'),
    (0, openapi_1.SwaggerPaginationResponse)(master_entity_1.Master),
    (0, common_1.Get)('user/data-master'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.ListReportMasterRequest]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "listMaster", null);
__decorate([
    (0, swagger_1.ApiTags)('User | Data Report'),
    (0, openapi_1.SwaggerPaginationResponse)(data_triwulan_entity_1.DataTriwulan),
    (0, common_1.Get)('user/data-triwulan'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.ListReportTriwulanRequest]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "listDataTriwulan", null);
__decorate([
    (0, swagger_1.ApiTags)('User | Data Report'),
    (0, openapi_1.SwaggerPaginationResponse)(master_entity_1.Master),
    (0, common_1.Get)('user/data-master/excel'),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.DownloadListReportMasterRequest, Object]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "downloadExcelListMaster", null);
__decorate([
    (0, swagger_1.ApiTags)('User | Data Report'),
    (0, openapi_1.SwaggerPaginationResponse)(data_triwulan_entity_1.DataTriwulan),
    (0, common_1.Get)('user/data-triwulan/excel'),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.DownloadListReportTriwulanRequest, Object]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "downloadExcelListDataTriwulan", null);
__decorate([
    (0, swagger_1.ApiTags)('User | Data Report'),
    (0, openapi_1.SwaggerPaginationResponse)(data_triwulan_entity_1.DataTriwulan),
    (0, common_1.Get)('user/data-triwulan/excel/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "downloadExcelSingleDataTriwulan", null);
__decorate([
    (0, swagger_1.ApiTags)('User | Data Report'),
    (0, openapi_1.SwaggerPaginationResponse)(master_entity_1.Master),
    (0, common_1.Header)('Content-Type', 'application/pdf'),
    (0, common_1.Get)('user/data-master/pdf'),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.DownloadListReportMasterRequest, Object]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "downloadPdfListMaster", null);
__decorate([
    (0, swagger_1.ApiTags)('User | Data Report'),
    (0, openapi_1.SwaggerPaginationResponse)(master_entity_1.Master),
    (0, common_1.Header)('Content-Type', 'application/pdf'),
    (0, common_1.Get)('user/data-triwulan/pdf'),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [report_dto_1.DownloadListReportTriwulanRequest, Object]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "downloadPdfListTriwulan", null);
__decorate([
    (0, authentication_guard_1.PublicAPI)(),
    (0, common_1.Get)('template/pdf/data-master'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, report_dto_1.DownloadListReportMasterRequest]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "templateDataMaster", null);
__decorate([
    (0, authentication_guard_1.PublicAPI)(),
    (0, common_1.Get)('template/pdf/data-triwulan'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, report_dto_1.DownloadListReportTriwulanRequest]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "templateDataTriwulan", null);
ReportController = __decorate([
    (0, swagger_1.ApiTags)('Data Report'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('data-report'),
    __metadata("design:paramtypes", [report_service_1.ReportService])
], ReportController);
exports.ReportController = ReportController;
//# sourceMappingURL=report.controller.js.map