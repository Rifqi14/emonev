"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const excel_service_1 = require("../../common/excel.service");
const data_triwulan_entity_1 = require("../../database/models/data-triwulan.entity");
const report_entity_1 = require("../../database/models/report.entity");
const user_entity_1 = require("../../database/models/user.entity");
const typeorm_1 = require("typeorm");
const dayjs_1 = __importDefault(require("dayjs"));
let ReportService = class ReportService {
    constructor(repository, masterRepository, dataTriwulanRepository, request, excelService) {
        this.repository = repository;
        this.masterRepository = masterRepository;
        this.dataTriwulanRepository = dataTriwulanRepository;
        this.request = request;
        this.excelService = excelService;
    }
    async create(data) {
        try {
            const { program_id, description, occassion_id, organization_id, program_description, triwulan_id, } = data;
            return await this.repository.manager.transaction(async (trx) => await trx
                .create(report_entity_1.Report, {
                description,
                program_description,
                triwulan_id,
                organization_id,
                occassion_id,
                program_id,
            })
                .save());
        }
        catch (error) {
            throw error;
        }
    }
    async detail(id) {
        var _a;
        try {
            const user = await this.repository.manager.findOneOrFail(user_entity_1.User, {
                where: { id: this.request.user.user_id },
            });
            if (user.role && user.role.id != 1 && !user.organization)
                throw new typeorm_1.EntityNotFoundError(user_entity_1.User, 'Silahkan lengkapi data anda');
            return await this.repository.findOneOrFail({
                relations: {
                    program: true,
                    triwulan: true,
                    occassion: true,
                    organization: true,
                },
                where: {
                    id,
                    organization_id: user.role &&
                        user.role.id != 1 &&
                        user.organization &&
                        user.organization.id
                        ? (_a = this.request.user.organization) === null || _a === void 0 ? void 0 : _a.id
                        : undefined,
                },
            });
        }
        catch (error) {
            throw error;
        }
    }
    async update(data) {
        try {
            const { program_id, description, occassion_id, organization_id, program_description, triwulan_id, data_report_id, } = data;
            await this.repository.manager.transaction(async (trx) => trx.update(report_entity_1.Report, { id: data_report_id }, {
                description,
                program_description,
                triwulan_id,
                organization_id,
                occassion_id,
                program_id,
            }));
            return await this.repository.findOneByOrFail({
                id: data_report_id,
            });
        }
        catch (error) {
            throw error;
        }
    }
    async delete(data_report_id) {
        try {
            await this.repository.delete({ id: data_report_id });
        }
        catch (error) {
            throw error;
        }
    }
    async list(query) {
        try {
            const { search, sort, month, year, triwulan_id } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const user = await this.repository.manager.findOneOrFail(user_entity_1.User, {
                where: { id: this.request.user.user_id },
            });
            if (user.role && user.role.id != 1 && !user.organization)
                throw new typeorm_1.EntityNotFoundError(user_entity_1.User, 'Silahkan lengkapi data anda');
            const queryBuilder = this.repository
                .createQueryBuilder('report')
                .leftJoinAndSelect('report.triwulan', 'triwulan')
                .leftJoinAndSelect('report.program', 'program')
                .leftJoinAndSelect('report.occassion', 'occassion')
                .leftJoinAndSelect('report.organization', 'organization')
                .where(new typeorm_1.Brackets((qb) => {
                qb.where(`report.description ilike :search`, {
                    search: `%${search !== null && search !== void 0 ? search : ''}%`,
                }).orWhere(`report.program_description ilike :search`, {
                    search: `%${search !== null && search !== void 0 ? search : ''}%`,
                });
            }));
            if (user.role && user.role.id != 1 && user.organization) {
                queryBuilder.andWhere(`report.organization_id = :organization_id`, {
                    organization_id: user.organization.id,
                });
            }
            if (month) {
                queryBuilder.andWhere(`extract(month from report.created_at) = :month`, { month });
            }
            if (year) {
                queryBuilder.andWhere(`extract(year from report.created_at) = :year`, {
                    year,
                });
            }
            if (triwulan_id) {
                queryBuilder.andWhere(`report.triwulan_id = :triwulan_id`, {
                    triwulan_id,
                });
            }
            const [column, order] = this.getOrderQuery(sort).split(' ');
            const [reports, count] = await queryBuilder
                .limit(limit)
                .offset((page - 1) * limit)
                .orderBy(column, order)
                .getManyAndCount();
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: reports,
            };
        }
        catch (error) {
            throw error;
        }
    }
    getOrderQuery(sort) {
        let sortRes = `report.id DESC`;
        switch (sort) {
            case 'terbaru':
                sortRes = `report.id DESC`;
                break;
            case 'terlama':
                sortRes = `report.id ASC`;
                break;
            case 'a-z':
                sortRes = `report.description ASC`;
                break;
            case 'z-a':
                sortRes = `report.description DESC`;
                break;
        }
        return sortRes;
    }
    async listMaster(query) {
        try {
            const { sort, month, year, triwulan_id } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const user = await this.repository.manager.findOneOrFail(user_entity_1.User, {
                where: { id: this.request.user.user_id },
                relations: {
                    role: true,
                },
            });
            const where = {};
            const order = {
                id: 'DESC',
            };
            if (month) {
                Object.assign(where, {
                    created_at: (0, typeorm_1.Raw)((alias) => `extract(month from ${alias}) = :month`, { month }),
                });
            }
            if (year) {
                Object.assign(where, {
                    created_at: (0, typeorm_1.Raw)((alias) => `extract(year from ${alias}) = :year`, {
                        year,
                    }),
                });
            }
            if (triwulan_id) {
                Object.assign(where, { triwulan_id });
            }
            if (user && user.role && user.role.name.toLowerCase() === 'opd') {
                Object.assign(where, { user_id: user.id });
            }
            if (sort) {
                switch (sort) {
                    case 'terbaru':
                        Object.assign(order, {
                            id: 'DESC',
                        });
                        break;
                    case 'terlama':
                        Object.assign(order, {
                            id: 'ASC',
                        });
                        break;
                    case 'a-z':
                        Object.assign(order, {
                            description: 'ASC',
                        });
                        break;
                    case 'z-a':
                        Object.assign(order, {
                            description: 'DESC',
                        });
                        break;
                }
            }
            const [masters, count] = await this.masterRepository.findAndCount({
                where: Object.keys(where).length > 0 ? where : undefined,
                order,
                relations: {
                    triwulan: true,
                    purpose: true,
                    organization: true,
                    masterOccassions: {
                        occassion: true,
                    },
                },
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: masters,
            };
        }
        catch (error) {
            throw error;
        }
    }
    async listDataTriwulan(query) {
        try {
            const { sort, month, year, fund_source_id } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const user = await this.repository.manager.findOneOrFail(user_entity_1.User, {
                where: { id: this.request.user.user_id },
                relations: {
                    role: true,
                },
            });
            const where = {};
            const order = {
                id: 'DESC',
            };
            if (month) {
                Object.assign(where, {
                    created_at: (0, typeorm_1.Raw)((alias) => `extract(month from ${alias}) = :month`, {
                        month,
                    }),
                });
            }
            if (year) {
                Object.assign(where, {
                    created_at: (0, typeorm_1.Raw)((alias) => `extract(year from ${alias}) = :year`, {
                        year,
                    }),
                });
            }
            if (fund_source_id) {
                Object.assign(where, {
                    fund_source_id: fund_source_id,
                });
            }
            if (user && user.role && user.role.name.toLowerCase() === 'opd') {
                Object.assign(where, { user_id: user.id });
            }
            if (sort) {
                switch (sort) {
                    case 'terbaru':
                        Object.assign(order, {
                            id: 'DESC',
                        });
                        break;
                    case 'terlama':
                        Object.assign(order, {
                            id: 'ASC',
                        });
                        break;
                    case 'a-z':
                        Object.assign(order, {
                            activity_name: 'ASC',
                        });
                        break;
                    case 'z-a':
                        Object.assign(order, {
                            activity_name: 'DESC',
                        });
                        break;
                }
            }
            const [dataTriwulan, count] = await this.dataTriwulanRepository.findAndCount({
                where: Object.keys(where).length > 0 ? where : undefined,
                order,
                relations: {
                    fundSource: true,
                    activity: {
                        program: true,
                    },
                    createdBy: {
                        userOrganization: true,
                    },
                },
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: dataTriwulan,
            };
        }
        catch (error) {
            throw error;
        }
    }
    async downloadListMaster(query) {
        try {
            const { sort, month, year, triwulan_id } = query;
            const where = {};
            const order = {
                id: 'DESC',
            };
            if (month) {
                Object.assign(where, {
                    created_at: (0, typeorm_1.Raw)((alias) => `extract(month from ${alias}) = :month`, { month }),
                });
            }
            if (year) {
                Object.assign(where, {
                    created_at: (0, typeorm_1.Raw)((alias) => `extract(year from ${alias}) = :year`, {
                        year,
                    }),
                });
            }
            if (triwulan_id) {
                Object.assign(where, { triwulan_id });
            }
            if (sort) {
                switch (sort) {
                    case 'terbaru':
                        Object.assign(order, {
                            id: 'DESC',
                        });
                        break;
                    case 'terlama':
                        Object.assign(order, {
                            id: 'ASC',
                        });
                        break;
                    case 'a-z':
                        Object.assign(order, {
                            description: 'ASC',
                        });
                        break;
                    case 'z-a':
                        Object.assign(order, {
                            description: 'DESC',
                        });
                        break;
                }
            }
            const masters = await this.masterRepository.find({
                where: Object.keys(where).length > 0 ? where : undefined,
                order,
                relations: {
                    triwulan: true,
                    purpose: true,
                    organization: true,
                    masterOccassions: {
                        occassion: true,
                    },
                },
            });
            const data = masters.map((data, idx) => {
                var _a, _b, _c, _d, _e, _f;
                return ({
                    no: idx + 1,
                    indikator_kegiatan: (_a = data.description) !== null && _a !== void 0 ? _a : '',
                    organisasi: (_c = (_b = data.organization) === null || _b === void 0 ? void 0 : _b.title) !== null && _c !== void 0 ? _c : '',
                    tanggal: (_d = (0, dayjs_1.default)(data.created_at).format('DD/MM/YYYY')) !== null && _d !== void 0 ? _d : '',
                    sasaran: (_f = (_e = data.purpose) === null || _e === void 0 ? void 0 : _e.title) !== null && _f !== void 0 ? _f : '',
                    urusan: data.masterOccassions
                        .map(({ occassion }) => { var _a; return (_a = occassion === null || occassion === void 0 ? void 0 : occassion.title) !== null && _a !== void 0 ? _a : ''; })
                        .filter((v) => v != '')
                        .join(', '),
                });
            });
            const path = await this.excelService.download({
                data: data,
                name: 'data-master',
            });
            return [path, data];
        }
        catch (error) {
            throw error;
        }
    }
    async downloadSingleDataTriwulan(id) {
        try {
            const dataTriwulan = await this.dataTriwulanRepository.find({
                where: { id: id },
                relations: {
                    fundSource: true,
                },
            });
            const data = dataTriwulan.map((data, idx) => {
                var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _0, _1, _2, _3, _4, _5, _6;
                return ({
                    no: idx + 1,
                    nama_kegiatan: data.activity_name,
                    lokasi_kegiatan: data.activity_location,
                    sumber_dana: (_b = (_a = data.fundSource) === null || _a === void 0 ? void 0 : _a.name) !== null && _b !== void 0 ? _b : '',
                    pagu_dana: (_d = (_c = data.fund_ceiling) === null || _c === void 0 ? void 0 : _c.toString()) !== null && _d !== void 0 ? _d : '',
                    opd_organisasi: (_e = data.management_organization) !== null && _e !== void 0 ? _e : '',
                    nama_pptk: (_f = data.pptk_name) !== null && _f !== void 0 ? _f : '',
                    nomor_dan_tanggal_kontrak: (_g = data.contract_number_date) !== null && _g !== void 0 ? _g : '',
                    nama_kontraktor: (_h = data.contractor_name) !== null && _h !== void 0 ? _h : '',
                    jangka_waktu: (_j = data.implementation_period) !== null && _j !== void 0 ? _j : '',
                    nilai_kontrak: (_l = (_k = data.contract_value) === null || _k === void 0 ? void 0 : _k.toString()) !== null && _l !== void 0 ? _l : '',
                    realisasi_fisik: (_o = ((_m = data.physical_realization) === null || _m === void 0 ? void 0 : _m.toString()) + '%') !== null && _o !== void 0 ? _o : '0%',
                    realisasi_keuangan: (_q = ((_p = data.fund_realization) === null || _p === void 0 ? void 0 : _p.toString()) + '%') !== null && _q !== void 0 ? _q : '0%',
                    volume_kegiatan: (_r = data.activity_volume) !== null && _r !== void 0 ? _r : '',
                    output_kegiatan: (_s = data.activity_output) !== null && _s !== void 0 ? _s : '',
                    manfaat_kegiatan_kelompok_sasaran_langsung: (_t = data.direct_target_group) !== null && _t !== void 0 ? _t : '',
                    manfaat_kegiatan_kelompok_sasaran_tidak_langsung: (_u = data.indirect_target_group) !== null && _u !== void 0 ? _u : '',
                    jumlah_tenaga_kerja_lokal: (_w = (_v = data.local_workforce) === null || _v === void 0 ? void 0 : _v.toString()) !== null && _w !== void 0 ? _w : '',
                    jumlah_tenaga_kerja_non_lokal: (_y = (_x = data.non_local_workforce) === null || _x === void 0 ? void 0 : _x.toString()) !== null && _y !== void 0 ? _y : '',
                    hambatan_dan_permasalahan: (_z = data.problems) !== null && _z !== void 0 ? _z : '',
                    solusi_permasalahan: (_0 = data.solution) !== null && _0 !== void 0 ? _0 : '',
                    jenis_pengadaan: (_1 = data.procurement_type) !== null && _1 !== void 0 ? _1 : '',
                    cara_pengadaan: (_2 = data.procurement_method) !== null && _2 !== void 0 ? _2 : '',
                    tanggal_kontrak: data.contract_date
                        ? (0, dayjs_1.default)(data.contract_date).format('DD/MM/YYYY')
                        : '',
                    nama_penanggung_jawab: (_3 = data.pic_name) !== null && _3 !== void 0 ? _3 : '',
                    nama_pimpinan: (_4 = data.leader_name) !== null && _4 !== void 0 ? _4 : '',
                    opsi: (_5 = data.optional) !== null && _5 !== void 0 ? _5 : '',
                    alasan: (_6 = data.reason) !== null && _6 !== void 0 ? _6 : '',
                });
            });
            const path = await this.excelService.download({
                data: data,
                name: `data-triwulan-${data[0].nama_kegiatan}`,
            });
            return [path, data[0]];
        }
        catch (error) {
            throw error;
        }
    }
    async downloadListDataTriwulan(query) {
        try {
            const { sort, month, year, fund_source_id } = query;
            const where = {};
            const order = {
                id: 'DESC',
            };
            if (month) {
                Object.assign(where, {
                    created_at: (0, typeorm_1.Raw)((alias) => `extract(month from ${alias}) = :month`, {
                        month,
                    }),
                });
            }
            if (year) {
                Object.assign(where, {
                    created_at: (0, typeorm_1.Raw)((alias) => `extract(year from ${alias}) = :year`, {
                        year,
                    }),
                });
            }
            if (fund_source_id) {
                Object.assign(where, {
                    fund_source_id: fund_source_id,
                });
            }
            if (sort) {
                switch (sort) {
                    case 'terbaru':
                        Object.assign(order, {
                            id: 'DESC',
                        });
                        break;
                    case 'terlama':
                        Object.assign(order, {
                            id: 'ASC',
                        });
                        break;
                    case 'a-z':
                        Object.assign(order, {
                            activity_name: 'ASC',
                        });
                        break;
                    case 'z-a':
                        Object.assign(order, {
                            activity_name: 'DESC',
                        });
                        break;
                }
            }
            const dataTriwulan = await this.dataTriwulanRepository.find({
                where: Object.keys(where).length > 0 ? where : undefined,
                order,
                relations: {
                    fundSource: true,
                },
            });
            if (dataTriwulan.length === 0)
                throw new typeorm_1.EntityNotFoundError(data_triwulan_entity_1.DataTriwulan, 'Data triwulan masih kosong');
            const data = dataTriwulan.map((data, idx) => {
                var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _0, _1, _2, _3, _4, _5, _6;
                return ({
                    no: idx + 1,
                    nama_kegiatan: data.activity_name,
                    lokasi_kegiatan: data.activity_location,
                    sumber_dana: (_b = (_a = data.fundSource) === null || _a === void 0 ? void 0 : _a.name) !== null && _b !== void 0 ? _b : '',
                    pagu_dana: (_d = (_c = data.fund_ceiling) === null || _c === void 0 ? void 0 : _c.toString()) !== null && _d !== void 0 ? _d : '',
                    opd_organisasi: (_e = data.management_organization) !== null && _e !== void 0 ? _e : '',
                    nama_pptk: (_f = data.pptk_name) !== null && _f !== void 0 ? _f : '',
                    nomor_dan_tanggal_kontrak: (_g = data.contract_number_date) !== null && _g !== void 0 ? _g : '',
                    nama_kontraktor: (_h = data.contractor_name) !== null && _h !== void 0 ? _h : '',
                    jangka_waktu: (_j = data.implementation_period) !== null && _j !== void 0 ? _j : '',
                    nilai_kontrak: (_l = (_k = data.contract_value) === null || _k === void 0 ? void 0 : _k.toString()) !== null && _l !== void 0 ? _l : '',
                    realisasi_fisik: (_o = ((_m = data.physical_realization) === null || _m === void 0 ? void 0 : _m.toString()) + '%') !== null && _o !== void 0 ? _o : '0%',
                    realisasi_keuangan: (_q = ((_p = data.fund_realization) === null || _p === void 0 ? void 0 : _p.toString()) + '%') !== null && _q !== void 0 ? _q : '0%',
                    volume_kegiatan: (_r = data.activity_volume) !== null && _r !== void 0 ? _r : '',
                    output_kegiatan: (_s = data.activity_output) !== null && _s !== void 0 ? _s : '',
                    manfaat_kegiatan_kelompok_sasaran_langsung: (_t = data.direct_target_group) !== null && _t !== void 0 ? _t : '',
                    manfaat_kegiatan_kelompok_sasaran_tidak_langsung: (_u = data.indirect_target_group) !== null && _u !== void 0 ? _u : '',
                    jumlah_tenaga_kerja_lokal: (_w = (_v = data.local_workforce) === null || _v === void 0 ? void 0 : _v.toString()) !== null && _w !== void 0 ? _w : '',
                    jumlah_tenaga_kerja_non_lokal: (_y = (_x = data.non_local_workforce) === null || _x === void 0 ? void 0 : _x.toString()) !== null && _y !== void 0 ? _y : '',
                    hambatan_dan_permasalahan: (_z = data.problems) !== null && _z !== void 0 ? _z : '',
                    solusi_permasalahan: (_0 = data.solution) !== null && _0 !== void 0 ? _0 : '',
                    jenis_pengadaan: (_1 = data.procurement_type) !== null && _1 !== void 0 ? _1 : '',
                    cara_pengadaan: (_2 = data.procurement_method) !== null && _2 !== void 0 ? _2 : '',
                    tanggal_kontrak: data.contract_date
                        ? (0, dayjs_1.default)(data.contract_date).format('DD/MM/YYYY')
                        : '',
                    nama_penanggung_jawab: (_3 = data.pic_name) !== null && _3 !== void 0 ? _3 : '',
                    nama_pimpinan: (_4 = data.leader_name) !== null && _4 !== void 0 ? _4 : '',
                    opsi: (_5 = data.optional) !== null && _5 !== void 0 ? _5 : '',
                    alasan: (_6 = data.reason) !== null && _6 !== void 0 ? _6 : '',
                });
            });
            const path = await this.excelService.download({
                data: data,
                name: 'data-triwulan',
            });
            return [path, data];
        }
        catch (error) {
            throw error;
        }
    }
    async downloadPdfListMaster(query) {
        try {
            const url = `${this.request.protocol}://${this.request.get('Host')}/data-report/template/pdf/data-master`;
            const path = await this.excelService.pdfDownload({
                url: url,
                name: 'data-master',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async downloadPdfListTriwulan(query) {
        try {
            const url = `${this.request.protocol}://${this.request.get('Host')}/data-report/template/pdf/data-triwulan`;
            const path = await this.excelService.pdfDownload({
                url: url,
                name: 'data-triwulan',
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
};
ReportService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)('REPORT_REPOSITORY')),
    __param(1, (0, common_1.Inject)('MASTER_REPOSITORY')),
    __param(2, (0, common_1.Inject)('DATA_TRIWULAN_REPOSITORY')),
    __param(3, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        typeorm_1.Repository,
        typeorm_1.Repository, Object, excel_service_1.ExcelService])
], ReportService);
exports.ReportService = ReportService;
//# sourceMappingURL=report.service.js.map