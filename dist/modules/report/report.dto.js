"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListReportTriwulanUserDownload = exports.ListReportMasterUserDownload = exports.ListReportTriwulanUserResponse = exports.ListReportMasterUserResponse = exports.DownloadListReportMasterRequest = exports.DownloadListReportTriwulanRequest = exports.ListReportMasterRequest = exports.ListReportTriwulanRequest = exports.ListReportUserRequest = exports.DetailReportResponse = exports.DataDetailResponse = exports.ListReportResponse = exports.ListReportRequest = exports.DeleteReportResponse = exports.DeleteReportRequest = exports.UpdateReportResponse = exports.UpdateReportRequest = exports.CreateReportResponse = exports.CreateReportRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const api_response_1 = require("../../common/api-response");
const report_entity_1 = require("../../database/models/report.entity");
const helper_1 = require("../../utils/helper");
class CreateReportRequest {
}
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CreateReportRequest.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CreateReportRequest.prototype, "program_description", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateReportRequest.prototype, "triwulan_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateReportRequest.prototype, "organization_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateReportRequest.prototype, "occassion_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateReportRequest.prototype, "program_id", void 0);
exports.CreateReportRequest = CreateReportRequest;
class CreateReportResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", report_entity_1.Report)
], CreateReportResponse.prototype, "data", void 0);
exports.CreateReportResponse = CreateReportResponse;
class UpdateReportRequest extends (0, swagger_1.PartialType)(CreateReportRequest) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateReportRequest.prototype, "data_report_id", void 0);
exports.UpdateReportRequest = UpdateReportRequest;
class UpdateReportResponse extends CreateReportResponse {
}
exports.UpdateReportResponse = UpdateReportResponse;
class DeleteReportRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeleteReportRequest.prototype, "data_report_id", void 0);
exports.DeleteReportRequest = DeleteReportRequest;
class DeleteReportResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteReportResponse = DeleteReportResponse;
class ListReportRequest extends helper_1.ListBaseRequest {
}
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListReportRequest.prototype, "month", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListReportRequest.prototype, "year", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListReportRequest.prototype, "triwulan_id", void 0);
exports.ListReportRequest = ListReportRequest;
class ListReportResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListReportResponse.prototype, "data", void 0);
exports.ListReportResponse = ListReportResponse;
class DataDetailResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", report_entity_1.Report)
], DataDetailResponse.prototype, "result", void 0);
exports.DataDetailResponse = DataDetailResponse;
class DetailReportResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", DataDetailResponse)
], DetailReportResponse.prototype, "data", void 0);
exports.DetailReportResponse = DetailReportResponse;
class ListReportUserRequest extends helper_1.ListBaseRequest {
}
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListReportUserRequest.prototype, "month", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListReportUserRequest.prototype, "year", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], ListReportUserRequest.prototype, "triwulan_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], ListReportUserRequest.prototype, "fund_source_id", void 0);
exports.ListReportUserRequest = ListReportUserRequest;
class ListReportTriwulanRequest extends (0, swagger_1.OmitType)(ListReportUserRequest, [
    'triwulan_id',
]) {
}
exports.ListReportTriwulanRequest = ListReportTriwulanRequest;
class ListReportMasterRequest extends (0, swagger_1.OmitType)(ListReportUserRequest, [
    'fund_source_id',
]) {
}
exports.ListReportMasterRequest = ListReportMasterRequest;
class DownloadListReportTriwulanRequest extends (0, swagger_1.OmitType)(ListReportUserRequest, ['triwulan_id', 'limit', 'page']) {
}
exports.DownloadListReportTriwulanRequest = DownloadListReportTriwulanRequest;
class DownloadListReportMasterRequest extends (0, swagger_1.OmitType)(ListReportUserRequest, ['fund_source_id', 'limit', 'page']) {
}
exports.DownloadListReportMasterRequest = DownloadListReportMasterRequest;
class ListReportMasterUserResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListReportMasterUserResponse.prototype, "data", void 0);
exports.ListReportMasterUserResponse = ListReportMasterUserResponse;
class ListReportTriwulanUserResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListReportTriwulanUserResponse.prototype, "data", void 0);
exports.ListReportTriwulanUserResponse = ListReportTriwulanUserResponse;
class ListReportMasterUserDownload {
}
exports.ListReportMasterUserDownload = ListReportMasterUserDownload;
class ListReportTriwulanUserDownload {
}
exports.ListReportTriwulanUserDownload = ListReportTriwulanUserDownload;
//# sourceMappingURL=report.dto.js.map