"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reportProviders = void 0;
const report_entity_1 = require("../../database/models/report.entity");
exports.reportProviders = [
    {
        provide: 'REPORT_REPOSITORY',
        useFactory: (ds) => ds.getRepository(report_entity_1.Report),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=report.providers.js.map