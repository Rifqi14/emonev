import { CreateReportRequest, CreateReportResponse, DeleteReportRequest, DeleteReportResponse, DetailReportResponse, DownloadListReportMasterRequest, DownloadListReportTriwulanRequest, ListReportMasterRequest, ListReportMasterUserResponse, ListReportRequest, ListReportResponse, ListReportTriwulanRequest, ListReportTriwulanUserResponse, UpdateReportRequest, UpdateReportResponse } from './report.dto';
import { ReportService } from './report.service';
import { Response } from 'express';
export declare class ReportController {
    private readonly reportService;
    constructor(reportService: ReportService);
    detail(id: number): Promise<DetailReportResponse>;
    list(query: ListReportRequest): Promise<ListReportResponse>;
    create(data: CreateReportRequest): Promise<CreateReportResponse>;
    update(data: UpdateReportRequest): Promise<UpdateReportResponse>;
    delete(data: DeleteReportRequest): Promise<DeleteReportResponse>;
    listMaster(query: ListReportMasterRequest): Promise<ListReportMasterUserResponse>;
    listDataTriwulan(query: ListReportTriwulanRequest): Promise<ListReportTriwulanUserResponse>;
    downloadExcelListMaster(query: DownloadListReportMasterRequest, res: Response): Promise<void>;
    downloadExcelListDataTriwulan(query: DownloadListReportTriwulanRequest, res: Response): Promise<void>;
    downloadExcelSingleDataTriwulan(id: number, res: Response): Promise<void>;
    downloadPdfListMaster(query: DownloadListReportMasterRequest, res: Response): Promise<void>;
    downloadPdfListTriwulan(query: DownloadListReportTriwulanRequest, res: Response): Promise<void>;
    templateDataMaster(res: Response, query: DownloadListReportMasterRequest): Promise<void>;
    templateDataTriwulan(res: Response, query: DownloadListReportTriwulanRequest): Promise<void>;
}
