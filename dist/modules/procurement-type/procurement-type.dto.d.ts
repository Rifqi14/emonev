import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { ProcurementType } from 'src/database/models/procurement-type.entity';
import { ListBaseRequest } from 'src/utils/helper';
export declare class CreateProcurementTypeRequest {
    name: string;
}
export declare class CreateProcurementTypeResponse extends BaseApiResponse {
    data: ProcurementType;
}
export declare class ListProcurementTypeRequest extends ListBaseRequest {
}
export declare class ListProcurementTypeResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<ProcurementType[]>;
}
