"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProcurementTypeModule = void 0;
const common_1 = require("@nestjs/common");
const procurement_type_service_1 = require("./procurement-type.service");
const procurement_type_controller_1 = require("./procurement-type.controller");
const procurement_type_providers_1 = require("./procurement-type.providers");
let ProcurementTypeModule = class ProcurementTypeModule {
};
ProcurementTypeModule = __decorate([
    (0, common_1.Module)({
        controllers: [procurement_type_controller_1.ProcurementTypeController],
        providers: [procurement_type_service_1.ProcurementTypeService, ...procurement_type_providers_1.procurementTypeProviders],
        exports: [procurement_type_service_1.ProcurementTypeService],
    })
], ProcurementTypeModule);
exports.ProcurementTypeModule = ProcurementTypeModule;
//# sourceMappingURL=procurement-type.module.js.map