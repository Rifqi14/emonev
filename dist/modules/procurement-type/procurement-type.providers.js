"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.procurementTypeProviders = void 0;
const procurement_type_entity_1 = require("../../database/models/procurement-type.entity");
exports.procurementTypeProviders = [
    {
        provide: 'PROCUREMENT_TYPE_REPOSITORY',
        useFactory: (ds) => ds.getRepository(procurement_type_entity_1.ProcurementType),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=procurement-type.providers.js.map