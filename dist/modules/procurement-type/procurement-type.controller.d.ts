import { ProcurementTypeService } from './procurement-type.service';
import { CreateProcurementTypeRequest, CreateProcurementTypeResponse, ListProcurementTypeRequest, ListProcurementTypeResponse } from './procurement-type.dto';
export declare class ProcurementTypeController {
    private readonly procurementTypeService;
    constructor(procurementTypeService: ProcurementTypeService);
    list(query: ListProcurementTypeRequest): Promise<ListProcurementTypeResponse>;
    create(data: CreateProcurementTypeRequest): Promise<CreateProcurementTypeResponse>;
}
