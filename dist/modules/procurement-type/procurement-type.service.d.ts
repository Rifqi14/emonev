import { ProcurementType } from 'src/database/models/procurement-type.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { CreateProcurementTypeRequest, ListProcurementTypeRequest } from './procurement-type.dto';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
export declare class ProcurementTypeService {
    private repository;
    constructor(repository: Repository<ProcurementType>);
    create(data: CreateProcurementTypeRequest): Promise<ProcurementType>;
    list(query: ListProcurementTypeRequest): Promise<DataWithPaginationApiResponse<Array<ProcurementType>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<ProcurementType>;
}
