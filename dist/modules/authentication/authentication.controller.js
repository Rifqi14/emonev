"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const openapi_1 = require("../../common/openapi");
const authentication_dto_1 = require("./authentication.dto");
const authentication_service_1 = require("./authentication.service");
const authentication_guard_1 = require("./authentication.guard");
const user_dto_1 = require("../user/user.dto");
let AuthenticationController = class AuthenticationController {
    constructor(authenticationService) {
        this.authenticationService = authenticationService;
    }
    signIn(data) {
        return this.authenticationService.signIn(data);
    }
    async getProfile(req) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.authenticationService.getProfile(req.user.username),
        };
    }
    async register(data) {
        return {
            statusCode: 200,
            message: 'Pembuatan data berhasil',
            data: await this.authenticationService.register(data),
        };
    }
    async forgotPassword(data) {
        await this.authenticationService.forgotPassword(data.email);
        return {
            statusCode: 200,
            message: 'Password changed successfully',
        };
    }
};
__decorate([
    (0, authentication_guard_1.PublicAPI)(),
    (0, openapi_1.SwaggerOkResponse)(authentication_dto_1.SignInResponse),
    (0, common_1.Post)('login'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [authentication_dto_1.SignInRequest]),
    __metadata("design:returntype", Promise)
], AuthenticationController.prototype, "signIn", null);
__decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, openapi_1.SwaggerOkResponse)(authentication_dto_1.GetProfileResponse),
    (0, common_1.Get)('profile'),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthenticationController.prototype, "getProfile", null);
__decorate([
    (0, authentication_guard_1.PublicAPI)(),
    (0, openapi_1.SwaggerOkResponse)(user_dto_1.CreateUserResponse),
    (0, common_1.Post)('register'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_dto_1.RegisterUserRequest]),
    __metadata("design:returntype", Promise)
], AuthenticationController.prototype, "register", null);
__decorate([
    (0, authentication_guard_1.PublicAPI)(),
    (0, openapi_1.SwaggerOkResponse)(user_dto_1.ForgotPasswordResponse),
    (0, common_1.Post)('forgot-password'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_dto_1.ForgotPasswordRequest]),
    __metadata("design:returntype", Promise)
], AuthenticationController.prototype, "forgotPassword", null);
AuthenticationController = __decorate([
    (0, swagger_1.ApiTags)('Authentication'),
    (0, common_1.Controller)('user'),
    __metadata("design:paramtypes", [authentication_service_1.AuthenticationService])
], AuthenticationController);
exports.AuthenticationController = AuthenticationController;
//# sourceMappingURL=authentication.controller.js.map