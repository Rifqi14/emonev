import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import { SignInRequest, SignInResponse } from './authentication.dto';
import { User } from 'src/database/models/user.entity';
import { CreateUserRequest } from '../user/user.dto';
import { MailService } from '../mail/mail.service';
export declare class AuthenticationService {
    private userService;
    private jwtService;
    private mailService;
    constructor(userService: UserService, jwtService: JwtService, mailService: MailService);
    signIn(req: SignInRequest): Promise<SignInResponse>;
    getProfile(username: string): Promise<User>;
    register(data: CreateUserRequest): Promise<User>;
    forgotPassword(email: string): Promise<void>;
}
