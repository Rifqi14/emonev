"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetProfileResponse = exports.SignInResponse = exports.PayloadClientResponse = exports.SignInRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const api_response_1 = require("../../common/api-response");
const organization_entity_1 = require("../../database/models/organization.entity");
const role_entity_1 = require("../../database/models/role.entity");
const status_entity_1 = require("../../database/models/status.entity");
const user_organization_entity_1 = require("../../database/models/user-organization.entity");
const user_entity_1 = require("../../database/models/user.entity");
class SignInRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignInRequest.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignInRequest.prototype, "password", void 0);
exports.SignInRequest = SignInRequest;
class PayloadClientResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], PayloadClientResponse.prototype, "user_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PayloadClientResponse.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: role_entity_1.Role }),
    __metadata("design:type", role_entity_1.Role)
], PayloadClientResponse.prototype, "role", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: organization_entity_1.Organization }),
    __metadata("design:type", organization_entity_1.Organization)
], PayloadClientResponse.prototype, "organization", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: status_entity_1.Status }),
    __metadata("design:type", status_entity_1.Status)
], PayloadClientResponse.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [user_organization_entity_1.UserOrganization] }),
    __metadata("design:type", Array)
], PayloadClientResponse.prototype, "userOrganization", void 0);
exports.PayloadClientResponse = PayloadClientResponse;
class SignInResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SignInResponse.prototype, "statusCode", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignInResponse.prototype, "access_token", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: PayloadClientResponse }),
    __metadata("design:type", PayloadClientResponse)
], SignInResponse.prototype, "payloadClient", void 0);
exports.SignInResponse = SignInResponse;
class GetProfileResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: user_entity_1.User }),
    __metadata("design:type", user_entity_1.User)
], GetProfileResponse.prototype, "data", void 0);
exports.GetProfileResponse = GetProfileResponse;
//# sourceMappingURL=authentication.dto.js.map