import { BaseApiResponse } from 'src/common/api-response';
import { Organization } from 'src/database/models/organization.entity';
import { Role } from 'src/database/models/role.entity';
import { Status } from 'src/database/models/status.entity';
import { UserOrganization } from 'src/database/models/user-organization.entity';
import { User } from 'src/database/models/user.entity';
export declare class SignInRequest {
    username: string;
    password: string;
}
export declare class PayloadClientResponse {
    user_id: number;
    username: string;
    role: Role;
    organization?: Organization;
    status?: Status;
    userOrganization?: UserOrganization[];
}
export declare class SignInResponse {
    statusCode: number;
    access_token: string;
    payloadClient: PayloadClientResponse;
}
export declare class GetProfileResponse extends BaseApiResponse {
    data: User;
}
