import { Request } from 'express';
import { GetProfileResponse, PayloadClientResponse, SignInRequest, SignInResponse } from './authentication.dto';
import { AuthenticationService } from './authentication.service';
import { CreateUserResponse, ForgotPasswordRequest, ForgotPasswordResponse, RegisterUserRequest } from '../user/user.dto';
export declare class AuthenticationController {
    private readonly authenticationService;
    constructor(authenticationService: AuthenticationService);
    signIn(data: SignInRequest): Promise<SignInResponse>;
    getProfile(req: Request & {
        user: PayloadClientResponse;
    }): Promise<GetProfileResponse>;
    register(data: RegisterUserRequest): Promise<CreateUserResponse>;
    forgotPassword(data: ForgotPasswordRequest): Promise<ForgotPasswordResponse>;
}
