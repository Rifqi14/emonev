"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListOrganizationResponse = exports.DeleteOrganizationResponse = exports.DeleteOrganizationRequest = exports.UpdateOrganizationResponse = exports.UpdateOrganizationRequest = exports.CreateOrganizationResponse = exports.CreateOrganizationRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const api_response_1 = require("../../common/api-response");
const organization_entity_1 = require("../../database/models/organization.entity");
class CreateOrganizationRequest {
}
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CreateOrganizationRequest.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateOrganizationRequest.prototype, "title", void 0);
exports.CreateOrganizationRequest = CreateOrganizationRequest;
class CreateOrganizationResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", organization_entity_1.Organization)
], CreateOrganizationResponse.prototype, "data", void 0);
exports.CreateOrganizationResponse = CreateOrganizationResponse;
class UpdateOrganizationRequest extends (0, swagger_1.PartialType)(CreateOrganizationRequest) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateOrganizationRequest.prototype, "organization_id", void 0);
exports.UpdateOrganizationRequest = UpdateOrganizationRequest;
class UpdateOrganizationResponse extends CreateOrganizationResponse {
}
exports.UpdateOrganizationResponse = UpdateOrganizationResponse;
class DeleteOrganizationRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeleteOrganizationRequest.prototype, "organization_id", void 0);
exports.DeleteOrganizationRequest = DeleteOrganizationRequest;
class DeleteOrganizationResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteOrganizationResponse = DeleteOrganizationResponse;
class ListOrganizationResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListOrganizationResponse.prototype, "data", void 0);
exports.ListOrganizationResponse = ListOrganizationResponse;
//# sourceMappingURL=organization.dto.js.map