import { OrganizationService } from './organization.service';
import { CreateOrganizationRequest, CreateOrganizationResponse, DeleteOrganizationRequest, DeleteOrganizationResponse, ListOrganizationResponse, UpdateOrganizationRequest, UpdateOrganizationResponse } from './organization.dto';
import { ListBaseRequest } from 'src/utils/helper';
export declare class OrganizationController {
    private readonly organizationService;
    constructor(organizationService: OrganizationService);
    detail(id: number): Promise<CreateOrganizationResponse>;
    list(query: ListBaseRequest): Promise<ListOrganizationResponse>;
    create(data: CreateOrganizationRequest): Promise<CreateOrganizationResponse>;
    update(data: UpdateOrganizationRequest): Promise<UpdateOrganizationResponse>;
    delete(data: DeleteOrganizationRequest): Promise<DeleteOrganizationResponse>;
}
