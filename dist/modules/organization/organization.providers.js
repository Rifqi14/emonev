"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.organizationProviders = void 0;
const organization_entity_1 = require("../../database/models/organization.entity");
exports.organizationProviders = [
    {
        provide: 'ORGANIZATION_REPOSITORY',
        useFactory: (ds) => ds.getRepository(organization_entity_1.Organization),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=organization.providers.js.map