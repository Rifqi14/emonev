"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationService = void 0;
const common_1 = require("@nestjs/common");
const organization_entity_1 = require("../../database/models/organization.entity");
const status_entity_1 = require("../../database/models/status.entity");
const helper_1 = require("../../utils/helper");
const typeorm_1 = require("typeorm");
let OrganizationService = class OrganizationService {
    constructor(repository) {
        this.repository = repository;
    }
    async create(data) {
        var _a, _b;
        try {
            const { title } = data;
            const latest = await this.repository.findOne({
                where: {},
                order: { id: 'DESC' },
            });
            const code = (_a = data.code) !== null && _a !== void 0 ? _a : (0, helper_1.generateCode)((_b = latest === null || latest === void 0 ? void 0 : latest.id) !== null && _b !== void 0 ? _b : 1, 'Org');
            return await this.repository.manager.transaction(async (trx) => await trx
                .create(organization_entity_1.Organization, {
                code,
                title,
                status: await trx.findOneByOrFail(status_entity_1.Status, { name: 'Active' }),
            })
                .save());
        }
        catch (error) {
            throw error;
        }
    }
    async detail(id) {
        try {
            return await this.repository.findOneOrFail({
                where: {
                    id,
                    status_id: (await this.repository.manager.findOneOrFail(status_entity_1.Status, {
                        where: { name: 'Active' },
                    })).id,
                },
                relations: {
                    status: true,
                },
            });
        }
        catch (error) {
            throw error;
        }
    }
    async update(data) {
        try {
            const { organization_id, code, title } = data;
            await this.repository.manager.transaction(async (trx) => trx.update(organization_entity_1.Organization, { id: organization_id }, {
                code,
                title,
            }));
            return await this.repository.findOneByOrFail({
                id: organization_id,
            });
        }
        catch (error) {
            throw error;
        }
    }
    async delete(organization_id) {
        try {
            const organization = await this.repository.findOneOrFail({
                where: { id: organization_id },
            });
            organization.status = await this.repository.manager.findOneOrFail(status_entity_1.Status, { where: { name: 'Deleted' } });
            await organization.save();
        }
        catch (error) {
            throw error;
        }
    }
    async list(query) {
        try {
            const { search, sort } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const { id: status_id } = await this.repository.manager.findOneOrFail(status_entity_1.Status, {
                where: { name: 'Active' },
            });
            const where = [
                {
                    status_id: status_id,
                },
            ];
            if (search) {
                where.pop();
                where.push({
                    title: (0, typeorm_1.ILike)(`%${search}%`),
                    status_id: status_id,
                });
                where.push({
                    code: (0, typeorm_1.ILike)(`%${search}%`),
                    status_id: status_id,
                });
            }
            const [organizations, count] = await this.repository.findAndCount({
                where: where,
                order: this.getOrderQuery(sort),
                relations: {
                    status: true,
                },
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: organizations,
            };
        }
        catch (error) {
            throw error;
        }
    }
    getOrderQuery(sort) {
        const sortRes = {
            id: 'DESC',
        };
        switch (sort) {
            case 'terbaru':
                Object.assign(sortRes, {
                    id: 'DESC',
                });
                break;
            case 'terlama':
                Object.assign(sortRes, {
                    id: 'ASC',
                });
                break;
            case 'a-z':
                Object.assign(sortRes, {
                    title: 'ASC',
                });
                break;
            case 'z-a':
                Object.assign(sortRes, {
                    title: 'DESC',
                });
                break;
        }
        return sortRes;
    }
};
OrganizationService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('ORGANIZATION_REPOSITORY')),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], OrganizationService);
exports.OrganizationService = OrganizationService;
//# sourceMappingURL=organization.service.js.map