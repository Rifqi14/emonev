import { Organization } from 'src/database/models/organization.entity';
import { ListBaseRequest } from 'src/utils/helper';
import { FindOptionsOrder, Repository } from 'typeorm';
import { CreateOrganizationRequest, UpdateOrganizationRequest } from './organization.dto';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
export declare class OrganizationService {
    private repository;
    constructor(repository: Repository<Organization>);
    create(data: CreateOrganizationRequest): Promise<Organization>;
    detail(id: number): Promise<Organization>;
    update(data: UpdateOrganizationRequest): Promise<Organization>;
    delete(organization_id: number): Promise<void>;
    list(query: ListBaseRequest): Promise<DataWithPaginationApiResponse<Array<Organization>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<Organization>;
}
