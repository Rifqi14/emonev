import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { Organization } from 'src/database/models/organization.entity';
export declare class CreateOrganizationRequest {
    code?: string;
    title: string;
}
export declare class CreateOrganizationResponse extends BaseApiResponse {
    data: Organization;
}
declare const UpdateOrganizationRequest_base: import("@nestjs/common").Type<Partial<CreateOrganizationRequest>>;
export declare class UpdateOrganizationRequest extends UpdateOrganizationRequest_base {
    organization_id: number;
}
export declare class UpdateOrganizationResponse extends CreateOrganizationResponse {
}
export declare class DeleteOrganizationRequest {
    organization_id: number;
}
export declare class DeleteOrganizationResponse extends BaseApiResponse {
}
export declare class ListOrganizationResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<Organization>>;
}
export {};
