import { Program } from 'src/database/models/program.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { CreateProgramRequest, UpdateProgramRequest } from './program.dto';
import { ListBaseRequest } from 'src/utils/helper';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
export declare class ProgramService {
    private repository;
    constructor(repository: Repository<Program>);
    create(data: CreateProgramRequest): Promise<Program>;
    detail(id: number): Promise<Program>;
    update(data: UpdateProgramRequest): Promise<Program>;
    delete(program_id: number): Promise<void>;
    list(query: ListBaseRequest): Promise<DataWithPaginationApiResponse<Array<Program>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<Program>;
}
