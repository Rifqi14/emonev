"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListProgramResponse = exports.DeleteProgramResponse = exports.DeleteProgramRequest = exports.UpdateProgramResponse = exports.UpdateProgramRequest = exports.CreateProgramResponse = exports.CreateProgramRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const api_response_1 = require("../../common/api-response");
const program_entity_1 = require("../../database/models/program.entity");
class CreateProgramRequest {
}
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: 'Judul harus diisi.' }),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateProgramRequest.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CreateProgramRequest.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateProgramRequest.prototype, "occassion_id", void 0);
exports.CreateProgramRequest = CreateProgramRequest;
class CreateProgramResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", program_entity_1.Program)
], CreateProgramResponse.prototype, "data", void 0);
exports.CreateProgramResponse = CreateProgramResponse;
class UpdateProgramRequest extends (0, swagger_1.PartialType)(CreateProgramRequest) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateProgramRequest.prototype, "program_id", void 0);
exports.UpdateProgramRequest = UpdateProgramRequest;
class UpdateProgramResponse extends CreateProgramResponse {
}
exports.UpdateProgramResponse = UpdateProgramResponse;
class DeleteProgramRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeleteProgramRequest.prototype, "program_id", void 0);
exports.DeleteProgramRequest = DeleteProgramRequest;
class DeleteProgramResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteProgramResponse = DeleteProgramResponse;
class ListProgramResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListProgramResponse.prototype, "data", void 0);
exports.ListProgramResponse = ListProgramResponse;
//# sourceMappingURL=program.dto.js.map