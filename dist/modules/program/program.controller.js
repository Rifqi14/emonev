"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProgramController = void 0;
const common_1 = require("@nestjs/common");
const program_service_1 = require("./program.service");
const openapi_1 = require("../../common/openapi");
const program_dto_1 = require("./program.dto");
const swagger_1 = require("@nestjs/swagger");
const helper_1 = require("../../utils/helper");
let ProgramController = class ProgramController {
    constructor(programService) {
        this.programService = programService;
    }
    async detail(id) {
        return {
            statusCode: 200,
            message: `Pengambilan data berhasil`,
            data: await this.programService.detail(id),
        };
    }
    async list(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.programService.list(query),
        };
    }
    async create(data) {
        return {
            statusCode: 200,
            message: `Pembuatan data berhasil`,
            data: await this.programService.create(data),
        };
    }
    async update(data) {
        return {
            statusCode: 200,
            message: 'Pengkinian data berhasil',
            data: await this.programService.update(data),
        };
    }
    async delete(data) {
        await this.programService.delete(data.program_id);
        return {
            statusCode: 200,
            message: 'Penghapusan data berhasil',
        };
    }
};
__decorate([
    (0, openapi_1.SwaggerOkResponse)(program_dto_1.CreateProgramResponse),
    (0, common_1.Get)('detail/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ProgramController.prototype, "detail", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(program_dto_1.ListProgramResponse),
    (0, common_1.Get)('list'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [helper_1.ListBaseRequest]),
    __metadata("design:returntype", Promise)
], ProgramController.prototype, "list", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(program_dto_1.CreateProgramResponse),
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [program_dto_1.CreateProgramRequest]),
    __metadata("design:returntype", Promise)
], ProgramController.prototype, "create", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(program_dto_1.UpdateProgramResponse),
    (0, common_1.Patch)('update'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [program_dto_1.UpdateProgramRequest]),
    __metadata("design:returntype", Promise)
], ProgramController.prototype, "update", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(program_dto_1.DeleteProgramResponse),
    (0, common_1.Patch)('delete'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [program_dto_1.DeleteProgramRequest]),
    __metadata("design:returntype", Promise)
], ProgramController.prototype, "delete", null);
ProgramController = __decorate([
    (0, swagger_1.ApiTags)('Program'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('program'),
    __metadata("design:paramtypes", [program_service_1.ProgramService])
], ProgramController);
exports.ProgramController = ProgramController;
//# sourceMappingURL=program.controller.js.map