import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { Program } from 'src/database/models/program.entity';
export declare class CreateProgramRequest {
    title: string;
    code?: string;
    occassion_id?: number;
}
export declare class CreateProgramResponse extends BaseApiResponse {
    data: Program;
}
declare const UpdateProgramRequest_base: import("@nestjs/common").Type<Partial<CreateProgramRequest>>;
export declare class UpdateProgramRequest extends UpdateProgramRequest_base {
    program_id: number;
}
export declare class UpdateProgramResponse extends CreateProgramResponse {
}
export declare class DeleteProgramRequest {
    program_id: number;
}
export declare class DeleteProgramResponse extends BaseApiResponse {
}
export declare class ListProgramResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<Program>>;
}
export {};
