"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.programProviders = void 0;
const program_entity_1 = require("../../database/models/program.entity");
exports.programProviders = [
    {
        provide: 'PROGRAM_REPOSITORY',
        useFactory: (ds) => ds.getRepository(program_entity_1.Program),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=program.providers.js.map