import { ProgramService } from './program.service';
import { CreateProgramRequest, CreateProgramResponse, DeleteProgramRequest, DeleteProgramResponse, ListProgramResponse, UpdateProgramRequest, UpdateProgramResponse } from './program.dto';
import { ListBaseRequest } from 'src/utils/helper';
export declare class ProgramController {
    private readonly programService;
    constructor(programService: ProgramService);
    detail(id: number): Promise<CreateProgramResponse>;
    list(query: ListBaseRequest): Promise<ListProgramResponse>;
    create(data: CreateProgramRequest): Promise<CreateProgramResponse>;
    update(data: UpdateProgramRequest): Promise<UpdateProgramResponse>;
    delete(data: DeleteProgramRequest): Promise<DeleteProgramResponse>;
}
