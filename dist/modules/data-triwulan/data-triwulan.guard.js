"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataTriwulanGuard = void 0;
const common_1 = require("@nestjs/common");
const setting_service_1 = require("../setting/setting.service");
const core_1 = require("@nestjs/core");
const dayjs_1 = __importDefault(require("dayjs"));
const isBetween_1 = __importDefault(require("dayjs/plugin/isBetween"));
dayjs_1.default.extend(isBetween_1.default);
let DataTriwulanGuard = class DataTriwulanGuard {
    constructor(settingService, reflector) {
        this.settingService = settingService;
        this.reflector = reflector;
    }
    async canActivate(context) {
        const currentDay = (0, dayjs_1.default)();
        const settings = await this.settingService.getSetting();
        const started = settings.find((setting) => setting.key === 'TRIWULAN_STARTED');
        const ended = settings.find((setting) => setting.key === 'TRIWULAN_ENDED');
        if (!started || !ended)
            throw new common_1.HttpException('Periode triwulan belum di set, silahkan hubungi admin.', common_1.HttpStatus.BAD_REQUEST);
        if (!currentDay.isBetween(started.value, ended.value))
            throw new common_1.HttpException('Data yang anda buat diluar periode data triwulan. Silahkan hubungi admin.', common_1.HttpStatus.BAD_REQUEST);
        return true;
    }
};
DataTriwulanGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [setting_service_1.SettingService,
        core_1.Reflector])
], DataTriwulanGuard);
exports.DataTriwulanGuard = DataTriwulanGuard;
//# sourceMappingURL=data-triwulan.guard.js.map