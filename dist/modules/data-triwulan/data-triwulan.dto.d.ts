/// <reference types="multer" />
import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { ListBaseRequest } from 'src/utils/helper';
export declare class CreateDataTriwulanRequest {
    activity_name: string;
    activity_location: string;
    fund_source_id?: number;
    fund_ceiling?: number;
    management_organization?: string;
    pptk_name?: string;
    contract_number_date?: string;
    contractor_name?: string;
    implementation_period?: string;
    contract_value?: number;
    physical_realization?: number;
    fund_realization?: number;
    activity_volume?: string;
    activity_output?: string;
    direct_target_group?: string;
    indirect_target_group?: string;
    local_workforce?: number;
    non_local_workforce?: number;
    problems?: string;
    solution?: string;
    procurement_type?: string;
    procurement_method?: string;
    activity_id?: number;
    activity_form?: 'fisik' | 'nonfisik';
    file?: Express.Multer.File;
    contract_date?: string;
    pic_name?: string;
    leader_name?: string;
    optional?: string;
    reason?: string;
}
export declare class CreateDataTriwulanResponse extends BaseApiResponse {
    data: DataTriwulan;
}
declare const UpdateDataTriwulanRequest_base: import("@nestjs/common").Type<Partial<CreateDataTriwulanRequest>>;
export declare class UpdateDataTriwulanRequest extends UpdateDataTriwulanRequest_base {
}
export declare class UpdateDataTriwulanResponse extends CreateDataTriwulanResponse {
}
export declare class DetailDataTriwulanResponse extends CreateDataTriwulanResponse {
}
export declare class ListDataTriwulanRequest extends ListBaseRequest {
    month?: string;
    year?: string;
    triwulan_id?: string;
    fund_source_id?: string;
}
export declare class ListHistoryDataTriwulanRequest extends ListBaseRequest {
    activity_name_search?: string;
    organization_id?: string;
}
export declare class ListDataTriwulanResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<DataTriwulan[]>;
}
export declare class DeleteDataTriwulanResponse extends BaseApiResponse {
}
export {};
