/// <reference types="multer" />
import { CreateDataTriwulanRequest, CreateDataTriwulanResponse, DeleteDataTriwulanResponse, DetailDataTriwulanResponse, ListDataTriwulanRequest, ListDataTriwulanResponse, ListHistoryDataTriwulanRequest, UpdateDataTriwulanRequest, UpdateDataTriwulanResponse } from './data-triwulan.dto';
import { DataTriwulanService } from './data-triwulan.service';
export declare class DataTriwulanController {
    private readonly dataTriwulanService;
    constructor(dataTriwulanService: DataTriwulanService);
    list(query: ListDataTriwulanRequest): Promise<ListDataTriwulanResponse>;
    history(query: ListHistoryDataTriwulanRequest): Promise<ListDataTriwulanResponse>;
    detail(id: number): Promise<DetailDataTriwulanResponse>;
    create(file: Express.Multer.File, data: CreateDataTriwulanRequest): Promise<CreateDataTriwulanResponse>;
    update(id: number, data: UpdateDataTriwulanRequest, file: Express.Multer.File): Promise<UpdateDataTriwulanResponse>;
    delete(id: number): Promise<DeleteDataTriwulanResponse>;
}
