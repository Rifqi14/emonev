"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataTriwulanModule = void 0;
const common_1 = require("@nestjs/common");
const data_triwulan_service_1 = require("./data-triwulan.service");
const data_triwulan_controller_1 = require("./data-triwulan.controller");
const data_triwulan_providers_1 = require("./data-triwulan.providers");
const setting_module_1 = require("../setting/setting.module");
let DataTriwulanModule = class DataTriwulanModule {
};
DataTriwulanModule = __decorate([
    (0, common_1.Module)({
        controllers: [data_triwulan_controller_1.DataTriwulanController],
        imports: [setting_module_1.SettingModule],
        providers: [data_triwulan_service_1.DataTriwulanService, ...data_triwulan_providers_1.dataTriwulanProviders],
        exports: [data_triwulan_service_1.DataTriwulanService],
    })
], DataTriwulanModule);
exports.DataTriwulanModule = DataTriwulanModule;
//# sourceMappingURL=data-triwulan.module.js.map