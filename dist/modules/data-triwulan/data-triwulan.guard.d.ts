import { CanActivate, ExecutionContext } from '@nestjs/common';
import { SettingService } from '../setting/setting.service';
import { Reflector } from '@nestjs/core';
export declare class DataTriwulanGuard implements CanActivate {
    private settingService;
    private reflector;
    constructor(settingService: SettingService, reflector: Reflector);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
