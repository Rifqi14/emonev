"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataTriwulanService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const fs_1 = require("fs");
const data_triwulan_entity_1 = require("../../database/models/data-triwulan.entity");
const user_entity_1 = require("../../database/models/user.entity");
const typeorm_1 = require("typeorm");
let DataTriwulanService = class DataTriwulanService {
    constructor(dataTriwulanRepository, request) {
        this.dataTriwulanRepository = dataTriwulanRepository;
        this.request = request;
    }
    async create(data, file) {
        try {
            return await this.dataTriwulanRepository.manager.transaction(async (trx) => {
                const creator = await trx.findOneOrFail(user_entity_1.User, {
                    where: {
                        id: this.request.user.user_id,
                    },
                });
                return await trx
                    .create(data_triwulan_entity_1.DataTriwulan, Object.assign(Object.assign({}, data), { fund_ceiling: data.fund_ceiling ? Number(data.fund_ceiling) : 0, contract_value: data.contract_value
                        ? Number(data.contract_value)
                        : 0, physical_realization: data.physical_realization
                        ? Number(data.physical_realization)
                        : 0, fund_realization: data.fund_realization
                        ? Number(data.fund_realization)
                        : 0, local_workforce: data.local_workforce
                        ? Number(data.local_workforce)
                        : 0, non_local_workforce: data.non_local_workforce
                        ? Number(data.non_local_workforce)
                        : 0, file: file && file.path.replace(/\\/g, '/'), createdBy: creator }))
                    .save();
            });
        }
        catch (error) {
            throw error;
        }
    }
    async detail(id) {
        try {
            const triwulan = await this.dataTriwulanRepository.findOneOrFail({
                relations: {
                    fundSource: true,
                    activity: {
                        program: true,
                    },
                },
                where: {
                    id,
                },
            });
            return triwulan;
        }
        catch (error) {
            throw error;
        }
    }
    async update(id, data, file) {
        try {
            await this.dataTriwulanRepository.manager.transaction(async (trx) => {
                const dataTriwulan = await this.dataTriwulanRepository.findOneByOrFail({
                    id,
                });
                if (file && dataTriwulan.file) {
                    (0, fs_1.unlinkSync)(dataTriwulan.file);
                }
                await trx
                    .create(data_triwulan_entity_1.DataTriwulan, Object.assign(Object.assign(Object.assign({}, dataTriwulan), data), { fund_ceiling: data.fund_ceiling
                        ? Number(data.fund_ceiling)
                        : data.fund_ceiling, contract_value: data.contract_value
                        ? Number(data.contract_value)
                        : data.contract_value, physical_realization: data.physical_realization
                        ? Number(data.physical_realization)
                        : data.physical_realization, fund_realization: data.fund_realization
                        ? Number(data.fund_realization)
                        : data.fund_realization, local_workforce: data.local_workforce
                        ? Number(data.local_workforce)
                        : data.local_workforce, non_local_workforce: data.non_local_workforce
                        ? Number(data.non_local_workforce)
                        : data.non_local_workforce, file: file ? file.path.replace(/\\/g, '/') : undefined }))
                    .save();
            });
            return await this.dataTriwulanRepository.findOneByOrFail({
                id,
            });
        }
        catch (error) {
            throw error;
        }
    }
    async delete(id) {
        try {
            await this.dataTriwulanRepository
                .findOneByOrFail({ id })
                .then((result) => result.file && (0, fs_1.unlinkSync)(result.file));
            await this.dataTriwulanRepository.delete({ id });
        }
        catch (error) {
            throw error;
        }
    }
    async list(query) {
        const organization = this.request.user.organization && this.request.user.organization.id
            ? [this.request.user.organization.id]
            : [];
        if (this.request.user.userOrganization &&
            this.request.user.userOrganization.length > 0) {
            organization.push(...this.request.user.userOrganization.map((organization) => organization.organization_id));
        }
        try {
            const { search, sort } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const where = [];
            if (search) {
                where.push({
                    activity_name: (0, typeorm_1.ILike)(`%${search}%`),
                });
                where.push({
                    activity_location: (0, typeorm_1.ILike)(`%${search}%`),
                });
            }
            if (this.request.is_admin_bidang) {
                if (!this.request.user.userOrganization ||
                    this.request.user.userOrganization.length === 0)
                    throw new common_1.UnauthorizedException();
                where[0] = Object.assign(Object.assign({}, where[0]), { createdBy: {
                        organization_id: (0, typeorm_1.In)(this.request.user.userOrganization.map((organization) => organization.organization_id)),
                    } });
            }
            if (this.request.user.role.name.toLowerCase() == 'opd') {
                if (where.length > 0) {
                    where.map((where) => {
                        return Object.assign(Object.assign({}, where), { createdBy: {
                                userOrganization: {
                                    organization_id: (0, typeorm_1.In)([...new Set(organization)]),
                                },
                            } });
                    });
                }
                else {
                    where.push({
                        createdBy: {
                            userOrganization: {
                                organization_id: (0, typeorm_1.In)([...new Set(organization)]),
                            },
                        },
                    });
                }
                console.log(where);
            }
            const [dataTriwulan, count] = await this.dataTriwulanRepository.findAndCount({
                where: where.length > 0 ? where : undefined,
                order: this.getOrderQuery(sort),
                relations: {
                    fundSource: true,
                    activity: {
                        program: true,
                    },
                },
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: dataTriwulan,
            };
        }
        catch (error) {
            throw error;
        }
    }
    async history(query) {
        try {
            const same = await this.dataTriwulanRepository.query(`select count(*) as total_sama, lower(data_triwulan.activity_name) as name from data_triwulan group by lower(activity_name) having count(*) > 1`);
            let activity_name = same.map((data) => data.name);
            const { activity_name_search: search, sort } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            if (search) {
                activity_name = activity_name.filter((name) => name.toLowerCase().includes(search.toLowerCase()));
            }
            console.log(activity_name);
            const where = [
                {
                    activity_name: (0, typeorm_1.Raw)((alias) => `${alias} ilike any(:activity_name)`, {
                        activity_name,
                    }),
                },
            ];
            if (query.organization_id) {
                where.pop();
                where.push({
                    createdBy: {
                        organization_id: +query.organization_id,
                    },
                    activity_name: (0, typeorm_1.Raw)((alias) => `${alias} ilike any(:activity_name)`, {
                        activity_name,
                    }),
                });
                where.push({
                    createdBy: {
                        userOrganization: {
                            organization_id: +query.organization_id,
                        },
                    },
                    activity_name: (0, typeorm_1.Raw)((alias) => `${alias} ilike any(:activity_name)`, {
                        activity_name,
                    }),
                });
            }
            const [dataTriwulan, count] = await this.dataTriwulanRepository.findAndCount({
                where: where.length > 0 ? where : undefined,
                order: this.getOrderQuery(sort),
                relations: {
                    fundSource: true,
                    createdBy: true,
                },
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: dataTriwulan,
            };
        }
        catch (error) {
            throw error;
        }
    }
    getOrderQuery(sort) {
        const sortRes = {
            id: 'DESC',
        };
        switch (sort) {
            case 'terbaru':
                Object.assign(sortRes, {
                    id: 'DESC',
                });
                break;
            case 'terlama':
                Object.assign(sortRes, {
                    id: 'ASC',
                });
                break;
            case 'a-z':
                Object.assign(sortRes, {
                    activity_name: 'ASC',
                });
                break;
            case 'z-a':
                Object.assign(sortRes, {
                    activity_name: 'DESC',
                });
                break;
        }
        return sortRes;
    }
};
DataTriwulanService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)('DATA_TRIWULAN_REPOSITORY')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [typeorm_1.Repository, Object])
], DataTriwulanService);
exports.DataTriwulanService = DataTriwulanService;
//# sourceMappingURL=data-triwulan.service.js.map