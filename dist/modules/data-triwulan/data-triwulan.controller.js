"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataTriwulanController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const swagger_1 = require("@nestjs/swagger");
const multer_1 = require("multer");
const path_1 = require("path");
const openapi_1 = require("../../common/openapi");
const data_triwulan_entity_1 = require("../../database/models/data-triwulan.entity");
const data_triwulan_dto_1 = require("./data-triwulan.dto");
const data_triwulan_guard_1 = require("./data-triwulan.guard");
const data_triwulan_service_1 = require("./data-triwulan.service");
let DataTriwulanController = class DataTriwulanController {
    constructor(dataTriwulanService) {
        this.dataTriwulanService = dataTriwulanService;
    }
    async list(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.dataTriwulanService.list(query),
        };
    }
    async history(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.dataTriwulanService.history(query),
        };
    }
    async detail(id) {
        return {
            statusCode: 200,
            message: `Pengambilan data berhasil`,
            data: await this.dataTriwulanService.detail(id),
        };
    }
    async create(file, data) {
        console.log(file);
        return {
            statusCode: 200,
            message: `Pembuatan data berhasil`,
            data: await this.dataTriwulanService.create(data, file),
        };
    }
    async update(id, data, file) {
        console.log(file);
        return {
            statusCode: 200,
            message: 'Pengkinian data berhasil',
            data: await this.dataTriwulanService.update(id, data, file),
        };
    }
    async delete(id) {
        await this.dataTriwulanService.delete(id);
        return {
            statusCode: 200,
            message: 'Penghapusan data berhasil',
        };
    }
};
__decorate([
    (0, openapi_1.SwaggerPaginationResponse)(data_triwulan_entity_1.DataTriwulan),
    (0, common_1.Get)('list'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [data_triwulan_dto_1.ListDataTriwulanRequest]),
    __metadata("design:returntype", Promise)
], DataTriwulanController.prototype, "list", null);
__decorate([
    (0, openapi_1.SwaggerPaginationResponse)(data_triwulan_entity_1.DataTriwulan),
    (0, common_1.Get)('list/history'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [data_triwulan_dto_1.ListHistoryDataTriwulanRequest]),
    __metadata("design:returntype", Promise)
], DataTriwulanController.prototype, "history", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(data_triwulan_dto_1.DetailDataTriwulanResponse),
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], DataTriwulanController.prototype, "detail", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(data_triwulan_dto_1.CreateDataTriwulanResponse),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file', {
        storage: (0, multer_1.diskStorage)({
            destination: (0, path_1.join)('public', 'upload', 'data-triwulan'),
            filename: (req, file, cb) => {
                const filename = `${new Date().getTime().toString()}_data-triwulan.${file.originalname.split('.')[1]}`;
                cb(null, filename);
            },
        }),
    })),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({
        type: data_triwulan_dto_1.CreateDataTriwulanRequest,
    }),
    (0, common_1.UseGuards)(data_triwulan_guard_1.DataTriwulanGuard),
    (0, common_1.Post)(''),
    __param(0, (0, common_1.UploadedFile)(new common_1.ParseFilePipe({
        validators: [
            new common_1.MaxFileSizeValidator({ maxSize: 1024 * 1024 * 5 }),
            new common_1.FileTypeValidator({ fileType: '.(png|jpeg|jpg|pdf|mp4)' }),
        ],
        fileIsRequired: false,
    }))),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, data_triwulan_dto_1.CreateDataTriwulanRequest]),
    __metadata("design:returntype", Promise)
], DataTriwulanController.prototype, "create", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(data_triwulan_dto_1.UpdateDataTriwulanResponse),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file', {
        storage: (0, multer_1.diskStorage)({
            destination: (0, path_1.join)('public', 'upload', 'data-triwulan'),
            filename: (req, file, cb) => {
                const filename = `${new Date().getTime().toString()}_data-triwulan.${file.originalname.split('.')[1]}`;
                cb(null, filename);
            },
        }),
    })),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({
        type: data_triwulan_dto_1.CreateDataTriwulanRequest,
    }),
    (0, common_1.UseGuards)(data_triwulan_guard_1.DataTriwulanGuard),
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.UploadedFile)(new common_1.ParseFilePipe({
        validators: [
            new common_1.MaxFileSizeValidator({ maxSize: 1024 * 1024 * 5 }),
            new common_1.FileTypeValidator({ fileType: '.(png|jpeg|jpg|pdf|mp4)' }),
        ],
        fileIsRequired: false,
    }))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, data_triwulan_dto_1.UpdateDataTriwulanRequest, Object]),
    __metadata("design:returntype", Promise)
], DataTriwulanController.prototype, "update", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(data_triwulan_dto_1.DeleteDataTriwulanResponse),
    (0, common_1.UseGuards)(data_triwulan_guard_1.DataTriwulanGuard),
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], DataTriwulanController.prototype, "delete", null);
DataTriwulanController = __decorate([
    (0, swagger_1.ApiTags)('User | Data Triwulan'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('data-triwulan'),
    __metadata("design:paramtypes", [data_triwulan_service_1.DataTriwulanService])
], DataTriwulanController);
exports.DataTriwulanController = DataTriwulanController;
//# sourceMappingURL=data-triwulan.controller.js.map