"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteDataTriwulanResponse = exports.ListDataTriwulanResponse = exports.ListHistoryDataTriwulanRequest = exports.ListDataTriwulanRequest = exports.DetailDataTriwulanResponse = exports.UpdateDataTriwulanResponse = exports.UpdateDataTriwulanRequest = exports.CreateDataTriwulanResponse = exports.CreateDataTriwulanRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const api_response_1 = require("../../common/api-response");
const data_triwulan_entity_1 = require("../../database/models/data-triwulan.entity");
const helper_1 = require("../../utils/helper");
class CreateDataTriwulanRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "activity_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "activity_location", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : +value)),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateDataTriwulanRequest.prototype, "fund_source_id", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : +value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Pagu Dana' }),
    __metadata("design:type", Number)
], CreateDataTriwulanRequest.prototype, "fund_ceiling", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'OPD Pengelola' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "management_organization", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Nama PPTK' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "pptk_name", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Nomor dan Tanggal Kontrak' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "contract_number_date", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Nama Kontraktor' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "contractor_name", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Jangka Waktu Pelaksanaan' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "implementation_period", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : +value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Nilai Kontrak' }),
    __metadata("design:type", Number)
], CreateDataTriwulanRequest.prototype, "contract_value", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : +value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Realisasi Fisik' }),
    __metadata("design:type", Number)
], CreateDataTriwulanRequest.prototype, "physical_realization", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : +value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Realisasi Keuangan' }),
    __metadata("design:type", Number)
], CreateDataTriwulanRequest.prototype, "fund_realization", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Volume Kegiatan' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "activity_volume", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Output Kegiatan' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "activity_output", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({
        description: 'Manfaat Kegiatan (Kelompok sasaran Langsung)',
    }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "direct_target_group", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({
        description: 'Manfaat Kegiatan (Kelompok sasaran Langsung)',
    }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "indirect_target_group", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : +value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Jumlah Tenaga Kerja (Lokal)' }),
    __metadata("design:type", Number)
], CreateDataTriwulanRequest.prototype, "local_workforce", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : +value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Jumlah Tenaga Kerja (Non Lokal)' }),
    __metadata("design:type", Number)
], CreateDataTriwulanRequest.prototype, "non_local_workforce", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({
        description: 'Hambatan Dan Permasalahan',
    }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "problems", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({
        description: 'Solusi Permasalahan',
    }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "solution", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Jenis pengadaan' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "procurement_type", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ description: 'Cara pengadaan' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "procurement_method", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: 'Sub Kegiatan' }),
    __metadata("design:type", Number)
], CreateDataTriwulanRequest.prototype, "activity_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({
        enum: ['fisik', 'nonfisik'],
        description: 'Bentuk kegiatan',
    }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "activity_form", void 0);
__decorate([
    (0, class_transformer_1.Transform)(({ value }) => (!value || value === '' ? undefined : value)),
    (0, swagger_1.ApiPropertyOptional)({ type: 'string', format: 'binary' }),
    __metadata("design:type", Object)
], CreateDataTriwulanRequest.prototype, "file", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: 'Tanggal kontrak. Format: YYYY-MM-DD' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "contract_date", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: 'Nama penanggung jawab' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "pic_name", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: 'Nama pimpinan' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "leader_name", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: 'Opsi. Static dari FE.' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "optional", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: 'Alasan terkait' }),
    __metadata("design:type", String)
], CreateDataTriwulanRequest.prototype, "reason", void 0);
exports.CreateDataTriwulanRequest = CreateDataTriwulanRequest;
class CreateDataTriwulanResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", data_triwulan_entity_1.DataTriwulan)
], CreateDataTriwulanResponse.prototype, "data", void 0);
exports.CreateDataTriwulanResponse = CreateDataTriwulanResponse;
class UpdateDataTriwulanRequest extends (0, swagger_1.PartialType)(CreateDataTriwulanRequest) {
}
exports.UpdateDataTriwulanRequest = UpdateDataTriwulanRequest;
class UpdateDataTriwulanResponse extends CreateDataTriwulanResponse {
}
exports.UpdateDataTriwulanResponse = UpdateDataTriwulanResponse;
class DetailDataTriwulanResponse extends CreateDataTriwulanResponse {
}
exports.DetailDataTriwulanResponse = DetailDataTriwulanResponse;
class ListDataTriwulanRequest extends helper_1.ListBaseRequest {
}
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListDataTriwulanRequest.prototype, "month", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListDataTriwulanRequest.prototype, "year", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListDataTriwulanRequest.prototype, "triwulan_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListDataTriwulanRequest.prototype, "fund_source_id", void 0);
exports.ListDataTriwulanRequest = ListDataTriwulanRequest;
class ListHistoryDataTriwulanRequest extends helper_1.ListBaseRequest {
}
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListHistoryDataTriwulanRequest.prototype, "activity_name_search", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListHistoryDataTriwulanRequest.prototype, "organization_id", void 0);
exports.ListHistoryDataTriwulanRequest = ListHistoryDataTriwulanRequest;
class ListDataTriwulanResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListDataTriwulanResponse.prototype, "data", void 0);
exports.ListDataTriwulanResponse = ListDataTriwulanResponse;
class DeleteDataTriwulanResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteDataTriwulanResponse = DeleteDataTriwulanResponse;
//# sourceMappingURL=data-triwulan.dto.js.map