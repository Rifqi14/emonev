/// <reference types="multer" />
import { DataWithPaginationApiResponse } from 'src/common/api-response';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { PayloadClientResponse } from '../authentication/authentication.dto';
import { CreateDataTriwulanRequest, ListDataTriwulanRequest, ListHistoryDataTriwulanRequest, UpdateDataTriwulanRequest } from './data-triwulan.dto';
export declare class DataTriwulanService {
    private dataTriwulanRepository;
    private request;
    constructor(dataTriwulanRepository: Repository<DataTriwulan>, request: Request & {
        user: PayloadClientResponse;
        is_admin_bidang: boolean;
    });
    create(data: CreateDataTriwulanRequest, file?: Express.Multer.File): Promise<DataTriwulan>;
    detail(id: number): Promise<DataTriwulan>;
    update(id: number, data: UpdateDataTriwulanRequest, file?: Express.Multer.File): Promise<DataTriwulan>;
    delete(id: number): Promise<void>;
    list(query: ListDataTriwulanRequest): Promise<DataWithPaginationApiResponse<Array<DataTriwulan>>>;
    history(query: ListHistoryDataTriwulanRequest): Promise<DataWithPaginationApiResponse<DataTriwulan[]>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<DataTriwulan>;
}
