"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dataTriwulanProviders = void 0;
const data_triwulan_entity_1 = require("../../database/models/data-triwulan.entity");
exports.dataTriwulanProviders = [
    {
        provide: 'DATA_TRIWULAN_REPOSITORY',
        useFactory: (ds) => ds.getRepository(data_triwulan_entity_1.DataTriwulan),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=data-triwulan.providers.js.map