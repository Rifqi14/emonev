import { Activity } from 'src/database/models/activity.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { CreateActivityRequest, UpdateActivityRequest } from './activity.dto';
import { ListBaseRequest } from 'src/utils/helper';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
export declare class ActivityService {
    private repository;
    constructor(repository: Repository<Activity>);
    create(data: CreateActivityRequest): Promise<Activity>;
    detail(id: number): Promise<Activity>;
    update(data: UpdateActivityRequest): Promise<Activity>;
    delete(activity_id: number): Promise<void>;
    list(query: ListBaseRequest): Promise<DataWithPaginationApiResponse<Array<Activity>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<Activity>;
}
