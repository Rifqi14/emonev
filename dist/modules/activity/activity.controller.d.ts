import { ActivityService } from './activity.service';
import { CreateActivityRequest, CreateActivityResponse, DeleteActivityRequest, DeleteActivityResponse, ListActivityResponse, UpdateActivityRequest, UpdateActivityResponse } from './activity.dto';
import { ListBaseRequest } from 'src/utils/helper';
export declare class ActivityController {
    private readonly activityService;
    constructor(activityService: ActivityService);
    detail(id: number): Promise<CreateActivityResponse>;
    list(query: ListBaseRequest): Promise<ListActivityResponse>;
    create(data: CreateActivityRequest): Promise<CreateActivityResponse>;
    update(data: UpdateActivityRequest): Promise<UpdateActivityResponse>;
    delete(data: DeleteActivityRequest): Promise<DeleteActivityResponse>;
}
