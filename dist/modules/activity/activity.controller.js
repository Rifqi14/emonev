"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivityController = void 0;
const common_1 = require("@nestjs/common");
const activity_service_1 = require("./activity.service");
const swagger_1 = require("@nestjs/swagger");
const openapi_1 = require("../../common/openapi");
const activity_dto_1 = require("./activity.dto");
const helper_1 = require("../../utils/helper");
let ActivityController = class ActivityController {
    constructor(activityService) {
        this.activityService = activityService;
    }
    async detail(id) {
        return {
            statusCode: 200,
            message: `Pengambilan data berhasil`,
            data: await this.activityService.detail(id),
        };
    }
    async list(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.activityService.list(query),
        };
    }
    async create(data) {
        return {
            statusCode: 200,
            message: `Pembuatan data berhasil`,
            data: await this.activityService.create(data),
        };
    }
    async update(data) {
        return {
            statusCode: 200,
            message: 'Pengkinian data berhasil',
            data: await this.activityService.update(data),
        };
    }
    async delete(data) {
        await this.activityService.delete(data.activity_id);
        return {
            statusCode: 200,
            message: 'Penghapusan data berhasil',
        };
    }
};
__decorate([
    (0, openapi_1.SwaggerOkResponse)(activity_dto_1.CreateActivityResponse),
    (0, common_1.Get)('detail/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "detail", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(activity_dto_1.ListActivityResponse),
    (0, common_1.Get)('list'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [helper_1.ListBaseRequest]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "list", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(activity_dto_1.CreateActivityResponse),
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [activity_dto_1.CreateActivityRequest]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "create", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(activity_dto_1.UpdateActivityResponse),
    (0, common_1.Patch)('update'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [activity_dto_1.UpdateActivityRequest]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "update", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(activity_dto_1.DeleteActivityResponse),
    (0, common_1.Patch)('delete'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [activity_dto_1.DeleteActivityRequest]),
    __metadata("design:returntype", Promise)
], ActivityController.prototype, "delete", null);
ActivityController = __decorate([
    (0, swagger_1.ApiTags)('Activity'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('activity'),
    __metadata("design:paramtypes", [activity_service_1.ActivityService])
], ActivityController);
exports.ActivityController = ActivityController;
//# sourceMappingURL=activity.controller.js.map