"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListActivityResponse = exports.DeleteActivityResponse = exports.DeleteActivityRequest = exports.UpdateActivityResponse = exports.UpdateActivityRequest = exports.CreateActivityResponse = exports.CreateActivityRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const api_response_1 = require("../../common/api-response");
const activity_entity_1 = require("../../database/models/activity.entity");
class CreateActivityRequest {
}
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: 'Judul harus diisi.' }),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateActivityRequest.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CreateActivityRequest.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateActivityRequest.prototype, "program_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CreateActivityRequest.prototype, "sub_activity", void 0);
exports.CreateActivityRequest = CreateActivityRequest;
class CreateActivityResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", activity_entity_1.Activity)
], CreateActivityResponse.prototype, "data", void 0);
exports.CreateActivityResponse = CreateActivityResponse;
class UpdateActivityRequest extends (0, swagger_1.PartialType)(CreateActivityRequest) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateActivityRequest.prototype, "activity_id", void 0);
exports.UpdateActivityRequest = UpdateActivityRequest;
class UpdateActivityResponse extends CreateActivityResponse {
}
exports.UpdateActivityResponse = UpdateActivityResponse;
class DeleteActivityRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeleteActivityRequest.prototype, "activity_id", void 0);
exports.DeleteActivityRequest = DeleteActivityRequest;
class DeleteActivityResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteActivityResponse = DeleteActivityResponse;
class ListActivityResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListActivityResponse.prototype, "data", void 0);
exports.ListActivityResponse = ListActivityResponse;
//# sourceMappingURL=activity.dto.js.map