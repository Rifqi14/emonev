import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { Activity } from 'src/database/models/activity.entity';
export declare class CreateActivityRequest {
    title: string;
    code?: string;
    program_id?: number;
    sub_activity?: string;
}
export declare class CreateActivityResponse extends BaseApiResponse {
    data: Activity;
}
declare const UpdateActivityRequest_base: import("@nestjs/common").Type<Partial<CreateActivityRequest>>;
export declare class UpdateActivityRequest extends UpdateActivityRequest_base {
    activity_id: number;
}
export declare class UpdateActivityResponse extends CreateActivityResponse {
}
export declare class DeleteActivityRequest {
    activity_id: number;
}
export declare class DeleteActivityResponse extends BaseApiResponse {
}
export declare class ListActivityResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<Activity>>;
}
export {};
