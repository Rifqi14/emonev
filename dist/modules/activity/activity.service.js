"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivityService = void 0;
const common_1 = require("@nestjs/common");
const activity_entity_1 = require("../../database/models/activity.entity");
const typeorm_1 = require("typeorm");
const helper_1 = require("../../utils/helper");
let ActivityService = class ActivityService {
    constructor(repository) {
        this.repository = repository;
    }
    async create(data) {
        var _a, _b;
        try {
            const { title, program_id, sub_activity } = data;
            const latest = await this.repository.findOne({
                where: {},
                order: { id: 'DESC' },
            });
            const code = (_a = data.code) !== null && _a !== void 0 ? _a : (0, helper_1.generateCode)((_b = latest === null || latest === void 0 ? void 0 : latest.id) !== null && _b !== void 0 ? _b : 1, 'A');
            return await this.repository.manager.transaction(async (trx) => await trx
                .create(activity_entity_1.Activity, {
                title,
                code,
                program_id,
                sub_activity,
            })
                .save());
        }
        catch (error) {
            throw error;
        }
    }
    async detail(id) {
        try {
            return await this.repository.findOneOrFail({
                relations: {
                    program: true,
                },
                where: {
                    id,
                },
            });
        }
        catch (error) {
            throw error;
        }
    }
    async update(data) {
        try {
            const { activity_id, code, program_id, title, sub_activity } = data;
            await this.repository.manager.transaction(async (trx) => trx.update(activity_entity_1.Activity, { id: activity_id }, {
                program_id,
                code,
                title,
                sub_activity,
            }));
            return await this.repository.findOneByOrFail({
                id: activity_id,
            });
        }
        catch (error) {
            throw error;
        }
    }
    async delete(activity_id) {
        try {
            await this.repository.delete({ id: activity_id });
        }
        catch (error) {
            throw error;
        }
    }
    async list(query) {
        try {
            const { search, sort } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const where = [];
            if (search) {
                where.push({
                    code: (0, typeorm_1.ILike)(`%${search}%`),
                });
                where.push({
                    title: (0, typeorm_1.ILike)(`%${search}%`),
                });
                where.push({
                    sub_activity: (0, typeorm_1.ILike)(`%${search}%`),
                });
                where.push({
                    program: {
                        title: (0, typeorm_1.ILike)(`%${search}%`),
                    },
                });
            }
            const [activities, count] = await this.repository.findAndCount({
                where: where,
                relations: {
                    program: true,
                },
                order: this.getOrderQuery(sort),
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: activities,
            };
        }
        catch (error) {
            throw error;
        }
    }
    getOrderQuery(sort) {
        const sortRes = {
            id: 'DESC',
        };
        switch (sort) {
            case 'terbaru':
                Object.assign(sortRes, {
                    id: 'DESC',
                });
                break;
            case 'terlama':
                Object.assign(sortRes, {
                    id: 'ASC',
                });
                break;
            case 'a-z':
                Object.assign(sortRes, {
                    title: 'ASC',
                });
                break;
            case 'z-a':
                Object.assign(sortRes, {
                    title: 'DESC',
                });
                break;
        }
        return sortRes;
    }
};
ActivityService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('ACTIVITY_REPOSITORY')),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], ActivityService);
exports.ActivityService = ActivityService;
//# sourceMappingURL=activity.service.js.map