"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModulesModule = void 0;
const common_1 = require("@nestjs/common");
const user_module_1 = require("./user/user.module");
const authentication_module_1 = require("./authentication/authentication.module");
const organization_module_1 = require("./organization/organization.module");
const purpose_module_1 = require("./purpose/purpose.module");
const program_module_1 = require("./program/program.module");
const occassion_module_1 = require("./occassion/occassion.module");
const activity_module_1 = require("./activity/activity.module");
const report_module_1 = require("./report/report.module");
const master_module_1 = require("./master/master.module");
const mail_module_1 = require("./mail/mail.module");
const fund_source_module_1 = require("./fund-source/fund-source.module");
const procurement_type_module_1 = require("./procurement-type/procurement-type.module");
const procurement_method_module_1 = require("./procurement-method/procurement-method.module");
const data_triwulan_module_1 = require("./data-triwulan/data-triwulan.module");
const setting_module_1 = require("./setting/setting.module");
const dashboard_module_1 = require("./dashboard/dashboard.module");
let ModulesModule = class ModulesModule {
};
ModulesModule = __decorate([
    (0, common_1.Module)({
        imports: [user_module_1.UserModule, authentication_module_1.AuthenticationModule, organization_module_1.OrganizationModule, purpose_module_1.PurposeModule, program_module_1.ProgramModule, occassion_module_1.OccassionModule, activity_module_1.ActivityModule, report_module_1.ReportModule, master_module_1.MasterModule, mail_module_1.MailModule, fund_source_module_1.FundSourceModule, procurement_type_module_1.ProcurementTypeModule, procurement_method_module_1.ProcurementMethodModule, data_triwulan_module_1.DataTriwulanModule, setting_module_1.SettingModule, dashboard_module_1.DashboardModule],
    })
], ModulesModule);
exports.ModulesModule = ModulesModule;
//# sourceMappingURL=modules.module.js.map