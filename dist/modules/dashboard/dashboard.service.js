"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DashboardService = void 0;
const common_1 = require("@nestjs/common");
const dayjs_1 = __importDefault(require("dayjs"));
const excel_service_1 = require("../../common/excel.service");
const activity_entity_1 = require("../../database/models/activity.entity");
const occassion_entity_1 = require("../../database/models/occassion.entity");
const organization_entity_1 = require("../../database/models/organization.entity");
const program_entity_1 = require("../../database/models/program.entity");
const purpose_entity_1 = require("../../database/models/purpose.entity");
const user_entity_1 = require("../../database/models/user.entity");
const typeorm_1 = require("typeorm");
const dashboard_dto_1 = require("./dashboard.dto");
const fund_sources_entity_1 = require("../../database/models/fund_sources.entity");
const data_triwulan_entity_1 = require("../../database/models/data-triwulan.entity");
let DashboardService = class DashboardService {
    constructor(ds, excelService) {
        this.ds = ds;
        this.excelService = excelService;
    }
    async getOccasion() {
        try {
            return (await this.ds
                .getRepository(occassion_entity_1.Occassion)
                .find({ relations: { status: true } })).map((occasion, idx) => {
                var _a, _b;
                return ({
                    no: idx + 1,
                    kode: occasion.code,
                    nama_urusan: occasion.title,
                    status: (_b = (_a = occasion.status) === null || _a === void 0 ? void 0 : _a.name) !== null && _b !== void 0 ? _b : '',
                });
            });
        }
        catch (error) {
            throw error;
        }
    }
    async getOrganization() {
        try {
            return (await this.ds
                .getRepository(organization_entity_1.Organization)
                .find({ relations: { status: true } })).map((organization, idx) => {
                var _a, _b;
                return ({
                    no: idx + 1,
                    kode: organization.code,
                    nama_organisasi: organization.title,
                    status: (_b = (_a = organization.status) === null || _a === void 0 ? void 0 : _a.name) !== null && _b !== void 0 ? _b : '',
                });
            });
        }
        catch (error) {
            throw error;
        }
    }
    async getProgram() {
        try {
            return (await this.ds
                .getRepository(program_entity_1.Program)
                .find({ relations: { status: true, occassion: true } })).map((program, idx) => {
                var _a, _b, _c, _d;
                return ({
                    no: idx + 1,
                    kode: program.code,
                    nama_program: program.title,
                    urusan: (_b = (_a = program.occassion) === null || _a === void 0 ? void 0 : _a.title) !== null && _b !== void 0 ? _b : '',
                    status: (_d = (_c = program.status) === null || _c === void 0 ? void 0 : _c.name) !== null && _d !== void 0 ? _d : '',
                });
            });
        }
        catch (error) {
            throw error;
        }
    }
    async getActivity() {
        try {
            return (await this.ds
                .getRepository(activity_entity_1.Activity)
                .find({ relations: { program: true } })).map((activity, idx) => {
                var _a, _b;
                return ({
                    no: idx + 1,
                    kode: activity.code,
                    nama_aktifitas: activity.title,
                    sub_aktifitas: activity.sub_activity,
                    program: (_b = (_a = activity.program) === null || _a === void 0 ? void 0 : _a.title) !== null && _b !== void 0 ? _b : '',
                });
            });
        }
        catch (error) {
            throw error;
        }
    }
    async getPurpose() {
        try {
            return (await this.ds.getRepository(purpose_entity_1.Purpose).find()).map((activity, idx) => ({
                no: idx + 1,
                nama_sasaran: activity.title,
            }));
        }
        catch (error) {
            throw error;
        }
    }
    async getUser() {
        try {
            return (await this.ds.getRepository(user_entity_1.User).find({
                relations: {
                    organization: true,
                    role: true,
                    status: true,
                    userOrganization: {
                        organization: true,
                    },
                },
            })).map((user, idx) => {
                var _a, _b, _c, _d;
                return ({
                    no: idx + 1,
                    username: user.username,
                    email: user.email,
                    nama: user.name,
                    organisasi: user.userOrganization.length > 0
                        ? user.userOrganization
                            .map((userOrganization) => userOrganization.organization.title)
                            .join(', ')
                        : (_b = (_a = user.organization) === null || _a === void 0 ? void 0 : _a.title) !== null && _b !== void 0 ? _b : '',
                    level_user: user.role.name,
                    status: (_d = (_c = user.status) === null || _c === void 0 ? void 0 : _c.name) !== null && _d !== void 0 ? _d : '',
                    tanggal_dibuat: (0, dayjs_1.default)(user.created_at).format('DD/MM/YYYY'),
                });
            });
        }
        catch (error) {
            throw error;
        }
    }
    async downloadAdmin() {
        try {
            const data = {
                urusan: await this.getOccasion(),
                organisasi: await this.getOrganization(),
                program: await this.getProgram(),
                kegiatan: await this.getActivity(),
                sasaran: await this.getPurpose(),
                user: await this.getUser(),
            };
            const path = await this.excelService.downloadMultipleSheets({
                data,
                name: 'dashboard',
                sheet: Object.keys(data),
            });
            return path;
        }
        catch (error) {
            throw error;
        }
    }
    async chartPaguDana(req) {
        try {
            const pagu_dana = await this.ds
                .getRepository(fund_sources_entity_1.FundSources)
                .findOneOrFail({ where: { id: req.pagu_dana_id } });
            const data_triwulan = await this.ds
                .getRepository(data_triwulan_entity_1.DataTriwulan)
                .find({ where: { fund_source_id: req.pagu_dana_id } });
            return (0, dashboard_dto_1.toChart)(pagu_dana, data_triwulan);
        }
        catch (error) {
            throw error;
        }
    }
};
DashboardService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('DATA_SOURCE')),
    __metadata("design:paramtypes", [typeorm_1.DataSource,
        excel_service_1.ExcelService])
], DashboardService);
exports.DashboardService = DashboardService;
//# sourceMappingURL=dashboard.service.js.map