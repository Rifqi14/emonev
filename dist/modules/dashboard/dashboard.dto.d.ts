import { BaseApiResponse } from 'src/common/api-response';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { FundSources } from 'src/database/models/fund_sources.entity';
export declare class PaguDana {
    pagu_dana_id: number;
    name: string;
    total_pagu_dana: number;
    total_pagu_dana_digunakan: number;
}
export declare class Triwulan {
    data_triwulan_id: number;
    nama_aktifitas: string;
    pagu_dana: number;
    persentase_pagu_dana: number;
    realisasi_fisik: number;
    persentase_pencapaian: number;
}
export declare class GetPaguDanaChartRequest {
    pagu_dana_id: number;
}
export declare class PaguDanaChartResponse {
    pagu_dana: PaguDana;
    triwulan: Triwulan[];
}
export declare class FundSourceResponse extends BaseApiResponse {
    data: PaguDanaChartResponse;
}
export declare const toChart: (fundSource: FundSources, triwulan: DataTriwulan[]) => PaguDanaChartResponse;
export declare const toTriwulan: (triwulan: DataTriwulan, pagu_dana: number) => Triwulan;
