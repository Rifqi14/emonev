import { ExcelService } from 'src/common/excel.service';
import { DataSource } from 'typeorm';
import { GetPaguDanaChartRequest, PaguDanaChartResponse } from './dashboard.dto';
export declare class DashboardService {
    private ds;
    private excelService;
    constructor(ds: DataSource, excelService: ExcelService);
    getOccasion(): Promise<{
        no: number;
        kode: string;
        nama_urusan: string;
        status: string;
    }[]>;
    getOrganization(): Promise<{
        no: number;
        kode: string;
        nama_organisasi: string;
        status: string;
    }[]>;
    getProgram(): Promise<{
        no: number;
        kode: string;
        nama_program: string;
        urusan: string;
        status: string;
    }[]>;
    getActivity(): Promise<{
        no: number;
        kode: string;
        nama_aktifitas: string;
        sub_aktifitas: string | undefined;
        program: string;
    }[]>;
    getPurpose(): Promise<{
        no: number;
        nama_sasaran: string;
    }[]>;
    getUser(): Promise<{
        no: number;
        username: string;
        email: string | undefined;
        nama: string | undefined;
        organisasi: string;
        level_user: string;
        status: string;
        tanggal_dibuat: string;
    }[]>;
    downloadAdmin(): Promise<string>;
    chartPaguDana(req: GetPaguDanaChartRequest): Promise<PaguDanaChartResponse>;
}
