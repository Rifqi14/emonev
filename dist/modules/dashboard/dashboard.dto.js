"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.toTriwulan = exports.toChart = exports.FundSourceResponse = exports.PaguDanaChartResponse = exports.GetPaguDanaChartRequest = exports.Triwulan = exports.PaguDana = void 0;
const swagger_1 = require("@nestjs/swagger");
const api_response_1 = require("../../common/api-response");
class PaguDana {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], PaguDana.prototype, "pagu_dana_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PaguDana.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], PaguDana.prototype, "total_pagu_dana", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], PaguDana.prototype, "total_pagu_dana_digunakan", void 0);
exports.PaguDana = PaguDana;
class Triwulan {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], Triwulan.prototype, "data_triwulan_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], Triwulan.prototype, "nama_aktifitas", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], Triwulan.prototype, "pagu_dana", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], Triwulan.prototype, "persentase_pagu_dana", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], Triwulan.prototype, "realisasi_fisik", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], Triwulan.prototype, "persentase_pencapaian", void 0);
exports.Triwulan = Triwulan;
class GetPaguDanaChartRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], GetPaguDanaChartRequest.prototype, "pagu_dana_id", void 0);
exports.GetPaguDanaChartRequest = GetPaguDanaChartRequest;
class PaguDanaChartResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", PaguDana)
], PaguDanaChartResponse.prototype, "pagu_dana", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], PaguDanaChartResponse.prototype, "triwulan", void 0);
exports.PaguDanaChartResponse = PaguDanaChartResponse;
class FundSourceResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: PaguDanaChartResponse }),
    __metadata("design:type", PaguDanaChartResponse)
], FundSourceResponse.prototype, "data", void 0);
exports.FundSourceResponse = FundSourceResponse;
const toChart = (fundSource, triwulan) => {
    var _a, _b, _c;
    const total_pagu_dana_digunakan = triwulan
        .map((v) => v.fund_ceiling)
        .reduce((acc, curr) => +acc + +curr, 0);
    return {
        pagu_dana: {
            pagu_dana_id: (_a = fundSource.id) !== null && _a !== void 0 ? _a : 0,
            name: (_b = fundSource.name) !== null && _b !== void 0 ? _b : '',
            total_pagu_dana: Math.abs((_c = fundSource.fund_source_total) !== null && _c !== void 0 ? _c : 0),
            total_pagu_dana_digunakan,
        },
        triwulan: triwulan.map((v) => { var _a; return (0, exports.toTriwulan)(v, (_a = fundSource.fund_source_total) !== null && _a !== void 0 ? _a : 0); }),
    };
};
exports.toChart = toChart;
const toTriwulan = (triwulan, pagu_dana) => {
    var _a, _b, _c, _d, _e;
    const triwulan_pagu_dana = (_a = +triwulan.fund_ceiling) !== null && _a !== void 0 ? _a : 0;
    const realisasi_fisik = (_b = +triwulan.physical_realization) !== null && _b !== void 0 ? _b : 0;
    const persentase_pagu_dana = (+triwulan_pagu_dana / +pagu_dana) * 100;
    const persentase_pencapaian = (+realisasi_fisik / +triwulan_pagu_dana) * 100;
    return {
        data_triwulan_id: (_c = triwulan.id) !== null && _c !== void 0 ? _c : 0,
        nama_aktifitas: (_d = triwulan.activity_name) !== null && _d !== void 0 ? _d : '',
        pagu_dana: (_e = +triwulan.fund_ceiling) !== null && _e !== void 0 ? _e : 0,
        persentase_pagu_dana: 100 - Math.abs(persentase_pagu_dana !== null && persentase_pagu_dana !== void 0 ? persentase_pagu_dana : 100),
        realisasi_fisik,
        persentase_pencapaian: 100 - Math.abs(persentase_pencapaian),
    };
};
exports.toTriwulan = toTriwulan;
//# sourceMappingURL=dashboard.dto.js.map