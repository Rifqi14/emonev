"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DashboardController = void 0;
const common_1 = require("@nestjs/common");
const dashboard_service_1 = require("./dashboard.service");
const fs_1 = require("fs");
const swagger_1 = require("@nestjs/swagger");
const dashboard_dto_1 = require("./dashboard.dto");
let DashboardController = class DashboardController {
    constructor(dashboardService) {
        this.dashboardService = dashboardService;
    }
    async downloadExcelDashboard(res) {
        const path = await this.dashboardService.downloadAdmin();
        res.download(path, (err) => {
            if (!err) {
                (0, fs_1.unlink)(path, (err) => {
                    if (err)
                        throw err;
                });
            }
        });
    }
    async chartPaguDana(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.dashboardService.chartPaguDana(query),
        };
    }
};
__decorate([
    (0, common_1.Header)('Content-Type', 'text/xlsx'),
    (0, common_1.Get)('excel'),
    __param(0, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DashboardController.prototype, "downloadExcelDashboard", null);
__decorate([
    (0, common_1.Get)('chart/pagu-dana'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dashboard_dto_1.GetPaguDanaChartRequest]),
    __metadata("design:returntype", Promise)
], DashboardController.prototype, "chartPaguDana", null);
DashboardController = __decorate([
    (0, swagger_1.ApiTags)('Dashboard'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('dashboard'),
    __metadata("design:paramtypes", [dashboard_service_1.DashboardService])
], DashboardController);
exports.DashboardController = DashboardController;
//# sourceMappingURL=dashboard.controller.js.map