import { DashboardService } from './dashboard.service';
import { Response } from 'express';
import { FundSourceResponse, GetPaguDanaChartRequest } from './dashboard.dto';
export declare class DashboardController {
    private readonly dashboardService;
    constructor(dashboardService: DashboardService);
    downloadExcelDashboard(res: Response): Promise<void>;
    chartPaguDana(query: GetPaguDanaChartRequest): Promise<FundSourceResponse>;
}
