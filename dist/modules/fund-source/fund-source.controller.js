"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FundSourceController = void 0;
const common_1 = require("@nestjs/common");
const fund_source_service_1 = require("./fund-source.service");
const swagger_1 = require("@nestjs/swagger");
const openapi_1 = require("../../common/openapi");
const fund_source_dto_1 = require("./fund-source.dto");
let FundSourceController = class FundSourceController {
    constructor(fundSourceService) {
        this.fundSourceService = fundSourceService;
    }
    async list(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.fundSourceService.list(query),
        };
    }
    async detail(id) {
        return {
            statusCode: 200,
            message: `Pengambilan data berhasil`,
            data: await this.fundSourceService.detail(id),
        };
    }
    async create(data) {
        return {
            statusCode: 200,
            message: `Pembuatan data berhasil`,
            data: await this.fundSourceService.create(data),
        };
    }
    async update(id, data) {
        return {
            statusCode: 200,
            message: 'Pengkinian data berhasil',
            data: await this.fundSourceService.update(id, data),
        };
    }
    async delete(id) {
        await this.fundSourceService.delete(id);
        return {
            statusCode: 200,
            message: 'Penghapusan data berhasil',
        };
    }
};
__decorate([
    (0, openapi_1.SwaggerOkResponse)(fund_source_dto_1.ListFundSourceResponse),
    (0, common_1.Get)('list'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [fund_source_dto_1.ListFundSourceRequest]),
    __metadata("design:returntype", Promise)
], FundSourceController.prototype, "list", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(fund_source_dto_1.CreateFundSourceResponse),
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], FundSourceController.prototype, "detail", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(fund_source_dto_1.CreateFundSourceResponse),
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [fund_source_dto_1.CreateFundSourceRequest]),
    __metadata("design:returntype", Promise)
], FundSourceController.prototype, "create", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(fund_source_dto_1.UpdateFundSourceResponse),
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, fund_source_dto_1.UpdateFundSourceRequest]),
    __metadata("design:returntype", Promise)
], FundSourceController.prototype, "update", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(fund_source_dto_1.DeleteFundSourceResponse),
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], FundSourceController.prototype, "delete", null);
FundSourceController = __decorate([
    (0, swagger_1.ApiTags)('User | Sumber Dana'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('fund-source'),
    __metadata("design:paramtypes", [fund_source_service_1.FundSourceService])
], FundSourceController);
exports.FundSourceController = FundSourceController;
//# sourceMappingURL=fund-source.controller.js.map