"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListFundSourceResponse = exports.ListFundSourceRequest = exports.DeleteFundSourceResponse = exports.UpdateFundSourceResponse = exports.UpdateFundSourceRequest = exports.CreateFundSourceResponse = exports.CreateFundSourceRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const api_response_1 = require("../../common/api-response");
const fund_sources_entity_1 = require("../../database/models/fund_sources.entity");
const helper_1 = require("../../utils/helper");
class CreateFundSourceRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateFundSourceRequest.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ description: 'Total Sumber Data' }),
    __metadata("design:type", Number)
], CreateFundSourceRequest.prototype, "fund_source_total", void 0);
exports.CreateFundSourceRequest = CreateFundSourceRequest;
class CreateFundSourceResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", fund_sources_entity_1.FundSources)
], CreateFundSourceResponse.prototype, "data", void 0);
exports.CreateFundSourceResponse = CreateFundSourceResponse;
class UpdateFundSourceRequest extends (0, swagger_1.PartialType)(CreateFundSourceRequest) {
}
exports.UpdateFundSourceRequest = UpdateFundSourceRequest;
class UpdateFundSourceResponse extends CreateFundSourceResponse {
}
exports.UpdateFundSourceResponse = UpdateFundSourceResponse;
class DeleteFundSourceResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteFundSourceResponse = DeleteFundSourceResponse;
class ListFundSourceRequest extends helper_1.ListBaseRequest {
}
exports.ListFundSourceRequest = ListFundSourceRequest;
class ListFundSourceResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListFundSourceResponse.prototype, "data", void 0);
exports.ListFundSourceResponse = ListFundSourceResponse;
//# sourceMappingURL=fund-source.dto.js.map