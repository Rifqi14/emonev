"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.fundSourceProviders = void 0;
const fund_sources_entity_1 = require("../../database/models/fund_sources.entity");
exports.fundSourceProviders = [
    {
        provide: 'FUND_SOURCE_REPOSITORY',
        useFactory: (ds) => ds.getRepository(fund_sources_entity_1.FundSources),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=fund-source.providers.js.map