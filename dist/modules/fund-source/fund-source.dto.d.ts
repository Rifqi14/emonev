import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { FundSources } from 'src/database/models/fund_sources.entity';
import { ListBaseRequest } from 'src/utils/helper';
export declare class CreateFundSourceRequest {
    name: string;
    fund_source_total?: number;
}
export declare class CreateFundSourceResponse extends BaseApiResponse {
    data: FundSources;
}
declare const UpdateFundSourceRequest_base: import("@nestjs/common").Type<Partial<CreateFundSourceRequest>>;
export declare class UpdateFundSourceRequest extends UpdateFundSourceRequest_base {
}
export declare class UpdateFundSourceResponse extends CreateFundSourceResponse {
}
export declare class DeleteFundSourceResponse extends BaseApiResponse {
}
export declare class ListFundSourceRequest extends ListBaseRequest {
}
export declare class ListFundSourceResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<FundSources[]>;
}
export {};
