import { FundSourceService } from './fund-source.service';
import { CreateFundSourceRequest, CreateFundSourceResponse, DeleteFundSourceResponse, ListFundSourceRequest, ListFundSourceResponse, UpdateFundSourceRequest, UpdateFundSourceResponse } from './fund-source.dto';
export declare class FundSourceController {
    private readonly fundSourceService;
    constructor(fundSourceService: FundSourceService);
    list(query: ListFundSourceRequest): Promise<ListFundSourceResponse>;
    detail(id: number): Promise<CreateFundSourceResponse>;
    create(data: CreateFundSourceRequest): Promise<CreateFundSourceResponse>;
    update(id: number, data: UpdateFundSourceRequest): Promise<UpdateFundSourceResponse>;
    delete(id: number): Promise<DeleteFundSourceResponse>;
}
