import { FundSources } from 'src/database/models/fund_sources.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { CreateFundSourceRequest, ListFundSourceRequest, UpdateFundSourceRequest } from './fund-source.dto';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
export declare class FundSourceService {
    private repository;
    constructor(repository: Repository<FundSources>);
    create(data: CreateFundSourceRequest): Promise<FundSources>;
    update(id: number, data: UpdateFundSourceRequest): Promise<FundSources>;
    delete(id: number): Promise<void>;
    detail(id: number): Promise<FundSources>;
    list(query: ListFundSourceRequest): Promise<DataWithPaginationApiResponse<Array<FundSources>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<FundSources>;
}
