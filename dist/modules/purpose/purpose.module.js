"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurposeModule = void 0;
const common_1 = require("@nestjs/common");
const purpose_service_1 = require("./purpose.service");
const purpose_controller_1 = require("./purpose.controller");
const purpose_providers_1 = require("./purpose.providers");
const database_module_1 = require("../../database/database.module");
let PurposeModule = class PurposeModule {
};
PurposeModule = __decorate([
    (0, common_1.Module)({
        imports: [database_module_1.DatabaseModule],
        controllers: [purpose_controller_1.PurposeController],
        providers: [purpose_service_1.PurposeService, ...purpose_providers_1.purposeProviders],
        exports: [purpose_service_1.PurposeService],
    })
], PurposeModule);
exports.PurposeModule = PurposeModule;
//# sourceMappingURL=purpose.module.js.map