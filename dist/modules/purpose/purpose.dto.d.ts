import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { Purpose } from 'src/database/models/purpose.entity';
export declare class CreatePurposeRequest {
    title: string;
}
export declare class CreatePurposeResponse extends BaseApiResponse {
    data: Purpose;
}
declare const UpdatePurposeRequest_base: import("@nestjs/common").Type<Partial<CreatePurposeRequest>>;
export declare class UpdatePurposeRequest extends UpdatePurposeRequest_base {
    purpose_id: number;
}
export declare class UpdatePurposeResponse extends CreatePurposeResponse {
}
export declare class DeletePurposeRequest {
    purpose_id: number;
}
export declare class DeletePurposeResponse extends BaseApiResponse {
}
export declare class ListPurposeResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<Purpose>>;
}
export {};
