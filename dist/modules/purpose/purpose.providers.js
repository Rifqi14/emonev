"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.purposeProviders = void 0;
const purpose_entity_1 = require("../../database/models/purpose.entity");
exports.purposeProviders = [
    {
        provide: 'PURPOSE_REPOSITORY',
        useFactory: (ds) => ds.getRepository(purpose_entity_1.Purpose),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=purpose.providers.js.map