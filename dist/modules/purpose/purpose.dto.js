"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListPurposeResponse = exports.DeletePurposeResponse = exports.DeletePurposeRequest = exports.UpdatePurposeResponse = exports.UpdatePurposeRequest = exports.CreatePurposeResponse = exports.CreatePurposeRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const api_response_1 = require("../../common/api-response");
const purpose_entity_1 = require("../../database/models/purpose.entity");
class CreatePurposeRequest {
}
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: 'Judul harus diisi.' }),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatePurposeRequest.prototype, "title", void 0);
exports.CreatePurposeRequest = CreatePurposeRequest;
class CreatePurposeResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", purpose_entity_1.Purpose)
], CreatePurposeResponse.prototype, "data", void 0);
exports.CreatePurposeResponse = CreatePurposeResponse;
class UpdatePurposeRequest extends (0, swagger_1.PartialType)(CreatePurposeRequest) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdatePurposeRequest.prototype, "purpose_id", void 0);
exports.UpdatePurposeRequest = UpdatePurposeRequest;
class UpdatePurposeResponse extends CreatePurposeResponse {
}
exports.UpdatePurposeResponse = UpdatePurposeResponse;
class DeletePurposeRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeletePurposeRequest.prototype, "purpose_id", void 0);
exports.DeletePurposeRequest = DeletePurposeRequest;
class DeletePurposeResponse extends api_response_1.BaseApiResponse {
}
exports.DeletePurposeResponse = DeletePurposeResponse;
class ListPurposeResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListPurposeResponse.prototype, "data", void 0);
exports.ListPurposeResponse = ListPurposeResponse;
//# sourceMappingURL=purpose.dto.js.map