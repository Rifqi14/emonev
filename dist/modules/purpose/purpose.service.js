"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurposeService = void 0;
const common_1 = require("@nestjs/common");
const purpose_entity_1 = require("../../database/models/purpose.entity");
const typeorm_1 = require("typeorm");
let PurposeService = class PurposeService {
    constructor(repository) {
        this.repository = repository;
    }
    async create(data) {
        try {
            const { title } = data;
            return await this.repository.manager.transaction(async (trx) => await trx
                .create(purpose_entity_1.Purpose, {
                title,
            })
                .save());
        }
        catch (error) {
            throw error;
        }
    }
    async detail(id) {
        try {
            return await this.repository.findOneOrFail({
                where: {
                    id,
                },
            });
        }
        catch (error) {
            throw error;
        }
    }
    async update(data) {
        try {
            const { purpose_id, title } = data;
            await this.repository.manager.transaction(async (trx) => trx.update(purpose_entity_1.Purpose, { id: purpose_id }, {
                title,
            }));
            return await this.repository.findOneByOrFail({
                id: purpose_id,
            });
        }
        catch (error) {
            throw error;
        }
    }
    async delete(purpose_id) {
        try {
            await this.repository.delete({ id: purpose_id });
        }
        catch (error) {
            throw error;
        }
    }
    async list(query) {
        try {
            const { search, sort } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const where = [];
            if (search) {
                where.push({
                    title: (0, typeorm_1.ILike)(`%${search}%`),
                });
            }
            const [purposes, count] = await this.repository.findAndCount({
                where: where,
                order: this.getOrderQuery(sort),
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: purposes,
            };
        }
        catch (error) {
            throw error;
        }
    }
    getOrderQuery(sort) {
        const sortRes = {
            id: 'DESC',
        };
        switch (sort) {
            case 'terbaru':
                Object.assign(sortRes, {
                    id: 'DESC',
                });
                break;
            case 'terlama':
                Object.assign(sortRes, {
                    id: 'ASC',
                });
                break;
            case 'a-z':
                Object.assign(sortRes, {
                    title: 'ASC',
                });
                break;
            case 'z-a':
                Object.assign(sortRes, {
                    title: 'DESC',
                });
                break;
        }
        return sortRes;
    }
};
PurposeService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('PURPOSE_REPOSITORY')),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], PurposeService);
exports.PurposeService = PurposeService;
//# sourceMappingURL=purpose.service.js.map