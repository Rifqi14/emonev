"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurposeController = void 0;
const common_1 = require("@nestjs/common");
const purpose_service_1 = require("./purpose.service");
const swagger_1 = require("@nestjs/swagger");
const openapi_1 = require("../../common/openapi");
const purpose_dto_1 = require("./purpose.dto");
const helper_1 = require("../../utils/helper");
let PurposeController = class PurposeController {
    constructor(purposeService) {
        this.purposeService = purposeService;
    }
    async detail(id) {
        return {
            statusCode: 200,
            message: `Pengambilan data berhasil`,
            data: await this.purposeService.detail(id),
        };
    }
    async list(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.purposeService.list(query),
        };
    }
    async create(data) {
        return {
            statusCode: 200,
            message: `Pembuatan data berhasil`,
            data: await this.purposeService.create(data),
        };
    }
    async update(data) {
        return {
            statusCode: 200,
            message: 'Pengkinian data berhasil',
            data: await this.purposeService.update(data),
        };
    }
    async delete(data) {
        await this.purposeService.delete(data.purpose_id);
        return {
            statusCode: 200,
            message: 'Penghapusan data berhasil',
        };
    }
};
__decorate([
    (0, openapi_1.SwaggerOkResponse)(purpose_dto_1.CreatePurposeResponse),
    (0, common_1.Get)('detail/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], PurposeController.prototype, "detail", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(purpose_dto_1.ListPurposeResponse),
    (0, common_1.Get)('list'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [helper_1.ListBaseRequest]),
    __metadata("design:returntype", Promise)
], PurposeController.prototype, "list", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(purpose_dto_1.CreatePurposeResponse),
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [purpose_dto_1.CreatePurposeRequest]),
    __metadata("design:returntype", Promise)
], PurposeController.prototype, "create", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(purpose_dto_1.UpdatePurposeResponse),
    (0, common_1.Patch)('update'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [purpose_dto_1.UpdatePurposeRequest]),
    __metadata("design:returntype", Promise)
], PurposeController.prototype, "update", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(purpose_dto_1.DeletePurposeResponse),
    (0, common_1.Patch)('delete'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [purpose_dto_1.DeletePurposeRequest]),
    __metadata("design:returntype", Promise)
], PurposeController.prototype, "delete", null);
PurposeController = __decorate([
    (0, swagger_1.ApiTags)('Purpose'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('purpose'),
    __metadata("design:paramtypes", [purpose_service_1.PurposeService])
], PurposeController);
exports.PurposeController = PurposeController;
//# sourceMappingURL=purpose.controller.js.map