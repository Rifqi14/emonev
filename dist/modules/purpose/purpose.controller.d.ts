import { PurposeService } from './purpose.service';
import { CreatePurposeRequest, CreatePurposeResponse, DeletePurposeRequest, DeletePurposeResponse, ListPurposeResponse, UpdatePurposeRequest, UpdatePurposeResponse } from './purpose.dto';
import { ListBaseRequest } from 'src/utils/helper';
export declare class PurposeController {
    private readonly purposeService;
    constructor(purposeService: PurposeService);
    detail(id: number): Promise<CreatePurposeResponse>;
    list(query: ListBaseRequest): Promise<ListPurposeResponse>;
    create(data: CreatePurposeRequest): Promise<CreatePurposeResponse>;
    update(data: UpdatePurposeRequest): Promise<UpdatePurposeResponse>;
    delete(data: DeletePurposeRequest): Promise<DeletePurposeResponse>;
}
