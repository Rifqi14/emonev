import { Purpose } from 'src/database/models/purpose.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { CreatePurposeRequest, UpdatePurposeRequest } from './purpose.dto';
import { ListBaseRequest } from 'src/utils/helper';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
export declare class PurposeService {
    private repository;
    constructor(repository: Repository<Purpose>);
    create(data: CreatePurposeRequest): Promise<Purpose>;
    detail(id: number): Promise<Purpose>;
    update(data: UpdatePurposeRequest): Promise<Purpose>;
    delete(purpose_id: number): Promise<void>;
    list(query: ListBaseRequest): Promise<DataWithPaginationApiResponse<Array<Purpose>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<Purpose>;
}
