"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.settingProviders = void 0;
const setting_entity_1 = require("../../database/models/setting.entity");
exports.settingProviders = [
    {
        provide: 'SETTING_REPOSITORY',
        useFactory: (ds) => ds.getRepository(setting_entity_1.Setting),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=setting.providers.js.map