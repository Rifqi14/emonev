"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const setting_service_1 = require("./setting.service");
const setting_dto_1 = require("./setting.dto");
let SettingController = class SettingController {
    constructor(settingService) {
        this.settingService = settingService;
    }
    async getSettingTriwulan() {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.settingService.getTriwulanSetting(),
        };
    }
    async updateSettingTriwulan(data) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.settingService.update(data),
        };
    }
};
__decorate([
    (0, common_1.Get)('triwulan'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "getSettingTriwulan", null);
__decorate([
    (0, common_1.Patch)('triwulan'),
    (0, swagger_1.ApiBody)({ type: [setting_dto_1.UpdateSettingRequest] }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "updateSettingTriwulan", null);
SettingController = __decorate([
    (0, swagger_1.ApiTags)('Configuration'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('setting'),
    __metadata("design:paramtypes", [setting_service_1.SettingService])
], SettingController);
exports.SettingController = SettingController;
//# sourceMappingURL=setting.controller.js.map