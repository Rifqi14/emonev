"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListSettingResponse = exports.DeleteSettingResponse = exports.DeleteSettingRequest = exports.UpdateSettingResponse = exports.UpdateSettingRequest = exports.CreateSettingResponse = exports.CreateSettingRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const api_response_1 = require("../../common/api-response");
const setting_entity_1 = require("../../database/models/setting.entity");
const type_1 = require("../../utils/type");
class CreateSettingRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateSettingRequest.prototype, "key", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateSettingRequest.prototype, "value", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: type_1.SettingDataTypeEnums }),
    __metadata("design:type", String)
], CreateSettingRequest.prototype, "data_type", void 0);
exports.CreateSettingRequest = CreateSettingRequest;
class CreateSettingResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", setting_entity_1.Setting)
], CreateSettingResponse.prototype, "data", void 0);
exports.CreateSettingResponse = CreateSettingResponse;
class UpdateSettingRequest extends (0, swagger_1.OmitType)(CreateSettingRequest, [
    'data_type',
]) {
}
exports.UpdateSettingRequest = UpdateSettingRequest;
class UpdateSettingResponse extends CreateSettingResponse {
}
exports.UpdateSettingResponse = UpdateSettingResponse;
class DeleteSettingRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeleteSettingRequest.prototype, "activity_id", void 0);
exports.DeleteSettingRequest = DeleteSettingRequest;
class DeleteSettingResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteSettingResponse = DeleteSettingResponse;
class ListSettingResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListSettingResponse.prototype, "data", void 0);
exports.ListSettingResponse = ListSettingResponse;
//# sourceMappingURL=setting.dto.js.map