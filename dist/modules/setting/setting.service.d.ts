import { Setting } from 'src/database/models/setting.entity';
import { Repository } from 'typeorm';
import { UpdateSettingRequest } from './setting.dto';
export declare class SettingService {
    private repository;
    constructor(repository: Repository<Setting>);
    getSetting(): Promise<Setting[]>;
    update(data: UpdateSettingRequest[]): Promise<Setting[]>;
    getTriwulanSetting(): Promise<Record<string, string>[]>;
}
