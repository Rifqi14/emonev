import { SettingService } from './setting.service';
import { UpdateSettingRequest } from './setting.dto';
export declare class SettingController {
    private readonly settingService;
    constructor(settingService: SettingService);
    getSettingTriwulan(): Promise<{
        statusCode: number;
        message: string;
        data: Record<string, string>[];
    }>;
    updateSettingTriwulan(data: UpdateSettingRequest[]): Promise<{
        statusCode: number;
        message: string;
        data: import("../../database/models/setting.entity").Setting[];
    }>;
}
