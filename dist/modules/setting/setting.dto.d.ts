import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { Setting } from 'src/database/models/setting.entity';
import { SettingDataType } from 'src/utils/type';
export declare class CreateSettingRequest {
    key: string;
    value: string;
    data_type: SettingDataType;
}
export declare class CreateSettingResponse extends BaseApiResponse {
    data: Setting;
}
declare const UpdateSettingRequest_base: import("@nestjs/common").Type<Omit<CreateSettingRequest, "data_type">>;
export declare class UpdateSettingRequest extends UpdateSettingRequest_base {
}
export declare class UpdateSettingResponse extends CreateSettingResponse {
}
export declare class DeleteSettingRequest {
    activity_id: number;
}
export declare class DeleteSettingResponse extends BaseApiResponse {
}
export declare class ListSettingResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<Setting>>;
}
export {};
