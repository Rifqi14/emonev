import { MasterService } from './master.service';
import { CreateMasterRequest, CreateMasterResponse, DeleteMasterRequest, DeleteMasterResponse, DetailMasterResponse, ListMasterRequest, ListMasterResponse, UpdateMasterRequest, UpdateMasterResponse } from './master.dto';
export declare class MasterController {
    private readonly masterService;
    constructor(masterService: MasterService);
    detail(id: number): Promise<DetailMasterResponse>;
    list(query: ListMasterRequest): Promise<ListMasterResponse>;
    create(data: CreateMasterRequest): Promise<CreateMasterResponse>;
    update(data: UpdateMasterRequest): Promise<UpdateMasterResponse>;
    delete(data: DeleteMasterRequest): Promise<DeleteMasterResponse>;
}
