import { DataWithPaginationApiResponse } from 'src/common/api-response';
import { Master } from 'src/database/models/master.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { PayloadClientResponse } from '../authentication/authentication.dto';
import { CreateMasterRequest, ListMasterRequest, UpdateMasterRequest } from './master.dto';
export declare class MasterService {
    private masterRepository;
    private request;
    constructor(masterRepository: Repository<Master>, request: Request & {
        user: PayloadClientResponse;
        is_admin_bidang: boolean;
    });
    create(data: CreateMasterRequest): Promise<Master>;
    detail(id: number): Promise<Master>;
    update(data: UpdateMasterRequest): Promise<Master>;
    delete(data_master_id: number): Promise<void>;
    list(query: ListMasterRequest): Promise<DataWithPaginationApiResponse<Array<Master>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<Master>;
}
