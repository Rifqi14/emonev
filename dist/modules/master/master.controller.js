"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MasterController = void 0;
const common_1 = require("@nestjs/common");
const master_service_1 = require("./master.service");
const swagger_1 = require("@nestjs/swagger");
const openapi_1 = require("../../common/openapi");
const master_dto_1 = require("./master.dto");
let MasterController = class MasterController {
    constructor(masterService) {
        this.masterService = masterService;
    }
    async detail(id) {
        return {
            statusCode: 200,
            message: `Pengambilan data berhasil`,
            data: {
                result: await this.masterService.detail(id),
            },
        };
    }
    async list(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.masterService.list(query),
        };
    }
    async create(data) {
        return {
            statusCode: 200,
            message: `Pembuatan data berhasil`,
            data: await this.masterService.create(data),
        };
    }
    async update(data) {
        return {
            statusCode: 200,
            message: 'Pengkinian data berhasil',
            data: await this.masterService.update(data),
        };
    }
    async delete(data) {
        await this.masterService.delete(data.data_master_id);
        return {
            statusCode: 200,
            message: 'Penghapusan data berhasil',
        };
    }
};
__decorate([
    (0, openapi_1.SwaggerOkResponse)(master_dto_1.DetailMasterResponse),
    (0, common_1.Get)('detail/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], MasterController.prototype, "detail", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(master_dto_1.ListMasterResponse),
    (0, common_1.Get)('list'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [master_dto_1.ListMasterRequest]),
    __metadata("design:returntype", Promise)
], MasterController.prototype, "list", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(master_dto_1.CreateMasterResponse),
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [master_dto_1.CreateMasterRequest]),
    __metadata("design:returntype", Promise)
], MasterController.prototype, "create", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(master_dto_1.UpdateMasterResponse),
    (0, common_1.Patch)('update'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [master_dto_1.UpdateMasterRequest]),
    __metadata("design:returntype", Promise)
], MasterController.prototype, "update", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(master_dto_1.DeleteMasterResponse),
    (0, common_1.Patch)('delete'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [master_dto_1.DeleteMasterRequest]),
    __metadata("design:returntype", Promise)
], MasterController.prototype, "delete", null);
MasterController = __decorate([
    (0, swagger_1.ApiTags)('Data Master'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('data-master'),
    __metadata("design:paramtypes", [master_service_1.MasterService])
], MasterController);
exports.MasterController = MasterController;
//# sourceMappingURL=master.controller.js.map