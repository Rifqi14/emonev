"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.masterProviders = void 0;
const master_entity_1 = require("../../database/models/master.entity");
const master_occassion_entity_1 = require("../../database/models/master_occassion.entity");
exports.masterProviders = [
    {
        provide: 'MASTER_REPOSITORY',
        useFactory: (ds) => ds.getRepository(master_entity_1.Master),
        inject: ['DATA_SOURCE'],
    },
    {
        provide: 'MASTER_OCCASSION_REPOSITORY',
        useFactory: (ds) => ds.getRepository(master_occassion_entity_1.MasterOccassion),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=master.providers.js.map