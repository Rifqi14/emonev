"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MasterService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const master_entity_1 = require("../../database/models/master.entity");
const master_occassion_entity_1 = require("../../database/models/master_occassion.entity");
const user_entity_1 = require("../../database/models/user.entity");
const typeorm_1 = require("typeorm");
let MasterService = class MasterService {
    constructor(masterRepository, request) {
        this.masterRepository = masterRepository;
        this.request = request;
    }
    async create(data) {
        try {
            const { description, organization_id, purpose_id, triwulan_id } = data;
            return await this.masterRepository.manager.transaction(async (trx) => {
                const creator = await trx.findOneOrFail(user_entity_1.User, {
                    where: { id: this.request.user.user_id },
                });
                const occassions = data.occassions
                    ? data.occassions.map((occassion) => trx.create(master_occassion_entity_1.MasterOccassion, {
                        occassion_id: occassion,
                    }))
                    : undefined;
                const master = await trx
                    .create(master_entity_1.Master, {
                    description,
                    organization_id,
                    purpose_id,
                    triwulan_id,
                    masterOccassions: occassions,
                    createdBy: creator,
                })
                    .save();
                return master;
            });
        }
        catch (error) {
            throw error;
        }
    }
    async detail(id) {
        try {
            const master = await this.masterRepository.findOneOrFail({
                relations: {
                    triwulan: true,
                    masterOccassions: {
                        occassion: true,
                    },
                    organization: true,
                    purpose: true,
                },
                where: {
                    id,
                },
            });
            return master;
        }
        catch (error) {
            throw error;
        }
    }
    async update(data) {
        try {
            const { data_master_id, description, organization_id, purpose_id, triwulan_id, } = data;
            await this.masterRepository.manager.transaction(async (trx) => {
                await trx.delete(master_occassion_entity_1.MasterOccassion, {
                    master_id: data_master_id,
                });
                const master = await this.masterRepository.findOneByOrFail({
                    id: data_master_id,
                });
                const occassions = data.occassions
                    ? data.occassions.map((occassion) => trx.create(master_occassion_entity_1.MasterOccassion, {
                        occassion_id: occassion,
                    }))
                    : undefined;
                master.description = description;
                master.organization_id = organization_id;
                master.purpose_id = purpose_id;
                master.triwulan_id = triwulan_id;
                if (occassions)
                    master.masterOccassions = occassions;
                await trx.save(master);
            });
            return await this.masterRepository.findOneByOrFail({
                id: data_master_id,
            });
        }
        catch (error) {
            throw error;
        }
    }
    async delete(data_master_id) {
        try {
            await this.masterRepository.delete({ id: data_master_id });
        }
        catch (error) {
            throw error;
        }
    }
    async list(query) {
        try {
            const { search, sort } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const where = [];
            if (search) {
                where.push({
                    description: (0, typeorm_1.ILike)(`%${search}%`),
                });
            }
            if (this.request.is_admin_bidang) {
                if (!this.request.user.userOrganization ||
                    this.request.user.userOrganization.length === 0)
                    throw new common_1.UnauthorizedException();
                where[0] = Object.assign(Object.assign({}, where[0]), { createdBy: {
                        organization_id: (0, typeorm_1.In)(this.request.user.userOrganization.map((organization) => organization.organization_id)),
                    } });
            }
            const [masters, count] = await this.masterRepository.findAndCount({
                where: where.length > 0 ? where : undefined,
                order: this.getOrderQuery(sort),
                relations: {
                    triwulan: true,
                    purpose: true,
                    organization: true,
                    masterOccassions: {
                        occassion: true,
                    },
                },
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: masters,
            };
        }
        catch (error) {
            throw error;
        }
    }
    getOrderQuery(sort) {
        const sortRes = {
            id: 'DESC',
        };
        switch (sort) {
            case 'terbaru':
                Object.assign(sortRes, {
                    id: 'DESC',
                });
                break;
            case 'terlama':
                Object.assign(sortRes, {
                    id: 'ASC',
                });
                break;
            case 'a-z':
                Object.assign(sortRes, {
                    description: 'ASC',
                });
                break;
            case 'z-a':
                Object.assign(sortRes, {
                    description: 'DESC',
                });
                break;
        }
        return sortRes;
    }
};
MasterService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)('MASTER_REPOSITORY')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [typeorm_1.Repository, Object])
], MasterService);
exports.MasterService = MasterService;
//# sourceMappingURL=master.service.js.map