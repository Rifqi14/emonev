"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetailMasterResponse = exports.DataDetailResponse = exports.ListMasterResponse = exports.ListMasterRequest = exports.DeleteMasterResponse = exports.DeleteMasterRequest = exports.UpdateMasterResponse = exports.UpdateMasterRequest = exports.CreateMasterResponse = exports.CreateMasterRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const api_response_1 = require("../../common/api-response");
const master_entity_1 = require("../../database/models/master.entity");
const helper_1 = require("../../utils/helper");
class CreateMasterRequest {
}
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CreateMasterRequest.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateMasterRequest.prototype, "triwulan_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateMasterRequest.prototype, "organization_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateMasterRequest.prototype, "purpose_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ type: [Number] }),
    __metadata("design:type", Array)
], CreateMasterRequest.prototype, "occassions", void 0);
exports.CreateMasterRequest = CreateMasterRequest;
class CreateMasterResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", master_entity_1.Master)
], CreateMasterResponse.prototype, "data", void 0);
exports.CreateMasterResponse = CreateMasterResponse;
class UpdateMasterRequest extends (0, swagger_1.PartialType)(CreateMasterRequest) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateMasterRequest.prototype, "data_master_id", void 0);
exports.UpdateMasterRequest = UpdateMasterRequest;
class UpdateMasterResponse extends CreateMasterResponse {
}
exports.UpdateMasterResponse = UpdateMasterResponse;
class DeleteMasterRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeleteMasterRequest.prototype, "data_master_id", void 0);
exports.DeleteMasterRequest = DeleteMasterRequest;
class DeleteMasterResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteMasterResponse = DeleteMasterResponse;
class ListMasterRequest extends helper_1.ListBaseRequest {
}
exports.ListMasterRequest = ListMasterRequest;
class ListMasterResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListMasterResponse.prototype, "data", void 0);
exports.ListMasterResponse = ListMasterResponse;
class DataDetailResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", master_entity_1.Master)
], DataDetailResponse.prototype, "result", void 0);
exports.DataDetailResponse = DataDetailResponse;
class DetailMasterResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", DataDetailResponse)
], DetailMasterResponse.prototype, "data", void 0);
exports.DetailMasterResponse = DetailMasterResponse;
//# sourceMappingURL=master.dto.js.map