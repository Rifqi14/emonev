import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { Master } from 'src/database/models/master.entity';
import { ListBaseRequest } from 'src/utils/helper';
export declare class CreateMasterRequest {
    description?: string;
    triwulan_id?: number;
    organization_id?: number;
    purpose_id?: number;
    occassions?: number[];
}
export declare class CreateMasterResponse extends BaseApiResponse {
    data: Master;
}
declare const UpdateMasterRequest_base: import("@nestjs/common").Type<Partial<CreateMasterRequest>>;
export declare class UpdateMasterRequest extends UpdateMasterRequest_base {
    data_master_id: number;
}
export declare class UpdateMasterResponse extends CreateMasterResponse {
}
export declare class DeleteMasterRequest {
    data_master_id: number;
}
export declare class DeleteMasterResponse extends BaseApiResponse {
}
export declare class ListMasterRequest extends ListBaseRequest {
}
export declare class ListMasterResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<Master>>;
}
export declare class DataDetailResponse {
    result: Master;
}
export declare class DetailMasterResponse extends BaseApiResponse {
    data: DataDetailResponse;
}
export {};
