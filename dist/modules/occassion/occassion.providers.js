"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.occassionProviders = void 0;
const occassion_entity_1 = require("../../database/models/occassion.entity");
exports.occassionProviders = [
    {
        provide: 'OCCASSION_REPOSITORY',
        useFactory: (ds) => ds.getRepository(occassion_entity_1.Occassion),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=occassion.providers.js.map