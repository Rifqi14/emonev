"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OccassionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const openapi_1 = require("../../common/openapi");
const occassion_service_1 = require("./occassion.service");
const occassion_dto_1 = require("./occassion.dto");
const helper_1 = require("../../utils/helper");
let OccassionController = class OccassionController {
    constructor(occassionService) {
        this.occassionService = occassionService;
    }
    async detail(id) {
        return {
            statusCode: 200,
            message: `Pengambilan data berhasil`,
            data: await this.occassionService.detail(id),
        };
    }
    async list(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.occassionService.list(query),
        };
    }
    async create(data) {
        return {
            statusCode: 200,
            message: `Pembuatan data berhasil`,
            data: await this.occassionService.create(data),
        };
    }
    async update(data) {
        return {
            statusCode: 200,
            message: 'Pengkinian data berhasil',
            data: await this.occassionService.update(data),
        };
    }
    async delete(data) {
        await this.occassionService.delete(data.occassion_id);
        return {
            statusCode: 200,
            message: 'Penghapusan data berhasil',
        };
    }
};
__decorate([
    (0, openapi_1.SwaggerOkResponse)(occassion_dto_1.CreateOccassionResponse),
    (0, common_1.Get)('detail/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], OccassionController.prototype, "detail", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(occassion_dto_1.ListOccassionResponse),
    (0, common_1.Get)('list'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [helper_1.ListBaseRequest]),
    __metadata("design:returntype", Promise)
], OccassionController.prototype, "list", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(occassion_dto_1.CreateOccassionResponse),
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [occassion_dto_1.CreateOccassionRequest]),
    __metadata("design:returntype", Promise)
], OccassionController.prototype, "create", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(occassion_dto_1.UpdateOccassionResponse),
    (0, common_1.Patch)('update'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [occassion_dto_1.UpdateOccassionRequest]),
    __metadata("design:returntype", Promise)
], OccassionController.prototype, "update", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(occassion_dto_1.DeleteOccassionResponse),
    (0, common_1.Patch)('delete'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [occassion_dto_1.DeleteOccassionRequest]),
    __metadata("design:returntype", Promise)
], OccassionController.prototype, "delete", null);
OccassionController = __decorate([
    (0, swagger_1.ApiTags)('Occassion'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('occassion'),
    __metadata("design:paramtypes", [occassion_service_1.OccassionService])
], OccassionController);
exports.OccassionController = OccassionController;
//# sourceMappingURL=occassion.controller.js.map