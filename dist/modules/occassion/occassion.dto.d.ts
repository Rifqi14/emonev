import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { Occassion } from 'src/database/models/occassion.entity';
export declare class CreateOccassionRequest {
    title: string;
    code?: string;
}
export declare class CreateOccassionResponse extends BaseApiResponse {
    data: Occassion;
}
declare const UpdateOccassionRequest_base: import("@nestjs/common").Type<Partial<CreateOccassionRequest>>;
export declare class UpdateOccassionRequest extends UpdateOccassionRequest_base {
    occassion_id: number;
}
export declare class UpdateOccassionResponse extends CreateOccassionResponse {
}
export declare class DeleteOccassionRequest {
    occassion_id: number;
}
export declare class DeleteOccassionResponse extends BaseApiResponse {
}
export declare class ListOccassionResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<Occassion>>;
}
export {};
