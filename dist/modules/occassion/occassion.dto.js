"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListOccassionResponse = exports.DeleteOccassionResponse = exports.DeleteOccassionRequest = exports.UpdateOccassionResponse = exports.UpdateOccassionRequest = exports.CreateOccassionResponse = exports.CreateOccassionRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const api_response_1 = require("../../common/api-response");
const occassion_entity_1 = require("../../database/models/occassion.entity");
const class_validator_1 = require("class-validator");
class CreateOccassionRequest {
}
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: 'Judul harus diisi.' }),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateOccassionRequest.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CreateOccassionRequest.prototype, "code", void 0);
exports.CreateOccassionRequest = CreateOccassionRequest;
class CreateOccassionResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", occassion_entity_1.Occassion)
], CreateOccassionResponse.prototype, "data", void 0);
exports.CreateOccassionResponse = CreateOccassionResponse;
class UpdateOccassionRequest extends (0, swagger_1.PartialType)(CreateOccassionRequest) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateOccassionRequest.prototype, "occassion_id", void 0);
exports.UpdateOccassionRequest = UpdateOccassionRequest;
class UpdateOccassionResponse extends CreateOccassionResponse {
}
exports.UpdateOccassionResponse = UpdateOccassionResponse;
class DeleteOccassionRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeleteOccassionRequest.prototype, "occassion_id", void 0);
exports.DeleteOccassionRequest = DeleteOccassionRequest;
class DeleteOccassionResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteOccassionResponse = DeleteOccassionResponse;
class ListOccassionResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListOccassionResponse.prototype, "data", void 0);
exports.ListOccassionResponse = ListOccassionResponse;
//# sourceMappingURL=occassion.dto.js.map