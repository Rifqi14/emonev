import { Occassion } from 'src/database/models/occassion.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { CreateOccassionRequest, UpdateOccassionRequest } from './occassion.dto';
import { ListBaseRequest } from 'src/utils/helper';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
export declare class OccassionService {
    private repository;
    constructor(repository: Repository<Occassion>);
    create(data: CreateOccassionRequest): Promise<Occassion>;
    detail(id: number): Promise<Occassion>;
    update(data: UpdateOccassionRequest): Promise<Occassion>;
    delete(occassion_id: number): Promise<void>;
    list(query: ListBaseRequest): Promise<DataWithPaginationApiResponse<Array<Occassion>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<Occassion>;
}
