import { OccassionService } from './occassion.service';
import { CreateOccassionRequest, CreateOccassionResponse, DeleteOccassionRequest, DeleteOccassionResponse, ListOccassionResponse, UpdateOccassionRequest, UpdateOccassionResponse } from './occassion.dto';
import { ListBaseRequest } from 'src/utils/helper';
export declare class OccassionController {
    private readonly occassionService;
    constructor(occassionService: OccassionService);
    detail(id: number): Promise<CreateOccassionResponse>;
    list(query: ListBaseRequest): Promise<ListOccassionResponse>;
    create(data: CreateOccassionRequest): Promise<CreateOccassionResponse>;
    update(data: UpdateOccassionRequest): Promise<UpdateOccassionResponse>;
    delete(data: DeleteOccassionRequest): Promise<DeleteOccassionResponse>;
}
