"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProcurementMethodModule = void 0;
const common_1 = require("@nestjs/common");
const procurement_method_service_1 = require("./procurement-method.service");
const procurement_method_controller_1 = require("./procurement-method.controller");
const procurement_method_providers_1 = require("./procurement-method.providers");
let ProcurementMethodModule = class ProcurementMethodModule {
};
ProcurementMethodModule = __decorate([
    (0, common_1.Module)({
        controllers: [procurement_method_controller_1.ProcurementMethodController],
        providers: [procurement_method_service_1.ProcurementMethodService, ...procurement_method_providers_1.procurementMethodProviders],
        exports: [procurement_method_service_1.ProcurementMethodService],
    })
], ProcurementMethodModule);
exports.ProcurementMethodModule = ProcurementMethodModule;
//# sourceMappingURL=procurement-method.module.js.map