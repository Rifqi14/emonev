"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProcurementMethodController = void 0;
const common_1 = require("@nestjs/common");
const procurement_method_service_1 = require("./procurement-method.service");
const openapi_1 = require("../../common/openapi");
const procurement_method_dto_1 = require("./procurement-method.dto");
const swagger_1 = require("@nestjs/swagger");
let ProcurementMethodController = class ProcurementMethodController {
    constructor(procurementMethodService) {
        this.procurementMethodService = procurementMethodService;
    }
    async list(query) {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.procurementMethodService.list(query),
        };
    }
    async create(data) {
        return {
            statusCode: 200,
            message: `Pembuatan data berhasil`,
            data: await this.procurementMethodService.create(data),
        };
    }
};
__decorate([
    (0, openapi_1.SwaggerOkResponse)(procurement_method_dto_1.ListProcurementMethodResponse),
    (0, common_1.Get)('list'),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [procurement_method_dto_1.ListProcurementMethodRequest]),
    __metadata("design:returntype", Promise)
], ProcurementMethodController.prototype, "list", null);
__decorate([
    (0, openapi_1.SwaggerOkResponse)(procurement_method_dto_1.CreateProcurementMethodResponse),
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [procurement_method_dto_1.CreateProcurementMethodRequest]),
    __metadata("design:returntype", Promise)
], ProcurementMethodController.prototype, "create", null);
ProcurementMethodController = __decorate([
    (0, swagger_1.ApiTags)('User | Cara Pengadaan'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('procurement-method'),
    __metadata("design:paramtypes", [procurement_method_service_1.ProcurementMethodService])
], ProcurementMethodController);
exports.ProcurementMethodController = ProcurementMethodController;
//# sourceMappingURL=procurement-method.controller.js.map