import { ProcurementMethod } from 'src/database/models/procurement-method.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { CreateProcurementMethodRequest, ListProcurementMethodRequest } from './procurement-method.dto';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
export declare class ProcurementMethodService {
    private repository;
    constructor(repository: Repository<ProcurementMethod>);
    create(data: CreateProcurementMethodRequest): Promise<ProcurementMethod>;
    list(query: ListProcurementMethodRequest): Promise<DataWithPaginationApiResponse<Array<ProcurementMethod>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<ProcurementMethod>;
}
