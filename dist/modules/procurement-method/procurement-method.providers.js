"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.procurementMethodProviders = void 0;
const procurement_method_entity_1 = require("../../database/models/procurement-method.entity");
exports.procurementMethodProviders = [
    {
        provide: 'PROCUREMENT_METHOD_REPOSITORY',
        useFactory: (ds) => ds.getRepository(procurement_method_entity_1.ProcurementMethod),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=procurement-method.providers.js.map