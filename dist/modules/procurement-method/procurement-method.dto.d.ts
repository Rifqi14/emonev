import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { ProcurementMethod } from 'src/database/models/procurement-method.entity';
import { ListBaseRequest } from 'src/utils/helper';
export declare class CreateProcurementMethodRequest {
    name: string;
}
export declare class CreateProcurementMethodResponse extends BaseApiResponse {
    data: ProcurementMethod;
}
export declare class ListProcurementMethodRequest extends ListBaseRequest {
}
export declare class ListProcurementMethodResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<ProcurementMethod[]>;
}
