import { ProcurementMethodService } from './procurement-method.service';
import { CreateProcurementMethodRequest, CreateProcurementMethodResponse, ListProcurementMethodRequest, ListProcurementMethodResponse } from './procurement-method.dto';
export declare class ProcurementMethodController {
    private readonly procurementMethodService;
    constructor(procurementMethodService: ProcurementMethodService);
    list(query: ListProcurementMethodRequest): Promise<ListProcurementMethodResponse>;
    create(data: CreateProcurementMethodRequest): Promise<CreateProcurementMethodResponse>;
}
