"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProcurementMethodService = void 0;
const common_1 = require("@nestjs/common");
const procurement_method_entity_1 = require("../../database/models/procurement-method.entity");
const typeorm_1 = require("typeorm");
let ProcurementMethodService = class ProcurementMethodService {
    constructor(repository) {
        this.repository = repository;
    }
    async create(data) {
        try {
            const { name } = data;
            return await this.repository.manager.transaction(async (trx) => await trx.create(procurement_method_entity_1.ProcurementMethod, { name }).save());
        }
        catch (error) {
            throw error;
        }
    }
    async list(query) {
        try {
            const { search, sort } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const where = [];
            if (search) {
                where.push({
                    name: (0, typeorm_1.ILike)(`%${search}%`),
                });
            }
            const [fundSources, count] = await this.repository.findAndCount({
                where: where.length > 0 ? where : undefined,
                order: this.getOrderQuery(sort),
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: fundSources,
            };
        }
        catch (error) {
            throw error;
        }
    }
    getOrderQuery(sort) {
        const sortRes = {
            id: 'DESC',
        };
        switch (sort) {
            case 'terbaru':
                Object.assign(sortRes, {
                    id: 'DESC',
                });
                break;
            case 'terlama':
                Object.assign(sortRes, {
                    id: 'ASC',
                });
                break;
            case 'a-z':
                Object.assign(sortRes, {
                    name: 'ASC',
                });
                break;
            case 'z-a':
                Object.assign(sortRes, {
                    name: 'DESC',
                });
                break;
        }
        return sortRes;
    }
};
ProcurementMethodService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('PROCUREMENT_METHOD_REPOSITORY')),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], ProcurementMethodService);
exports.ProcurementMethodService = ProcurementMethodService;
//# sourceMappingURL=procurement-method.service.js.map