"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userProviders = void 0;
const user_entity_1 = require("../../database/models/user.entity");
exports.userProviders = [
    {
        provide: 'USER_REPOSITORY',
        useFactory: (ds) => ds.getRepository(user_entity_1.User),
        inject: ['DATA_SOURCE'],
    },
];
//# sourceMappingURL=user.providers.js.map