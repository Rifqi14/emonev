"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../../database/models/user.entity");
const typeorm_1 = require("typeorm");
const role_entity_1 = require("../../database/models/role.entity");
const status_entity_1 = require("../../database/models/status.entity");
const organization_entity_1 = require("../../database/models/organization.entity");
const user_organization_entity_1 = require("../../database/models/user-organization.entity");
const core_1 = require("@nestjs/core");
let UserService = class UserService {
    constructor(repository, request) {
        this.repository = repository;
        this.request = request;
    }
    async findByUsername(username) {
        try {
            const user = await this.repository
                .createQueryBuilder('user')
                .addSelect('user.password')
                .leftJoinAndSelect('user.status', 'status')
                .leftJoinAndSelect('user.role', 'role')
                .leftJoinAndSelect('user.organization', 'organization')
                .leftJoinAndSelect('user.userOrganization', 'userOrganization')
                .where(`username = :username`, { username })
                .getOneOrFail();
            return user;
        }
        catch (error) {
            throw error;
        }
    }
    async findByEmail(email) {
        try {
            const user = await this.repository.findOneOrFail({ where: { email } });
            return user;
        }
        catch (error) {
            throw error;
        }
    }
    async getProfile(username) {
        try {
            return await this.repository.findOneOrFail({
                where: { username },
                relations: {
                    status: true,
                    organization: true,
                    role: true,
                    userOrganization: true,
                },
            });
        }
        catch (error) {
            throw error;
        }
    }
    async create(data) {
        try {
            const { name, password, username, admin_role_id, status_id, email, organization_id, } = data;
            return await this.repository.manager.transaction(async (trx) => {
                let organization = undefined;
                let organizations = undefined;
                if (organization_id) {
                    if (organization_id.length > 1) {
                        organizations = await this.repository.manager.find(organization_entity_1.Organization, {
                            where: { id: (0, typeorm_1.In)(organization_id) },
                        });
                    }
                    organization = await this.repository.manager.findOneOrFail(organization_entity_1.Organization, {
                        where: { id: organization_id[0] },
                    });
                }
                const user = trx.create(user_entity_1.User, {
                    name,
                    email,
                    password,
                    organization,
                    username,
                    role_id: admin_role_id !== null && admin_role_id !== void 0 ? admin_role_id : (await trx.findOneByOrFail(role_entity_1.Role, { name: 'OPD' })).id,
                    status_id: status_id !== null && status_id !== void 0 ? status_id : (await trx.findOneByOrFail(status_entity_1.Status, { name: 'Active' })).id,
                });
                if (organizations) {
                    user.userOrganization = organizations.map((organization) => {
                        return trx.create(user_organization_entity_1.UserOrganization, {
                            organization,
                        });
                    });
                }
                return user.save();
            });
        }
        catch (error) {
            throw error;
        }
    }
    async updatePassword(password, user_id) {
        try {
            await this.repository.manager.transaction(async (trx) => {
                const user = trx.create(user_entity_1.User, {
                    password,
                });
                await trx.update(user_entity_1.User, { id: user_id }, user);
            });
        }
        catch (error) {
            throw error;
        }
    }
    async getDetail(id) {
        try {
            return await this.repository.findOneOrFail({
                where: {
                    id,
                    status_id: (await this.repository.manager.findOneOrFail(status_entity_1.Status, {
                        where: { name: 'Active' },
                    })).id,
                },
                relations: {
                    status: true,
                    organization: true,
                    role: true,
                    userOrganization: true,
                },
            });
        }
        catch (error) {
            throw error;
        }
    }
    async update(data) {
        try {
            const { admin_role_id, name, password, status_id, username, user_id, email, } = data;
            await this.repository.manager.transaction(async (trx) => {
                const user = trx.create(user_entity_1.User, {
                    username,
                    name,
                    email,
                    password,
                    status_id,
                    role_id: admin_role_id,
                });
                await trx.update(user_entity_1.User, { id: user_id }, user);
            });
            return this.repository.findOneOrFail({
                where: {
                    id: user_id,
                },
            });
        }
        catch (error) {
            throw error;
        }
    }
    async delete(user_id) {
        try {
            const user = await this.repository.findOneOrFail({
                where: { id: user_id },
            });
            user.status = await this.repository.manager.findOneOrFail(status_entity_1.Status, {
                where: { name: 'Deleted' },
            });
            await user.save();
        }
        catch (error) {
            throw error;
        }
    }
    async list(query) {
        try {
            const { search, sort } = query;
            const page = query.page && query.page > 0 ? Number(query.page) : 1;
            const limit = query.limit ? Number(query.limit) : 10;
            const { id: status_id } = await this.repository.manager.findOneOrFail(status_entity_1.Status, {
                where: { name: 'Active' },
            });
            let where = [
                {
                    status_id: status_id,
                },
            ];
            if (search) {
                where.pop();
                where.push({
                    username: (0, typeorm_1.ILike)(`%${search}%`),
                    status_id: status_id,
                });
                where.push({
                    name: (0, typeorm_1.ILike)(`%${search}%`),
                    status_id: status_id,
                });
                where.push({
                    email: (0, typeorm_1.ILike)(`%${search}%`),
                    status_id: status_id,
                });
            }
            const user = await this.repository.findOne({
                where: {
                    username: this.request.user.username,
                },
                relations: { role: true },
            });
            if (user && user.role && user.role.name.toLowerCase() === 'opd') {
                where = [
                    {
                        username: (0, typeorm_1.ILike)(`%${search !== null && search !== void 0 ? search : ''}%`),
                        status_id: status_id,
                        created_by: user.username,
                    },
                    {
                        name: (0, typeorm_1.ILike)(`%${search !== null && search !== void 0 ? search : ''}%`),
                        status_id: status_id,
                        created_by: user.username,
                    },
                    {
                        email: (0, typeorm_1.ILike)(`%${search !== null && search !== void 0 ? search : ''}%`),
                        status_id: status_id,
                        created_by: user.username,
                    },
                ];
            }
            const [users, count] = await this.repository.findAndCount({
                where: where,
                order: this.getOrderQuery(sort),
                relations: {
                    status: true,
                    organization: true,
                    role: true,
                    userOrganization: true,
                },
                skip: (page - 1) * limit,
                take: limit,
            });
            return {
                total: count,
                page: page,
                pages: count > 0 ? Math.ceil(count / limit) : 1,
                result: users,
            };
        }
        catch (error) {
            throw error;
        }
    }
    getOrderQuery(sort) {
        const sortRes = {
            id: 'DESC',
        };
        switch (sort) {
            case 'terbaru':
                Object.assign(sortRes, {
                    id: 'DESC',
                });
                break;
            case 'terlama':
                Object.assign(sortRes, {
                    id: 'ASC',
                });
                break;
            case 'a-z':
                Object.assign(sortRes, {
                    username: 'ASC',
                });
                break;
            case 'z-a':
                Object.assign(sortRes, {
                    username: 'DESC',
                });
                break;
        }
        return sortRes;
    }
};
UserService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)('USER_REPOSITORY')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [typeorm_1.Repository, Object])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map