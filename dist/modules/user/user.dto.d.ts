import { BaseApiResponse, DataWithPaginationApiResponse } from 'src/common/api-response';
import { User } from 'src/database/models/user.entity';
export declare class CreateUserRequest {
    username: string;
    password: string;
    name: string;
    email: string;
    admin_role_id?: number;
    status_id?: number;
    organization_id?: number[];
}
declare const RegisterUserRequest_base: import("@nestjs/common").Type<Omit<CreateUserRequest, "organization_id" | "admin_role_id" | "status_id">>;
export declare class RegisterUserRequest extends RegisterUserRequest_base {
}
export declare class CreateUserResponse extends BaseApiResponse {
    data: User;
}
declare const UpdateUserRequest_base: import("@nestjs/common").Type<Partial<CreateUserRequest>>;
export declare class UpdateUserRequest extends UpdateUserRequest_base {
    user_id: number;
}
export declare class UpdateUserResponse extends CreateUserResponse {
}
export declare class DeleteUserRequest {
    user_id: number;
}
export declare class DeleteUserResponse extends BaseApiResponse {
}
export declare class ListUserRequest {
    limit?: number;
    page?: number;
    search?: string;
    sort?: 'terbaru' | 'terlama' | 'a-z' | 'z-a';
}
export declare class ListUserResponse extends BaseApiResponse {
    data: DataWithPaginationApiResponse<Array<User>>;
}
export declare class ForgotPasswordRequest {
    email: string;
}
export declare class ForgotPasswordResponse extends BaseApiResponse {
}
export {};
