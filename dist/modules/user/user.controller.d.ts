import { UserService } from './user.service';
import { CreateUserRequest, CreateUserResponse, DeleteUserRequest, DeleteUserResponse, ListUserRequest, ListUserResponse, UpdateUserRequest, UpdateUserResponse } from './user.dto';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    update(data: UpdateUserRequest): Promise<UpdateUserResponse>;
    create(data: CreateUserRequest): Promise<CreateUserResponse>;
    delete(data: DeleteUserRequest): Promise<DeleteUserResponse>;
    detail(id: number): Promise<CreateUserResponse>;
    list(query: ListUserRequest): Promise<ListUserResponse>;
}
