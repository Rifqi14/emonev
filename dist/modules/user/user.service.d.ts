import { User } from 'src/database/models/user.entity';
import { FindOptionsOrder, Repository } from 'typeorm';
import { CreateUserRequest, ListUserRequest, UpdateUserRequest } from './user.dto';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
import { PayloadClientResponse } from '../authentication/authentication.dto';
import { Request } from 'express';
export declare class UserService {
    private repository;
    private request;
    constructor(repository: Repository<User>, request: Request & {
        user: PayloadClientResponse;
        is_admin_bidang: boolean;
    });
    findByUsername(username: string): Promise<User>;
    findByEmail(email: string): Promise<User>;
    getProfile(username: string): Promise<User>;
    create(data: CreateUserRequest): Promise<User>;
    updatePassword(password: string, user_id: number): Promise<void>;
    getDetail(id: number): Promise<User>;
    update(data: UpdateUserRequest): Promise<User>;
    delete(user_id: number): Promise<void>;
    list(query: ListUserRequest): Promise<DataWithPaginationApiResponse<Array<User>>>;
    getOrderQuery(sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined): FindOptionsOrder<User>;
}
