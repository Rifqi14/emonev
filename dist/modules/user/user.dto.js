"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ForgotPasswordResponse = exports.ForgotPasswordRequest = exports.ListUserResponse = exports.ListUserRequest = exports.DeleteUserResponse = exports.DeleteUserRequest = exports.UpdateUserResponse = exports.UpdateUserRequest = exports.CreateUserResponse = exports.RegisterUserRequest = exports.CreateUserRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const api_response_1 = require("../../common/api-response");
const user_entity_1 = require("../../database/models/user.entity");
class CreateUserRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateUserRequest.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateUserRequest.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateUserRequest.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateUserRequest.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateUserRequest.prototype, "admin_role_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], CreateUserRequest.prototype, "status_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({
        type: [Number],
        description: 'Pada saat registrasi tidak usah dikirim',
    }),
    __metadata("design:type", Array)
], CreateUserRequest.prototype, "organization_id", void 0);
exports.CreateUserRequest = CreateUserRequest;
class RegisterUserRequest extends (0, swagger_1.OmitType)(CreateUserRequest, [
    'admin_role_id',
    'status_id',
    'organization_id',
]) {
}
exports.RegisterUserRequest = RegisterUserRequest;
class CreateUserResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", user_entity_1.User)
], CreateUserResponse.prototype, "data", void 0);
exports.CreateUserResponse = CreateUserResponse;
class UpdateUserRequest extends (0, swagger_1.PartialType)(CreateUserRequest) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateUserRequest.prototype, "user_id", void 0);
exports.UpdateUserRequest = UpdateUserRequest;
class UpdateUserResponse extends CreateUserResponse {
}
exports.UpdateUserResponse = UpdateUserResponse;
class DeleteUserRequest {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeleteUserRequest.prototype, "user_id", void 0);
exports.DeleteUserRequest = DeleteUserRequest;
class DeleteUserResponse extends api_response_1.BaseApiResponse {
}
exports.DeleteUserResponse = DeleteUserResponse;
class ListUserRequest {
}
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], ListUserRequest.prototype, "limit", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], ListUserRequest.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ListUserRequest.prototype, "search", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({
        enum: ['terbaru', 'terlama', 'a-z', 'z-a'],
    }),
    __metadata("design:type", String)
], ListUserRequest.prototype, "sort", void 0);
exports.ListUserRequest = ListUserRequest;
class ListUserResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: (api_response_1.DataWithPaginationApiResponse) }),
    __metadata("design:type", api_response_1.DataWithPaginationApiResponse)
], ListUserResponse.prototype, "data", void 0);
exports.ListUserResponse = ListUserResponse;
class ForgotPasswordRequest {
}
__decorate([
    (0, class_validator_1.IsEmail)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ForgotPasswordRequest.prototype, "email", void 0);
exports.ForgotPasswordRequest = ForgotPasswordRequest;
class ForgotPasswordResponse extends api_response_1.BaseApiResponse {
}
exports.ForgotPasswordResponse = ForgotPasswordResponse;
//# sourceMappingURL=user.dto.js.map