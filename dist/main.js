"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const core_1 = require("@nestjs/core");
const hbs = __importStar(require("express-handlebars"));
const path_1 = require("path");
const app_module_1 = require("./app.module");
const openapi_1 = require("./common/openapi");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    const configService = app.get(config_1.ConfigService);
    const port = configService.get('APP_PORT', 4000);
    const hostname = configService.get('APP_HOST', 'localhost');
    const env = configService.get('NODE_ENV', 'development');
    app.useStaticAssets((0, path_1.join)(__dirname, '..', 'public'));
    app.setBaseViewsDir((0, path_1.join)(__dirname, 'views'));
    app.engine('hbs', hbs.engine({ extname: 'hbs' }));
    app.setViewEngine('hbs');
    if (env === 'development') {
        (0, openapi_1.OpenAPIDoc)({
            app,
            title: 'emonev',
            description: `Download schema for postman collection: <a href="/docs-json" target="_blank">Download here!</a>`,
            endpoint: 'docs',
        });
    }
    app.useGlobalPipes(new common_1.ValidationPipe({
        transform: true,
        exceptionFactory: (errors) => {
            const result = errors.map((error) => ({
                property: error.property,
                message: error.constraints
                    ? error.constraints[Object.keys(error.constraints)[0]]
                    : '',
            }));
            return new common_1.BadRequestException(result);
        },
    }));
    app.useGlobalInterceptors(new common_1.ClassSerializerInterceptor(app.get(core_1.Reflector)));
    app.enableCors();
    await app.listen(port, () => {
        new common_1.Logger().log(`App listening on ${hostname}:${port}`, 'AppRunning');
    });
}
bootstrap();
//# sourceMappingURL=main.js.map