import { WinstonLogger } from 'nest-winston';
import { AppService } from './app.service';
import { StaticRoleApiResponse, StaticStatusApiResponse, StaticTriwulanApiResponse } from './app.providers';
export declare class AppController {
    private readonly appService;
    private readonly logger;
    constructor(appService: AppService, logger: WinstonLogger);
    getHello(): string;
    staticStatus(): Promise<StaticStatusApiResponse>;
    staticRole(): Promise<StaticRoleApiResponse>;
    staticTriwulan(): Promise<StaticTriwulanApiResponse>;
}
