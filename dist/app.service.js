"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("typeorm");
let AppService = class AppService {
    constructor(configService, statusRepository, roleRepository, triwulanRepository) {
        this.configService = configService;
        this.statusRepository = statusRepository;
        this.roleRepository = roleRepository;
        this.triwulanRepository = triwulanRepository;
    }
    getHello() {
        return 'Hello World!';
    }
    getStatus() {
        try {
            return this.statusRepository.find();
        }
        catch (error) {
            throw error;
        }
    }
    getRole() {
        try {
            return this.roleRepository.find();
        }
        catch (error) {
            throw error;
        }
    }
    getTriwulan() {
        try {
            return this.triwulanRepository.find();
        }
        catch (error) {
            throw error;
        }
    }
};
AppService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, common_1.Inject)('STATUS_REPOSITORY')),
    __param(2, (0, common_1.Inject)('ROLE_REPOSITORY')),
    __param(3, (0, common_1.Inject)('TRIWULAN_REPOSITORY')),
    __metadata("design:paramtypes", [config_1.ConfigService,
        typeorm_1.Repository,
        typeorm_1.Repository,
        typeorm_1.Repository])
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map