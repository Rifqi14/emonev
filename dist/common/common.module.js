"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommonModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const nest_winston_1 = require("nest-winston");
const path_1 = require("path");
const process_1 = require("process");
const winston_1 = require("winston");
const jwt_1 = require("@nestjs/jwt");
require("winston-daily-rotate-file");
const excel_service_1 = require("./excel.service");
let CommonModule = class CommonModule {
};
CommonModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                isGlobal: true,
            }),
            nest_winston_1.WinstonModule.forRoot({
                transports: [
                    new winston_1.transports.DailyRotateFile({
                        filename: (0, path_1.join)((0, process_1.cwd)(), 'public', 'logs', '%DATE%-error.log'),
                        level: 'error',
                        format: winston_1.format.combine(winston_1.format.timestamp(), winston_1.format.json()),
                        datePattern: 'YYYY-MM-DD',
                        zippedArchive: false,
                        maxFiles: '7d',
                    }),
                ],
            }),
            jwt_1.JwtModule.registerAsync({
                imports: [config_1.ConfigModule],
                inject: [config_1.ConfigService],
                useFactory: (config) => ({
                    global: true,
                    secret: config.get('APP_SECRET', 'secretKeys'),
                    signOptions: { expiresIn: '30d' },
                }),
            }),
        ],
        exports: [config_1.ConfigModule, jwt_1.JwtModule],
        providers: [excel_service_1.ExcelService],
    })
], CommonModule);
exports.CommonModule = CommonModule;
//# sourceMappingURL=common.module.js.map