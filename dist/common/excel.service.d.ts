import { PDFOptions } from 'puppeteer';
type Props = {
    data: Array<Record<string, any>>;
    name: string;
};
type PropsMultipleSheet = {
    data: Record<string, Array<Record<string, any>>>;
    sheet: string[];
    name: string;
};
type PdfProps = {
    url: string;
    name: string;
    opt?: PDFOptions;
};
export declare class ExcelService {
    download({ data, name }: Props): Promise<string>;
    downloadMultipleSheets({ data, name, sheet, }: PropsMultipleSheet): Promise<string>;
    pdfDownload({ url, name, opt }: PdfProps): Promise<string>;
}
export {};
