"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExcelService = void 0;
const common_1 = require("@nestjs/common");
const dayjs_1 = __importDefault(require("dayjs"));
const exceljs_1 = require("exceljs");
const path_1 = require("path");
const process_1 = require("process");
const puppeteer_1 = __importDefault(require("puppeteer"));
let ExcelService = class ExcelService {
    async download({ data, name }) {
        const headers = Array.from(Object.keys(data[0]));
        headers.unshift('no');
        const book = new exceljs_1.Workbook();
        const sheet = book.addWorksheet();
        sheet.columns = headers
            .filter((key) => key != 'id')
            .map((key) => ({
            key,
            header: key
                .split('_')
                .map((column) => column.charAt(0).toUpperCase() + column.slice(1))
                .join(' '),
        }));
        data.forEach((item, idx) => sheet.addRow(Object.assign({ no: idx + 1 }, item)));
        const lastRow = sheet.actualRowCount;
        sheet.eachRow((row, rowNumber) => {
            if (rowNumber > 1) {
                row.eachCell((cell) => {
                    cell.style = {
                        border: {
                            top: { style: 'thin' },
                            left: { style: 'thin' },
                            right: { style: 'thin' },
                            bottom: { style: 'thin' },
                        },
                    };
                });
            }
            if (rowNumber === 1) {
                row.eachCell((cell) => {
                    cell.style = {
                        font: {
                            bold: true,
                        },
                        fill: {
                            type: 'pattern',
                            pattern: 'solid',
                            fgColor: {
                                argb: '92D050',
                            },
                        },
                        border: {
                            top: { style: 'thin' },
                            left: { style: 'thin' },
                            right: { style: 'thin' },
                            bottom: { style: 'double' },
                        },
                    };
                });
            }
        });
        sheet.getCell(`B${lastRow + 2}`).alignment = { horizontal: 'center' };
        sheet.getCell(`B${lastRow + 2}`).value = (0, dayjs_1.default)().format('DD MMMM YYYY');
        sheet.getCell(`B${lastRow + 3}`).alignment = { horizontal: 'center' };
        sheet.getCell(`B${lastRow + 3}`).value = `Bupati Sorong`;
        sheet.getCell(`B${lastRow + 7}`).alignment = { horizontal: 'center' };
        sheet.getCell(`B${lastRow + 7}`).value = `Petahana Yan Piet Moso`;
        const exportPath = (0, path_1.join)((0, process_1.cwd)(), 'public', 'tmp', `${new Date().getTime().toString()}-${name}.xlsx`);
        try {
            await book.xlsx.writeFile(exportPath);
        }
        catch (error) {
            throw error;
        }
        return exportPath;
    }
    async downloadMultipleSheets({ data, name, sheet, }) {
        const book = new exceljs_1.Workbook();
        for (let i = 0; i < sheet.length; i++) {
            book.addWorksheet(sheet[i]);
        }
        book.eachSheet((sheet) => {
            const exportedData = data[sheet.name];
            const headers = Array.from(Object.keys(exportedData[0]));
            const rows = [];
            let i = 0;
            while (i < exportedData.length) {
                rows.push(Object.values(exportedData[i]));
                i++;
            }
            sheet.columns = headers.map((key) => ({
                key,
                header: key
                    .split('_')
                    .map((column) => column.charAt(0).toUpperCase() + column.slice(1))
                    .join(' '),
            }));
            exportedData.forEach((item) => sheet.addRow(item));
        });
        const exportPath = (0, path_1.join)((0, process_1.cwd)(), 'public', 'tmp', `${new Date().getTime().toString()}-${name}.xlsx`);
        try {
            await book.xlsx.writeFile(exportPath);
        }
        catch (error) {
            throw error;
        }
        return exportPath;
    }
    async pdfDownload({ url, name, opt }) {
        try {
            const browser = await puppeteer_1.default.launch({
                headless: 'new',
                args: [
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                    '--disable-dev-shm-usage',
                ],
            });
            const page = await browser.newPage();
            await page.goto(url, { waitUntil: 'networkidle0' });
            await page.emulateMediaType('screen');
            const exportPath = (0, path_1.join)((0, process_1.cwd)(), 'public', 'tmp', `${new Date().getTime().toString()}-${name}.pdf`);
            const pdf = await page.pdf(Object.assign({ path: exportPath, format: 'A4', displayHeaderFooter: false, landscape: true, printBackground: true }, opt));
            await browser.close();
            return exportPath;
        }
        catch (error) {
            throw error;
        }
    }
};
ExcelService = __decorate([
    (0, common_1.Injectable)()
], ExcelService);
exports.ExcelService = ExcelService;
//# sourceMappingURL=excel.service.js.map