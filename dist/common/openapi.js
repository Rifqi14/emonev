"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SwaggerAuthResponse = exports.SwaggerPaginationResponse = exports.SwaggerOkResponse = exports.OpenAPIDoc = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const api_response_1 = require("./api-response");
const OpenAPIDoc = ({ app, description, endpoint, title, }) => {
    const config = new swagger_1.DocumentBuilder()
        .setTitle(title)
        .setDescription(description)
        .addBearerAuth({ type: 'http', scheme: 'bearer', bearerFormat: 'JWT', name: 'bearer' }, 'bearer')
        .build();
    const options = {
        customSiteTitle: title,
        swaggerOptions: {
            docExpansion: 'none',
            tagsSorter: 'alpha',
            persistAuthorization: true,
            syntaxHighlight: {
                active: true,
                theme: 'monokai',
            },
        },
    };
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup(endpoint, app, document, options);
};
exports.OpenAPIDoc = OpenAPIDoc;
const SwaggerOkResponse = (data, props) => {
    if (typeof data !== 'function') {
        return (0, common_1.applyDecorators)((0, swagger_1.ApiOkResponse)({
            schema: {
                properties: {
                    statusCode: {
                        type: 'number',
                        example: 200,
                    },
                    message: {
                        type: 'string',
                    },
                    data: {
                        type: typeof data,
                    },
                },
            },
        }));
    }
    return (0, common_1.applyDecorators)((0, swagger_1.ApiExtraModels)(data), (0, swagger_1.ApiOkResponse)({
        schema: {
            allOf: [{ $ref: (0, swagger_1.getSchemaPath)(data) }],
        },
    }));
};
exports.SwaggerOkResponse = SwaggerOkResponse;
const SwaggerPaginationResponse = (data) => (0, common_1.applyDecorators)((0, swagger_1.ApiExtraModels)(api_response_1.BaseApiResponse, data, api_response_1.DataWithPaginationApiResponse), (0, swagger_1.ApiOkResponse)({
    schema: {
        allOf: [
            { $ref: (0, swagger_1.getSchemaPath)(api_response_1.BaseApiResponse) },
            {
                properties: {
                    data: {
                        allOf: [
                            { $ref: (0, swagger_1.getSchemaPath)(api_response_1.DataWithPaginationApiResponse) },
                            {
                                properties: {
                                    result: {
                                        type: 'array',
                                        items: { $ref: (0, swagger_1.getSchemaPath)(data) },
                                    },
                                },
                            },
                        ],
                    },
                },
            },
        ],
    },
}));
exports.SwaggerPaginationResponse = SwaggerPaginationResponse;
const SwaggerAuthResponse = () => (0, common_1.applyDecorators)((0, swagger_1.ApiOkResponse)({
    schema: {
        properties: {
            statusCode: {
                type: 'number',
                example: 200,
            },
            access_token: {
                type: 'string',
            },
            payloadClient: {
                properties: {
                    id: { type: 'number' },
                    username: { type: 'string' },
                    name: { type: 'string' },
                    organization_id: { type: 'number' },
                    admin_role_id: { type: 'number' },
                    status_id: { type: 'number' },
                    created_at: { type: 'string' },
                    updated_at: { type: 'string' },
                },
            },
        },
    },
}));
exports.SwaggerAuthResponse = SwaggerAuthResponse;
//# sourceMappingURL=openapi.js.map