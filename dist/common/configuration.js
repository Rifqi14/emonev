"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.appEnv = exports.envVariables = exports.NodeEnvStage = void 0;
const zod_1 = require("zod");
exports.NodeEnvStage = ['development', 'production'];
const databaseVariables = {
    DATABASE_HOST: zod_1.z.string(),
    DATABASE_PORT: zod_1.z.number(),
    DATABASE_USER: zod_1.z.string(),
    DATABASE_PASS: zod_1.z.string(),
    DATABASE_NAME: zod_1.z.string(),
};
const emailVariables = {
    MAIL_HOST: zod_1.z.string(),
    MAIL_USER: zod_1.z.string(),
    MAIL_PASS: zod_1.z.string(),
    MAIL_FROM: zod_1.z.string(),
};
exports.envVariables = zod_1.z.object(Object.assign(Object.assign({ NODE_ENV: zod_1.z.enum(exports.NodeEnvStage), APP_PORT: zod_1.z.string(), APP_HOST: zod_1.z.string(), SWAGGER_DOC_URL: zod_1.z.string(), APP_SECRET: zod_1.z.string() }, databaseVariables), emailVariables));
exports.appEnv = exports.envVariables.parse(process.env);
//# sourceMappingURL=configuration.js.map