import { ArgumentsHost, ExceptionFilter, Logger } from '@nestjs/common';
export declare class BaseApiResponse {
    statusCode: number;
    message: string;
}
export declare class DataApiResponse<TData> {
    result: TData;
}
export declare class DataWithPaginationApiResponse<TData> extends DataApiResponse<TData> {
    total: number;
    page: number;
    pages: number;
}
export declare class GlobalExceptionFilter implements ExceptionFilter {
    private readonly logger;
    constructor(logger: Logger);
    catch(exception: any, host: ArgumentsHost): void;
}
