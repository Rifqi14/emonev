import { INestApplication, Type } from '@nestjs/common';
type OpenAPIProps = {
    title: string;
    endpoint: string;
    description: string;
    app: INestApplication;
};
export declare const OpenAPIDoc: ({ app, description, endpoint, title, }: OpenAPIProps) => void;
type Opts = {
    isArray?: boolean;
};
export declare const SwaggerOkResponse: <TData extends string | number | Type<unknown> | undefined>(data: TData, props?: Opts) => <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol | undefined, descriptor?: TypedPropertyDescriptor<Y> | undefined) => void;
export declare const SwaggerPaginationResponse: <TData extends Type<unknown>>(data: TData) => <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol | undefined, descriptor?: TypedPropertyDescriptor<Y> | undefined) => void;
export declare const SwaggerAuthResponse: () => <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol | undefined, descriptor?: TypedPropertyDescriptor<Y> | undefined) => void;
export {};
