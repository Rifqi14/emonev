"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GlobalExceptionFilter = exports.DataWithPaginationApiResponse = exports.DataApiResponse = exports.BaseApiResponse = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const nest_winston_1 = require("nest-winston");
const typeorm_1 = require("typeorm");
class BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: 200 }),
    __metadata("design:type", Number)
], BaseApiResponse.prototype, "statusCode", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseApiResponse.prototype, "message", void 0);
exports.BaseApiResponse = BaseApiResponse;
class DataApiResponse {
}
exports.DataApiResponse = DataApiResponse;
class DataWithPaginationApiResponse extends DataApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DataWithPaginationApiResponse.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DataWithPaginationApiResponse.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DataWithPaginationApiResponse.prototype, "pages", void 0);
exports.DataWithPaginationApiResponse = DataWithPaginationApiResponse;
let GlobalExceptionFilter = class GlobalExceptionFilter {
    constructor(logger) {
        this.logger = logger;
    }
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        let message = exception.message;
        this.logger.error(message);
        switch (exception.constructor) {
            case typeorm_1.QueryFailedError:
                message = exception.message;
                break;
            case typeorm_1.EntityNotFoundError:
                message =
                    process.env.NODE_ENV === 'production'
                        ? exception.message.split(':')[0]
                        : exception.message;
                break;
            case common_1.BadRequestException:
                message = exception.message;
                break;
        }
        response
            .status(response.statusCode)
            .json({ statusCode: 400, message: message });
    }
};
GlobalExceptionFilter = __decorate([
    (0, common_1.Catch)(),
    __param(0, (0, common_1.Inject)(nest_winston_1.WINSTON_MODULE_PROVIDER)),
    __metadata("design:paramtypes", [common_1.Logger])
], GlobalExceptionFilter);
exports.GlobalExceptionFilter = GlobalExceptionFilter;
//# sourceMappingURL=api-response.js.map