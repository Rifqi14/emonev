import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateMasterOccassionTable1694751050540 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
