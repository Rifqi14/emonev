import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateProcurementTypeTable1695312018827 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
