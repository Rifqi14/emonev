import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddFileColumnToDataTriwulanTable1695393107298 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
