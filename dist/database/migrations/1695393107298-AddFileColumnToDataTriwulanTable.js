"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddFileColumnToDataTriwulanTable1695393107298 = void 0;
class AddFileColumnToDataTriwulanTable1695393107298 {
    constructor() {
        this.name = 'AddFileColumnToDataTriwulanTable1695393107298';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "file" text`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "file"`);
    }
}
exports.AddFileColumnToDataTriwulanTable1695393107298 = AddFileColumnToDataTriwulanTable1695393107298;
//# sourceMappingURL=1695393107298-AddFileColumnToDataTriwulanTable.js.map