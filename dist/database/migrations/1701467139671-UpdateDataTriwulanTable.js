"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateDataTriwulanTable1701467139671 = void 0;
class UpdateDataTriwulanTable1701467139671 {
    constructor() {
        this.name = 'UpdateDataTriwulanTable1701467139671';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "contract_date" date`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "pic_name" character varying`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "leader_name" character varying`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "optional" character varying`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "reason" text`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "reason"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "optional"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "leader_name"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "pic_name"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "contract_date"`);
    }
}
exports.UpdateDataTriwulanTable1701467139671 = UpdateDataTriwulanTable1701467139671;
//# sourceMappingURL=1701467139671-UpdateDataTriwulanTable.js.map