"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateMasterTable1694751591203 = void 0;
class CreateMasterTable1694751591203 {
    constructor() {
        this.name = 'CreateMasterTable1694751591203';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "master" ("id" BIGSERIAL NOT NULL, "description" text, "triwulan_id" bigint, "organization_id" integer, "purpose_id" bigint, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_master" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "master" ADD CONSTRAINT "fk_triwulan_id_master" FOREIGN KEY ("triwulan_id") REFERENCES "triwulan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "master" ADD CONSTRAINT "fk_purpose_id_master" FOREIGN KEY ("purpose_id") REFERENCES "purpose"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "master" ADD CONSTRAINT "fk_organization_id_master" FOREIGN KEY ("organization_id") REFERENCES "organization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "master" DROP CONSTRAINT "fk_organization_id_master"`);
        await queryRunner.query(`ALTER TABLE "master" DROP CONSTRAINT "fk_purpose_id_master"`);
        await queryRunner.query(`ALTER TABLE "master" DROP CONSTRAINT "fk_triwulan_id_master"`);
        await queryRunner.query(`DROP TABLE "master"`);
    }
}
exports.CreateMasterTable1694751591203 = CreateMasterTable1694751591203;
//# sourceMappingURL=1694751591203-CreateMasterTable.js.map