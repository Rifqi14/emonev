"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddSubActivityToActivityTable1695886981496 = void 0;
class AddSubActivityToActivityTable1695886981496 {
    constructor() {
        this.name = 'AddSubActivityToActivityTable1695886981496';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "activity" ADD "sub_activity" character varying`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "activity" DROP COLUMN "sub_activity"`);
    }
}
exports.AddSubActivityToActivityTable1695886981496 = AddSubActivityToActivityTable1695886981496;
//# sourceMappingURL=1695886981496-AddSubActivityToActivityTable.js.map