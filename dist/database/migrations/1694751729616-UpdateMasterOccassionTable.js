"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateMasterOccassionTable1694751729616 = void 0;
class UpdateMasterOccassionTable1694751729616 {
    constructor() {
        this.name = 'UpdateMasterOccassionTable1694751729616';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "master_occassion" ADD CONSTRAINT "fk_master_id_master_occassion" FOREIGN KEY ("master_id") REFERENCES "master"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "master_occassion" DROP CONSTRAINT "fk_master_id_master_occassion"`);
    }
}
exports.UpdateMasterOccassionTable1694751729616 = UpdateMasterOccassionTable1694751729616;
//# sourceMappingURL=1694751729616-UpdateMasterOccassionTable.js.map