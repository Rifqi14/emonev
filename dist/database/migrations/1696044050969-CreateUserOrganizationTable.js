"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateUserOrganizationTable1696044050969 = void 0;
class CreateUserOrganizationTable1696044050969 {
    constructor() {
        this.name = 'CreateUserOrganizationTable1696044050969';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "user_organization" ("user_id" bigint NOT NULL, "organization_id" integer NOT NULL, CONSTRAINT "pk_user_organization" PRIMARY KEY ("user_id", "organization_id"))`);
        await queryRunner.query(`ALTER TABLE "user_organization" ADD CONSTRAINT "fk_organization_id_user_organization" FOREIGN KEY ("organization_id") REFERENCES "organization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_organization" ADD CONSTRAINT "fk_user_id_user_organization" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user_organization" DROP CONSTRAINT "fk_user_id_user_organization"`);
        await queryRunner.query(`ALTER TABLE "user_organization" DROP CONSTRAINT "fk_organization_id_user_organization"`);
        await queryRunner.query(`DROP TABLE "user_organization"`);
    }
}
exports.CreateUserOrganizationTable1696044050969 = CreateUserOrganizationTable1696044050969;
//# sourceMappingURL=1696044050969-CreateUserOrganizationTable.js.map