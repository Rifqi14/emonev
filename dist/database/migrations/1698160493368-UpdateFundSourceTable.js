"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateFundSourceTable1698160493368 = void 0;
class UpdateFundSourceTable1698160493368 {
    constructor() {
        this.name = 'UpdateFundSourceTable1698160493368';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "fund_sources" ADD "fund_source_total" numeric(20,2) DEFAULT '0'`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "fund_sources" DROP COLUMN "fund_source_total"`);
    }
}
exports.UpdateFundSourceTable1698160493368 = UpdateFundSourceTable1698160493368;
//# sourceMappingURL=1698160493368-UpdateFundSourceTable.js.map