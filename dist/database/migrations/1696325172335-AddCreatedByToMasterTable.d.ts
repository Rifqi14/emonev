import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddCreatedByToMasterTable1696325172335 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
