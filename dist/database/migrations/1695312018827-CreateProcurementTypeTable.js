"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateProcurementTypeTable1695312018827 = void 0;
class CreateProcurementTypeTable1695312018827 {
    constructor() {
        this.name = 'CreateProcurementTypeTable1695312018827';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "procurement_type" ("id" BIGSERIAL NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_procurement_type" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "procurement_type"`);
    }
}
exports.CreateProcurementTypeTable1695312018827 = CreateProcurementTypeTable1695312018827;
//# sourceMappingURL=1695312018827-CreateProcurementTypeTable.js.map