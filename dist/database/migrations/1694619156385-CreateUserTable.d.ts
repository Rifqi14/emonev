import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateUserTable1694619156385 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
