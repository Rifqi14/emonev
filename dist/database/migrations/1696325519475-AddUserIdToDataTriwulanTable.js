"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddUserIdToDataTriwulanTable1696325519475 = void 0;
class AddUserIdToDataTriwulanTable1696325519475 {
    constructor() {
        this.name = 'AddUserIdToDataTriwulanTable1696325519475';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "user_id" bigint`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD CONSTRAINT "fk_user_id_data_triwulan" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP CONSTRAINT "fk_user_id_data_triwulan"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "user_id"`);
    }
}
exports.AddUserIdToDataTriwulanTable1696325519475 = AddUserIdToDataTriwulanTable1696325519475;
//# sourceMappingURL=1696325519475-AddUserIdToDataTriwulanTable.js.map