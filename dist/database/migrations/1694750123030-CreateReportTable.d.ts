import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateReportTable1694750123030 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
