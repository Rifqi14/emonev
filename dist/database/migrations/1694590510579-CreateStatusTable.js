"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateStatusTable1694590510579 = void 0;
class CreateStatusTable1694590510579 {
    constructor() {
        this.name = 'CreateStatusTable1694590510579';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "status" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_status" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "status"`);
    }
}
exports.CreateStatusTable1694590510579 = CreateStatusTable1694590510579;
//# sourceMappingURL=1694590510579-CreateStatusTable.js.map