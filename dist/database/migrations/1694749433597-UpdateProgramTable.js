"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateProgramTable1694749433597 = void 0;
class UpdateProgramTable1694749433597 {
    constructor() {
        this.name = 'UpdateProgramTable1694749433597';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "program" DROP CONSTRAINT "fk_status_id_program"`);
        await queryRunner.query(`ALTER TABLE "program" ALTER COLUMN "status_id" DROP NOT NULL`);
        await queryRunner.query(`CREATE UNIQUE INDEX "idx_code_unique_program" ON "program" ("code") `);
        await queryRunner.query(`ALTER TABLE "program" ADD CONSTRAINT "fk_status_id_program" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "program" DROP CONSTRAINT "fk_status_id_program"`);
        await queryRunner.query(`DROP INDEX "public"."idx_code_unique_program"`);
        await queryRunner.query(`ALTER TABLE "program" ALTER COLUMN "status_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "program" ADD CONSTRAINT "fk_status_id_program" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
}
exports.UpdateProgramTable1694749433597 = UpdateProgramTable1694749433597;
//# sourceMappingURL=1694749433597-UpdateProgramTable.js.map