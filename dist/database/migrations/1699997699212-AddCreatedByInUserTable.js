"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddCreatedByInUserTable1699997699212 = void 0;
class AddCreatedByInUserTable1699997699212 {
    constructor() {
        this.name = 'AddCreatedByInUserTable1699997699212';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user" ADD "created_by" bigint`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "fk_created_by_user" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "fk_created_by_user"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "created_by"`);
    }
}
exports.AddCreatedByInUserTable1699997699212 = AddCreatedByInUserTable1699997699212;
//# sourceMappingURL=1699997699212-AddCreatedByInUserTable.js.map