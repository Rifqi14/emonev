import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateUserOrganizationTable1696044050969 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
