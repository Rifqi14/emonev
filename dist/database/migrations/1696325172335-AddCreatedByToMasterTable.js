"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddCreatedByToMasterTable1696325172335 = void 0;
class AddCreatedByToMasterTable1696325172335 {
    constructor() {
        this.name = 'AddCreatedByToMasterTable1696325172335';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "master" ADD "user_id" bigint`);
        await queryRunner.query(`ALTER TABLE "master" ADD CONSTRAINT "fk_user_id_master" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "master" DROP CONSTRAINT "fk_user_id_master"`);
        await queryRunner.query(`ALTER TABLE "master" DROP COLUMN "user_id"`);
    }
}
exports.AddCreatedByToMasterTable1696325172335 = AddCreatedByToMasterTable1696325172335;
//# sourceMappingURL=1696325172335-AddCreatedByToMasterTable.js.map