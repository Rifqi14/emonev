"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChangeCreatedByInUserTable1699997897813 = void 0;
class ChangeCreatedByInUserTable1699997897813 {
    constructor() {
        this.name = 'ChangeCreatedByInUserTable1699997897813';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "fk_created_by_user"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "created_by"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "created_by" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "fk_created_by_user" FOREIGN KEY ("created_by") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "fk_created_by_user"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "created_by"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "created_by" bigint`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "fk_created_by_user" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
}
exports.ChangeCreatedByInUserTable1699997897813 = ChangeCreatedByInUserTable1699997897813;
//# sourceMappingURL=1699997897813-ChangeCreatedByInUserTable.js.map