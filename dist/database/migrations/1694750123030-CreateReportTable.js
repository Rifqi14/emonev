"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateReportTable1694750123030 = void 0;
class CreateReportTable1694750123030 {
    constructor() {
        this.name = 'CreateReportTable1694750123030';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "report" ("id" BIGSERIAL NOT NULL, "description" text, "program_description" text, "triwulan_id" bigint, "organization_id" integer, "occassion_id" bigint, "program_id" bigint, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_report" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "report" ADD CONSTRAINT "fk_triwulan_id_report" FOREIGN KEY ("triwulan_id") REFERENCES "triwulan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "report" ADD CONSTRAINT "fk_organization_id_report" FOREIGN KEY ("organization_id") REFERENCES "organization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "report" ADD CONSTRAINT "fk_occassion_id_report" FOREIGN KEY ("occassion_id") REFERENCES "occassion"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "report" ADD CONSTRAINT "fk_program_id_report" FOREIGN KEY ("program_id") REFERENCES "program"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "report" DROP CONSTRAINT "fk_program_id_report"`);
        await queryRunner.query(`ALTER TABLE "report" DROP CONSTRAINT "fk_occassion_id_report"`);
        await queryRunner.query(`ALTER TABLE "report" DROP CONSTRAINT "fk_organization_id_report"`);
        await queryRunner.query(`ALTER TABLE "report" DROP CONSTRAINT "fk_triwulan_id_report"`);
        await queryRunner.query(`DROP TABLE "report"`);
    }
}
exports.CreateReportTable1694750123030 = CreateReportTable1694750123030;
//# sourceMappingURL=1694750123030-CreateReportTable.js.map