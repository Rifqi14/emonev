import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddUserIdToDataTriwulanTable1696325519475 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
