"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateDataTriwulanTable1698162234580 = void 0;
class UpdateDataTriwulanTable1698162234580 {
    constructor() {
        this.name = 'UpdateDataTriwulanTable1698162234580';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "activity_id" bigint`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "activity_form" character varying`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD CONSTRAINT "fk_activity_id_data_triwulan" FOREIGN KEY ("activity_id") REFERENCES "activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP CONSTRAINT "fk_activity_id_data_triwulan"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "activity_form"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "activity_id"`);
    }
}
exports.UpdateDataTriwulanTable1698162234580 = UpdateDataTriwulanTable1698162234580;
//# sourceMappingURL=1698162234580-UpdateDataTriwulanTable.js.map