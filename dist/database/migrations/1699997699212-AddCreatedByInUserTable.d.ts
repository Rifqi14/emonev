import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddCreatedByInUserTable1699997699212 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
