import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateFundSourcesTable1695310615977 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
