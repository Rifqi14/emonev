import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateActivityTable1694750655842 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
