import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateOccassionTable1694749207413 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
