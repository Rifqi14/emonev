"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemoveRelationInDataTriwulanTable1696326526279 = void 0;
class RemoveRelationInDataTriwulanTable1696326526279 {
    constructor() {
        this.name = 'RemoveRelationInDataTriwulanTable1696326526279';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP CONSTRAINT "fk_procurement_method_id_data_triwulan"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP CONSTRAINT "fk_procurement_type_id_data_triwulan"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "procurement_type_id"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "procurement_method_id"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "procurement_type" character varying`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "procurement_method" character varying`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "procurement_method"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "procurement_type"`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "procurement_method_id" bigint`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "procurement_type_id" bigint`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD CONSTRAINT "fk_procurement_type_id_data_triwulan" FOREIGN KEY ("procurement_type_id") REFERENCES "procurement_type"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "data_triwulan" ADD CONSTRAINT "fk_procurement_method_id_data_triwulan" FOREIGN KEY ("procurement_method_id") REFERENCES "procurement_method"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
}
exports.RemoveRelationInDataTriwulanTable1696326526279 = RemoveRelationInDataTriwulanTable1696326526279;
//# sourceMappingURL=1696326526279-RemoveRelationInDataTriwulanTable.js.map