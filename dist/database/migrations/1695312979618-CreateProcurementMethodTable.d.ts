import { MigrationInterface, QueryRunner } from "typeorm";
export declare class CreateProcurementMethodTable1695312979618 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
