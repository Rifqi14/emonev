import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddRelationInProgramToOccassionTable1695218576411 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
