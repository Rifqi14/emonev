"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateUserTable1694619156385 = void 0;
class CreateUserTable1694619156385 {
    constructor() {
        this.name = 'CreateUserTable1694619156385';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "user" ("id" BIGSERIAL NOT NULL, "username" character varying NOT NULL, "password" character varying NOT NULL, "name" character varying, "organization_id" integer, "role_id" integer NOT NULL, "status_id" integer, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_user" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "idx_username_unique" ON "user" ("username") `);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "fk_organization_id_user" FOREIGN KEY ("organization_id") REFERENCES "organization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "fk_role_id_user" FOREIGN KEY ("role_id") REFERENCES "role"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "fk_status_id_user" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "fk_status_id_user"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "fk_role_id_user"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "fk_organization_id_user"`);
        await queryRunner.query(`DROP INDEX "public"."idx_username_unique"`);
        await queryRunner.query(`DROP TABLE "user"`);
    }
}
exports.CreateUserTable1694619156385 = CreateUserTable1694619156385;
//# sourceMappingURL=1694619156385-CreateUserTable.js.map