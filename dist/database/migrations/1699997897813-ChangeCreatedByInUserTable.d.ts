import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class ChangeCreatedByInUserTable1699997897813 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
