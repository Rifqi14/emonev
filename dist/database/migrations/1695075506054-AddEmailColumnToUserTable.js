"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddEmailColumnToUserTable1695075506054 = void 0;
class AddEmailColumnToUserTable1695075506054 {
    constructor() {
        this.name = 'AddEmailColumnToUserTable1695075506054';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "user" ADD "email" character varying`);
        await queryRunner.query(`CREATE UNIQUE INDEX "idx_email_unique_user" ON "user" ("email") `);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP INDEX "public"."idx_email_unique_user"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "email"`);
    }
}
exports.AddEmailColumnToUserTable1695075506054 = AddEmailColumnToUserTable1695075506054;
//# sourceMappingURL=1695075506054-AddEmailColumnToUserTable.js.map