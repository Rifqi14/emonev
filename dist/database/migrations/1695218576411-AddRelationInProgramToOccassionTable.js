"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddRelationInProgramToOccassionTable1695218576411 = void 0;
class AddRelationInProgramToOccassionTable1695218576411 {
    constructor() {
        this.name = 'AddRelationInProgramToOccassionTable1695218576411';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE "program" ADD "occassion_id" bigint`);
        await queryRunner.query(`ALTER TABLE "program" ADD CONSTRAINT "fk_occassion_id_program" FOREIGN KEY ("occassion_id") REFERENCES "occassion"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "program" DROP CONSTRAINT "fk_occassion_id_program"`);
        await queryRunner.query(`ALTER TABLE "program" DROP COLUMN "occassion_id"`);
    }
}
exports.AddRelationInProgramToOccassionTable1695218576411 = AddRelationInProgramToOccassionTable1695218576411;
//# sourceMappingURL=1695218576411-AddRelationInProgramToOccassionTable.js.map