"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateProgramTable1694748593805 = void 0;
class CreateProgramTable1694748593805 {
    constructor() {
        this.name = 'CreateProgramTable1694748593805';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "program" ("id" BIGSERIAL NOT NULL, "title" character varying NOT NULL, "code" character varying NOT NULL, "status_id" integer NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_program" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "program" ADD CONSTRAINT "fk_status_id_program" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "program" DROP CONSTRAINT "fk_status_id_program"`);
        await queryRunner.query(`DROP TABLE "program"`);
    }
}
exports.CreateProgramTable1694748593805 = CreateProgramTable1694748593805;
//# sourceMappingURL=1694748593805-CreateProgramTable.js.map