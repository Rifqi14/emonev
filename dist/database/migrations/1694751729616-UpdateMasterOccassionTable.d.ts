import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class UpdateMasterOccassionTable1694751729616 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
