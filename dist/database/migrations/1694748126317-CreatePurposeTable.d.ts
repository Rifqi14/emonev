import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreatePurposeTable1694748126317 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
