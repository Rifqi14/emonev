import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class RemoveRelationInDataTriwulanTable1696326526279 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
