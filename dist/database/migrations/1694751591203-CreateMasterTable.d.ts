import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateMasterTable1694751591203 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
