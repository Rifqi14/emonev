"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateRoleTable1694617845458 = void 0;
class CreateRoleTable1694617845458 {
    constructor() {
        this.name = 'CreateRoleTable1694617845458';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "role" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_role" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "role"`);
    }
}
exports.CreateRoleTable1694617845458 = CreateRoleTable1694617845458;
//# sourceMappingURL=1694617845458-CreateRoleTable.js.map