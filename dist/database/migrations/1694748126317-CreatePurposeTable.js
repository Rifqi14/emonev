"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePurposeTable1694748126317 = void 0;
class CreatePurposeTable1694748126317 {
    constructor() {
        this.name = 'CreatePurposeTable1694748126317';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "purpose" ("id" BIGSERIAL NOT NULL, "title" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_purpose" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "purpose"`);
    }
}
exports.CreatePurposeTable1694748126317 = CreatePurposeTable1694748126317;
//# sourceMappingURL=1694748126317-CreatePurposeTable.js.map