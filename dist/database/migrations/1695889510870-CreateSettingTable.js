"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateSettingTable1695889510870 = void 0;
class CreateSettingTable1695889510870 {
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "setting" ("id" BIGSERIAL NOT NULL, "key" character varying NOT NULL, "value" text NOT NULL, "data_type" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_setting" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "setting"`);
    }
}
exports.CreateSettingTable1695889510870 = CreateSettingTable1695889510870;
//# sourceMappingURL=1695889510870-CreateSettingTable.js.map