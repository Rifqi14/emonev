"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateOrganizationTable1694618266524 = void 0;
class CreateOrganizationTable1694618266524 {
    constructor() {
        this.name = 'CreateOrganizationTable1694618266524';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "organization" ("id" SERIAL NOT NULL, "code" character varying NOT NULL, "title" character varying NOT NULL, "status_id" integer NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_organization" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "idx_code_unique" ON "organization" ("code") `);
        await queryRunner.query(`ALTER TABLE "organization" ADD CONSTRAINT "fk_status_id_organization" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "organization" DROP CONSTRAINT "fk_status_id_organization"`);
        await queryRunner.query(`DROP INDEX "public"."idx_code_unique"`);
        await queryRunner.query(`DROP TABLE "organization"`);
    }
}
exports.CreateOrganizationTable1694618266524 = CreateOrganizationTable1694618266524;
//# sourceMappingURL=1694618266524-CreateOrganizationTable.js.map