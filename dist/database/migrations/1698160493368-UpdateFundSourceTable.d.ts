import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class UpdateFundSourceTable1698160493368 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
