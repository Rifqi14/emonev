"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTriwulanTable1694747371384 = void 0;
class CreateTriwulanTable1694747371384 {
    constructor() {
        this.name = 'CreateTriwulanTable1694747371384';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "triwulan" ("id" BIGSERIAL NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_triwulan" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "triwulan"`);
    }
}
exports.CreateTriwulanTable1694747371384 = CreateTriwulanTable1694747371384;
//# sourceMappingURL=1694747371384-CreateTriwulanTable.js.map