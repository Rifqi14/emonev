"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateProcurementMethodTable1695312979618 = void 0;
class CreateProcurementMethodTable1695312979618 {
    constructor() {
        this.name = 'CreateProcurementMethodTable1695312979618';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "procurement_method" ("id" BIGSERIAL NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_procurement_method" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "procurement_method"`);
    }
}
exports.CreateProcurementMethodTable1695312979618 = CreateProcurementMethodTable1695312979618;
//# sourceMappingURL=1695312979618-CreateProcurementMethodTable.js.map