import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class AddEmailColumnToUserTable1695075506054 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
