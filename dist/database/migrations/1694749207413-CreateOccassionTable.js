"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateOccassionTable1694749207413 = void 0;
class CreateOccassionTable1694749207413 {
    constructor() {
        this.name = 'CreateOccassionTable1694749207413';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "occassion" ("id" BIGSERIAL NOT NULL, "code" character varying NOT NULL, "title" character varying NOT NULL, "status_id" integer, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_occassion" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "idx_code_unique_occassion" ON "occassion" ("code") `);
        await queryRunner.query(`ALTER TABLE "occassion" ADD CONSTRAINT "fk_status_id_occassion" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "occassion" DROP CONSTRAINT "fk_status_id_occassion"`);
        await queryRunner.query(`DROP INDEX "public"."idx_code_unique_occassion"`);
        await queryRunner.query(`DROP TABLE "occassion"`);
    }
}
exports.CreateOccassionTable1694749207413 = CreateOccassionTable1694749207413;
//# sourceMappingURL=1694749207413-CreateOccassionTable.js.map