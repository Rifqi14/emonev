import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateProgramTable1694748593805 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
