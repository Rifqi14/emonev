"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateFundSourcesTable1695310615977 = void 0;
class CreateFundSourcesTable1695310615977 {
    constructor() {
        this.name = 'CreateFundSourcesTable1695310615977';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "fund_sources" ("id" BIGSERIAL NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_fund_source" PRIMARY KEY ("id"))`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "fund_sources"`);
    }
}
exports.CreateFundSourcesTable1695310615977 = CreateFundSourcesTable1695310615977;
//# sourceMappingURL=1695310615977-CreateFundSourcesTable.js.map