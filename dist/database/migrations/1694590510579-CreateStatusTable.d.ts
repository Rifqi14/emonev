import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class CreateStatusTable1694590510579 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
