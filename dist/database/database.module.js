"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const connection_1 = require("./connection");
const seed_1 = require("./seed");
let DatabaseModule = class DatabaseModule {
};
DatabaseModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forRoot(Object.assign({}, connection_1.connectionOptions))],
        providers: [
            {
                provide: 'DATA_SOURCE',
                useFactory: async () => {
                    const dataSource = new typeorm_2.DataSource(Object.assign({}, connection_1.connectionOptions));
                    const ds = dataSource.initialize();
                    ds.then(async (ds) => {
                        await (0, seed_1.seed)(ds);
                    });
                    return ds;
                },
            },
        ],
        exports: [
            {
                provide: 'DATA_SOURCE',
                useExisting: true,
            },
        ],
    })
], DatabaseModule);
exports.DatabaseModule = DatabaseModule;
//# sourceMappingURL=database.module.js.map