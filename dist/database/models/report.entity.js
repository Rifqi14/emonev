"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Report = void 0;
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const triwulan_entity_1 = require("./triwulan.entity");
const organization_entity_1 = require("./organization.entity");
const occassion_entity_1 = require("./occassion.entity");
const program_entity_1 = require("./program.entity");
let Report = class Report extends typeorm_1.BaseEntity {
};
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', {
        type: 'int8',
        primaryKeyConstraintName: 'pk_report',
    }),
    __metadata("design:type", Number)
], Report.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('text', { nullable: true }),
    __metadata("design:type", String)
], Report.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('text', { nullable: true }),
    __metadata("design:type", String)
], Report.prototype, "program_description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], Report.prototype, "triwulan_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], Report.prototype, "organization_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], Report.prototype, "occassion_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], Report.prototype, "program_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], Report.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], Report.prototype, "updated_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => triwulan_entity_1.Triwulan, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'triwulan_id',
        foreignKeyConstraintName: 'fk_triwulan_id_report',
    }),
    __metadata("design:type", triwulan_entity_1.Triwulan)
], Report.prototype, "triwulan", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => organization_entity_1.Organization, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'organization_id',
        foreignKeyConstraintName: 'fk_organization_id_report',
    }),
    __metadata("design:type", organization_entity_1.Organization)
], Report.prototype, "organization", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => occassion_entity_1.Occassion, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'occassion_id',
        foreignKeyConstraintName: 'fk_occassion_id_report',
    }),
    __metadata("design:type", occassion_entity_1.Occassion)
], Report.prototype, "occassion", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => program_entity_1.Program, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'program_id',
        foreignKeyConstraintName: 'fk_program_id_report',
    }),
    __metadata("design:type", program_entity_1.Program)
], Report.prototype, "program", void 0);
Report = __decorate([
    (0, typeorm_1.Entity)()
], Report);
exports.Report = Report;
//# sourceMappingURL=report.entity.js.map