"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Program = void 0;
const typeorm_1 = require("typeorm");
const status_entity_1 = require("./status.entity");
const swagger_1 = require("@nestjs/swagger");
const occassion_entity_1 = require("./occassion.entity");
let Program = class Program extends typeorm_1.BaseEntity {
};
__decorate([
    (0, swagger_1.ApiProperty)({ type: Number }),
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', {
        type: 'int8',
        primaryKeyConstraintName: 'pk_program',
    }),
    __metadata("design:type", Number)
], Program.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: String }),
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], Program.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: String }),
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], Program.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: Number }),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], Program.prototype, "occassion_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: Number }),
    (0, typeorm_1.Column)('int', { nullable: true }),
    __metadata("design:type", Number)
], Program.prototype, "status_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], Program.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], Program.prototype, "updated_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => occassion_entity_1.Occassion, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        foreignKeyConstraintName: 'fk_occassion_id_program',
        name: 'occassion_id',
    }),
    __metadata("design:type", occassion_entity_1.Occassion)
], Program.prototype, "occassion", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: status_entity_1.Status }),
    (0, typeorm_1.ManyToOne)(() => status_entity_1.Status, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        foreignKeyConstraintName: 'fk_status_id_program',
        name: 'status_id',
    }),
    __metadata("design:type", status_entity_1.Status)
], Program.prototype, "status", void 0);
Program = __decorate([
    (0, typeorm_1.Entity)(),
    (0, typeorm_1.Index)('idx_code_unique_program', ['code'], { unique: true })
], Program);
exports.Program = Program;
//# sourceMappingURL=program.entity.js.map