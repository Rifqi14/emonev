import { BaseEntity } from 'typeorm';
export declare class Purpose extends BaseEntity {
    id: number;
    title: string;
    created_at: Date;
    updated_at: Date;
}
