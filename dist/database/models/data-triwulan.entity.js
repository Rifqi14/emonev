"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataTriwulan = void 0;
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const activity_entity_1 = require("./activity.entity");
const fund_sources_entity_1 = require("./fund_sources.entity");
const user_entity_1 = require("./user.entity");
let DataTriwulan = class DataTriwulan extends typeorm_1.BaseEntity {
};
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', {
        type: 'int8',
        primaryKeyConstraintName: 'pk_data_triwulan',
    }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], DataTriwulan.prototype, "activity_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], DataTriwulan.prototype, "activity_location", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "fund_source_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)({ type: 'numeric', scale: 2, precision: 16, default: 0 }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "fund_ceiling", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "management_organization", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "pptk_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "contract_number_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('date', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "contract_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "contractor_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "implementation_period", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)({ type: 'numeric', scale: 2, precision: 16, default: 0 }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "contract_value", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)({ type: 'numeric', scale: 2, precision: 16, default: 0 }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "physical_realization", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)({ type: 'numeric', scale: 2, precision: 16, default: 0 }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "fund_realization", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "activity_volume", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "activity_output", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "direct_target_group", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "indirect_target_group", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)({ type: 'numeric', scale: 2, precision: 16, default: 0 }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "local_workforce", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)({ type: 'numeric', scale: 2, precision: 16, default: 0 }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "non_local_workforce", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "problems", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "solution", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "procurement_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "procurement_method", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('text', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "file", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "user_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], DataTriwulan.prototype, "activity_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "activity_form", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "pic_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "leader_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "optional", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('text', { nullable: true }),
    __metadata("design:type", String)
], DataTriwulan.prototype, "reason", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], DataTriwulan.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], DataTriwulan.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => fund_sources_entity_1.FundSources, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'fund_source_id',
        foreignKeyConstraintName: 'fk_fund_source_id_data_triwulan',
    }),
    __metadata("design:type", fund_sources_entity_1.FundSources)
], DataTriwulan.prototype, "fundSource", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.User, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'user_id',
        foreignKeyConstraintName: 'fk_user_id_data_triwulan',
    }),
    __metadata("design:type", user_entity_1.User)
], DataTriwulan.prototype, "createdBy", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => activity_entity_1.Activity, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'activity_id',
        foreignKeyConstraintName: 'fk_activity_id_data_triwulan',
    }),
    __metadata("design:type", activity_entity_1.Activity)
], DataTriwulan.prototype, "activity", void 0);
DataTriwulan = __decorate([
    (0, typeorm_1.Entity)()
], DataTriwulan);
exports.DataTriwulan = DataTriwulan;
//# sourceMappingURL=data-triwulan.entity.js.map