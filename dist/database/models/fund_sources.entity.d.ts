import { BaseEntity } from 'typeorm';
export declare class FundSources extends BaseEntity {
    id: number;
    name: string;
    fund_source_total?: number;
    created_at: Date;
    updated_at: Date;
}
