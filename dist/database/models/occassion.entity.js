"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Occassion = void 0;
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const status_entity_1 = require("./status.entity");
let Occassion = class Occassion extends typeorm_1.BaseEntity {
};
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', {
        type: 'int8',
        primaryKeyConstraintName: 'pk_occassion',
    }),
    __metadata("design:type", Number)
], Occassion.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], Occassion.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], Occassion.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int', { nullable: true }),
    __metadata("design:type", Number)
], Occassion.prototype, "status_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], Occassion.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], Occassion.prototype, "updated_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: status_entity_1.Status }),
    (0, typeorm_1.ManyToOne)(() => status_entity_1.Status, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'status_id',
        foreignKeyConstraintName: 'fk_status_id_occassion',
    }),
    __metadata("design:type", status_entity_1.Status)
], Occassion.prototype, "status", void 0);
Occassion = __decorate([
    (0, typeorm_1.Entity)(),
    (0, typeorm_1.Index)('idx_code_unique_occassion', ['code'], { unique: true })
], Occassion);
exports.Occassion = Occassion;
//# sourceMappingURL=occassion.entity.js.map