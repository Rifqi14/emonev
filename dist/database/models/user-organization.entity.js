"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserOrganization = void 0;
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const organization_entity_1 = require("./organization.entity");
const user_entity_1 = require("./user.entity");
let UserOrganization = class UserOrganization {
};
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.PrimaryColumn)('int8', { primaryKeyConstraintName: 'pk_user_organization' }),
    __metadata("design:type", Number)
], UserOrganization.prototype, "user_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.PrimaryColumn)('int8', { primaryKeyConstraintName: 'pk_user_organization' }),
    __metadata("design:type", Number)
], UserOrganization.prototype, "organization_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => organization_entity_1.Organization, { eager: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'organization_id',
        foreignKeyConstraintName: 'fk_organization_id_user_organization',
    }),
    __metadata("design:type", organization_entity_1.Organization)
], UserOrganization.prototype, "organization", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.User),
    (0, typeorm_1.JoinColumn)({
        name: 'user_id',
        foreignKeyConstraintName: 'fk_user_id_user_organization',
    }),
    __metadata("design:type", user_entity_1.User)
], UserOrganization.prototype, "user", void 0);
UserOrganization = __decorate([
    (0, typeorm_1.Entity)()
], UserOrganization);
exports.UserOrganization = UserOrganization;
//# sourceMappingURL=user-organization.entity.js.map