import { Organization } from './organization.entity';
import { User } from './user.entity';
export declare class UserOrganization {
    user_id: number;
    organization_id: number;
    organization: Organization;
    user: User;
}
