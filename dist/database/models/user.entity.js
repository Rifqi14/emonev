"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var User_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const swagger_1 = require("@nestjs/swagger");
const bcrypt_1 = require("bcrypt");
const class_transformer_1 = require("class-transformer");
const typeorm_1 = require("typeorm");
const organization_entity_1 = require("./organization.entity");
const role_entity_1 = require("./role.entity");
const status_entity_1 = require("./status.entity");
const user_organization_entity_1 = require("./user-organization.entity");
let User = User_1 = class User extends typeorm_1.BaseEntity {
    createPassword() {
        if (this.password) {
            const salt = (0, bcrypt_1.genSaltSync)(10);
            this.password = (0, bcrypt_1.hashSync)(this.password.replace(' ', ''), salt);
        }
    }
};
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', {
        type: 'int8',
        primaryKeyConstraintName: 'pk_user',
    }),
    __metadata("design:type", Number)
], User.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar'),
    __metadata("design:type", String)
], User.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    (0, class_transformer_1.Exclude)({ toPlainOnly: true }),
    (0, typeorm_1.Column)('varchar', { select: false }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], User.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int', { nullable: true }),
    __metadata("design:type", Number)
], User.prototype, "organization_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int'),
    __metadata("design:type", Number)
], User.prototype, "role_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int', { nullable: true }),
    __metadata("design:type", Number)
], User.prototype, "status_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('varchar', { nullable: true }),
    __metadata("design:type", String)
], User.prototype, "created_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], User.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], User.prototype, "updated_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => User_1, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'created_by',
        foreignKeyConstraintName: 'fk_created_by_user',
        referencedColumnName: 'username',
    }),
    __metadata("design:type", User)
], User.prototype, "createdBy", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => organization_entity_1.Organization, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'organization_id',
        foreignKeyConstraintName: 'fk_organization_id_user',
    }),
    __metadata("design:type", organization_entity_1.Organization)
], User.prototype, "organization", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => role_entity_1.Role),
    (0, typeorm_1.JoinColumn)({ name: 'role_id', foreignKeyConstraintName: 'fk_role_id_user' }),
    __metadata("design:type", role_entity_1.Role)
], User.prototype, "role", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => status_entity_1.Status, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'status_id',
        foreignKeyConstraintName: 'fk_status_id_user',
    }),
    __metadata("design:type", status_entity_1.Status)
], User.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.OneToMany)(() => user_organization_entity_1.UserOrganization, (userOrganization) => userOrganization.user, { cascade: true }),
    __metadata("design:type", Array)
], User.prototype, "userOrganization", void 0);
__decorate([
    (0, typeorm_1.BeforeInsert)(),
    (0, typeorm_1.BeforeUpdate)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], User.prototype, "createPassword", null);
User = User_1 = __decorate([
    (0, typeorm_1.Entity)(),
    (0, typeorm_1.Index)('idx_username_unique', ['username'], { unique: true }),
    (0, typeorm_1.Index)('idx_email_unique_user', ['email'], { unique: true })
], User);
exports.User = User;
//# sourceMappingURL=user.entity.js.map