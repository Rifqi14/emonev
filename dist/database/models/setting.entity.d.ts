import { SettingDataType } from 'src/utils/type';
import { BaseEntity } from 'typeorm';
export declare class Setting extends BaseEntity {
    id: number;
    key: string;
    value: string;
    data_type: SettingDataType;
    created_at: Date;
    updated_at: Date;
}
