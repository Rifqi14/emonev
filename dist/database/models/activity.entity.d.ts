import { BaseEntity } from 'typeorm';
import { Program } from './program.entity';
export declare class Activity extends BaseEntity {
    id: number;
    title: string;
    code: string;
    sub_activity?: string;
    program_id?: number;
    created_at: Date;
    updated_at: Date;
    program?: Program;
}
