"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Master = void 0;
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const triwulan_entity_1 = require("./triwulan.entity");
const purpose_entity_1 = require("./purpose.entity");
const organization_entity_1 = require("./organization.entity");
const master_occassion_entity_1 = require("./master_occassion.entity");
const user_entity_1 = require("./user.entity");
let Master = class Master extends typeorm_1.BaseEntity {
};
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', {
        type: 'int8',
        primaryKeyConstraintName: 'pk_master',
    }),
    __metadata("design:type", Number)
], Master.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('text', { nullable: true }),
    __metadata("design:type", String)
], Master.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], Master.prototype, "triwulan_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], Master.prototype, "organization_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], Master.prototype, "purpose_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], Master.prototype, "user_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], Master.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], Master.prototype, "updated_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => triwulan_entity_1.Triwulan, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'triwulan_id',
        foreignKeyConstraintName: 'fk_triwulan_id_master',
    }),
    __metadata("design:type", triwulan_entity_1.Triwulan)
], Master.prototype, "triwulan", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => purpose_entity_1.Purpose, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'purpose_id',
        foreignKeyConstraintName: 'fk_purpose_id_master',
    }),
    __metadata("design:type", purpose_entity_1.Purpose)
], Master.prototype, "purpose", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => organization_entity_1.Organization, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'organization_id',
        foreignKeyConstraintName: 'fk_organization_id_master',
    }),
    __metadata("design:type", organization_entity_1.Organization)
], Master.prototype, "organization", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [master_occassion_entity_1.MasterOccassion] }),
    (0, typeorm_1.OneToMany)(() => master_occassion_entity_1.MasterOccassion, (masterOccasion) => masterOccasion.master, {
        eager: true,
        cascade: true,
    }),
    __metadata("design:type", Array)
], Master.prototype, "masterOccassions", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => user_entity_1.User, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'user_id',
        foreignKeyConstraintName: 'fk_user_id_master',
    }),
    __metadata("design:type", user_entity_1.User)
], Master.prototype, "createdBy", void 0);
Master = __decorate([
    (0, typeorm_1.Entity)()
], Master);
exports.Master = Master;
//# sourceMappingURL=master.entity.js.map