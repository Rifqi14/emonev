import { BaseEntity } from 'typeorm';
import { Status } from './status.entity';
import { Occassion } from './occassion.entity';
export declare class Program extends BaseEntity {
    id: number;
    title: string;
    code: string;
    occassion_id?: number;
    status_id?: number;
    created_at: Date;
    updated_at: Date;
    occassion?: Occassion;
    status?: Status;
}
