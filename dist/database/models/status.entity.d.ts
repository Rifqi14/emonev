import { BaseEntity } from 'typeorm';
export declare class Status extends BaseEntity {
    id: number;
    name: string;
    created_at: Date;
    updated_at: Date;
}
