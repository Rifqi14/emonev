"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MasterOccassion = void 0;
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const occassion_entity_1 = require("./occassion.entity");
const master_entity_1 = require("./master.entity");
let MasterOccassion = class MasterOccassion extends typeorm_1.BaseEntity {
};
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.PrimaryGeneratedColumn)('increment', {
        type: 'int8',
        primaryKeyConstraintName: 'pk_master_occassion',
    }),
    __metadata("design:type", Number)
], MasterOccassion.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], MasterOccassion.prototype, "master_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Column)('int8', { nullable: true }),
    __metadata("design:type", Number)
], MasterOccassion.prototype, "occassion_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], MasterOccassion.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], MasterOccassion.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => master_entity_1.Master, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'master_id',
        foreignKeyConstraintName: 'fk_master_id_master_occassion',
    }),
    __metadata("design:type", master_entity_1.Master)
], MasterOccassion.prototype, "master", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.ManyToOne)(() => occassion_entity_1.Occassion, { nullable: true }),
    (0, typeorm_1.JoinColumn)({
        name: 'occassion_id',
        foreignKeyConstraintName: 'fk_occassion_id_master_occassion',
    }),
    __metadata("design:type", occassion_entity_1.Occassion)
], MasterOccassion.prototype, "occassion", void 0);
MasterOccassion = __decorate([
    (0, typeorm_1.Entity)()
], MasterOccassion);
exports.MasterOccassion = MasterOccassion;
//# sourceMappingURL=master_occassion.entity.js.map