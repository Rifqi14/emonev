import { BaseEntity } from 'typeorm';
import { Organization } from './organization.entity';
import { Role } from './role.entity';
import { Status } from './status.entity';
import { UserOrganization } from './user-organization.entity';
export declare class User extends BaseEntity {
    id: number;
    username: string;
    email?: string;
    password: string;
    name?: string;
    organization_id?: number;
    role_id: number;
    status_id?: number;
    created_by?: string;
    created_at: Date;
    updated_at: Date;
    createdBy?: User;
    organization?: Organization;
    role: Role;
    status?: Status;
    userOrganization: UserOrganization[];
    createPassword(): void;
}
