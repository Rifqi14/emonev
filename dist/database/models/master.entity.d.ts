import { BaseEntity } from 'typeorm';
import { Triwulan } from './triwulan.entity';
import { Purpose } from './purpose.entity';
import { Organization } from './organization.entity';
import { MasterOccassion } from './master_occassion.entity';
import { User } from './user.entity';
export declare class Master extends BaseEntity {
    id: number;
    description?: string;
    triwulan_id?: number;
    organization_id?: number;
    purpose_id?: number;
    user_id?: number;
    created_at: Date;
    updated_at: Date;
    triwulan?: Triwulan;
    purpose?: Purpose;
    organization?: Organization;
    masterOccassions: MasterOccassion[];
    createdBy?: User;
}
