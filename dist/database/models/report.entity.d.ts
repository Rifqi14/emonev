import { BaseEntity } from 'typeorm';
import { Triwulan } from './triwulan.entity';
import { Organization } from './organization.entity';
import { Occassion } from './occassion.entity';
import { Program } from './program.entity';
export declare class Report extends BaseEntity {
    id: number;
    description?: string;
    program_description?: string;
    triwulan_id?: number;
    organization_id?: number;
    occassion_id?: number;
    program_id?: number;
    created_at: Date;
    updated_at: Date;
    triwulan?: Triwulan;
    organization?: Organization;
    occassion?: Occassion;
    program?: Program;
}
