import { BaseEntity } from 'typeorm';
import { Status } from './status.entity';
export declare class Organization extends BaseEntity {
    id: number;
    code: string;
    title: string;
    status_id: number;
    created_at: Date;
    updated_at: Date;
    status: Status;
}
