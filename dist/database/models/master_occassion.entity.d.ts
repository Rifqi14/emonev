import { BaseEntity } from 'typeorm';
import { Occassion } from './occassion.entity';
import { Master } from './master.entity';
export declare class MasterOccassion extends BaseEntity {
    id: number;
    master_id?: number;
    occassion_id?: number;
    created_at: Date;
    updated_at: Date;
    master?: Master;
    occassion?: Occassion;
}
