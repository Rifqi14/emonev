"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const connection_1 = require("./connection");
exports.default = new typeorm_1.DataSource(connection_1.connectionOptions);
//# sourceMappingURL=typeorm.js.map