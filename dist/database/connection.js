"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.connectionOptions = void 0;
const config_1 = require("@nestjs/config");
const dotenv_1 = require("dotenv");
const path_1 = require("path");
(0, dotenv_1.config)();
const configService = new config_1.ConfigService();
exports.connectionOptions = {
    type: 'postgres',
    host: configService.get('DATABASE_HOST', 'localhost'),
    port: configService.get('DATABASE_PORT', 5432),
    username: configService.get('DATABASE_USER', 'postgres'),
    password: configService.get('DATABASE_PASS', 'postgres'),
    database: configService.get('DATABASE_NAME', 'postgres'),
    entities: [(0, path_1.join)(__dirname, '**', '*.entity{.ts,.js}')],
    migrations: [(0, path_1.join)(__dirname, 'migrations', '*{.ts,.js}')],
    logging: configService.get('NODE_ENV', 'development') ===
        'development',
};
//# sourceMappingURL=connection.js.map