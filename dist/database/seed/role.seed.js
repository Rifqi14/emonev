"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.roleSeed = void 0;
const common_1 = require("@nestjs/common");
const roles_json_1 = __importDefault(require("../data/roles.json"));
const role_entity_1 = require("../models/role.entity");
const roleSeed = async (ds) => {
    if (roles_json_1.default) {
        const manager = ds.manager;
        const logger = new common_1.Logger();
        roles_json_1.default.forEach((role) => {
            manager
                .exists(role_entity_1.Role, { where: { name: role.name } })
                .then(async (exist) => {
                if (!exist) {
                    manager
                        .create(role_entity_1.Role, { id: role.id, name: role.name })
                        .save()
                        .then((res) => logger.log(`Success seed role: ${res.name}`, 'SeedRole'))
                        .catch((err) => logger.error(`Error seed role: ${role.name} with error: ${err}`, 'SeedRole'));
                }
            });
        });
    }
};
exports.roleSeed = roleSeed;
//# sourceMappingURL=role.seed.js.map