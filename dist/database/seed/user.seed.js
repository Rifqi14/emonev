"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userSeed = void 0;
const common_1 = require("@nestjs/common");
const users_json_1 = __importDefault(require("../data/users.json"));
const role_entity_1 = require("../models/role.entity");
const status_entity_1 = require("../models/status.entity");
const user_entity_1 = require("../models/user.entity");
const userSeed = async (ds) => {
    if (users_json_1.default) {
        const manager = ds.manager;
        const logger = new common_1.Logger();
        users_json_1.default.forEach(async (user) => {
            manager
                .exists(user_entity_1.User, { where: { username: user.username } })
                .then(async (exist) => {
                if (!exist) {
                    manager
                        .create(user_entity_1.User, {
                        id: user.id,
                        name: user.name,
                        username: user.username,
                        password: user.username,
                        role: await manager
                            .findOneByOrFail(role_entity_1.Role, { name: user.role })
                            .catch(async (_) => {
                            const role = manager.create(role_entity_1.Role, {
                                name: user.role,
                            });
                            await manager.insert(role_entity_1.Role, role);
                            return role;
                        }),
                        status: await manager.findOneByOrFail(status_entity_1.Status, {
                            name: user.status,
                        }),
                    })
                        .save()
                        .then((res) => logger.log(`Success seed user: ${res.name}`, 'SeedUser'))
                        .catch((err) => logger.error(`Error seed user: ${user.name} with error: ${err}`, 'SeedUser'));
                }
            });
        });
    }
};
exports.userSeed = userSeed;
//# sourceMappingURL=user.seed.js.map