"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.settingSeed = void 0;
const common_1 = require("@nestjs/common");
const type_1 = require("../../utils/type");
const setting_entity_1 = require("../models/setting.entity");
const settingSeed = async (ds) => {
    if (type_1.SeedSetting) {
        const manager = ds.manager;
        const logger = new common_1.Logger();
        type_1.SeedSetting.forEach(async (setting) => {
            await manager
                .exists(setting_entity_1.Setting, { where: { id: setting.id } })
                .then(async (exist) => {
                if (!exist) {
                    await manager
                        .create(setting_entity_1.Setting, Object.assign({}, setting))
                        .save()
                        .then((res) => logger.log(`Success seed setting: ${res.key}`, 'SeedSetting'))
                        .catch((err) => logger.error(`Error seed setting: ${setting.key} with error: ${err}`, 'SeedSetting'));
                }
            });
        });
    }
};
exports.settingSeed = settingSeed;
//# sourceMappingURL=setting.seed.js.map