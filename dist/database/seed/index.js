"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.seed = void 0;
const dotenv_1 = require("dotenv");
const path_1 = require("path");
const typeorm_1 = require("typeorm");
const role_seed_1 = require("./role.seed");
const setting_seed_1 = require("./setting.seed");
const status_seed_1 = require("./status.seed");
const triwulan_seed_1 = require("./triwulan.seed");
const user_seed_1 = require("./user.seed");
(0, dotenv_1.config)();
const seed = async (ds) => {
    const conn = ds !== null && ds !== void 0 ? ds : (await new typeorm_1.DataSource({
        type: 'postgres',
        host: process.env.DATABASE_HOST,
        port: process.env.DATABASE_PORT ? +process.env.DATABASE_PORT : 5432,
        username: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASS,
        database: process.env.DATABASE_NAME,
        entities: [(0, path_1.join)(__dirname, '..', '..', '**', '*.entity{.ts,.js}')],
    }).initialize());
    await (0, status_seed_1.statusSeed)(conn);
    await (0, role_seed_1.roleSeed)(conn);
    await (0, user_seed_1.userSeed)(conn);
    await (0, triwulan_seed_1.triwulanSeed)(conn);
    await (0, setting_seed_1.settingSeed)(conn);
};
exports.seed = seed;
(0, exports.seed)();
//# sourceMappingURL=index.js.map