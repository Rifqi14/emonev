"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.triwulanSeed = void 0;
const common_1 = require("@nestjs/common");
const triwulan_json_1 = __importDefault(require("../data/triwulan.json"));
const triwulan_entity_1 = require("../models/triwulan.entity");
const triwulanSeed = async (ds) => {
    if (triwulan_json_1.default) {
        const transaction = ds.createQueryRunner();
        await transaction.startTransaction();
        const logger = new common_1.Logger();
        try {
            triwulan_json_1.default.forEach(async (triwulan) => {
                await transaction.manager
                    .exists(triwulan_entity_1.Triwulan, { where: { name: triwulan.name } })
                    .then(async (exist) => {
                    if (!exist) {
                        await transaction.manager
                            .create(triwulan_entity_1.Triwulan, { id: triwulan.id, name: triwulan.name })
                            .save()
                            .then((res) => logger.log(`Success triwulan status: ${res.name}`, 'SeedTriwulan'))
                            .catch((err) => logger.error(`Error triwulan status: ${triwulan.name} with error: ${err}`, 'SeedTriwulan'));
                    }
                });
            });
        }
        catch (error) {
            await transaction.rollbackTransaction();
            throw error;
        }
    }
};
exports.triwulanSeed = triwulanSeed;
//# sourceMappingURL=triwulan.seed.js.map