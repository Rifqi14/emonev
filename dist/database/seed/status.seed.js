"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.statusSeed = void 0;
const common_1 = require("@nestjs/common");
const status_json_1 = __importDefault(require("../data/status.json"));
const status_entity_1 = require("../models/status.entity");
const statusSeed = async (ds) => {
    if (status_json_1.default) {
        const manager = ds.manager;
        const logger = new common_1.Logger();
        status_json_1.default.forEach((status) => {
            manager
                .exists(status_entity_1.Status, { where: { id: status.id } })
                .then(async (exist) => {
                if (!exist) {
                    manager
                        .create(status_entity_1.Status, { id: status.id, name: status.name })
                        .save()
                        .then((res) => logger.log(`Success seed status: ${res.name}`, 'SeedStatus'))
                        .catch((err) => logger.error(`Error seed status: ${status.name} with error: ${err}`, 'SeedStatus'));
                }
            });
        });
    }
};
exports.statusSeed = statusSeed;
//# sourceMappingURL=status.seed.js.map