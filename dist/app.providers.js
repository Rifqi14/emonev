"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaticRoleApiResponse = exports.StaticTriwulanApiResponse = exports.StaticStatusApiResponse = exports.appProviders = void 0;
const triwulan_entity_1 = require("./database/models/triwulan.entity");
const status_entity_1 = require("./database/models/status.entity");
const role_entity_1 = require("./database/models/role.entity");
const api_response_1 = require("./common/api-response");
const swagger_1 = require("@nestjs/swagger");
exports.appProviders = [
    {
        provide: 'TRIWULAN_REPOSITORY',
        useFactory: (ds) => ds.getRepository(triwulan_entity_1.Triwulan),
        inject: ['DATA_SOURCE'],
    },
    {
        provide: 'STATUS_REPOSITORY',
        useFactory: (ds) => ds.getRepository(status_entity_1.Status),
        inject: ['DATA_SOURCE'],
    },
    {
        provide: 'ROLE_REPOSITORY',
        useFactory: (ds) => ds.getRepository(role_entity_1.Role),
        inject: ['DATA_SOURCE'],
    },
];
class StaticStatusApiResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [status_entity_1.Status] }),
    __metadata("design:type", Array)
], StaticStatusApiResponse.prototype, "data", void 0);
exports.StaticStatusApiResponse = StaticStatusApiResponse;
class StaticTriwulanApiResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [triwulan_entity_1.Triwulan] }),
    __metadata("design:type", Array)
], StaticTriwulanApiResponse.prototype, "data", void 0);
exports.StaticTriwulanApiResponse = StaticTriwulanApiResponse;
class StaticRoleApiResponse extends api_response_1.BaseApiResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [role_entity_1.Role] }),
    __metadata("design:type", Array)
], StaticRoleApiResponse.prototype, "data", void 0);
exports.StaticRoleApiResponse = StaticRoleApiResponse;
//# sourceMappingURL=app.providers.js.map