import { ConfigService } from '@nestjs/config';
import { Repository } from 'typeorm';
import { Status } from './database/models/status.entity';
import { Triwulan } from './database/models/triwulan.entity';
import { Role } from './database/models/role.entity';
export declare class AppService {
    private configService;
    private statusRepository;
    private roleRepository;
    private triwulanRepository;
    constructor(configService: ConfigService, statusRepository: Repository<Status>, roleRepository: Repository<Role>, triwulanRepository: Repository<Triwulan>);
    getHello(): string;
    getStatus(): Promise<Status[]>;
    getRole(): Promise<Role[]>;
    getTriwulan(): Promise<Triwulan[]>;
}
