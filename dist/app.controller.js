"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppController = void 0;
const common_1 = require("@nestjs/common");
const nest_winston_1 = require("nest-winston");
const app_service_1 = require("./app.service");
const swagger_1 = require("@nestjs/swagger");
const openapi_1 = require("./common/openapi");
const app_providers_1 = require("./app.providers");
const authentication_guard_1 = require("./modules/authentication/authentication.guard");
let AppController = class AppController {
    constructor(appService, logger) {
        this.appService = appService;
        this.logger = logger;
    }
    getHello() {
        return this.appService.getHello();
    }
    async staticStatus() {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.appService.getStatus(),
        };
    }
    async staticRole() {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.appService.getRole(),
        };
    }
    async staticTriwulan() {
        return {
            statusCode: 200,
            message: 'Pengambilan data berhasil',
            data: await this.appService.getTriwulan(),
        };
    }
};
__decorate([
    (0, authentication_guard_1.PublicAPI)(),
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", String)
], AppController.prototype, "getHello", null);
__decorate([
    (0, authentication_guard_1.PublicAPI)(),
    (0, openapi_1.SwaggerOkResponse)(app_providers_1.StaticStatusApiResponse),
    (0, common_1.Get)('static/status'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AppController.prototype, "staticStatus", null);
__decorate([
    (0, authentication_guard_1.PublicAPI)(),
    (0, openapi_1.SwaggerOkResponse)(app_providers_1.StaticRoleApiResponse),
    (0, common_1.Get)('static/admin-role'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AppController.prototype, "staticRole", null);
__decorate([
    (0, authentication_guard_1.PublicAPI)(),
    (0, openapi_1.SwaggerOkResponse)(app_providers_1.StaticTriwulanApiResponse),
    (0, common_1.Get)('static/triwulan'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AppController.prototype, "staticTriwulan", null);
AppController = __decorate([
    (0, swagger_1.ApiTags)('Static'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)(),
    __param(1, (0, common_1.Inject)(nest_winston_1.WINSTON_MODULE_PROVIDER)),
    __metadata("design:paramtypes", [app_service_1.AppService,
        nest_winston_1.WinstonLogger])
], AppController);
exports.AppController = AppController;
//# sourceMappingURL=app.controller.js.map