export declare const generateCode: (id: number, prefix: string) => string;
export declare class ListBaseRequest {
    limit?: number;
    page?: number;
    search?: string;
    sort?: 'terbaru' | 'terlama' | 'a-z' | 'z-a';
}
export declare const generateString: (length: number) => string;
