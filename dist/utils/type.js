"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeedSetting = exports.SettingDataTypeEnums = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const quarterOfYear_1 = __importDefault(require("dayjs/plugin/quarterOfYear"));
dayjs_1.default.extend(quarterOfYear_1.default);
var SettingDataTypeEnums;
(function (SettingDataTypeEnums) {
    SettingDataTypeEnums["NUMBER"] = "NUMBER";
    SettingDataTypeEnums["STRING"] = "STRING";
    SettingDataTypeEnums["ARRAY"] = "ARRAY";
    SettingDataTypeEnums["DATE"] = "DATE";
})(SettingDataTypeEnums = exports.SettingDataTypeEnums || (exports.SettingDataTypeEnums = {}));
exports.SeedSetting = [
    {
        key: 'TRIWULAN_STARTED',
        value: (0, dayjs_1.default)().startOf('quarter').toISOString(),
        data_type: 'DATE',
    },
    {
        key: 'TRIWULAN_ENDED',
        value: (0, dayjs_1.default)().endOf('quarter').toISOString(),
        data_type: 'DATE',
    },
];
//# sourceMappingURL=type.js.map