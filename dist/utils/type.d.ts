export declare enum SettingDataTypeEnums {
    NUMBER = "NUMBER",
    STRING = "STRING",
    ARRAY = "ARRAY",
    DATE = "DATE"
}
export type SettingDataType = keyof typeof SettingDataTypeEnums;
export declare const SeedSetting: Record<string, any>[];
