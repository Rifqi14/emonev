import { Provider } from '@nestjs/common';
import { Triwulan } from './database/models/triwulan.entity';
import { Status } from './database/models/status.entity';
import { Role } from './database/models/role.entity';
import { BaseApiResponse } from './common/api-response';
export declare const appProviders: Array<Provider>;
export declare class StaticStatusApiResponse extends BaseApiResponse {
    data: Array<Status>;
}
export declare class StaticTriwulanApiResponse extends BaseApiResponse {
    data: Array<Triwulan>;
}
export declare class StaticRoleApiResponse extends BaseApiResponse {
    data: Array<Role>;
}
