import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Repository } from 'typeorm';
import { Status } from './database/models/status.entity';
import { Triwulan } from './database/models/triwulan.entity';
import { Role } from './database/models/role.entity';

@Injectable()
export class AppService {
  constructor(
    private configService: ConfigService,
    @Inject('STATUS_REPOSITORY') private statusRepository: Repository<Status>,
    @Inject('ROLE_REPOSITORY') private roleRepository: Repository<Role>,
    @Inject('TRIWULAN_REPOSITORY')
    private triwulanRepository: Repository<Triwulan>,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  getStatus(): Promise<Status[]> {
    try {
      return this.statusRepository.find();
    } catch (error) {
      throw error;
    }
  }

  getRole(): Promise<Role[]> {
    try {
      return this.roleRepository.find();
    } catch (error) {
      throw error;
    }
  }

  getTriwulan(): Promise<Triwulan[]> {
    try {
      return this.triwulanRepository.find();
    } catch (error) {
      throw error;
    }
  }
}
