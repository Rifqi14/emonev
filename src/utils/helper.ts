import { ApiPropertyOptional } from '@nestjs/swagger';

export const generateCode = (id: number, prefix: string): string => {
  const date = new Date();
  return `${prefix}${date.getMilliseconds()}${date.getDate()}${date.getMonth()}${
    +id + 1
  }`;
};

export class ListBaseRequest {
  @ApiPropertyOptional()
  limit?: number;

  @ApiPropertyOptional()
  page?: number;

  @ApiPropertyOptional()
  search?: string;

  @ApiPropertyOptional({
    enum: ['terbaru', 'terlama', 'a-z', 'z-a'],
  })
  sort?: 'terbaru' | 'terlama' | 'a-z' | 'z-a';
}

export const generateString = (length: number): string => {
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = ' ';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
};
