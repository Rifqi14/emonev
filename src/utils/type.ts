import dayjs from 'dayjs';
import quarterOfYear from 'dayjs/plugin/quarterOfYear';

dayjs.extend(quarterOfYear);

export enum SettingDataTypeEnums {
  NUMBER = 'NUMBER',
  STRING = 'STRING',
  ARRAY = 'ARRAY',
  DATE = 'DATE',
}

export type SettingDataType = keyof typeof SettingDataTypeEnums;

export const SeedSetting: Record<string, any>[] = [
  {
    key: 'TRIWULAN_STARTED',
    value: dayjs().startOf('quarter').toISOString(),
    data_type: 'DATE',
  },
  {
    key: 'TRIWULAN_ENDED',
    value: dayjs().endOf('quarter').toISOString(),
    data_type: 'DATE',
  },
];
