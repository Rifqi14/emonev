import { INestApplication, Type, applyDecorators } from '@nestjs/common';
import {
  DocumentBuilder,
  SwaggerModule,
  SwaggerCustomOptions,
  ApiOkResponse,
  getSchemaPath,
  ApiExtraModels,
} from '@nestjs/swagger';
import { BaseApiResponse, DataWithPaginationApiResponse } from './api-response';

type OpenAPIProps = {
  title: string;
  endpoint: string;
  description: string;
  app: INestApplication;
};

export const OpenAPIDoc = ({
  app,
  description,
  endpoint,
  title,
}: OpenAPIProps) => {
  const config = new DocumentBuilder()
    .setTitle(title)
    .setDescription(description)
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT', name: 'bearer' },
      'bearer',
    )
    .build();

  const options: SwaggerCustomOptions = {
    customSiteTitle: title,
    swaggerOptions: {
      docExpansion: 'none',
      tagsSorter: 'alpha',
      persistAuthorization: true,
      syntaxHighlight: {
        active: true,
        theme: 'monokai',
      },
    },
  };

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup(endpoint, app, document, options);
};

type Opts = {
  isArray?: boolean;
};

export const SwaggerOkResponse = <
  TData extends Type<unknown> | string | number | undefined,
>(
  data: TData,
  props?: Opts,
) => {
  if (typeof data !== 'function') {
    return applyDecorators(
      ApiOkResponse({
        schema: {
          properties: {
            statusCode: {
              type: 'number',
              example: 200,
            },
            message: {
              type: 'string',
            },
            data: {
              type: typeof data,
            },
          },
        },
      }),
    );
  }

  return applyDecorators(
    ApiExtraModels(data),
    ApiOkResponse({
      schema: {
        allOf: [{ $ref: getSchemaPath(data) }],
      },
    }),
  );
};

export const SwaggerPaginationResponse = <TData extends Type<unknown>>(
  data: TData,
) =>
  applyDecorators(
    ApiExtraModels(BaseApiResponse, data, DataWithPaginationApiResponse),
    ApiOkResponse({
      schema: {
        allOf: [
          { $ref: getSchemaPath(BaseApiResponse) },
          {
            properties: {
              data: {
                allOf: [
                  { $ref: getSchemaPath(DataWithPaginationApiResponse) },
                  {
                    properties: {
                      result: {
                        type: 'array',
                        items: { $ref: getSchemaPath(data) },
                      },
                    },
                  },
                ],
              },
            },
          },
        ],
      },
    }),
  );

export const SwaggerAuthResponse = () =>
  applyDecorators(
    ApiOkResponse({
      schema: {
        properties: {
          statusCode: {
            type: 'number',
            example: 200,
          },
          access_token: {
            type: 'string',
          },
          payloadClient: {
            properties: {
              id: { type: 'number' },
              username: { type: 'string' },
              name: { type: 'string' },
              organization_id: { type: 'number' },
              admin_role_id: { type: 'number' },
              status_id: { type: 'number' },
              created_at: { type: 'string' },
              updated_at: { type: 'string' },
            },
          },
        },
      },
    }),
  );
