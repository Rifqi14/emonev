import { z } from 'zod';

export const NodeEnvStage = ['development', 'production'] as const;

export type NodeEnvStageType = (typeof NodeEnvStage)[number];

const databaseVariables = {
  DATABASE_HOST: z.string(),
  DATABASE_PORT: z.number(),
  DATABASE_USER: z.string(),
  DATABASE_PASS: z.string(),
  DATABASE_NAME: z.string(),
};

const emailVariables = {
  MAIL_HOST: z.string(),
  MAIL_USER: z.string(),
  MAIL_PASS: z.string(),
  MAIL_FROM: z.string(),
};

export const envVariables = z.object({
  NODE_ENV: z.enum(NodeEnvStage),
  APP_PORT: z.string(),
  APP_HOST: z.string(),
  SWAGGER_DOC_URL: z.string(),
  APP_SECRET: z.string(),
  ...databaseVariables,
  ...emailVariables,
});

export const appEnv = envVariables.parse(process.env);

export type AppEnv = typeof appEnv;
