import { Injectable } from '@nestjs/common';
import dayjs from 'dayjs';
import { Column, Workbook } from 'exceljs';
import { join } from 'path';
import { cwd } from 'process';
import puppeteer, { PDFOptions } from 'puppeteer';

type Props = {
  data: Array<Record<string, any>>;
  name: string;
};

type PropsMultipleSheet = {
  data: Record<string, Array<Record<string, any>>>;
  sheet: string[];
  name: string;
};

type PdfProps = {
  url: string;
  name: string;
  opt?: PDFOptions;
};

@Injectable()
export class ExcelService {
  async download({ data, name }: Props) {
    const headers: string[] = Array.from(Object.keys(data[0]));
    // const rows: any[][] = [];
    // let i = 0;

    // while (i < data.length) {
    //   rows.push(Object.values(data[i]));
    //   i++;
    // }
    headers.unshift('no');

    // Creating a workbook
    const book = new Workbook();

    // Create worksheet
    const sheet = book.addWorksheet();

    sheet.columns = headers
      .filter((key) => key != 'id')
      .map<Partial<Column>>((key) => ({
        key,
        header: key
          .split('_')
          .map((column) => column.charAt(0).toUpperCase() + column.slice(1))
          .join(' '),
      }));

    data.forEach((item, idx) => sheet.addRow({ no: idx + 1, ...item }));

    const lastRow = sheet.actualRowCount;

    sheet.eachRow((row, rowNumber) => {
      if (rowNumber > 1) {
        row.eachCell((cell) => {
          cell.style = {
            border: {
              top: { style: 'thin' },
              left: { style: 'thin' },
              right: { style: 'thin' },
              bottom: { style: 'thin' },
            },
          };
        });
      }
      if (rowNumber === 1) {
        row.eachCell((cell) => {
          cell.style = {
            font: {
              bold: true,
            },
            fill: {
              type: 'pattern',
              pattern: 'solid',
              fgColor: {
                argb: '92D050',
              },
            },
            border: {
              top: { style: 'thin' },
              left: { style: 'thin' },
              right: { style: 'thin' },
              bottom: { style: 'double' },
            },
          };
        });
      }
    });

    sheet.getCell(`B${lastRow + 2}`).alignment = { horizontal: 'center' };
    sheet.getCell(`B${lastRow + 2}`).value = dayjs().format('DD MMMM YYYY');
    sheet.getCell(`B${lastRow + 3}`).alignment = { horizontal: 'center' };
    sheet.getCell(`B${lastRow + 3}`).value = `Bupati Sorong`;
    sheet.getCell(`B${lastRow + 7}`).alignment = { horizontal: 'center' };
    sheet.getCell(`B${lastRow + 7}`).value = `Petahana Yan Piet Moso`;

    const exportPath = join(
      cwd(),
      'public',
      'tmp',
      `${new Date().getTime().toString()}-${name}.xlsx`,
    );

    try {
      await book.xlsx.writeFile(exportPath);
    } catch (error) {
      throw error;
    }

    return exportPath;
  }

  async downloadMultipleSheets({
    data,
    name,
    sheet,
  }: PropsMultipleSheet): Promise<string> {
    const book = new Workbook();

    // Create a sbeet;
    for (let i = 0; i < sheet.length; i++) {
      book.addWorksheet(sheet[i]);
    }

    book.eachSheet((sheet) => {
      const exportedData = data[sheet.name];
      const headers: string[] = Array.from(Object.keys(exportedData[0]));
      const rows: any[][] = [];
      let i = 0;

      while (i < exportedData.length) {
        rows.push(Object.values(exportedData[i]));
        i++;
      }

      sheet.columns = headers.map<Partial<Column>>((key) => ({
        key,
        header: key
          .split('_')
          .map((column) => column.charAt(0).toUpperCase() + column.slice(1))
          .join(' '),
      }));

      exportedData.forEach((item) => sheet.addRow(item));
    });

    const exportPath = join(
      cwd(),
      'public',
      'tmp',
      `${new Date().getTime().toString()}-${name}.xlsx`,
    );

    try {
      await book.xlsx.writeFile(exportPath);
    } catch (error) {
      throw error;
    }

    return exportPath;
  }

  async pdfDownload({ url, name, opt }: PdfProps) {
    try {
      const browser = await puppeteer.launch({
        headless: 'new',
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--disable-dev-shm-usage',
        ],
      });
      const page = await browser.newPage();
      await page.goto(url, { waitUntil: 'networkidle0' });
      await page.emulateMediaType('screen');

      const exportPath = join(
        cwd(),
        'public',
        'tmp',
        `${new Date().getTime().toString()}-${name}.pdf`,
      );
      const pdf = await page.pdf({
        path: exportPath,
        format: 'A4',
        displayHeaderFooter: false,
        landscape: true,
        printBackground: true,
        ...opt,
      });

      // writeFile(exportPath, Buffer.from(pdf), (err) => {
      //   if (err) throw err;
      // });

      await browser.close();

      return exportPath;
    } catch (error) {
      throw error;
    }
  }
}
