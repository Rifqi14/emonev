import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { WinstonModule } from 'nest-winston';
import { join } from 'path';
import { cwd } from 'process';
import { transports, format } from 'winston';
import { JwtModule } from '@nestjs/jwt';
import 'winston-daily-rotate-file';
import { AppEnv } from './configuration';
import { ExcelService } from './excel.service';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    WinstonModule.forRoot({
      transports: [
        // Log error to file in public directory
        new transports.DailyRotateFile({
          filename: join(cwd(), 'public', 'logs', '%DATE%-error.log'),
          level: 'error',
          format: format.combine(format.timestamp(), format.json()),
          datePattern: 'YYYY-MM-DD',
          zippedArchive: false,
          maxFiles: '7d',
        }),
      ],
    }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService<AppEnv>) => ({
        global: true,
        secret: config.get<string>('APP_SECRET', 'secretKeys'),
        signOptions: { expiresIn: '30d' },
      }),
    }),
  ],
  exports: [ConfigModule, JwtModule],
  providers: [ExcelService],
})
export class CommonModule {}
