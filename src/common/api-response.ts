import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
  Inject,
  Logger,
} from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Response } from 'express';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { EntityNotFoundError, QueryFailedError } from 'typeorm';

export class BaseApiResponse {
  @ApiProperty({ example: 200 })
  statusCode: number;

  @ApiProperty()
  message: string;
}

export class DataApiResponse<TData> {
  result: TData;
}

export class DataWithPaginationApiResponse<
  TData,
> extends DataApiResponse<TData> {
  @ApiProperty()
  total: number;

  @ApiProperty()
  page: number;

  @ApiProperty()
  pages: number;
}

@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
  ) {}

  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response<BaseApiResponse>>();

    let message = (exception as any).message;

    this.logger.error(message);

    switch (exception.constructor) {
      case QueryFailedError:
        message = (exception as QueryFailedError).message;
        break;
      case EntityNotFoundError:
        message =
          process.env.NODE_ENV === 'production'
            ? (exception as EntityNotFoundError).message.split(':')[0]
            : (exception as EntityNotFoundError).message;
        break;
      case BadRequestException:
        message = (exception as BadRequestException).message;
        break;
    }

    response
      .status(response.statusCode)
      .json({ statusCode: 400, message: message });
  }
}
