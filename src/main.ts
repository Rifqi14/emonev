import {
	BadRequestException,
	ClassSerializerInterceptor,
	Logger,
	ValidationPipe,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory, Reflector } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as hbs from 'express-handlebars';
import { join } from 'path';
import { AppModule } from './app.module';
import { AppEnv } from './common/configuration';
import { OpenAPIDoc } from './common/openapi';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const configService = app.get<ConfigService<AppEnv>>(ConfigService);

  const port = configService.get<number>('APP_PORT', 4000);
  const hostname = configService.get<string>('APP_HOST', 'localhost');
  const env = configService.get<string>('NODE_ENV', 'development');

  app.useStaticAssets(join(__dirname, '..', 'public'));

  app.setBaseViewsDir(join(__dirname, 'views'));

  app.engine('hbs', hbs.engine({ extname: 'hbs' }));

  app.setViewEngine('hbs');

	OpenAPIDoc({
		app,
		title: 'emonev',
		description: `Download schema for postman collection: <a href="/docs-json" target="_blank">Download here!</a>`,
		endpoint: 'docs',
	});

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      exceptionFactory: (errors) => {
        const result = errors.map((error) => ({
          property: error.property,
          message: error.constraints
            ? error.constraints[Object.keys(error.constraints)[0]]
            : '',
        }));
        return new BadRequestException(result);
      },
    }),
  );
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  app.enableCors();

  await app.listen(port, () => {
    new Logger().log(`App listening on ${hostname}:${port}`, 'AppRunning');
  });
}
bootstrap();
