import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { SettingService } from '../setting/setting.service';
import { Reflector } from '@nestjs/core';
import dayjs from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween';
dayjs.extend(isBetween);

@Injectable()
export class DataTriwulanGuard implements CanActivate {
  constructor(
    private settingService: SettingService,
    private reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const currentDay = dayjs();
    const settings = await this.settingService.getSetting();

    const started = settings.find(
      (setting) => setting.key === 'TRIWULAN_STARTED',
    );
    const ended = settings.find((setting) => setting.key === 'TRIWULAN_ENDED');

    if (!started || !ended)
      throw new HttpException(
        'Periode triwulan belum di set, silahkan hubungi admin.',
        HttpStatus.BAD_REQUEST,
      );

    if (!currentDay.isBetween(started.value, ended.value))
      throw new HttpException(
        'Data yang anda buat diluar periode data triwulan. Silahkan hubungi admin.',
        HttpStatus.BAD_REQUEST,
      );

    return true;
  }
}
