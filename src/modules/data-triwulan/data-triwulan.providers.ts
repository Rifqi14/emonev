import { Provider } from '@nestjs/common';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { DataSource } from 'typeorm';

export const dataTriwulanProviders: Array<Provider> = [
  {
    provide: 'DATA_TRIWULAN_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(DataTriwulan),
    inject: ['DATA_SOURCE'],
  },
];
