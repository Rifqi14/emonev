import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { ListBaseRequest } from 'src/utils/helper';

export class CreateDataTriwulanRequest {
  @ApiProperty()
  activity_name: string;

  @ApiProperty()
  activity_location: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : +value))
  @ApiPropertyOptional()
  fund_source_id?: number;

  @Transform(({ value }) => (!value || value === '' ? undefined : +value))
  @ApiPropertyOptional({ description: 'Pagu Dana' })
  fund_ceiling?: number;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ description: 'OPD Pengelola' })
  management_organization?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ description: 'Nama PPTK' })
  pptk_name?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ description: 'Nomor dan Tanggal Kontrak' })
  contract_number_date?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ description: 'Nama Kontraktor' })
  contractor_name?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ description: 'Jangka Waktu Pelaksanaan' })
  implementation_period?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : +value))
  @ApiPropertyOptional({ description: 'Nilai Kontrak' })
  contract_value?: number;

  @Transform(({ value }) => (!value || value === '' ? undefined : +value))
  @ApiPropertyOptional({ description: 'Realisasi Fisik' })
  physical_realization?: number;

  @Transform(({ value }) => (!value || value === '' ? undefined : +value))
  @ApiPropertyOptional({ description: 'Realisasi Keuangan' })
  fund_realization?: number;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ description: 'Volume Kegiatan' })
  activity_volume?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ description: 'Output Kegiatan' })
  activity_output?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({
    description: 'Manfaat Kegiatan (Kelompok sasaran Langsung)',
  })
  direct_target_group?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({
    description: 'Manfaat Kegiatan (Kelompok sasaran Langsung)',
  })
  indirect_target_group?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : +value))
  @ApiPropertyOptional({ description: 'Jumlah Tenaga Kerja (Lokal)' })
  local_workforce?: number;

  @Transform(({ value }) => (!value || value === '' ? undefined : +value))
  @ApiPropertyOptional({ description: 'Jumlah Tenaga Kerja (Non Lokal)' })
  non_local_workforce?: number;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({
    description: 'Hambatan Dan Permasalahan',
  })
  problems?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({
    description: 'Solusi Permasalahan',
  })
  solution?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ description: 'Jenis pengadaan' })
  procurement_type?: string;

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ description: 'Cara pengadaan' })
  procurement_method?: string;

  @ApiPropertyOptional({ description: 'Sub Kegiatan' })
  activity_id?: number;

  @ApiPropertyOptional({
    enum: ['fisik', 'nonfisik'],
    description: 'Bentuk kegiatan',
  })
  activity_form?: 'fisik' | 'nonfisik';

  @Transform(({ value }) => (!value || value === '' ? undefined : value))
  @ApiPropertyOptional({ type: 'string', format: 'binary' })
  file?: Express.Multer.File;

  @ApiPropertyOptional({ description: 'Tanggal kontrak. Format: YYYY-MM-DD' })
  contract_date?: string;

  @ApiPropertyOptional({ description: 'Nama penanggung jawab' })
  pic_name?: string;

  @ApiPropertyOptional({ description: 'Nama pimpinan' })
  leader_name?: string;

  @ApiPropertyOptional({ description: 'Opsi. Static dari FE.' })
  optional?: string;

  @ApiPropertyOptional({ description: 'Alasan terkait' })
  reason?: string;
}

export class CreateDataTriwulanResponse extends BaseApiResponse {
  @ApiProperty()
  data: DataTriwulan;
}

export class UpdateDataTriwulanRequest extends PartialType(
  CreateDataTriwulanRequest,
) {}

export class UpdateDataTriwulanResponse extends CreateDataTriwulanResponse {}

export class DetailDataTriwulanResponse extends CreateDataTriwulanResponse {}

export class ListDataTriwulanRequest extends ListBaseRequest {
  @ApiPropertyOptional()
  month?: string;

  @ApiPropertyOptional()
  year?: string;

  @ApiPropertyOptional()
  triwulan_id?: string;

  @ApiPropertyOptional()
  fund_source_id?: string;
}

export class ListHistoryDataTriwulanRequest extends ListBaseRequest {
  @ApiPropertyOptional()
  activity_name_search?: string;

  @ApiPropertyOptional()
  organization_id?: string;
}

export class ListDataTriwulanResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<DataTriwulan[]> })
  data: DataWithPaginationApiResponse<DataTriwulan[]>;
}

export class DeleteDataTriwulanResponse extends BaseApiResponse {}
