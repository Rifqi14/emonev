import {
  Body,
  Controller,
  Delete,
  FileTypeValidator,
  Get,
  MaxFileSizeValidator,
  Param,
  ParseFilePipe,
  Patch,
  Post,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { join } from 'path';
import {
  SwaggerOkResponse,
  SwaggerPaginationResponse,
} from 'src/common/openapi';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import {
  CreateDataTriwulanRequest,
  CreateDataTriwulanResponse,
  DeleteDataTriwulanResponse,
  DetailDataTriwulanResponse,
  ListDataTriwulanRequest,
  ListDataTriwulanResponse,
  ListHistoryDataTriwulanRequest,
  UpdateDataTriwulanRequest,
  UpdateDataTriwulanResponse,
} from './data-triwulan.dto';
import { DataTriwulanGuard } from './data-triwulan.guard';
import { DataTriwulanService } from './data-triwulan.service';

@ApiTags('User | Data Triwulan')
@ApiBearerAuth()
@Controller('data-triwulan')
export class DataTriwulanController {
  constructor(private readonly dataTriwulanService: DataTriwulanService) {}

  @SwaggerPaginationResponse(DataTriwulan)
  @Get('list')
  async list(
    @Query() query: ListDataTriwulanRequest,
  ): Promise<ListDataTriwulanResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.dataTriwulanService.list(query),
    };
  }

  @SwaggerPaginationResponse(DataTriwulan)
  @Get('list/history')
  async history(
    @Query() query: ListHistoryDataTriwulanRequest,
  ): Promise<ListDataTriwulanResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.dataTriwulanService.history(query),
    };
  }

  @SwaggerOkResponse(DetailDataTriwulanResponse)
  @Get(':id')
  async detail(@Param('id') id: number): Promise<DetailDataTriwulanResponse> {
    return {
      statusCode: 200,
      message: `Pengambilan data berhasil`,
      data: await this.dataTriwulanService.detail(id),
    };
  }

  @SwaggerOkResponse(CreateDataTriwulanResponse)
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: join('public', 'upload', 'data-triwulan'),
        filename: (req, file, cb) => {
          const filename = `${new Date().getTime().toString()}_data-triwulan.${
            file.originalname.split('.')[1]
          }`;
          cb(null, filename);
        },
      }),
    }),
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: CreateDataTriwulanRequest,
  })
  @UseGuards(DataTriwulanGuard)
  @Post('')
  async create(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 5 }),
          new FileTypeValidator({ fileType: '.(png|jpeg|jpg|pdf|mp4)' }),
        ],
        fileIsRequired: false,
      }),
    )
    file: Express.Multer.File,
    @Body() data: CreateDataTriwulanRequest,
  ): Promise<CreateDataTriwulanResponse> {
    console.log(file);
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.dataTriwulanService.create(data, file),
    };
  }

  @SwaggerOkResponse(UpdateDataTriwulanResponse)
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: join('public', 'upload', 'data-triwulan'),
        filename: (req, file, cb) => {
          const filename = `${new Date().getTime().toString()}_data-triwulan.${
            file.originalname.split('.')[1]
          }`;
          cb(null, filename);
        },
      }),
    }),
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: CreateDataTriwulanRequest,
  })
  @UseGuards(DataTriwulanGuard)
  @Patch(':id')
  async update(
    @Param('id') id: number,
    @Body() data: UpdateDataTriwulanRequest,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 5 }),
          new FileTypeValidator({ fileType: '.(png|jpeg|jpg|pdf|mp4)' }),
        ],
        fileIsRequired: false,
      }),
    )
    file: Express.Multer.File,
  ): Promise<UpdateDataTriwulanResponse> {
    console.log(file);
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.dataTriwulanService.update(id, data, file),
    };
  }

  @SwaggerOkResponse(DeleteDataTriwulanResponse)
  @UseGuards(DataTriwulanGuard)
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteDataTriwulanResponse> {
    await this.dataTriwulanService.delete(id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }
}
