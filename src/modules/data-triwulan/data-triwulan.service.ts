import {
  Inject,
  Injectable,
  Scope,
  UnauthorizedException,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { unlinkSync } from 'fs';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { User } from 'src/database/models/user.entity';
import {
  FindOptionsOrder,
  FindOptionsWhere,
  ILike,
  In,
  Raw,
  Repository,
} from 'typeorm';
import { PayloadClientResponse } from '../authentication/authentication.dto';
import {
  CreateDataTriwulanRequest,
  ListDataTriwulanRequest,
  ListHistoryDataTriwulanRequest,
  UpdateDataTriwulanRequest,
} from './data-triwulan.dto';
import dayjs from 'dayjs';

@Injectable({ scope: Scope.REQUEST })
export class DataTriwulanService {
  constructor(
    @Inject('DATA_TRIWULAN_REPOSITORY')
    private dataTriwulanRepository: Repository<DataTriwulan>,
    @Inject(REQUEST)
    private request: Request & {
      user: PayloadClientResponse;
      is_admin_bidang: boolean;
    },
  ) {}

  async create(
    data: CreateDataTriwulanRequest,
    file?: Express.Multer.File,
  ): Promise<DataTriwulan> {
    try {
      return await this.dataTriwulanRepository.manager.transaction(
        async (trx) => {
          const creator = await trx.findOneOrFail(User, {
            where: {
              id: this.request.user.user_id,
            },
          });
          return await trx
            .create(DataTriwulan, {
              ...data,
              fund_ceiling: data.fund_ceiling ? Number(data.fund_ceiling) : 0,
              contract_value: data.contract_value
                ? Number(data.contract_value)
                : 0,
              physical_realization: data.physical_realization
                ? Number(data.physical_realization)
                : 0,
              fund_realization: data.fund_realization
                ? Number(data.fund_realization)
                : 0,
              local_workforce: data.local_workforce
                ? Number(data.local_workforce)
                : 0,
              non_local_workforce: data.non_local_workforce
                ? Number(data.non_local_workforce)
                : 0,
              file: file && file.path.replace(/\\/g, '/'),
              createdBy: creator,
            })
            .save();
        },
      );
    } catch (error) {
      throw error;
    }
  }

  async detail(id: number): Promise<DataTriwulan> {
    try {
      const triwulan = await this.dataTriwulanRepository.findOneOrFail({
        relations: {
          fundSource: true,
          activity: {
            program: true,
          },
        },
        where: {
          id,
        },
      });

      return triwulan;
    } catch (error) {
      throw error;
    }
  }

  async update(
    id: number,
    data: UpdateDataTriwulanRequest,
    file?: Express.Multer.File,
  ): Promise<DataTriwulan> {
    try {
      await this.dataTriwulanRepository.manager.transaction(async (trx) => {
        const dataTriwulan = await this.dataTriwulanRepository.findOneByOrFail({
          id,
        });

        if (file && dataTriwulan.file) {
          unlinkSync(dataTriwulan.file);
        }

        await trx
          .create(DataTriwulan, {
            ...dataTriwulan,
            ...data,
            fund_ceiling: data.fund_ceiling
              ? Number(data.fund_ceiling)
              : data.fund_ceiling,
            contract_value: data.contract_value
              ? Number(data.contract_value)
              : data.contract_value,
            physical_realization: data.physical_realization
              ? Number(data.physical_realization)
              : data.physical_realization,
            fund_realization: data.fund_realization
              ? Number(data.fund_realization)
              : data.fund_realization,
            local_workforce: data.local_workforce
              ? Number(data.local_workforce)
              : data.local_workforce,
            non_local_workforce: data.non_local_workforce
              ? Number(data.non_local_workforce)
              : data.non_local_workforce,
            file: file ? file.path.replace(/\\/g, '/') : undefined,
          })
          .save();
      });

      return await this.dataTriwulanRepository.findOneByOrFail({
        id,
      });
    } catch (error) {
      throw error;
    }
  }

  async delete(id: number): Promise<void> {
    try {
      await this.dataTriwulanRepository
        .findOneByOrFail({ id })
        .then((result) => result.file && unlinkSync(result.file));
      await this.dataTriwulanRepository.delete({ id });
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListDataTriwulanRequest,
  ): Promise<DataWithPaginationApiResponse<Array<DataTriwulan>>> {
    const organization: number[] =
      this.request.user.organization && this.request.user.organization.id
        ? [this.request.user.organization.id]
        : [];
    if (
      this.request.user.userOrganization &&
      this.request.user.userOrganization.length > 0
    ) {
      organization.push(
        ...this.request.user.userOrganization.map(
          (organization) => organization.organization_id,
        ),
      );
    }
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const where: Array<FindOptionsWhere<DataTriwulan>> = [];

      if (search) {
        where.push({
          activity_name: ILike(`%${search}%`),
        });
        where.push({
          activity_location: ILike(`%${search}%`),
        });
      }

      if (this.request.is_admin_bidang) {
        if (
          !this.request.user.userOrganization ||
          this.request.user.userOrganization.length === 0
        )
          throw new UnauthorizedException();
        where[0] = {
          ...where[0],
          createdBy: {
            organization_id: In(
              this.request.user.userOrganization.map(
                (organization) => organization.organization_id,
              ),
            ),
          },
        };
      }

      if (this.request.user.role.name.toLowerCase() == 'opd') {
        if (where.length > 0) {
          where.map((where) => {
            return {
              ...where,
              createdBy: {
                userOrganization: {
                  organization_id: In([...new Set(organization)]),
                },
              },
            };
          });
        } else {
          where.push({
            createdBy: {
              userOrganization: {
                organization_id: In([...new Set(organization)]),
              },
            },
          });
        }
        console.log(where);
      }

      const [dataTriwulan, count] =
        await this.dataTriwulanRepository.findAndCount({
          where: where.length > 0 ? where : undefined,
          order: this.getOrderQuery(sort),
          relations: {
            fundSource: true,
            activity: {
              program: true,
            },
          },
          skip: (page - 1) * limit,
          take: limit,
        });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: dataTriwulan,
      };
    } catch (error) {
      throw error;
    }
  }

  async history(
    query: ListHistoryDataTriwulanRequest,
  ): Promise<DataWithPaginationApiResponse<DataTriwulan[]>> {
    try {
      const same = await this.dataTriwulanRepository.query(
        `select count(*) as total_sama, lower(data_triwulan.activity_name) as name from data_triwulan group by lower(activity_name) having count(*) > 1`,
      );

      let activity_name: string[] = (
        same as Array<Record<string, any>>
      ).map<string>((data) => data.name as string);

      const { activity_name_search: search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;
      if (search) {
        activity_name = activity_name.filter((name) =>
          name.toLowerCase().includes(search.toLowerCase()),
        );
      }
      console.log(activity_name);

      const where: Array<FindOptionsWhere<DataTriwulan>> = [
        {
          activity_name: Raw((alias) => `${alias} ilike any(:activity_name)`, {
            activity_name,
          }),
        },
      ];

      if (query.organization_id) {
        where.pop();
        where.push({
          createdBy: {
            organization_id: +query.organization_id,
          },
          activity_name: Raw((alias) => `${alias} ilike any(:activity_name)`, {
            activity_name,
          }),
        });
        where.push({
          createdBy: {
            userOrganization: {
              organization_id: +query.organization_id,
            },
          },
          activity_name: Raw((alias) => `${alias} ilike any(:activity_name)`, {
            activity_name,
          }),
        });
      }

      // if (search) {
      //   where.push({
      //     activity_name: ILike(`%${search}%`),
      //   });
      //   where.push({
      //     activity_location: ILike(`%${search}%`),
      //   });
      // }

      const [dataTriwulan, count] =
        await this.dataTriwulanRepository.findAndCount({
          where: where.length > 0 ? where : undefined,
          order: this.getOrderQuery(sort),
          relations: {
            fundSource: true,
            createdBy: true,
          },
          skip: (page - 1) * limit,
          take: limit,
        });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: dataTriwulan,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<DataTriwulan> {
    const sortRes: FindOptionsOrder<DataTriwulan> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          activity_name: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          activity_name: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
