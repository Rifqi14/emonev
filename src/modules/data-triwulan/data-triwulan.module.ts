import { Module } from '@nestjs/common';
import { DataTriwulanService } from './data-triwulan.service';
import { DataTriwulanController } from './data-triwulan.controller';
import { dataTriwulanProviders } from './data-triwulan.providers';
import { SettingModule } from '../setting/setting.module';

@Module({
  controllers: [DataTriwulanController],
  imports: [SettingModule],
  providers: [DataTriwulanService, ...dataTriwulanProviders],
  exports: [DataTriwulanService],
})
export class DataTriwulanModule {}
