import { Module } from '@nestjs/common';
import { ReportService } from './report.service';
import { ReportController } from './report.controller';
import { reportProviders } from './report.providers';
import { DatabaseModule } from 'src/database/database.module';
import { masterProviders } from '../master/master.providers';
import { dataTriwulanProviders } from '../data-triwulan/data-triwulan.providers';
import { ExcelService } from 'src/common/excel.service';

@Module({
  imports: [DatabaseModule],
  controllers: [ReportController],
  providers: [
    ReportService,
    ExcelService,
    ...reportProviders,
    ...masterProviders,
    ...dataTriwulanProviders,
  ],
  exports: [ReportService],
})
export class ReportModule {}
