import {
  ApiProperty,
  ApiPropertyOptional,
  OmitType,
  PartialType,
} from '@nestjs/swagger';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { Master } from 'src/database/models/master.entity';
import { Report } from 'src/database/models/report.entity';
import { ListBaseRequest } from 'src/utils/helper';

export class CreateReportRequest {
  @ApiPropertyOptional()
  description?: string;

  @ApiPropertyOptional()
  program_description?: string;

  @ApiPropertyOptional()
  triwulan_id?: number;

  @ApiPropertyOptional()
  organization_id?: number;

  @ApiPropertyOptional()
  occassion_id?: number;

  @ApiPropertyOptional()
  program_id?: number;
}

export class CreateReportResponse extends BaseApiResponse {
  @ApiProperty()
  data: Report;
}

// Update
export class UpdateReportRequest extends PartialType(CreateReportRequest) {
  @ApiProperty()
  data_report_id: number;
}

export class UpdateReportResponse extends CreateReportResponse {}

// Delete
export class DeleteReportRequest {
  @ApiProperty()
  data_report_id: number;
}

export class DeleteReportResponse extends BaseApiResponse {}

// List
export class ListReportRequest extends ListBaseRequest {
  @ApiPropertyOptional()
  month?: string;

  @ApiPropertyOptional()
  year?: string;

  @ApiPropertyOptional()
  triwulan_id?: string;
}
export class ListReportResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<Report>> })
  data: DataWithPaginationApiResponse<Array<Report>>;
}

// Detail
export class DataDetailResponse {
  @ApiProperty()
  result: Report;
}
export class DetailReportResponse extends BaseApiResponse {
  @ApiProperty()
  data: DataDetailResponse;
}

// List Laporan Master
export class ListReportUserRequest extends ListBaseRequest {
  @ApiPropertyOptional()
  month?: string;

  @ApiPropertyOptional()
  year?: string;

  @ApiPropertyOptional()
  triwulan_id?: number;

  @ApiPropertyOptional()
  fund_source_id?: number;
}

export class ListReportTriwulanRequest extends OmitType(ListReportUserRequest, [
  'triwulan_id',
]) {}

export class ListReportMasterRequest extends OmitType(ListReportUserRequest, [
  'fund_source_id',
]) {}

export class DownloadListReportTriwulanRequest extends OmitType(
  ListReportUserRequest,
  ['triwulan_id', 'limit', 'page'],
) {}

export class DownloadListReportMasterRequest extends OmitType(
  ListReportUserRequest,
  ['fund_source_id', 'limit', 'page'],
) {}

export class ListReportMasterUserResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<Master>> })
  data: DataWithPaginationApiResponse<Array<Master>>;
}

export class ListReportTriwulanUserResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<DataTriwulan>> })
  data: DataWithPaginationApiResponse<Array<DataTriwulan>>;
}

export class ListReportMasterUserDownload {
  no: number;
  tanggal: string;
  urusan: string;
  sasaran: string;
  organisasi: string;
  indikator_kegiatan: string;
}

export class ListReportTriwulanUserDownload {
  no: number;
  nama_kegiatan: string;
  lokasi_kegiatan: string;
  sumber_dana: string;
  pagu_dana: string;
  opd_organisasi: string;
  nama_pptk: string;
  nomor_dan_tanggal_kontrak: string;
  nama_kontraktor: string;
  jangka_waktu: string;
  nilai_kontrak: string;
  realisasi_fisik: string;
  realisasi_keuangan: string;
  volume_kegiatan: string;
  output_kegiatan: string;
  manfaat_kegiatan_kelompok_sasaran_langsung: string;
  manfaat_kegiatan_kelompok_sasaran_tidak_langsung: string;
  jumlah_tenaga_kerja_lokal: string;
  jumlah_tenaga_kerja_non_lokal: string;
  hambatan_dan_permasalahan: string;
  solusi_permasalahan: string;
  jenis_pengadaan: string;
  cara_pengadaan: string;
  tanggal_kontrak: string;
  nama_penanggung_jawab: string;
  nama_pimpinan: string;
  opsi: string;
  alasan: string;
}
