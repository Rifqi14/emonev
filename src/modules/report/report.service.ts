import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
import { ExcelService } from 'src/common/excel.service';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { Master } from 'src/database/models/master.entity';
import { Report } from 'src/database/models/report.entity';
import { User } from 'src/database/models/user.entity';
import {
  Brackets,
  EntityNotFoundError,
  FindOptionsOrder,
  FindOptionsWhere,
  In,
  Raw,
  Repository,
} from 'typeorm';
import { PayloadClientResponse } from '../authentication/authentication.dto';
import {
  CreateReportRequest,
  DownloadListReportMasterRequest,
  DownloadListReportTriwulanRequest,
  ListReportMasterUserDownload,
  ListReportRequest,
  ListReportTriwulanUserDownload,
  ListReportUserRequest,
  UpdateReportRequest,
} from './report.dto';
import dayjs from 'dayjs';

@Injectable({ scope: Scope.REQUEST })
export class ReportService {
  constructor(
    @Inject('REPORT_REPOSITORY')
    private repository: Repository<Report>,
    @Inject('MASTER_REPOSITORY')
    private masterRepository: Repository<Master>,
    @Inject('DATA_TRIWULAN_REPOSITORY')
    private dataTriwulanRepository: Repository<DataTriwulan>,
    @Inject(REQUEST)
    private request: Request & {
      user: PayloadClientResponse;
      is_admin_bidang: boolean;
    },
    private excelService: ExcelService,
  ) {}

  async create(data: CreateReportRequest): Promise<Report> {
    try {
      const {
        program_id,
        description,
        occassion_id,
        organization_id,
        program_description,
        triwulan_id,
      } = data;

      return await this.repository.manager.transaction(
        async (trx) =>
          await trx
            .create(Report, {
              description,
              program_description,
              triwulan_id,
              organization_id,
              occassion_id,
              program_id,
            })
            .save(),
      );
    } catch (error) {
      throw error;
    }
  }

  async detail(id: number): Promise<Report> {
    try {
      const user = await this.repository.manager.findOneOrFail(User, {
        where: { id: this.request.user.user_id },
      });

      if (user.role && user.role.id != 1 && !user.organization)
        throw new EntityNotFoundError(User, 'Silahkan lengkapi data anda');

      return await this.repository.findOneOrFail({
        relations: {
          program: true,
          triwulan: true,
          occassion: true,
          organization: true,
        },
        where: {
          id,
          organization_id:
            user.role &&
            user.role.id != 1 &&
            user.organization &&
            user.organization.id
              ? this.request.user.organization?.id
              : undefined,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async update(data: UpdateReportRequest): Promise<Report> {
    try {
      const {
        program_id,
        description,
        occassion_id,
        organization_id,
        program_description,
        triwulan_id,
        data_report_id,
      } = data;

      await this.repository.manager.transaction(async (trx) =>
        trx.update(
          Report,
          { id: data_report_id },
          {
            description,
            program_description,
            triwulan_id,
            organization_id,
            occassion_id,
            program_id,
          },
        ),
      );

      return await this.repository.findOneByOrFail({
        id: data_report_id,
      });
    } catch (error) {
      throw error;
    }
  }

  async delete(data_report_id: number): Promise<void> {
    try {
      await this.repository.delete({ id: data_report_id });
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListReportRequest,
  ): Promise<DataWithPaginationApiResponse<Array<Report>>> {
    try {
      const { search, sort, month, year, triwulan_id } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const user = await this.repository.manager.findOneOrFail(User, {
        where: { id: this.request.user.user_id },
      });

      if (user.role && user.role.id != 1 && !user.organization)
        throw new EntityNotFoundError(User, 'Silahkan lengkapi data anda');

      const queryBuilder = this.repository
        .createQueryBuilder('report')
        .leftJoinAndSelect('report.triwulan', 'triwulan')
        .leftJoinAndSelect('report.program', 'program')
        .leftJoinAndSelect('report.occassion', 'occassion')
        .leftJoinAndSelect('report.organization', 'organization')
        .where(
          new Brackets((qb) => {
            qb.where(`report.description ilike :search`, {
              search: `%${search ?? ''}%`,
            }).orWhere(`report.program_description ilike :search`, {
              search: `%${search ?? ''}%`,
            });
          }),
        );

      if (user.role && user.role.id != 1 && user.organization) {
        queryBuilder.andWhere(`report.organization_id = :organization_id`, {
          organization_id: user.organization.id,
        });
      }

      if (month) {
        queryBuilder.andWhere(
          `extract(month from report.created_at) = :month`,
          { month },
        );
      }

      if (year) {
        queryBuilder.andWhere(`extract(year from report.created_at) = :year`, {
          year,
        });
      }

      if (triwulan_id) {
        queryBuilder.andWhere(`report.triwulan_id = :triwulan_id`, {
          triwulan_id,
        });
      }

      const [column, order] = this.getOrderQuery(sort).split(' ');

      const [reports, count] = await queryBuilder
        .limit(limit)
        .offset((page - 1) * limit)
        .orderBy(column, order as 'ASC' | 'DESC' | undefined)
        .getManyAndCount();

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: reports,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): string {
    let sortRes = `report.id DESC`;

    switch (sort) {
      case 'terbaru':
        sortRes = `report.id DESC`;
        break;
      case 'terlama':
        sortRes = `report.id ASC`;
        break;
      case 'a-z':
        sortRes = `report.description ASC`;
        break;
      case 'z-a':
        sortRes = `report.description DESC`;
        break;
    }

    return sortRes;
  }

  async listMaster(
    query: Omit<ListReportUserRequest, 'fund_source_id'>,
  ): Promise<DataWithPaginationApiResponse<Array<Master>>> {
    try {
      const { sort, month, year, triwulan_id } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const user = await this.repository.manager.findOneOrFail(User, {
        where: { id: this.request.user.user_id },
        relations: {
          role: true,
        },
      });

      const where: FindOptionsWhere<Master> | FindOptionsWhere<Master>[] = {};
      const order: FindOptionsOrder<Master> = {
        id: 'DESC',
      };

      if (month) {
        Object.assign<FindOptionsWhere<Master>, FindOptionsWhere<Master>>(
          where,
          {
            created_at: Raw(
              (alias) => `extract(month from ${alias}) = :month`,
              { month },
            ),
          },
        );
      }
      if (year) {
        Object.assign<FindOptionsWhere<Master>, FindOptionsWhere<Master>>(
          where,
          {
            created_at: Raw((alias) => `extract(year from ${alias}) = :year`, {
              year,
            }),
          },
        );
      }
      if (triwulan_id) {
        Object.assign<FindOptionsWhere<Master>, FindOptionsWhere<Master>>(
          where,
          { triwulan_id },
        );
      }
      if (user && user.role && user.role.name.toLowerCase() === 'opd') {
        Object.assign<FindOptionsWhere<Master>, FindOptionsWhere<Master>>(
          where,
          { user_id: user.id },
        );
      }

      if (sort) {
        switch (sort) {
          case 'terbaru':
            Object.assign<FindOptionsOrder<Master>, FindOptionsOrder<Master>>(
              order,
              {
                id: 'DESC',
              },
            );
            break;
          case 'terlama':
            Object.assign<FindOptionsOrder<Master>, FindOptionsOrder<Master>>(
              order,
              {
                id: 'ASC',
              },
            );
            break;
          case 'a-z':
            Object.assign<FindOptionsOrder<Master>, FindOptionsOrder<Master>>(
              order,
              {
                description: 'ASC',
              },
            );
            break;
          case 'z-a':
            Object.assign<FindOptionsOrder<Master>, FindOptionsOrder<Master>>(
              order,
              {
                description: 'DESC',
              },
            );
            break;
        }
      }

      const [masters, count] = await this.masterRepository.findAndCount({
        where: Object.keys(where).length > 0 ? where : undefined,
        order,
        relations: {
          triwulan: true,
          purpose: true,
          organization: true,
          masterOccassions: {
            occassion: true,
          },
        },
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: masters,
      };
    } catch (error) {
      throw error;
    }
  }

  async listDataTriwulan(
    query: Omit<ListReportUserRequest, 'triwulan_id'>,
  ): Promise<DataWithPaginationApiResponse<Array<DataTriwulan>>> {
    // const organization: number[] =
    //   this.request.user.organization && this.request.user.organization.id
    //     ? [this.request.user.organization.id]
    //     : [];
    // if (
    //   this.request.user.userOrganization &&
    //   this.request.user.userOrganization.length > 0
    // ) {
    //   organization.push(
    //     ...this.request.user.userOrganization.map(
    //       (organization) => organization.organization_id,
    //     ),
    //   );
    // }
    try {
      const { sort, month, year, fund_source_id } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const user = await this.repository.manager.findOneOrFail(User, {
        where: { id: this.request.user.user_id },
        relations: {
          role: true,
        },
      });

      const where:
        | FindOptionsWhere<DataTriwulan>
        | FindOptionsWhere<DataTriwulan>[] = {};
      const order: FindOptionsOrder<DataTriwulan> = {
        id: 'DESC',
      };

      if (month) {
        Object.assign<
          FindOptionsWhere<DataTriwulan>,
          FindOptionsWhere<DataTriwulan>
        >(where, {
          created_at: Raw((alias) => `extract(month from ${alias}) = :month`, {
            month,
          }),
        });
      }
      if (year) {
        Object.assign<
          FindOptionsWhere<DataTriwulan>,
          FindOptionsWhere<DataTriwulan>
        >(where, {
          created_at: Raw((alias) => `extract(year from ${alias}) = :year`, {
            year,
          }),
        });
      }
      if (fund_source_id) {
        Object.assign<
          FindOptionsWhere<DataTriwulan>,
          FindOptionsWhere<DataTriwulan>
        >(where, {
          fund_source_id: fund_source_id,
        });
      }
      if (user && user.role && user.role.name.toLowerCase() === 'opd') {
        Object.assign<FindOptionsWhere<Master>, FindOptionsWhere<Master>>(
          where,
          { user_id: user.id },
        );
      }

      if (sort) {
        switch (sort) {
          case 'terbaru':
            Object.assign<
              FindOptionsOrder<DataTriwulan>,
              FindOptionsOrder<DataTriwulan>
            >(order, {
              id: 'DESC',
            });
            break;
          case 'terlama':
            Object.assign<
              FindOptionsOrder<DataTriwulan>,
              FindOptionsOrder<DataTriwulan>
            >(order, {
              id: 'ASC',
            });
            break;
          case 'a-z':
            Object.assign<
              FindOptionsOrder<DataTriwulan>,
              FindOptionsOrder<DataTriwulan>
            >(order, {
              activity_name: 'ASC',
            });
            break;
          case 'z-a':
            Object.assign<
              FindOptionsOrder<DataTriwulan>,
              FindOptionsOrder<DataTriwulan>
            >(order, {
              activity_name: 'DESC',
            });
            break;
        }
      }

      // if (this.request.user.role.name.toLowerCase() != 'superadmin') {
      //   Object.assign<
      //     FindOptionsWhere<DataTriwulan>,
      //     FindOptionsWhere<DataTriwulan>
      //   >(where, {
      //     createdBy: {
      //       userOrganization: {
      //         organization_id: In([...new Set(organization)]),
      //       },
      //     },
      //   });
      // }

      const [dataTriwulan, count] =
        await this.dataTriwulanRepository.findAndCount({
          where: Object.keys(where).length > 0 ? where : undefined,
          order,
          relations: {
            fundSource: true,
            activity: {
              program: true,
            },
            createdBy: {
              userOrganization: true,
            },
          },
          skip: (page - 1) * limit,
          take: limit,
        });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: dataTriwulan,
      };
    } catch (error) {
      throw error;
    }
  }

  async downloadListMaster(
    query: DownloadListReportMasterRequest,
  ): Promise<[string, ListReportMasterUserDownload[]]> {
    try {
      const { sort, month, year, triwulan_id } = query;

      const where: FindOptionsWhere<Master> | FindOptionsWhere<Master>[] = {};
      const order: FindOptionsOrder<Master> = {
        id: 'DESC',
      };

      if (month) {
        Object.assign<FindOptionsWhere<Master>, FindOptionsWhere<Master>>(
          where,
          {
            created_at: Raw(
              (alias) => `extract(month from ${alias}) = :month`,
              { month },
            ),
          },
        );
      }
      if (year) {
        Object.assign<FindOptionsWhere<Master>, FindOptionsWhere<Master>>(
          where,
          {
            created_at: Raw((alias) => `extract(year from ${alias}) = :year`, {
              year,
            }),
          },
        );
      }
      if (triwulan_id) {
        Object.assign<FindOptionsWhere<Master>, FindOptionsWhere<Master>>(
          where,
          { triwulan_id },
        );
      }

      if (sort) {
        switch (sort) {
          case 'terbaru':
            Object.assign<FindOptionsOrder<Master>, FindOptionsOrder<Master>>(
              order,
              {
                id: 'DESC',
              },
            );
            break;
          case 'terlama':
            Object.assign<FindOptionsOrder<Master>, FindOptionsOrder<Master>>(
              order,
              {
                id: 'ASC',
              },
            );
            break;
          case 'a-z':
            Object.assign<FindOptionsOrder<Master>, FindOptionsOrder<Master>>(
              order,
              {
                description: 'ASC',
              },
            );
            break;
          case 'z-a':
            Object.assign<FindOptionsOrder<Master>, FindOptionsOrder<Master>>(
              order,
              {
                description: 'DESC',
              },
            );
            break;
        }
      }

      const masters = await this.masterRepository.find({
        where: Object.keys(where).length > 0 ? where : undefined,
        order,
        relations: {
          triwulan: true,
          purpose: true,
          organization: true,
          masterOccassions: {
            occassion: true,
          },
        },
      });

      const data = masters.map<ListReportMasterUserDownload>((data, idx) => ({
        no: idx + 1,
        indikator_kegiatan: data.description ?? '',
        organisasi: data.organization?.title ?? '',
        tanggal: dayjs(data.created_at).format('DD/MM/YYYY') ?? '',
        sasaran: data.purpose?.title ?? '',
        urusan: data.masterOccassions
          .map<string>(({ occassion }) => occassion?.title ?? '')
          .filter((v) => v != '')
          .join(', '),
      }));

      const path = await this.excelService.download({
        data: data,
        name: 'data-master',
      });

      return [path, data];
    } catch (error) {
      throw error;
    }
  }

  async downloadSingleDataTriwulan(
    id: number,
  ): Promise<[string, ListReportTriwulanUserDownload]> {
    try {
      const dataTriwulan = await this.dataTriwulanRepository.find({
        where: { id: id },
        relations: {
          fundSource: true,
        },
      });

      const data = dataTriwulan.map<ListReportTriwulanUserDownload>(
        (data, idx) => ({
          no: idx + 1,
          nama_kegiatan: data.activity_name,
          lokasi_kegiatan: data.activity_location,
          sumber_dana: data.fundSource?.name ?? '',
          pagu_dana: data.fund_ceiling?.toString() ?? '',
          opd_organisasi: data.management_organization ?? '',
          nama_pptk: data.pptk_name ?? '',
          nomor_dan_tanggal_kontrak: data.contract_number_date ?? '',
          nama_kontraktor: data.contractor_name ?? '',
          jangka_waktu: data.implementation_period ?? '',
          nilai_kontrak: data.contract_value?.toString() ?? '',
          realisasi_fisik: data.physical_realization?.toString() + '%' ?? '0%',
          realisasi_keuangan: data.fund_realization?.toString() + '%' ?? '0%',
          volume_kegiatan: data.activity_volume ?? '',
          output_kegiatan: data.activity_output ?? '',
          manfaat_kegiatan_kelompok_sasaran_langsung:
            data.direct_target_group ?? '',
          manfaat_kegiatan_kelompok_sasaran_tidak_langsung:
            data.indirect_target_group ?? '',
          jumlah_tenaga_kerja_lokal: data.local_workforce?.toString() ?? '',
          jumlah_tenaga_kerja_non_lokal:
            data.non_local_workforce?.toString() ?? '',
          hambatan_dan_permasalahan: data.problems ?? '',
          solusi_permasalahan: data.solution ?? '',
          jenis_pengadaan: data.procurement_type ?? '',
          cara_pengadaan: data.procurement_method ?? '',
          tanggal_kontrak: data.contract_date
            ? dayjs(data.contract_date).format('DD/MM/YYYY')
            : '',
          nama_penanggung_jawab: data.pic_name ?? '',
          nama_pimpinan: data.leader_name ?? '',
          opsi: data.optional ?? '',
          alasan: data.reason ?? '',
        }),
      );

      const path = await this.excelService.download({
        data: data,
        name: `data-triwulan-${data[0].nama_kegiatan}`,
      });

      return [path, data[0]];
    } catch (error) {
      throw error;
    }
  }

  async downloadListDataTriwulan(
    query: DownloadListReportTriwulanRequest,
  ): Promise<[string, ListReportTriwulanUserDownload[]]> {
    try {
      const { sort, month, year, fund_source_id } = query;

      const where:
        | FindOptionsWhere<DataTriwulan>
        | FindOptionsWhere<DataTriwulan>[] = {};
      const order: FindOptionsOrder<DataTriwulan> = {
        id: 'DESC',
      };

      if (month) {
        Object.assign<
          FindOptionsWhere<DataTriwulan>,
          FindOptionsWhere<DataTriwulan>
        >(where, {
          created_at: Raw((alias) => `extract(month from ${alias}) = :month`, {
            month,
          }),
        });
      }
      if (year) {
        Object.assign<
          FindOptionsWhere<DataTriwulan>,
          FindOptionsWhere<DataTriwulan>
        >(where, {
          created_at: Raw((alias) => `extract(year from ${alias}) = :year`, {
            year,
          }),
        });
      }
      if (fund_source_id) {
        Object.assign<
          FindOptionsWhere<DataTriwulan>,
          FindOptionsWhere<DataTriwulan>
        >(where, {
          fund_source_id: fund_source_id,
        });
      }

      if (sort) {
        switch (sort) {
          case 'terbaru':
            Object.assign<
              FindOptionsOrder<DataTriwulan>,
              FindOptionsOrder<DataTriwulan>
            >(order, {
              id: 'DESC',
            });
            break;
          case 'terlama':
            Object.assign<
              FindOptionsOrder<DataTriwulan>,
              FindOptionsOrder<DataTriwulan>
            >(order, {
              id: 'ASC',
            });
            break;
          case 'a-z':
            Object.assign<
              FindOptionsOrder<DataTriwulan>,
              FindOptionsOrder<DataTriwulan>
            >(order, {
              activity_name: 'ASC',
            });
            break;
          case 'z-a':
            Object.assign<
              FindOptionsOrder<DataTriwulan>,
              FindOptionsOrder<DataTriwulan>
            >(order, {
              activity_name: 'DESC',
            });
            break;
        }
      }

      const dataTriwulan = await this.dataTriwulanRepository.find({
        where: Object.keys(where).length > 0 ? where : undefined,
        order,
        relations: {
          fundSource: true,
        },
      });

      if (dataTriwulan.length === 0)
        throw new EntityNotFoundError(
          DataTriwulan,
          'Data triwulan masih kosong',
        );

      const data = dataTriwulan.map<ListReportTriwulanUserDownload>(
        (data, idx) => ({
          no: idx + 1,
          nama_kegiatan: data.activity_name,
          lokasi_kegiatan: data.activity_location,
          sumber_dana: data.fundSource?.name ?? '',
          pagu_dana: data.fund_ceiling?.toString() ?? '',
          opd_organisasi: data.management_organization ?? '',
          nama_pptk: data.pptk_name ?? '',
          nomor_dan_tanggal_kontrak: data.contract_number_date ?? '',
          nama_kontraktor: data.contractor_name ?? '',
          jangka_waktu: data.implementation_period ?? '',
          nilai_kontrak: data.contract_value?.toString() ?? '',
          realisasi_fisik: data.physical_realization?.toString() + '%' ?? '0%',
          realisasi_keuangan: data.fund_realization?.toString() + '%' ?? '0%',
          volume_kegiatan: data.activity_volume ?? '',
          output_kegiatan: data.activity_output ?? '',
          manfaat_kegiatan_kelompok_sasaran_langsung:
            data.direct_target_group ?? '',
          manfaat_kegiatan_kelompok_sasaran_tidak_langsung:
            data.indirect_target_group ?? '',
          jumlah_tenaga_kerja_lokal: data.local_workforce?.toString() ?? '',
          jumlah_tenaga_kerja_non_lokal:
            data.non_local_workforce?.toString() ?? '',
          hambatan_dan_permasalahan: data.problems ?? '',
          solusi_permasalahan: data.solution ?? '',
          jenis_pengadaan: data.procurement_type ?? '',
          cara_pengadaan: data.procurement_method ?? '',
          tanggal_kontrak: data.contract_date
            ? dayjs(data.contract_date).format('DD/MM/YYYY')
            : '',
          nama_penanggung_jawab: data.pic_name ?? '',
          nama_pimpinan: data.leader_name ?? '',
          opsi: data.optional ?? '',
          alasan: data.reason ?? '',
        }),
      );

      const path = await this.excelService.download({
        data: data,
        name: 'data-triwulan',
      });

      return [path, data];
    } catch (error) {
      throw error;
    }
  }

  async downloadPdfListMaster(
    query: DownloadListReportMasterRequest,
  ): Promise<string> {
    try {
      const url = `${this.request.protocol}://${this.request.get(
        'Host',
      )}/data-report/template/pdf/data-master`;
      const path = await this.excelService.pdfDownload({
        url: url,
        name: 'data-master',
      });

      return path;
    } catch (error) {
      throw error;
    }
  }

  async downloadPdfListTriwulan(
    query: DownloadListReportTriwulanRequest,
  ): Promise<string> {
    try {
      const url = `${this.request.protocol}://${this.request.get(
        'Host',
      )}/data-report/template/pdf/data-triwulan`;
      const path = await this.excelService.pdfDownload({
        url: url,
        name: 'data-triwulan',
      });

      return path;
    } catch (error) {
      throw error;
    }
  }
}
