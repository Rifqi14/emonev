import {
  Body,
  Controller,
  Get,
  Header,
  Param,
  Patch,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiProduces,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  SwaggerOkResponse,
  SwaggerPaginationResponse,
} from 'src/common/openapi';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { Master } from 'src/database/models/master.entity';
import {
  CreateReportRequest,
  CreateReportResponse,
  DeleteReportRequest,
  DeleteReportResponse,
  DetailReportResponse,
  DownloadListReportMasterRequest,
  DownloadListReportTriwulanRequest,
  ListReportMasterRequest,
  ListReportMasterUserResponse,
  ListReportRequest,
  ListReportResponse,
  ListReportTriwulanRequest,
  ListReportTriwulanUserResponse,
  UpdateReportRequest,
  UpdateReportResponse,
} from './report.dto';
import { ReportService } from './report.service';
import { Response } from 'express';
import { unlink } from 'fs';
import { PublicAPI } from '../authentication/authentication.guard';
import dayjs from 'dayjs';

@ApiTags('Data Report')
@ApiBearerAuth()
@Controller('data-report')
export class ReportController {
  constructor(private readonly reportService: ReportService) {}

  @SwaggerOkResponse(DetailReportResponse)
  @Get('detail/:id')
  async detail(@Param('id') id: number): Promise<DetailReportResponse> {
    return {
      statusCode: 200,
      message: `Pengambilan data berhasil`,
      data: {
        result: await this.reportService.detail(id),
      },
    };
  }

  @SwaggerOkResponse(ListReportResponse)
  @Get('list')
  async list(@Query() query: ListReportRequest): Promise<ListReportResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.reportService.list(query),
    };
  }

  @SwaggerOkResponse(CreateReportResponse)
  @Post('create')
  async create(
    @Body() data: CreateReportRequest,
  ): Promise<CreateReportResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.reportService.create(data),
    };
  }

  @SwaggerOkResponse(UpdateReportResponse)
  @Patch('update')
  async update(
    @Body() data: UpdateReportRequest,
  ): Promise<UpdateReportResponse> {
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.reportService.update(data),
    };
  }

  @SwaggerOkResponse(DeleteReportResponse)
  @Patch('delete')
  async delete(
    @Body() data: DeleteReportRequest,
  ): Promise<DeleteReportResponse> {
    await this.reportService.delete(data.data_report_id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }

  @ApiTags('User | Data Report')
  @SwaggerPaginationResponse(Master)
  @Get('user/data-master')
  async listMaster(
    @Query() query: ListReportMasterRequest,
  ): Promise<ListReportMasterUserResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.reportService.listMaster(query),
    };
  }

  @ApiTags('User | Data Report')
  @SwaggerPaginationResponse(DataTriwulan)
  @Get('user/data-triwulan')
  async listDataTriwulan(
    @Query() query: ListReportTriwulanRequest,
  ): Promise<ListReportTriwulanUserResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.reportService.listDataTriwulan(query),
    };
  }

  @ApiTags('User | Data Report')
  @SwaggerPaginationResponse(Master)
  @Get('user/data-master/excel')
  async downloadExcelListMaster(
    @Query() query: DownloadListReportMasterRequest,
    @Res() res: Response,
  ): Promise<void> {
    const [path, data] = await this.reportService.downloadListMaster(query);
    if (!data || data.length === 0) {
      res.status(404).json({
        statusCode: 404,
        message: 'Data tidak ditemukan',
      });
      return;
    }

    res.download(path, (err) => {
      if (!err) {
        unlink(path, (err) => {
          if (err) throw err;
        });
      }
    });
  }

  @ApiTags('User | Data Report')
  @SwaggerPaginationResponse(DataTriwulan)
  @Get('user/data-triwulan/excel')
  async downloadExcelListDataTriwulan(
    @Query() query: DownloadListReportTriwulanRequest,
    @Res() res: Response,
  ): Promise<void> {
    const [path, data] = await this.reportService.downloadListDataTriwulan(
      query,
    );
    if (!data || data.length === 0) {
      res.status(404).json({
        statusCode: 404,
        message: 'Data tidak ditemukan',
      });
      return;
    }

    res.download(path, (err) => {
      if (!err) {
        unlink(path, (err) => {
          if (err) throw err;
        });
      }
    });
  }

  @ApiTags('User | Data Report')
  @SwaggerPaginationResponse(DataTriwulan)
  @Get('user/data-triwulan/excel/:id')
  async downloadExcelSingleDataTriwulan(
    @Param('id') id: number,
    @Res() res: Response,
  ): Promise<void> {
    const [path, data] = await this.reportService.downloadSingleDataTriwulan(
      id,
    );
    if (!data) {
      res.status(404).json({
        statusCode: 404,
        message: 'Data tidak ditemukan',
      });
      return;
    }

    res.download(path, (err) => {
      if (!err) {
        unlink(path, (err) => {
          if (err) throw err;
        });
      }
    });
  }

  @ApiTags('User | Data Report')
  @SwaggerPaginationResponse(Master)
  @Header('Content-Type', 'application/pdf')
  @Get('user/data-master/pdf')
  async downloadPdfListMaster(
    @Query() query: DownloadListReportMasterRequest,
    @Res() res: Response,
  ): Promise<void> {
    const path = await this.reportService.downloadPdfListMaster(query);

    res.download(path, (err) => {
      if (!err) {
        unlink(path, (err) => {
          if (err) throw err;
        });
      }
    });
  }

  @ApiTags('User | Data Report')
  @SwaggerPaginationResponse(Master)
  @Header('Content-Type', 'application/pdf')
  @Get('user/data-triwulan/pdf')
  async downloadPdfListTriwulan(
    @Query() query: DownloadListReportTriwulanRequest,
    @Res() res: Response,
  ): Promise<void> {
    const path = await this.reportService.downloadPdfListTriwulan(query);

    res.download(path, (err) => {
      if (!err) {
        unlink(path, (err) => {
          if (err) throw err;
        });
      }
    });
  }

  @PublicAPI()
  @Get('template/pdf/data-master')
  async templateDataMaster(
    @Res() res: Response,
    @Query() query: DownloadListReportMasterRequest,
  ) {
    const [, data] = await this.reportService.downloadListMaster(query);
    return res.render('data-triwulan', {
      title: 'Laporan Master',
      headers: Object.keys(data[0]).map((key) =>
        key
          .split('_')
          .map((column) => column.charAt(0).toUpperCase() + column.slice(1))
          .join(' '),
      ),
      data: data,
      date: dayjs().format('DD MMMM YYYY'),
    });
  }

  @PublicAPI()
  @Get('template/pdf/data-triwulan')
  async templateDataTriwulan(
    @Res() res: Response,
    @Query() query: DownloadListReportTriwulanRequest,
  ) {
    const [, data] = await this.reportService.downloadListDataTriwulan(query);
    return res.render('data-triwulan', {
      title: 'Laporan Triwulan',
      headers: Object.keys(data[0])
        .filter((v) => v != 'id')
        .map((key) =>
          key
            .split('_')
            .map((column) => column.charAt(0).toUpperCase() + column.slice(1))
            .join(' '),
        ),
      data: data,
      date: dayjs().format('DD MMMM YYYY'),
    });
  }
}
