import { Provider } from '@nestjs/common';
import { Report } from 'src/database/models/report.entity';
import { DataSource } from 'typeorm';

export const reportProviders: Array<Provider> = [
  {
    provide: 'REPORT_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Report),
    inject: ['DATA_SOURCE'],
  },
];
