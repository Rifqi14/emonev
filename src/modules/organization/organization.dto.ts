import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { Organization } from 'src/database/models/organization.entity';

export class CreateOrganizationRequest {
  @ApiPropertyOptional()
  code?: string;

  @ApiProperty()
  title: string;
}

export class CreateOrganizationResponse extends BaseApiResponse {
  @ApiProperty()
  data: Organization;
}

// Update
export class UpdateOrganizationRequest extends PartialType(
  CreateOrganizationRequest,
) {
  @ApiProperty()
  organization_id: number;
}

export class UpdateOrganizationResponse extends CreateOrganizationResponse {}

// Delete
export class DeleteOrganizationRequest {
  @ApiProperty()
  organization_id: number;
}

export class DeleteOrganizationResponse extends BaseApiResponse {}

// List
export class ListOrganizationResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<Organization>> })
  data: DataWithPaginationApiResponse<Array<Organization>>;
}
