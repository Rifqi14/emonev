import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { OrganizationService } from './organization.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  CreateOrganizationRequest,
  CreateOrganizationResponse,
  DeleteOrganizationRequest,
  DeleteOrganizationResponse,
  ListOrganizationResponse,
  UpdateOrganizationRequest,
  UpdateOrganizationResponse,
} from './organization.dto';
import { ListBaseRequest } from 'src/utils/helper';

@ApiTags('Organization')
@ApiBearerAuth()
@Controller('org')
export class OrganizationController {
  constructor(private readonly organizationService: OrganizationService) {}

  @SwaggerOkResponse(CreateOrganizationResponse)
  @Get('detail/:id')
  async detail(@Param('id') id: number): Promise<CreateOrganizationResponse> {
    return {
      statusCode: 200,
      message: `Pengambilan data berhasil`,
      data: await this.organizationService.detail(id),
    };
  }

  @SwaggerOkResponse(ListOrganizationResponse)
  @Get('list')
  async list(
    @Query() query: ListBaseRequest,
  ): Promise<ListOrganizationResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.organizationService.list(query),
    };
  }

  @SwaggerOkResponse(CreateOrganizationResponse)
  @Post('create')
  async create(
    @Body() data: CreateOrganizationRequest,
  ): Promise<CreateOrganizationResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.organizationService.create(data),
    };
  }

  @SwaggerOkResponse(UpdateOrganizationResponse)
  @Patch('update')
  async update(
    @Body() data: UpdateOrganizationRequest,
  ): Promise<UpdateOrganizationResponse> {
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.organizationService.update(data),
    };
  }

  @SwaggerOkResponse(DeleteOrganizationResponse)
  @Patch('delete')
  async delete(
    @Body() data: DeleteOrganizationRequest,
  ): Promise<DeleteOrganizationResponse> {
    await this.organizationService.delete(data.organization_id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }
}
