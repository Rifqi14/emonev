import { Inject, Injectable } from '@nestjs/common';
import { Organization } from 'src/database/models/organization.entity';
import { Status } from 'src/database/models/status.entity';
import { ListBaseRequest, generateCode } from 'src/utils/helper';
import { FindOptionsOrder, FindOptionsWhere, ILike, Repository } from 'typeorm';
import {
  CreateOrganizationRequest,
  UpdateOrganizationRequest,
} from './organization.dto';
import { DataWithPaginationApiResponse } from 'src/common/api-response';

@Injectable()
export class OrganizationService {
  constructor(
    @Inject('ORGANIZATION_REPOSITORY')
    private repository: Repository<Organization>,
  ) {}

  async create(data: CreateOrganizationRequest): Promise<Organization> {
    try {
      const { title } = data;
      const latest = await this.repository.findOne({
        where: {},
        order: { id: 'DESC' },
      });
      const code = data.code ?? generateCode(latest?.id ?? 1, 'Org');

      return await this.repository.manager.transaction(
        async (trx) =>
          await trx
            .create(Organization, {
              code,
              title,
              status: await trx.findOneByOrFail(Status, { name: 'Active' }),
            })
            .save(),
      );
    } catch (error) {
      throw error;
    }
  }

  async detail(id: number): Promise<Organization> {
    try {
      return await this.repository.findOneOrFail({
        where: {
          id,
          status_id: (
            await this.repository.manager.findOneOrFail(Status, {
              where: { name: 'Active' },
            })
          ).id,
        },
        relations: {
          status: true,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async update(data: UpdateOrganizationRequest): Promise<Organization> {
    try {
      const { organization_id, code, title } = data;

      await this.repository.manager.transaction(async (trx) =>
        trx.update(
          Organization,
          { id: organization_id },
          {
            code,
            title,
          },
        ),
      );

      return await this.repository.findOneByOrFail({
        id: organization_id,
      });
    } catch (error) {
      throw error;
    }
  }

  async delete(organization_id: number): Promise<void> {
    try {
      const organization = await this.repository.findOneOrFail({
        where: { id: organization_id },
      });

      organization.status = await this.repository.manager.findOneOrFail(
        Status,
        { where: { name: 'Deleted' } },
      );

      await organization.save();
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListBaseRequest,
  ): Promise<DataWithPaginationApiResponse<Array<Organization>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;
      const { id: status_id } = await this.repository.manager.findOneOrFail(
        Status,
        {
          where: { name: 'Active' },
        },
      );

      const where: Array<FindOptionsWhere<Organization>> = [
        {
          status_id: status_id,
        },
      ];

      if (search) {
        where.pop();
        where.push({
          title: ILike(`%${search}%`),
          status_id: status_id,
        });
        where.push({
          code: ILike(`%${search}%`),
          status_id: status_id,
        });
      }

      const [organizations, count] = await this.repository.findAndCount({
        where: where,
        order: this.getOrderQuery(sort),
        relations: {
          status: true,
        },
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: organizations,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<Organization> {
    const sortRes: FindOptionsOrder<Organization> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          title: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          title: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
