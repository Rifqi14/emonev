import { Provider } from '@nestjs/common';
import { Organization } from 'src/database/models/organization.entity';
import { DataSource } from 'typeorm';

export const organizationProviders: Array<Provider> = [
  {
    provide: 'ORGANIZATION_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Organization),
    inject: ['DATA_SOURCE'],
  },
];
