import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendForgotPassword(password: string, email: string): Promise<void> {
    await this.mailerService.sendMail({
      to: email,
      subject: `Reset Password`,
      template: './reset-password',
      context: {
        password,
      },
    });
  }
}
