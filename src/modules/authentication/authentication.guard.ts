import {
  CanActivate,
  ExecutionContext,
  Injectable,
  SetMetadata,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { config } from 'dotenv';
import { Request } from 'express';
import { PayloadClientResponse } from './authentication.dto';

export const IS_PUBLIC_KEY = 'isPublic';
export const PublicAPI = () => SetMetadata(IS_PUBLIC_KEY, true);

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private jwtService: JwtService, private reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    config();
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) {
      return true;
    }

    const request = context
      .switchToHttp()
      .getRequest<
        Request & { user: PayloadClientResponse; is_admin_bidang: boolean }
      >();
    const token = this.extractTokenFromHeader(request);
    if (!token) {
      throw new UnauthorizedException();
    }
    const secret = process.env.APP_SECRET;
    try {
      const payload = await this.jwtService.verifyAsync<PayloadClientResponse>(
        token,
        {
          secret: secret,
        },
      );
      const admin = payload.role.name.toLowerCase().includes('admin');
      const bidang = payload.role.name.toLowerCase().includes('bidang');
      request['user'] = payload;
      request['is_admin_bidang'] = admin && bidang;
    } catch {
      throw new UnauthorizedException();
    }
    return true;
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
