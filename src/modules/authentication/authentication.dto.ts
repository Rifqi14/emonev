import { ApiProperty } from '@nestjs/swagger';
import { BaseApiResponse } from 'src/common/api-response';
import { Organization } from 'src/database/models/organization.entity';
import { Role } from 'src/database/models/role.entity';
import { Status } from 'src/database/models/status.entity';
import { UserOrganization } from 'src/database/models/user-organization.entity';
import { User } from 'src/database/models/user.entity';

export class SignInRequest {
  @ApiProperty()
  username: string;

  @ApiProperty()
  password: string;
}

export class PayloadClientResponse {
  @ApiProperty()
  user_id: number;

  @ApiProperty()
  username: string;

  @ApiProperty({ type: Role })
  role: Role;

  @ApiProperty({ type: Organization })
  organization?: Organization;

  @ApiProperty({ type: Status })
  status?: Status;

  @ApiProperty({ type: [UserOrganization] })
  userOrganization?: UserOrganization[];
}

export class SignInResponse {
  @ApiProperty()
  statusCode: number;

  @ApiProperty()
  access_token: string;

  @ApiProperty({ type: PayloadClientResponse })
  payloadClient: PayloadClientResponse;
}

export class GetProfileResponse extends BaseApiResponse {
  @ApiProperty({ type: User })
  data: User;
}
