import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compareSync } from 'bcrypt';
import { UserService } from '../user/user.service';
import {
  PayloadClientResponse,
  SignInRequest,
  SignInResponse,
} from './authentication.dto';
import { User } from 'src/database/models/user.entity';
import { CreateUserRequest } from '../user/user.dto';
import { MailService } from '../mail/mail.service';
import { generateString } from 'src/utils/helper';

@Injectable()
export class AuthenticationService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private mailService: MailService,
  ) {}

  async signIn(req: SignInRequest): Promise<SignInResponse> {
    try {
      const user = await this.userService.findByUsername(req.username);

      if (!user || !compareSync(req.password, user.password))
        throw new UnauthorizedException();

      const payloadClient: PayloadClientResponse = {
        user_id: user.id,
        username: user.username,
        role: user.role,
        organization: user.organization,
        status: user.status,
        userOrganization: user.userOrganization,
      };

      const access_token = await this.jwtService.signAsync(payloadClient);

      return {
        statusCode: 200,
        access_token,
        payloadClient,
      };
    } catch (error) {
      throw error;
    }
  }

  async getProfile(username: string): Promise<User> {
    try {
      return await this.userService.getProfile(username);
    } catch (error) {
      throw error;
    }
  }

  async register(data: CreateUserRequest): Promise<User> {
    try {
      return await this.userService.create(data);
    } catch (error) {
      throw error;
    }
  }

  async forgotPassword(email: string): Promise<void> {
    try {
      const user = await this.userService.findByEmail(email);
      const generatedPassword = generateString(8).replace(' ', '');
      await this.userService.update({
        password: generatedPassword,
        user_id: user.id,
      });
      await this.mailService.sendForgotPassword(generatedPassword, email);
    } catch (error) {
      throw error;
    }
  }
}
