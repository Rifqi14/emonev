import { Body, Controller, Get, Post, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  GetProfileResponse,
  PayloadClientResponse,
  SignInRequest,
  SignInResponse,
} from './authentication.dto';
import { AuthenticationService } from './authentication.service';
import { PublicAPI } from './authentication.guard';
import {
  CreateUserRequest,
  CreateUserResponse,
  ForgotPasswordRequest,
  ForgotPasswordResponse,
  RegisterUserRequest,
} from '../user/user.dto';

@ApiTags('Authentication')
@Controller('user')
export class AuthenticationController {
  constructor(private readonly authenticationService: AuthenticationService) {}

  @PublicAPI()
  @SwaggerOkResponse(SignInResponse)
  @Post('login')
  signIn(@Body() data: SignInRequest): Promise<SignInResponse> {
    return this.authenticationService.signIn(data);
  }

  @ApiBearerAuth()
  @SwaggerOkResponse(GetProfileResponse)
  @Get('profile')
  async getProfile(
    @Req() req: Request & { user: PayloadClientResponse },
  ): Promise<GetProfileResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.authenticationService.getProfile(req.user.username),
    };
  }

  @PublicAPI()
  @SwaggerOkResponse(CreateUserResponse)
  @Post('register')
  async register(
    @Body() data: RegisterUserRequest,
  ): Promise<CreateUserResponse> {
    return {
      statusCode: 200,
      message: 'Pembuatan data berhasil',
      data: await this.authenticationService.register(data),
    };
  }

  @PublicAPI()
  @SwaggerOkResponse(ForgotPasswordResponse)
  @Post('forgot-password')
  async forgotPassword(
    @Body() data: ForgotPasswordRequest,
  ): Promise<ForgotPasswordResponse> {
    await this.authenticationService.forgotPassword(data.email);
    return {
      statusCode: 200,
      message: 'Password changed successfully',
    };
  }
}
