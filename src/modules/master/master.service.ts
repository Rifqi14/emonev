import {
  Inject,
  Injectable,
  Scope,
  UnauthorizedException,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
import { Master } from 'src/database/models/master.entity';
import { MasterOccassion } from 'src/database/models/master_occassion.entity';
import { User } from 'src/database/models/user.entity';
import {
  FindOptionsOrder,
  FindOptionsWhere,
  ILike,
  In,
  Repository,
} from 'typeorm';
import { PayloadClientResponse } from '../authentication/authentication.dto';
import {
  CreateMasterRequest,
  ListMasterRequest,
  UpdateMasterRequest,
} from './master.dto';

@Injectable({ scope: Scope.REQUEST })
export class MasterService {
  constructor(
    @Inject('MASTER_REPOSITORY')
    private masterRepository: Repository<Master>,
    @Inject(REQUEST)
    private request: Request & {
      user: PayloadClientResponse;
      is_admin_bidang: boolean;
    },
  ) {}

  async create(data: CreateMasterRequest): Promise<Master> {
    try {
      const { description, organization_id, purpose_id, triwulan_id } = data;

      return await this.masterRepository.manager.transaction(async (trx) => {
        const creator = await trx.findOneOrFail(User, {
          where: { id: this.request.user.user_id },
        });
        const occassions = data.occassions
          ? data.occassions.map((occassion) =>
              trx.create(MasterOccassion, {
                occassion_id: occassion,
              }),
            )
          : undefined;
        const master = await trx
          .create(Master, {
            description,
            organization_id,
            purpose_id,
            triwulan_id,
            masterOccassions: occassions,
            createdBy: creator,
          })
          .save();

        return master;
      });
    } catch (error) {
      throw error;
    }
  }

  async detail(id: number): Promise<Master> {
    try {
      const master = await this.masterRepository.findOneOrFail({
        relations: {
          triwulan: true,
          masterOccassions: {
            occassion: true,
          },
          organization: true,
          purpose: true,
        },
        where: {
          id,
        },
      });

      return master;
    } catch (error) {
      throw error;
    }
  }

  async update(data: UpdateMasterRequest): Promise<Master> {
    try {
      const {
        data_master_id,
        description,
        organization_id,
        purpose_id,
        triwulan_id,
      } = data;

      await this.masterRepository.manager.transaction(async (trx) => {
        await trx.delete(MasterOccassion, {
          master_id: data_master_id,
        });

        const master = await this.masterRepository.findOneByOrFail({
          id: data_master_id,
        });

        const occassions = data.occassions
          ? data.occassions.map((occassion) =>
              trx.create(MasterOccassion, {
                occassion_id: occassion,
              }),
            )
          : undefined;

        master.description = description;
        master.organization_id = organization_id;
        master.purpose_id = purpose_id;
        master.triwulan_id = triwulan_id;
        if (occassions) master.masterOccassions = occassions;

        await trx.save(master);
      });

      return await this.masterRepository.findOneByOrFail({
        id: data_master_id,
      });
    } catch (error) {
      throw error;
    }
  }

  async delete(data_master_id: number): Promise<void> {
    try {
      await this.masterRepository.delete({ id: data_master_id });
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListMasterRequest,
  ): Promise<DataWithPaginationApiResponse<Array<Master>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const where: Array<FindOptionsWhere<Master>> = [];

      if (search) {
        where.push({
          description: ILike(`%${search}%`),
        });
      }

      if (this.request.is_admin_bidang) {
        if (
          !this.request.user.userOrganization ||
          this.request.user.userOrganization.length === 0
        )
          throw new UnauthorizedException();
        where[0] = {
          ...where[0],
          createdBy: {
            organization_id: In(
              this.request.user.userOrganization.map(
                (organization) => organization.organization_id,
              ),
            ),
          },
        };
      }

      const [masters, count] = await this.masterRepository.findAndCount({
        where: where.length > 0 ? where : undefined,
        order: this.getOrderQuery(sort),
        relations: {
          triwulan: true,
          purpose: true,
          organization: true,
          masterOccassions: {
            occassion: true,
          },
        },
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: masters,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<Master> {
    const sortRes: FindOptionsOrder<Master> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          description: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          description: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
