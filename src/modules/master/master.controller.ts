import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { MasterService } from './master.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  CreateMasterRequest,
  CreateMasterResponse,
  DeleteMasterRequest,
  DeleteMasterResponse,
  DetailMasterResponse,
  ListMasterRequest,
  ListMasterResponse,
  UpdateMasterRequest,
  UpdateMasterResponse,
} from './master.dto';

@ApiTags('Data Master')
@ApiBearerAuth()
@Controller('data-master')
export class MasterController {
  constructor(private readonly masterService: MasterService) {}

  @SwaggerOkResponse(DetailMasterResponse)
  @Get('detail/:id')
  async detail(@Param('id') id: number): Promise<DetailMasterResponse> {
    return {
      statusCode: 200,
      message: `Pengambilan data berhasil`,
      data: {
        result: await this.masterService.detail(id),
      },
    };
  }

  @SwaggerOkResponse(ListMasterResponse)
  @Get('list')
  async list(@Query() query: ListMasterRequest): Promise<ListMasterResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.masterService.list(query),
    };
  }

  @SwaggerOkResponse(CreateMasterResponse)
  @Post('create')
  async create(
    @Body() data: CreateMasterRequest,
  ): Promise<CreateMasterResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.masterService.create(data),
    };
  }

  @SwaggerOkResponse(UpdateMasterResponse)
  @Patch('update')
  async update(
    @Body() data: UpdateMasterRequest,
  ): Promise<UpdateMasterResponse> {
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.masterService.update(data),
    };
  }

  @SwaggerOkResponse(DeleteMasterResponse)
  @Patch('delete')
  async delete(
    @Body() data: DeleteMasterRequest,
  ): Promise<DeleteMasterResponse> {
    await this.masterService.delete(data.data_master_id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }
}
