import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { Master } from 'src/database/models/master.entity';
import { ListBaseRequest } from 'src/utils/helper';

export class CreateMasterRequest {
  @ApiPropertyOptional()
  description?: string;

  @ApiPropertyOptional()
  triwulan_id?: number;

  @ApiPropertyOptional()
  organization_id?: number;

  @ApiPropertyOptional()
  purpose_id?: number;

  @ApiPropertyOptional({ type: [Number] })
  occassions?: number[];
}

export class CreateMasterResponse extends BaseApiResponse {
  @ApiProperty()
  data: Master;
}

// Update
export class UpdateMasterRequest extends PartialType(CreateMasterRequest) {
  @ApiProperty()
  data_master_id: number;
}

export class UpdateMasterResponse extends CreateMasterResponse {}

// Delete
export class DeleteMasterRequest {
  @ApiProperty()
  data_master_id: number;
}

export class DeleteMasterResponse extends BaseApiResponse {}

// List
export class ListMasterRequest extends ListBaseRequest {}

export class ListMasterResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<Master>> })
  data: DataWithPaginationApiResponse<Array<Master>>;
}

// Detail
export class DataDetailResponse {
  @ApiProperty()
  result: Master;
}
export class DetailMasterResponse extends BaseApiResponse {
  @ApiProperty()
  data: DataDetailResponse;
}
