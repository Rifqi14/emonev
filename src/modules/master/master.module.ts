import { Module } from '@nestjs/common';
import { MasterService } from './master.service';
import { MasterController } from './master.controller';
import { masterProviders } from './master.providers';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [MasterController],
  providers: [MasterService, ...masterProviders],
  exports: [MasterService],
})
export class MasterModule {}
