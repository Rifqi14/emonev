import { Provider } from '@nestjs/common';
import { Master } from 'src/database/models/master.entity';
import { MasterOccassion } from 'src/database/models/master_occassion.entity';
import { DataSource } from 'typeorm';

export const masterProviders: Array<Provider> = [
  {
    provide: 'MASTER_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Master),
    inject: ['DATA_SOURCE'],
  },
  {
    provide: 'MASTER_OCCASSION_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(MasterOccassion),
    inject: ['DATA_SOURCE'],
  },
];
