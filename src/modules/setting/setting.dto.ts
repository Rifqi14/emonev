import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { Setting } from 'src/database/models/setting.entity';
import { SettingDataType, SettingDataTypeEnums } from 'src/utils/type';

export class CreateSettingRequest {
  @ApiProperty()
  key: string;

  @ApiProperty()
  value: string;

  @ApiProperty({ enum: SettingDataTypeEnums })
  data_type: SettingDataType;
}

export class CreateSettingResponse extends BaseApiResponse {
  @ApiProperty()
  data: Setting;
}

// Update
export class UpdateSettingRequest extends OmitType(CreateSettingRequest, [
  'data_type',
]) {}

export class UpdateSettingResponse extends CreateSettingResponse {}

// Delete
export class DeleteSettingRequest {
  @ApiProperty()
  activity_id: number;
}

export class DeleteSettingResponse extends BaseApiResponse {}

// List
export class ListSettingResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<Setting>> })
  data: DataWithPaginationApiResponse<Array<Setting>>;
}
