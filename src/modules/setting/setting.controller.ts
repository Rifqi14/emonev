import { Body, Controller, Get, Patch } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { SettingService } from './setting.service';
import { UpdateSettingRequest } from './setting.dto';

@ApiTags('Configuration')
@ApiBearerAuth()
@Controller('setting')
export class SettingController {
  constructor(private readonly settingService: SettingService) {}

  @Get('triwulan')
  async getSettingTriwulan() {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.settingService.getTriwulanSetting(),
    };
  }

  @Patch('triwulan')
  @ApiBody({ type: [UpdateSettingRequest] })
  async updateSettingTriwulan(@Body() data: UpdateSettingRequest[]) {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.settingService.update(data),
    };
  }
}
