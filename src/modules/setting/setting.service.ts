import { Inject, Injectable } from '@nestjs/common';
import { Setting } from 'src/database/models/setting.entity';
import { ILike, In, Repository } from 'typeorm';
import { UpdateSettingRequest } from './setting.dto';

@Injectable()
export class SettingService {
  constructor(
    @Inject('SETTING_REPOSITORY')
    private repository: Repository<Setting>,
  ) {}

  async getSetting(): Promise<Setting[]> {
    try {
      return await this.repository.find();
    } catch (error) {
      throw error;
    }
  }

  async update(data: UpdateSettingRequest[]): Promise<Setting[]> {
    try {
      data.forEach(async (setting) => {
        const find = await this.repository.findOneByOrFail({
          key: setting.key,
        });

        find.value = setting.value;

        await find.save();

        return find;
      });

      return await this.repository.findBy({
        key: In(data.map((setting) => setting.key)),
      });
    } catch (error) {
      throw error;
    }
  }

  async getTriwulanSetting(): Promise<Record<string, string>[]> {
    try {
      return (await this.repository.findBy({ key: ILike('TRIWULAN%') })).map(
        (record) => {
          return {
            [record.key]: record.value,
          };
        },
      );
    } catch (error) {
      throw error;
    }
  }
}
