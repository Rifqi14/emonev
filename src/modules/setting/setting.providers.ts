import { Provider } from '@nestjs/common';
import { Setting } from 'src/database/models/setting.entity';
import { DataSource } from 'typeorm';

export const settingProviders: Array<Provider> = [
  {
    provide: 'SETTING_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Setting),
    inject: ['DATA_SOURCE'],
  },
];
