import { Global, Module } from '@nestjs/common';
import { SettingService } from './setting.service';
import { SettingController } from './setting.controller';
import { settingProviders } from './setting.providers';

@Global()
@Module({
  controllers: [SettingController],
  providers: [SettingService, ...settingProviders],
  exports: [SettingService],
})
export class SettingModule {}
