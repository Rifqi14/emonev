import { Provider } from '@nestjs/common';
import { FundSources } from 'src/database/models/fund_sources.entity';
import { DataSource } from 'typeorm';

export const fundSourceProviders: Array<Provider> = [
  {
    provide: 'FUND_SOURCE_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(FundSources),
    inject: ['DATA_SOURCE'],
  },
];
