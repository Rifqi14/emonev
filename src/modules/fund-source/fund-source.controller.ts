import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { FundSourceService } from './fund-source.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  CreateFundSourceRequest,
  CreateFundSourceResponse,
  DeleteFundSourceResponse,
  ListFundSourceRequest,
  ListFundSourceResponse,
  UpdateFundSourceRequest,
  UpdateFundSourceResponse,
} from './fund-source.dto';

@ApiTags('User | Sumber Dana')
@ApiBearerAuth()
@Controller('fund-source')
export class FundSourceController {
  constructor(private readonly fundSourceService: FundSourceService) {}

  @SwaggerOkResponse(ListFundSourceResponse)
  @Get('list')
  async list(
    @Query() query: ListFundSourceRequest,
  ): Promise<ListFundSourceResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.fundSourceService.list(query),
    };
  }

  @SwaggerOkResponse(CreateFundSourceResponse)
  @Get(':id')
  async detail(@Param('id') id: number): Promise<CreateFundSourceResponse> {
    return {
      statusCode: 200,
      message: `Pengambilan data berhasil`,
      data: await this.fundSourceService.detail(id),
    };
  }

  @SwaggerOkResponse(CreateFundSourceResponse)
  @Post('create')
  async create(
    @Body() data: CreateFundSourceRequest,
  ): Promise<CreateFundSourceResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.fundSourceService.create(data),
    };
  }

  @SwaggerOkResponse(UpdateFundSourceResponse)
  @Patch(':id')
  async update(
    @Param('id') id: number,
    @Body() data: UpdateFundSourceRequest,
  ): Promise<UpdateFundSourceResponse> {
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.fundSourceService.update(id, data),
    };
  }

  @SwaggerOkResponse(DeleteFundSourceResponse)
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<DeleteFundSourceResponse> {
    await this.fundSourceService.delete(id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }
}
