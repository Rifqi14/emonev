import { Inject, Injectable } from '@nestjs/common';
import { FundSources } from 'src/database/models/fund_sources.entity';
import { FindOptionsOrder, FindOptionsWhere, ILike, Repository } from 'typeorm';
import {
  CreateFundSourceRequest,
  ListFundSourceRequest,
  UpdateFundSourceRequest,
} from './fund-source.dto';
import { DataWithPaginationApiResponse } from 'src/common/api-response';

@Injectable()
export class FundSourceService {
  constructor(
    @Inject('FUND_SOURCE_REPOSITORY')
    private repository: Repository<FundSources>,
  ) {}

  async create(data: CreateFundSourceRequest): Promise<FundSources> {
    try {
      return await this.repository.manager.transaction(
        async (trx) => await trx.create(FundSources, { ...data }).save(),
      );
    } catch (error) {
      throw error;
    }
  }

  async update(
    id: number,
    data: UpdateFundSourceRequest,
  ): Promise<FundSources> {
    try {
      await this.repository.update(id, { ...data });
      return await this.repository.findOneByOrFail({ id });
    } catch (error) {
      throw error;
    }
  }

  async delete(id: number): Promise<void> {
    try {
      await this.repository.delete(id);
    } catch (error) {
      throw error;
    }
  }

  async detail(id: number): Promise<FundSources> {
    try {
      return await this.repository.findOneByOrFail({ id });
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListFundSourceRequest,
  ): Promise<DataWithPaginationApiResponse<Array<FundSources>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const where: Array<FindOptionsWhere<FundSources>> = [];

      if (search) {
        where.push({
          name: ILike(`%${search}%`),
        });
      }

      const [fundSources, count] = await this.repository.findAndCount({
        where: where.length > 0 ? where : undefined,
        order: this.getOrderQuery(sort),
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: fundSources,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<FundSources> {
    const sortRes: FindOptionsOrder<FundSources> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          name: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          name: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
