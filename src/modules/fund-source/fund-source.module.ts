import { Module } from '@nestjs/common';
import { FundSourceService } from './fund-source.service';
import { FundSourceController } from './fund-source.controller';
import { fundSourceProviders } from './fund-source.providers';

@Module({
  controllers: [FundSourceController],
  providers: [FundSourceService, ...fundSourceProviders],
  exports: [FundSourceService],
})
export class FundSourceModule {}
