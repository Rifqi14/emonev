import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { FundSources } from 'src/database/models/fund_sources.entity';
import { ListBaseRequest } from 'src/utils/helper';

export class CreateFundSourceRequest {
  @ApiProperty()
  name: string;

  @ApiPropertyOptional({ description: 'Total Sumber Data' })
  fund_source_total?: number;
}

export class CreateFundSourceResponse extends BaseApiResponse {
  @ApiProperty()
  data: FundSources;
}

export class UpdateFundSourceRequest extends PartialType(
  CreateFundSourceRequest,
) {}

export class UpdateFundSourceResponse extends CreateFundSourceResponse {}

export class DeleteFundSourceResponse extends BaseApiResponse {}

export class ListFundSourceRequest extends ListBaseRequest {}

export class ListFundSourceResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<FundSources[]> })
  data: DataWithPaginationApiResponse<FundSources[]>;
}
