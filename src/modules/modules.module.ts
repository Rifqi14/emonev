import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { OrganizationModule } from './organization/organization.module';
import { PurposeModule } from './purpose/purpose.module';
import { ProgramModule } from './program/program.module';
import { OccassionModule } from './occassion/occassion.module';
import { ActivityModule } from './activity/activity.module';
import { ReportModule } from './report/report.module';
import { MasterModule } from './master/master.module';
import { MailModule } from './mail/mail.module';
import { FundSourceModule } from './fund-source/fund-source.module';
import { ProcurementTypeModule } from './procurement-type/procurement-type.module';
import { ProcurementMethodModule } from './procurement-method/procurement-method.module';
import { DataTriwulanModule } from './data-triwulan/data-triwulan.module';
import { SettingModule } from './setting/setting.module';
import { DashboardModule } from './dashboard/dashboard.module';

@Module({
  imports: [UserModule, AuthenticationModule, OrganizationModule, PurposeModule, ProgramModule, OccassionModule, ActivityModule, ReportModule, MasterModule, MailModule, FundSourceModule, ProcurementTypeModule, ProcurementMethodModule, DataTriwulanModule, SettingModule, DashboardModule],
})
export class ModulesModule {}
