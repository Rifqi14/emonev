import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ProgramService } from './program.service';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  CreateProgramRequest,
  CreateProgramResponse,
  DeleteProgramRequest,
  DeleteProgramResponse,
  ListProgramResponse,
  UpdateProgramRequest,
  UpdateProgramResponse,
} from './program.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ListBaseRequest } from 'src/utils/helper';

@ApiTags('Program')
@ApiBearerAuth()
@Controller('program')
export class ProgramController {
  constructor(private readonly programService: ProgramService) {}

  @SwaggerOkResponse(CreateProgramResponse)
  @Get('detail/:id')
  async detail(@Param('id') id: number): Promise<CreateProgramResponse> {
    return {
      statusCode: 200,
      message: `Pengambilan data berhasil`,
      data: await this.programService.detail(id),
    };
  }

  @SwaggerOkResponse(ListProgramResponse)
  @Get('list')
  async list(@Query() query: ListBaseRequest): Promise<ListProgramResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.programService.list(query),
    };
  }

  @SwaggerOkResponse(CreateProgramResponse)
  @Post('create')
  async create(
    @Body() data: CreateProgramRequest,
  ): Promise<CreateProgramResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.programService.create(data),
    };
  }

  @SwaggerOkResponse(UpdateProgramResponse)
  @Patch('update')
  async update(
    @Body() data: UpdateProgramRequest,
  ): Promise<UpdateProgramResponse> {
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.programService.update(data),
    };
  }

  @SwaggerOkResponse(DeleteProgramResponse)
  @Patch('delete')
  async delete(
    @Body() data: DeleteProgramRequest,
  ): Promise<DeleteProgramResponse> {
    await this.programService.delete(data.program_id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }
}
