import { Inject, Injectable } from '@nestjs/common';
import { Program } from 'src/database/models/program.entity';
import { FindOptionsOrder, FindOptionsWhere, ILike, Repository } from 'typeorm';
import { CreateProgramRequest, UpdateProgramRequest } from './program.dto';
import { ListBaseRequest, generateCode } from 'src/utils/helper';
import { Status } from 'src/database/models/status.entity';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
import { Occassion } from 'src/database/models/occassion.entity';

@Injectable()
export class ProgramService {
  constructor(
    @Inject('PROGRAM_REPOSITORY') private repository: Repository<Program>,
  ) {}

  async create(data: CreateProgramRequest): Promise<Program> {
    try {
      const { title, occassion_id } = data;
      const latest = await this.repository.findOne({
        where: {},
        order: { id: 'DESC' },
      });
      const code = data.code ?? generateCode(latest?.id ?? 1, 'P');

      return await this.repository.manager.transaction(
        async (trx) =>
          await trx
            .create(Program, {
              code,
              title,
              occassion: await trx.findOneByOrFail(Occassion, {
                id: occassion_id,
              }),
              status: await trx.findOneByOrFail(Status, { name: 'Active' }),
            })
            .save(),
      );
    } catch (error) {
      throw error;
    }
  }

  async detail(id: number): Promise<Program> {
    try {
      return await this.repository.findOneOrFail({
        where: {
          id,
          status_id: (
            await this.repository.manager.findOneOrFail(Status, {
              where: { name: 'Active' },
            })
          ).id,
        },
        relations: {
          status: true,
          occassion: true,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async update(data: UpdateProgramRequest): Promise<Program> {
    try {
      const { program_id, code, title, occassion_id } = data;

      await this.repository.manager.transaction(async (trx) =>
        trx.update(
          Program,
          { id: program_id },
          {
            code,
            title,
            occassion: await trx.findOneByOrFail(Occassion, {
              id: occassion_id,
            }),
          },
        ),
      );

      return await this.repository.findOneByOrFail({
        id: program_id,
      });
    } catch (error) {
      throw error;
    }
  }

  async delete(program_id: number): Promise<void> {
    try {
      const program = await this.repository.findOneOrFail({
        where: { id: program_id },
      });

      program.status = await this.repository.manager.findOneOrFail(Status, {
        where: { name: 'Deleted' },
      });

      await program.save();
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListBaseRequest,
  ): Promise<DataWithPaginationApiResponse<Array<Program>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;
      const { id: status_id } = await this.repository.manager.findOneOrFail(
        Status,
        {
          where: { name: 'Active' },
        },
      );

      const where: Array<FindOptionsWhere<Program>> = [
        {
          status_id: status_id,
        },
      ];

      if (search) {
        where.pop();
        where.push({
          title: ILike(`%${search}%`),
          status_id: status_id,
        });
        where.push({
          code: ILike(`%${search}%`),
          status_id: status_id,
        });
        where.push({
          occassion: {
            title: ILike(`%${search}%`),
          },
          status_id: status_id,
        });
      }

      const [programs, count] = await this.repository.findAndCount({
        where: where,
        order: this.getOrderQuery(sort),
        relations: {
          occassion: true,
          status: true,
        },
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: programs,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<Program> {
    const sortRes: FindOptionsOrder<Program> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          title: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          title: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
