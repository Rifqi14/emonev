import { Module } from '@nestjs/common';
import { ProgramService } from './program.service';
import { ProgramController } from './program.controller';
import { programProviders } from './program.providers';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [ProgramController],
  providers: [ProgramService, ...programProviders],
  exports: [ProgramService],
})
export class ProgramModule {}
