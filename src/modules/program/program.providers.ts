import { Provider } from '@nestjs/common';
import { Program } from 'src/database/models/program.entity';
import { DataSource } from 'typeorm';

export const programProviders: Array<Provider> = [
  {
    provide: 'PROGRAM_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Program),
    inject: ['DATA_SOURCE'],
  },
];
