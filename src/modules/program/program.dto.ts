import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { Program } from 'src/database/models/program.entity';

export class CreateProgramRequest {
  @IsNotEmpty({ message: 'Judul harus diisi.' })
  @ApiProperty()
  title: string;

  @ApiPropertyOptional()
  code?: string;

  @ApiPropertyOptional()
  occassion_id?: number;
}

export class CreateProgramResponse extends BaseApiResponse {
  @ApiProperty()
  data: Program;
}

// Update
export class UpdateProgramRequest extends PartialType(CreateProgramRequest) {
  @ApiProperty()
  program_id: number;
}

export class UpdateProgramResponse extends CreateProgramResponse {}

// Delete
export class DeleteProgramRequest {
  @ApiProperty()
  program_id: number;
}

export class DeleteProgramResponse extends BaseApiResponse {}

// List
export class ListProgramResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<Program>> })
  data: DataWithPaginationApiResponse<Array<Program>>;
}
