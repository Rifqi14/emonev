import { Controller, Get, Header, Query, Res } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { Response } from 'express';
import { unlink } from 'fs';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { FundSourceResponse, GetPaguDanaChartRequest } from './dashboard.dto';

@ApiTags('Dashboard')
@ApiBearerAuth()
@Controller('dashboard')
export class DashboardController {
  constructor(private readonly dashboardService: DashboardService) {}

  @Header('Content-Type', 'text/xlsx')
  @Get('excel')
  async downloadExcelDashboard(@Res() res: Response): Promise<void> {
    const path = await this.dashboardService.downloadAdmin();

    res.download(path, (err) => {
      if (!err) {
        unlink(path, (err) => {
          if (err) throw err;
        });
      }
    });
  }

  @Get('chart/pagu-dana')
  async chartPaguDana(
    @Query() query: GetPaguDanaChartRequest,
  ): Promise<FundSourceResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.dashboardService.chartPaguDana(query),
    };
  }
}
