import { Inject, Injectable } from '@nestjs/common';
import dayjs from 'dayjs';
import { ExcelService } from 'src/common/excel.service';
import { Activity } from 'src/database/models/activity.entity';
import { Occassion } from 'src/database/models/occassion.entity';
import { Organization } from 'src/database/models/organization.entity';
import { Program } from 'src/database/models/program.entity';
import { Purpose } from 'src/database/models/purpose.entity';
import { User } from 'src/database/models/user.entity';
import { DataSource } from 'typeorm';
import {
  GetPaguDanaChartRequest,
  PaguDanaChartResponse,
  toChart,
} from './dashboard.dto';
import { FundSources } from 'src/database/models/fund_sources.entity';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';

@Injectable()
export class DashboardService {
  constructor(
    @Inject('DATA_SOURCE') private ds: DataSource,
    private excelService: ExcelService,
  ) {}

  async getOccasion() {
    try {
      return (
        await this.ds
          .getRepository(Occassion)
          .find({ relations: { status: true } })
      ).map((occasion, idx) => ({
        no: idx + 1,
        kode: occasion.code,
        nama_urusan: occasion.title,
        status: occasion.status?.name ?? '',
      }));
    } catch (error) {
      throw error;
    }
  }

  async getOrganization() {
    try {
      return (
        await this.ds
          .getRepository(Organization)
          .find({ relations: { status: true } })
      ).map((organization, idx) => ({
        no: idx + 1,
        kode: organization.code,
        nama_organisasi: organization.title,
        status: organization.status?.name ?? '',
      }));
    } catch (error) {
      throw error;
    }
  }

  async getProgram() {
    try {
      return (
        await this.ds
          .getRepository(Program)
          .find({ relations: { status: true, occassion: true } })
      ).map((program, idx) => ({
        no: idx + 1,
        kode: program.code,
        nama_program: program.title,
        urusan: program.occassion?.title ?? '',
        status: program.status?.name ?? '',
      }));
    } catch (error) {
      throw error;
    }
  }

  async getActivity() {
    try {
      return (
        await this.ds
          .getRepository(Activity)
          .find({ relations: { program: true } })
      ).map((activity, idx) => ({
        no: idx + 1,
        kode: activity.code,
        nama_aktifitas: activity.title,
        sub_aktifitas: activity.sub_activity,
        program: activity.program?.title ?? '',
      }));
    } catch (error) {
      throw error;
    }
  }

  async getPurpose() {
    try {
      return (await this.ds.getRepository(Purpose).find()).map(
        (activity, idx) => ({
          no: idx + 1,
          nama_sasaran: activity.title,
        }),
      );
    } catch (error) {
      throw error;
    }
  }

  async getUser() {
    try {
      return (
        await this.ds.getRepository(User).find({
          relations: {
            organization: true,
            role: true,
            status: true,
            userOrganization: {
              organization: true,
            },
          },
        })
      ).map((user, idx) => ({
        no: idx + 1,
        username: user.username,
        email: user.email,
        nama: user.name,
        organisasi:
          user.userOrganization.length > 0
            ? user.userOrganization
                .map((userOrganization) => userOrganization.organization.title)
                .join(', ')
            : user.organization?.title ?? '',
        level_user: user.role.name,
        status: user.status?.name ?? '',
        tanggal_dibuat: dayjs(user.created_at).format('DD/MM/YYYY'),
      }));
    } catch (error) {
      throw error;
    }
  }

  async downloadAdmin() {
    try {
      const data: Record<string, Array<Record<string, any>>> = {
        urusan: await this.getOccasion(),
        organisasi: await this.getOrganization(),
        program: await this.getProgram(),
        kegiatan: await this.getActivity(),
        sasaran: await this.getPurpose(),
        user: await this.getUser(),
      };

      const path = await this.excelService.downloadMultipleSheets({
        data,
        name: 'dashboard',
        sheet: Object.keys(data),
      });

      return path;
    } catch (error) {
      throw error;
    }
  }

  async chartPaguDana(
    req: GetPaguDanaChartRequest,
  ): Promise<PaguDanaChartResponse> {
    try {
      const pagu_dana = await this.ds
        .getRepository(FundSources)
        .findOneOrFail({ where: { id: req.pagu_dana_id } });

      const data_triwulan = await this.ds
        .getRepository(DataTriwulan)
        .find({ where: { fund_source_id: req.pagu_dana_id } });

      return toChart(pagu_dana, data_triwulan);
    } catch (error) {
      throw error;
    }
  }
}
