import { Module } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardController } from './dashboard.controller';
import { ExcelService } from 'src/common/excel.service';

@Module({
  controllers: [DashboardController],
  providers: [DashboardService, ExcelService],
})
export class DashboardModule {}
