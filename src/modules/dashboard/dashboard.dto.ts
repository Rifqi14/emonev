import { ApiProperty } from '@nestjs/swagger';
import { BaseApiResponse } from 'src/common/api-response';
import { DataTriwulan } from 'src/database/models/data-triwulan.entity';
import { FundSources } from 'src/database/models/fund_sources.entity';

export class PaguDana {
  @ApiProperty()
  pagu_dana_id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  total_pagu_dana: number;

  @ApiProperty()
  total_pagu_dana_digunakan: number;
}

export class Triwulan {
  @ApiProperty()
  data_triwulan_id: number;

  @ApiProperty()
  nama_aktifitas: string;

  @ApiProperty()
  pagu_dana: number;

  @ApiProperty()
  persentase_pagu_dana: number;

  @ApiProperty()
  realisasi_fisik: number;

  @ApiProperty()
  persentase_pencapaian: number;
}

export class GetPaguDanaChartRequest {
  @ApiProperty()
  pagu_dana_id: number;
}

export class PaguDanaChartResponse {
  @ApiProperty()
  pagu_dana: PaguDana;
  @ApiProperty()
  triwulan: Triwulan[];
}

export class FundSourceResponse extends BaseApiResponse {
  @ApiProperty({ type: PaguDanaChartResponse })
  data: PaguDanaChartResponse;
}

export const toChart = (
  fundSource: FundSources,
  triwulan: DataTriwulan[],
): PaguDanaChartResponse => {
  const total_pagu_dana_digunakan = triwulan
    .map((v) => v.fund_ceiling)
    .reduce((acc, curr) => +acc + +curr, 0);
  return {
    pagu_dana: {
      pagu_dana_id: fundSource.id ?? 0,
      name: fundSource.name ?? '',
      total_pagu_dana: Math.abs(fundSource.fund_source_total ?? 0),
      total_pagu_dana_digunakan,
    },
    triwulan: triwulan.map<Triwulan>((v) =>
      toTriwulan(v, fundSource.fund_source_total ?? 0),
    ),
  };
};

export const toTriwulan = (
  triwulan: DataTriwulan,
  pagu_dana: number,
): Triwulan => {
  const triwulan_pagu_dana = +triwulan.fund_ceiling ?? 0;
  const realisasi_fisik = +triwulan.physical_realization ?? 0;
  const persentase_pagu_dana = (+triwulan_pagu_dana / +pagu_dana) * 100;
  const persentase_pencapaian = (+realisasi_fisik / +triwulan_pagu_dana) * 100;
  return {
    data_triwulan_id: triwulan.id ?? 0,
    nama_aktifitas: triwulan.activity_name ?? '',
    pagu_dana: +triwulan.fund_ceiling ?? 0,
    persentase_pagu_dana: 100 - Math.abs(persentase_pagu_dana ?? 100),
    realisasi_fisik,
    persentase_pencapaian: 100 - Math.abs(persentase_pencapaian),
  };
};
