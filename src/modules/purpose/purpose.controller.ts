import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { PurposeService } from './purpose.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  CreatePurposeRequest,
  CreatePurposeResponse,
  DeletePurposeRequest,
  DeletePurposeResponse,
  ListPurposeResponse,
  UpdatePurposeRequest,
  UpdatePurposeResponse,
} from './purpose.dto';
import { ListBaseRequest } from 'src/utils/helper';

@ApiTags('Purpose')
@ApiBearerAuth()
@Controller('purpose')
export class PurposeController {
  constructor(private readonly purposeService: PurposeService) {}

  @SwaggerOkResponse(CreatePurposeResponse)
  @Get('detail/:id')
  async detail(@Param('id') id: number): Promise<CreatePurposeResponse> {
    return {
      statusCode: 200,
      message: `Pengambilan data berhasil`,
      data: await this.purposeService.detail(id),
    };
  }

  @SwaggerOkResponse(ListPurposeResponse)
  @Get('list')
  async list(@Query() query: ListBaseRequest): Promise<ListPurposeResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.purposeService.list(query),
    };
  }

  @SwaggerOkResponse(CreatePurposeResponse)
  @Post('create')
  async create(
    @Body() data: CreatePurposeRequest,
  ): Promise<CreatePurposeResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.purposeService.create(data),
    };
  }

  @SwaggerOkResponse(UpdatePurposeResponse)
  @Patch('update')
  async update(
    @Body() data: UpdatePurposeRequest,
  ): Promise<UpdatePurposeResponse> {
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.purposeService.update(data),
    };
  }

  @SwaggerOkResponse(DeletePurposeResponse)
  @Patch('delete')
  async delete(
    @Body() data: DeletePurposeRequest,
  ): Promise<DeletePurposeResponse> {
    await this.purposeService.delete(data.purpose_id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }
}
