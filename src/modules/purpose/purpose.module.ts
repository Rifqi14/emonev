import { Module } from '@nestjs/common';
import { PurposeService } from './purpose.service';
import { PurposeController } from './purpose.controller';
import { purposeProviders } from './purpose.providers';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [PurposeController],
  providers: [PurposeService, ...purposeProviders],
  exports: [PurposeService],
})
export class PurposeModule {}
