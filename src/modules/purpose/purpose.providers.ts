import { Provider } from '@nestjs/common';
import { Purpose } from 'src/database/models/purpose.entity';
import { DataSource } from 'typeorm';

export const purposeProviders: Array<Provider> = [
  {
    provide: 'PURPOSE_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Purpose),
    inject: ['DATA_SOURCE'],
  },
];
