import { Inject, Injectable } from '@nestjs/common';
import { Purpose } from 'src/database/models/purpose.entity';
import { FindOptionsOrder, FindOptionsWhere, ILike, Repository } from 'typeorm';
import { CreatePurposeRequest, UpdatePurposeRequest } from './purpose.dto';
import { ListBaseRequest } from 'src/utils/helper';
import { DataWithPaginationApiResponse } from 'src/common/api-response';

@Injectable()
export class PurposeService {
  constructor(
    @Inject('PURPOSE_REPOSITORY')
    private repository: Repository<Purpose>,
  ) {}

  async create(data: CreatePurposeRequest): Promise<Purpose> {
    try {
      const { title } = data;

      return await this.repository.manager.transaction(
        async (trx) =>
          await trx
            .create(Purpose, {
              title,
            })
            .save(),
      );
    } catch (error) {
      throw error;
    }
  }

  async detail(id: number): Promise<Purpose> {
    try {
      return await this.repository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async update(data: UpdatePurposeRequest): Promise<Purpose> {
    try {
      const { purpose_id, title } = data;

      await this.repository.manager.transaction(async (trx) =>
        trx.update(
          Purpose,
          { id: purpose_id },
          {
            title,
          },
        ),
      );

      return await this.repository.findOneByOrFail({
        id: purpose_id,
      });
    } catch (error) {
      throw error;
    }
  }

  async delete(purpose_id: number): Promise<void> {
    try {
      await this.repository.delete({ id: purpose_id });
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListBaseRequest,
  ): Promise<DataWithPaginationApiResponse<Array<Purpose>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const where: Array<FindOptionsWhere<Purpose>> = [];

      if (search) {
        where.push({
          title: ILike(`%${search}%`),
        });
      }

      const [purposes, count] = await this.repository.findAndCount({
        where: where,
        order: this.getOrderQuery(sort),
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: purposes,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<Purpose> {
    const sortRes: FindOptionsOrder<Purpose> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          title: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          title: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
