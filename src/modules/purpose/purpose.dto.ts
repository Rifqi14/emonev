import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { Purpose } from 'src/database/models/purpose.entity';

export class CreatePurposeRequest {
  @IsNotEmpty({ message: 'Judul harus diisi.' })
  @ApiProperty()
  title: string;
}

export class CreatePurposeResponse extends BaseApiResponse {
  @ApiProperty()
  data: Purpose;
}

// Update
export class UpdatePurposeRequest extends PartialType(CreatePurposeRequest) {
  @ApiProperty()
  purpose_id: number;
}

export class UpdatePurposeResponse extends CreatePurposeResponse {}

// Delete
export class DeletePurposeRequest {
  @ApiProperty()
  purpose_id: number;
}

export class DeletePurposeResponse extends BaseApiResponse {}

// List
export class ListPurposeResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<Purpose>> })
  data: DataWithPaginationApiResponse<Array<Purpose>>;
}
