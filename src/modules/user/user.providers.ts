import { Provider } from '@nestjs/common';
import { User } from 'src/database/models/user.entity';
import { DataSource } from 'typeorm';

export const userProviders: Array<Provider> = [
  {
    provide: 'USER_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(User),
    inject: ['DATA_SOURCE'],
  },
];
