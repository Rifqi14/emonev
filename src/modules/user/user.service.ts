import { Inject, Injectable, Scope } from '@nestjs/common';
import { User } from 'src/database/models/user.entity';
import {
  FindOptionsOrder,
  FindOptionsWhere,
  ILike,
  In,
  Repository,
} from 'typeorm';
import {
  CreateUserRequest,
  ListUserRequest,
  UpdateUserRequest,
} from './user.dto';
import { Role } from 'src/database/models/role.entity';
import { Status } from 'src/database/models/status.entity';
import { DataWithPaginationApiResponse } from 'src/common/api-response';
import { Organization } from 'src/database/models/organization.entity';
import { UserOrganization } from 'src/database/models/user-organization.entity';
import { REQUEST } from '@nestjs/core';
import { PayloadClientResponse } from '../authentication/authentication.dto';
import { Request } from 'express';

@Injectable({ scope: Scope.REQUEST })
export class UserService {
  constructor(
    @Inject('USER_REPOSITORY') private repository: Repository<User>,
    @Inject(REQUEST)
    private request: Request & {
      user: PayloadClientResponse;
      is_admin_bidang: boolean;
    },
  ) {}

  async findByUsername(username: string): Promise<User> {
    try {
      const user = await this.repository
        .createQueryBuilder('user')
        .addSelect('user.password')
        .leftJoinAndSelect('user.status', 'status')
        .leftJoinAndSelect('user.role', 'role')
        .leftJoinAndSelect('user.organization', 'organization')
        .leftJoinAndSelect('user.userOrganization', 'userOrganization')
        .where(`username = :username`, { username })
        .getOneOrFail();

      return user;
    } catch (error) {
      throw error;
    }
  }

  async findByEmail(email: string): Promise<User> {
    try {
      const user = await this.repository.findOneOrFail({ where: { email } });

      return user;
    } catch (error) {
      throw error;
    }
  }

  async getProfile(username: string): Promise<User> {
    try {
      return await this.repository.findOneOrFail({
        where: { username },
        relations: {
          status: true,
          organization: true,
          role: true,
          userOrganization: true,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async create(data: CreateUserRequest): Promise<User> {
    try {
      const {
        name,
        password,
        username,
        admin_role_id,
        status_id,
        email,
        organization_id,
      } = data;
      return await this.repository.manager.transaction(async (trx) => {
        let organization: Organization | undefined = undefined;
        let organizations: Organization[] | undefined = undefined;

        if (organization_id) {
          if (organization_id.length > 1) {
            organizations = await this.repository.manager.find(Organization, {
              where: { id: In(organization_id) },
            });
          }
          organization = await this.repository.manager.findOneOrFail(
            Organization,
            {
              where: { id: organization_id[0] },
            },
          );
        }
        const user = trx.create(User, {
          name,
          email,
          password,
          organization,
          username,
          role_id:
            admin_role_id ??
            (await trx.findOneByOrFail(Role, { name: 'OPD' })).id,
          status_id:
            status_id ??
            (await trx.findOneByOrFail(Status, { name: 'Active' })).id,
        });

        if (organizations) {
          user.userOrganization = organizations.map<UserOrganization>(
            (organization) => {
              return trx.create(UserOrganization, {
                organization,
              });
            },
          );
        }
        return user.save();
      });
    } catch (error) {
      throw error;
    }
  }

  async updatePassword(password: string, user_id: number): Promise<void> {
    try {
      await this.repository.manager.transaction(async (trx) => {
        const user = trx.create(User, {
          password,
        });
        await trx.update(User, { id: user_id }, user);
      });
    } catch (error) {
      throw error;
    }
  }

  async getDetail(id: number): Promise<User> {
    try {
      return await this.repository.findOneOrFail({
        where: {
          id,
          status_id: (
            await this.repository.manager.findOneOrFail(Status, {
              where: { name: 'Active' },
            })
          ).id,
        },
        relations: {
          status: true,
          organization: true,
          role: true,
          userOrganization: true,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async update(data: UpdateUserRequest): Promise<User> {
    try {
      const {
        admin_role_id,
        name,
        password,
        status_id,
        username,
        user_id,
        email,
      } = data;

      await this.repository.manager.transaction(async (trx) => {
        const user = trx.create(User, {
          username,
          name,
          email,
          password,
          status_id,
          role_id: admin_role_id,
        });
        await trx.update(User, { id: user_id }, user);
      });

      return this.repository.findOneOrFail({
        where: {
          id: user_id,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async delete(user_id: number): Promise<void> {
    try {
      const user = await this.repository.findOneOrFail({
        where: { id: user_id },
      });

      user.status = await this.repository.manager.findOneOrFail(Status, {
        where: { name: 'Deleted' },
      });

      await user.save();
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListUserRequest,
  ): Promise<DataWithPaginationApiResponse<Array<User>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;
      const { id: status_id } = await this.repository.manager.findOneOrFail(
        Status,
        {
          where: { name: 'Active' },
        },
      );

      let where: Array<FindOptionsWhere<User>> = [
        {
          status_id: status_id,
        },
      ];

      if (search) {
        where.pop();
        where.push({
          username: ILike(`%${search}%`),
          status_id: status_id,
        });
        where.push({
          name: ILike(`%${search}%`),
          status_id: status_id,
        });
        where.push({
          email: ILike(`%${search}%`),
          status_id: status_id,
        });
      }

      const user = await this.repository.findOne({
        where: {
          username: this.request.user.username,
        },
        relations: { role: true },
      });
      if (user && user.role && user.role.name.toLowerCase() === 'opd') {
        where = [
          {
            username: ILike(`%${search ?? ''}%`),
            status_id: status_id,
            created_by: user.username,
          },
          {
            name: ILike(`%${search ?? ''}%`),
            status_id: status_id,
            created_by: user.username,
          },
          {
            email: ILike(`%${search ?? ''}%`),
            status_id: status_id,
            created_by: user.username,
          },
        ];
      }

      const [users, count] = await this.repository.findAndCount({
        where: where,
        order: this.getOrderQuery(sort),
        relations: {
          status: true,
          organization: true,
          role: true,
          userOrganization: true,
        },
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: users,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<User> {
    const sortRes: FindOptionsOrder<User> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          username: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          username: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
