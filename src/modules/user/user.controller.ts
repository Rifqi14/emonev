import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  CreateUserRequest,
  CreateUserResponse,
  DeleteUserRequest,
  DeleteUserResponse,
  ListUserRequest,
  ListUserResponse,
  UpdateUserRequest,
  UpdateUserResponse,
} from './user.dto';

@ApiTags('User')
@ApiBearerAuth()
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @SwaggerOkResponse(UpdateUserResponse)
  @Patch('update')
  async update(@Body() data: UpdateUserRequest): Promise<UpdateUserResponse> {
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.userService.update(data),
    };
  }

  @SwaggerOkResponse(CreateUserResponse)
  @Post('create')
  async create(@Body() data: CreateUserRequest): Promise<CreateUserResponse> {
    return {
      statusCode: 200,
      message: 'Penambahan data berhasil',
      data: await this.userService.create(data),
    };
  }

  @SwaggerOkResponse(DeleteUserResponse)
  @Patch('delete')
  async delete(@Body() data: DeleteUserRequest): Promise<DeleteUserResponse> {
    await this.userService.delete(data.user_id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }

  @SwaggerOkResponse(CreateUserResponse)
  @Get('detail/:id')
  async detail(@Param('id') id: number): Promise<CreateUserResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.userService.getDetail(id),
    };
  }

  @SwaggerOkResponse(ListUserResponse)
  @Get('list')
  async list(@Query() query: ListUserRequest): Promise<ListUserResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.userService.list(query),
    };
  }
}
