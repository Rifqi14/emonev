import {
  ApiProperty,
  ApiPropertyOptional,
  OmitType,
  PartialType,
} from '@nestjs/swagger';
import { IsEmail } from 'class-validator';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { User } from 'src/database/models/user.entity';

export class CreateUserRequest {
  @ApiProperty()
  username: string;

  @ApiProperty()
  password: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  email: string;

  @ApiPropertyOptional()
  admin_role_id?: number;

  @ApiPropertyOptional()
  status_id?: number;

  @ApiPropertyOptional({
    type: [Number],
    description: 'Pada saat registrasi tidak usah dikirim',
  })
  organization_id?: number[];
}

export class RegisterUserRequest extends OmitType(CreateUserRequest, [
  'admin_role_id',
  'status_id',
  'organization_id',
]) {}

export class CreateUserResponse extends BaseApiResponse {
  @ApiProperty()
  data: User;
}

// Update
export class UpdateUserRequest extends PartialType(CreateUserRequest) {
  @ApiProperty()
  user_id: number;
}

export class UpdateUserResponse extends CreateUserResponse {}

// Delete
export class DeleteUserRequest {
  @ApiProperty()
  user_id: number;
}

export class DeleteUserResponse extends BaseApiResponse {}

// List
export class ListUserRequest {
  @ApiPropertyOptional()
  limit?: number;

  @ApiPropertyOptional()
  page?: number;

  @ApiPropertyOptional()
  search?: string;

  @ApiPropertyOptional({
    enum: ['terbaru', 'terlama', 'a-z', 'z-a'],
  })
  sort?: 'terbaru' | 'terlama' | 'a-z' | 'z-a';
}

export class ListUserResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<User>> })
  data: DataWithPaginationApiResponse<Array<User>>;
}

// Forgot password
export class ForgotPasswordRequest {
  @IsEmail()
  @ApiProperty()
  email: string;
}

export class ForgotPasswordResponse extends BaseApiResponse {}
