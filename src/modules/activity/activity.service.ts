import { Inject, Injectable } from '@nestjs/common';
import { Activity } from 'src/database/models/activity.entity';
import { FindOptionsOrder, FindOptionsWhere, ILike, Repository } from 'typeorm';
import { CreateActivityRequest, UpdateActivityRequest } from './activity.dto';
import { ListBaseRequest, generateCode } from 'src/utils/helper';
import { DataWithPaginationApiResponse } from 'src/common/api-response';

@Injectable()
export class ActivityService {
  constructor(
    @Inject('ACTIVITY_REPOSITORY')
    private repository: Repository<Activity>,
  ) {}

  async create(data: CreateActivityRequest): Promise<Activity> {
    try {
      const { title, program_id, sub_activity } = data;
      const latest = await this.repository.findOne({
        where: {},
        order: { id: 'DESC' },
      });
      const code = data.code ?? generateCode(latest?.id ?? 1, 'A');

      return await this.repository.manager.transaction(
        async (trx) =>
          await trx
            .create(Activity, {
              title,
              code,
              program_id,
              sub_activity,
            })
            .save(),
      );
    } catch (error) {
      throw error;
    }
  }

  async detail(id: number): Promise<Activity> {
    try {
      return await this.repository.findOneOrFail({
        relations: {
          program: true,
        },
        where: {
          id,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async update(data: UpdateActivityRequest): Promise<Activity> {
    try {
      const { activity_id, code, program_id, title, sub_activity } = data;

      await this.repository.manager.transaction(async (trx) =>
        trx.update(
          Activity,
          { id: activity_id },
          {
            program_id,
            code,
            title,
            sub_activity,
          },
        ),
      );

      return await this.repository.findOneByOrFail({
        id: activity_id,
      });
    } catch (error) {
      throw error;
    }
  }

  async delete(activity_id: number): Promise<void> {
    try {
      await this.repository.delete({ id: activity_id });
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListBaseRequest,
  ): Promise<DataWithPaginationApiResponse<Array<Activity>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const where: Array<FindOptionsWhere<Activity>> = [];

      if (search) {
        where.push({
          code: ILike(`%${search}%`),
        });
        where.push({
          title: ILike(`%${search}%`),
        });
        where.push({
          sub_activity: ILike(`%${search}%`),
        });
        where.push({
          program: {
            title: ILike(`%${search}%`),
          },
        });
      }

      const [activities, count] = await this.repository.findAndCount({
        where: where,
        relations: {
          program: true,
        },
        order: this.getOrderQuery(sort),
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: activities,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<Activity> {
    const sortRes: FindOptionsOrder<Activity> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          title: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          title: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
