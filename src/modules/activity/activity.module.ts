import { Module } from '@nestjs/common';
import { ActivityService } from './activity.service';
import { ActivityController } from './activity.controller';
import { DatabaseModule } from 'src/database/database.module';
import { activityProviders } from './activity.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [ActivityController],
  providers: [ActivityService, ...activityProviders],
  exports: [ActivityService],
})
export class ActivityModule {}
