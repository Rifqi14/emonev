import { Provider } from '@nestjs/common';
import { Activity } from 'src/database/models/activity.entity';
import { DataSource } from 'typeorm';

export const activityProviders: Array<Provider> = [
  {
    provide: 'ACTIVITY_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Activity),
    inject: ['DATA_SOURCE'],
  },
];
