import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { Activity } from 'src/database/models/activity.entity';

export class CreateActivityRequest {
  @IsNotEmpty({ message: 'Judul harus diisi.' })
  @ApiProperty()
  title: string;

  @ApiPropertyOptional()
  code?: string;

  @ApiPropertyOptional()
  program_id?: number;

  @ApiPropertyOptional()
  sub_activity?: string;
}

export class CreateActivityResponse extends BaseApiResponse {
  @ApiProperty()
  data: Activity;
}

// Update
export class UpdateActivityRequest extends PartialType(CreateActivityRequest) {
  @ApiProperty()
  activity_id: number;
}

export class UpdateActivityResponse extends CreateActivityResponse {}

// Delete
export class DeleteActivityRequest {
  @ApiProperty()
  activity_id: number;
}

export class DeleteActivityResponse extends BaseApiResponse {}

// List
export class ListActivityResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<Activity>> })
  data: DataWithPaginationApiResponse<Array<Activity>>;
}
