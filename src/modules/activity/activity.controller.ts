import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ActivityService } from './activity.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  CreateActivityRequest,
  CreateActivityResponse,
  DeleteActivityRequest,
  DeleteActivityResponse,
  ListActivityResponse,
  UpdateActivityRequest,
  UpdateActivityResponse,
} from './activity.dto';
import { ListBaseRequest } from 'src/utils/helper';

@ApiTags('Activity')
@ApiBearerAuth()
@Controller('activity')
export class ActivityController {
  constructor(private readonly activityService: ActivityService) {}

  @SwaggerOkResponse(CreateActivityResponse)
  @Get('detail/:id')
  async detail(@Param('id') id: number): Promise<CreateActivityResponse> {
    return {
      statusCode: 200,
      message: `Pengambilan data berhasil`,
      data: await this.activityService.detail(id),
    };
  }

  @SwaggerOkResponse(ListActivityResponse)
  @Get('list')
  async list(@Query() query: ListBaseRequest): Promise<ListActivityResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.activityService.list(query),
    };
  }

  @SwaggerOkResponse(CreateActivityResponse)
  @Post('create')
  async create(
    @Body() data: CreateActivityRequest,
  ): Promise<CreateActivityResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.activityService.create(data),
    };
  }

  @SwaggerOkResponse(UpdateActivityResponse)
  @Patch('update')
  async update(
    @Body() data: UpdateActivityRequest,
  ): Promise<UpdateActivityResponse> {
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.activityService.update(data),
    };
  }

  @SwaggerOkResponse(DeleteActivityResponse)
  @Patch('delete')
  async delete(
    @Body() data: DeleteActivityRequest,
  ): Promise<DeleteActivityResponse> {
    await this.activityService.delete(data.activity_id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }
}
