import { ApiProperty } from '@nestjs/swagger';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { ProcurementMethod } from 'src/database/models/procurement-method.entity';
import { ListBaseRequest } from 'src/utils/helper';

export class CreateProcurementMethodRequest {
  @ApiProperty()
  name: string;
}

export class CreateProcurementMethodResponse extends BaseApiResponse {
  @ApiProperty()
  data: ProcurementMethod;
}

export class ListProcurementMethodRequest extends ListBaseRequest {}

export class ListProcurementMethodResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<ProcurementMethod[]> })
  data: DataWithPaginationApiResponse<ProcurementMethod[]>;
}
