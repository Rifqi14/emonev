import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ProcurementMethodService } from './procurement-method.service';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  CreateProcurementMethodRequest,
  CreateProcurementMethodResponse,
  ListProcurementMethodRequest,
  ListProcurementMethodResponse,
} from './procurement-method.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('User | Cara Pengadaan')
@ApiBearerAuth()
@Controller('procurement-method')
export class ProcurementMethodController {
  constructor(
    private readonly procurementMethodService: ProcurementMethodService,
  ) {}

  @SwaggerOkResponse(ListProcurementMethodResponse)
  @Get('list')
  async list(
    @Query() query: ListProcurementMethodRequest,
  ): Promise<ListProcurementMethodResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.procurementMethodService.list(query),
    };
  }

  @SwaggerOkResponse(CreateProcurementMethodResponse)
  @Post('create')
  async create(
    @Body() data: CreateProcurementMethodRequest,
  ): Promise<CreateProcurementMethodResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.procurementMethodService.create(data),
    };
  }
}
