import { Inject, Injectable } from '@nestjs/common';
import { ProcurementMethod } from 'src/database/models/procurement-method.entity';
import { FindOptionsOrder, FindOptionsWhere, ILike, Repository } from 'typeorm';
import {
  CreateProcurementMethodRequest,
  ListProcurementMethodRequest,
} from './procurement-method.dto';
import { DataWithPaginationApiResponse } from 'src/common/api-response';

@Injectable()
export class ProcurementMethodService {
  constructor(
    @Inject('PROCUREMENT_METHOD_REPOSITORY')
    private repository: Repository<ProcurementMethod>,
  ) {}

  async create(
    data: CreateProcurementMethodRequest,
  ): Promise<ProcurementMethod> {
    try {
      const { name } = data;

      return await this.repository.manager.transaction(
        async (trx) => await trx.create(ProcurementMethod, { name }).save(),
      );
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListProcurementMethodRequest,
  ): Promise<DataWithPaginationApiResponse<Array<ProcurementMethod>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const where: Array<FindOptionsWhere<ProcurementMethod>> = [];

      if (search) {
        where.push({
          name: ILike(`%${search}%`),
        });
      }

      const [fundSources, count] = await this.repository.findAndCount({
        where: where.length > 0 ? where : undefined,
        order: this.getOrderQuery(sort),
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: fundSources,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<ProcurementMethod> {
    const sortRes: FindOptionsOrder<ProcurementMethod> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          name: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          name: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
