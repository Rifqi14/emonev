import { Module } from '@nestjs/common';
import { ProcurementMethodService } from './procurement-method.service';
import { ProcurementMethodController } from './procurement-method.controller';
import { procurementMethodProviders } from './procurement-method.providers';

@Module({
  controllers: [ProcurementMethodController],
  providers: [ProcurementMethodService, ...procurementMethodProviders],
  exports: [ProcurementMethodService],
})
export class ProcurementMethodModule {}
