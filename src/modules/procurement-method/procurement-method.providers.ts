import { Provider } from '@nestjs/common';
import { ProcurementMethod } from 'src/database/models/procurement-method.entity';
import { DataSource } from 'typeorm';

export const procurementMethodProviders: Array<Provider> = [
  {
    provide: 'PROCUREMENT_METHOD_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(ProcurementMethod),
    inject: ['DATA_SOURCE'],
  },
];
