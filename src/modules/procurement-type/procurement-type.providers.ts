import { Provider } from '@nestjs/common';
import { ProcurementType } from 'src/database/models/procurement-type.entity';
import { DataSource } from 'typeorm';

export const procurementTypeProviders: Array<Provider> = [
  {
    provide: 'PROCUREMENT_TYPE_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(ProcurementType),
    inject: ['DATA_SOURCE'],
  },
];
