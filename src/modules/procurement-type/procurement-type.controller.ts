import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ProcurementTypeService } from './procurement-type.service';
import { SwaggerOkResponse } from 'src/common/openapi';
import {
  CreateProcurementTypeRequest,
  CreateProcurementTypeResponse,
  ListProcurementTypeRequest,
  ListProcurementTypeResponse,
} from './procurement-type.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('User | Jenis Pengadaan')
@ApiBearerAuth()
@Controller('procurement-type')
export class ProcurementTypeController {
  constructor(
    private readonly procurementTypeService: ProcurementTypeService,
  ) {}

  @SwaggerOkResponse(ListProcurementTypeResponse)
  @Get('list')
  async list(
    @Query() query: ListProcurementTypeRequest,
  ): Promise<ListProcurementTypeResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.procurementTypeService.list(query),
    };
  }

  @SwaggerOkResponse(CreateProcurementTypeResponse)
  @Post('create')
  async create(
    @Body() data: CreateProcurementTypeRequest,
  ): Promise<CreateProcurementTypeResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.procurementTypeService.create(data),
    };
  }
}
