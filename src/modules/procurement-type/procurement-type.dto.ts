import { ApiProperty } from '@nestjs/swagger';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { ProcurementType } from 'src/database/models/procurement-type.entity';
import { ListBaseRequest } from 'src/utils/helper';

export class CreateProcurementTypeRequest {
  @ApiProperty()
  name: string;
}

export class CreateProcurementTypeResponse extends BaseApiResponse {
  @ApiProperty()
  data: ProcurementType;
}

export class ListProcurementTypeRequest extends ListBaseRequest {}

export class ListProcurementTypeResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<ProcurementType[]> })
  data: DataWithPaginationApiResponse<ProcurementType[]>;
}
