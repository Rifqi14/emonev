import { Inject, Injectable } from '@nestjs/common';
import { ProcurementType } from 'src/database/models/procurement-type.entity';
import { FindOptionsOrder, FindOptionsWhere, ILike, Repository } from 'typeorm';
import {
  CreateProcurementTypeRequest,
  ListProcurementTypeRequest,
} from './procurement-type.dto';
import { DataWithPaginationApiResponse } from 'src/common/api-response';

@Injectable()
export class ProcurementTypeService {
  constructor(
    @Inject('PROCUREMENT_TYPE_REPOSITORY')
    private repository: Repository<ProcurementType>,
  ) {}

  async create(data: CreateProcurementTypeRequest): Promise<ProcurementType> {
    try {
      const { name } = data;

      return await this.repository.manager.transaction(
        async (trx) => await trx.create(ProcurementType, { name }).save(),
      );
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListProcurementTypeRequest,
  ): Promise<DataWithPaginationApiResponse<Array<ProcurementType>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;

      const where: Array<FindOptionsWhere<ProcurementType>> = [];

      if (search) {
        where.push({
          name: ILike(`%${search}%`),
        });
      }

      const [fundSources, count] = await this.repository.findAndCount({
        where: where.length > 0 ? where : undefined,
        order: this.getOrderQuery(sort),
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: fundSources,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<ProcurementType> {
    const sortRes: FindOptionsOrder<ProcurementType> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          name: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          name: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
