import { Module } from '@nestjs/common';
import { ProcurementTypeService } from './procurement-type.service';
import { ProcurementTypeController } from './procurement-type.controller';
import { procurementTypeProviders } from './procurement-type.providers';

@Module({
  controllers: [ProcurementTypeController],
  providers: [ProcurementTypeService, ...procurementTypeProviders],
  exports: [ProcurementTypeService],
})
export class ProcurementTypeModule {}
