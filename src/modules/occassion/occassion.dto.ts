import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import {
  BaseApiResponse,
  DataWithPaginationApiResponse,
} from 'src/common/api-response';
import { Occassion } from 'src/database/models/occassion.entity';
import { IsNotEmpty } from 'class-validator';

export class CreateOccassionRequest {
  @IsNotEmpty({ message: 'Judul harus diisi.' })
  @ApiProperty()
  title: string;

  @ApiPropertyOptional()
  code?: string;
}

export class CreateOccassionResponse extends BaseApiResponse {
  @ApiProperty()
  data: Occassion;
}

// Update
export class UpdateOccassionRequest extends PartialType(
  CreateOccassionRequest,
) {
  @ApiProperty()
  occassion_id: number;
}

export class UpdateOccassionResponse extends CreateOccassionResponse {}

// Delete
export class DeleteOccassionRequest {
  @ApiProperty()
  occassion_id: number;
}

export class DeleteOccassionResponse extends BaseApiResponse {}

// List
export class ListOccassionResponse extends BaseApiResponse {
  @ApiProperty({ type: DataWithPaginationApiResponse<Array<Occassion>> })
  data: DataWithPaginationApiResponse<Array<Occassion>>;
}
