import { Provider } from '@nestjs/common';
import { Occassion } from 'src/database/models/occassion.entity';
import { DataSource } from 'typeorm';

export const occassionProviders: Array<Provider> = [
  {
    provide: 'OCCASSION_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Occassion),
    inject: ['DATA_SOURCE'],
  },
];
