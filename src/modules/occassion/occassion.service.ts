import { Inject, Injectable } from '@nestjs/common';
import { Occassion } from 'src/database/models/occassion.entity';
import { FindOptionsOrder, FindOptionsWhere, ILike, Repository } from 'typeorm';
import {
  CreateOccassionRequest,
  UpdateOccassionRequest,
} from './occassion.dto';
import { ListBaseRequest, generateCode } from 'src/utils/helper';
import { Status } from 'src/database/models/status.entity';
import { DataWithPaginationApiResponse } from 'src/common/api-response';

@Injectable()
export class OccassionService {
  constructor(
    @Inject('OCCASSION_REPOSITORY') private repository: Repository<Occassion>,
  ) {}

  async create(data: CreateOccassionRequest): Promise<Occassion> {
    try {
      const { title } = data;
      const latest = await this.repository.findOne({
        where: {},
        order: { id: 'DESC' },
      });
      const code = data.code ?? generateCode(latest?.id ?? 1, 'O');

      return await this.repository.manager.transaction(
        async (trx) =>
          await trx
            .create(Occassion, {
              code,
              title,
              status: await trx.findOneByOrFail(Status, { name: 'Active' }),
            })
            .save(),
      );
    } catch (error) {
      throw error;
    }
  }

  async detail(id: number): Promise<Occassion> {
    try {
      return await this.repository.findOneOrFail({
        where: {
          id,
          status_id: (
            await this.repository.manager.findOneOrFail(Status, {
              where: { name: 'Active' },
            })
          ).id,
        },
        relations: {
          status: true,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async update(data: UpdateOccassionRequest): Promise<Occassion> {
    try {
      const { occassion_id, code, title } = data;

      await this.repository.manager.transaction(async (trx) =>
        trx.update(
          Occassion,
          { id: occassion_id },
          {
            code,
            title,
          },
        ),
      );

      return await this.repository.findOneByOrFail({
        id: occassion_id,
      });
    } catch (error) {
      throw error;
    }
  }

  async delete(occassion_id: number): Promise<void> {
    try {
      const occassion = await this.repository.findOneOrFail({
        where: { id: occassion_id },
      });

      occassion.status = await this.repository.manager.findOneOrFail(Status, {
        where: { name: 'Deleted' },
      });

      await occassion.save();
    } catch (error) {
      throw error;
    }
  }

  async list(
    query: ListBaseRequest,
  ): Promise<DataWithPaginationApiResponse<Array<Occassion>>> {
    try {
      const { search, sort } = query;
      const page = query.page && query.page > 0 ? Number(query.page) : 1;
      const limit = query.limit ? Number(query.limit) : 10;
      const { id: status_id } = await this.repository.manager.findOneOrFail(
        Status,
        {
          where: { name: 'Active' },
        },
      );

      const where: Array<FindOptionsWhere<Occassion>> = [
        {
          status_id: status_id,
        },
      ];

      if (search) {
        where.pop();
        where.push({
          title: ILike(`%${search}%`),
          status_id: status_id,
        });
        where.push({
          code: ILike(`%${search}%`),
          status_id: status_id,
        });
      }

      const [occassions, count] = await this.repository.findAndCount({
        where: where,
        order: this.getOrderQuery(sort),
        relations: {
          status: true,
        },
        skip: (page - 1) * limit,
        take: limit,
      });

      return {
        total: count,
        page: page,
        pages: count > 0 ? Math.ceil(count / limit) : 1,
        result: occassions,
      };
    } catch (error) {
      throw error;
    }
  }

  getOrderQuery(
    sort: 'terbaru' | 'terlama' | 'a-z' | 'z-a' | undefined,
  ): FindOptionsOrder<Occassion> {
    const sortRes: FindOptionsOrder<Occassion> = {
      id: 'DESC',
    };

    switch (sort) {
      case 'terbaru':
        Object.assign(sortRes, {
          id: 'DESC',
        });
        break;
      case 'terlama':
        Object.assign(sortRes, {
          id: 'ASC',
        });
        break;
      case 'a-z':
        Object.assign(sortRes, {
          title: 'ASC',
        });
        break;
      case 'z-a':
        Object.assign(sortRes, {
          title: 'DESC',
        });
        break;
    }

    return sortRes;
  }
}
