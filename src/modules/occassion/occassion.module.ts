import { Module } from '@nestjs/common';
import { OccassionService } from './occassion.service';
import { OccassionController } from './occassion.controller';
import { occassionProviders } from './occassion.providers';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [OccassionController],
  providers: [OccassionService, ...occassionProviders],
  exports: [OccassionService],
})
export class OccassionModule {}
