import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SwaggerOkResponse } from 'src/common/openapi';
import { OccassionService } from './occassion.service';
import {
  CreateOccassionRequest,
  CreateOccassionResponse,
  DeleteOccassionRequest,
  DeleteOccassionResponse,
  ListOccassionResponse,
  UpdateOccassionRequest,
  UpdateOccassionResponse,
} from './occassion.dto';
import { ListBaseRequest } from 'src/utils/helper';

@ApiTags('Occassion')
@ApiBearerAuth()
@Controller('occassion')
export class OccassionController {
  constructor(private readonly occassionService: OccassionService) {}

  @SwaggerOkResponse(CreateOccassionResponse)
  @Get('detail/:id')
  async detail(@Param('id') id: number): Promise<CreateOccassionResponse> {
    return {
      statusCode: 200,
      message: `Pengambilan data berhasil`,
      data: await this.occassionService.detail(id),
    };
  }

  @SwaggerOkResponse(ListOccassionResponse)
  @Get('list')
  async list(@Query() query: ListBaseRequest): Promise<ListOccassionResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.occassionService.list(query),
    };
  }

  @SwaggerOkResponse(CreateOccassionResponse)
  @Post('create')
  async create(
    @Body() data: CreateOccassionRequest,
  ): Promise<CreateOccassionResponse> {
    return {
      statusCode: 200,
      message: `Pembuatan data berhasil`,
      data: await this.occassionService.create(data),
    };
  }

  @SwaggerOkResponse(UpdateOccassionResponse)
  @Patch('update')
  async update(
    @Body() data: UpdateOccassionRequest,
  ): Promise<UpdateOccassionResponse> {
    return {
      statusCode: 200,
      message: 'Pengkinian data berhasil',
      data: await this.occassionService.update(data),
    };
  }

  @SwaggerOkResponse(DeleteOccassionResponse)
  @Patch('delete')
  async delete(
    @Body() data: DeleteOccassionRequest,
  ): Promise<DeleteOccassionResponse> {
    await this.occassionService.delete(data.occassion_id);
    return {
      statusCode: 200,
      message: 'Penghapusan data berhasil',
    };
  }
}
