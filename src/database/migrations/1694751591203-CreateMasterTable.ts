import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateMasterTable1694751591203 implements MigrationInterface {
  name = 'CreateMasterTable1694751591203';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "master" ("id" BIGSERIAL NOT NULL, "description" text, "triwulan_id" bigint, "organization_id" integer, "purpose_id" bigint, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_master" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "master" ADD CONSTRAINT "fk_triwulan_id_master" FOREIGN KEY ("triwulan_id") REFERENCES "triwulan"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "master" ADD CONSTRAINT "fk_purpose_id_master" FOREIGN KEY ("purpose_id") REFERENCES "purpose"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "master" ADD CONSTRAINT "fk_organization_id_master" FOREIGN KEY ("organization_id") REFERENCES "organization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "master" DROP CONSTRAINT "fk_organization_id_master"`,
    );
    await queryRunner.query(
      `ALTER TABLE "master" DROP CONSTRAINT "fk_purpose_id_master"`,
    );
    await queryRunner.query(
      `ALTER TABLE "master" DROP CONSTRAINT "fk_triwulan_id_master"`,
    );
    await queryRunner.query(`DROP TABLE "master"`);
  }
}
