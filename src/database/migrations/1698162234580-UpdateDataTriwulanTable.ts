import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateDataTriwulanTable1698162234580
  implements MigrationInterface
{
  name = 'UpdateDataTriwulanTable1698162234580';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD "activity_id" bigint`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD "activity_form" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD CONSTRAINT "fk_activity_id_data_triwulan" FOREIGN KEY ("activity_id") REFERENCES "activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP CONSTRAINT "fk_activity_id_data_triwulan"`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP COLUMN "activity_form"`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP COLUMN "activity_id"`,
    );
  }
}
