import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddFileColumnToDataTriwulanTable1695393107298
  implements MigrationInterface
{
  name = 'AddFileColumnToDataTriwulanTable1695393107298';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "file" text`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "file"`);
  }
}
