import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateMasterOccassionTable1694751050540
  implements MigrationInterface
{
  name = 'CreateMasterOccassionTable1694751050540';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "master_occassion" ("id" BIGSERIAL NOT NULL, "master_id" bigint, "occassion_id" bigint, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_master_occassion" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "master_occassion" ADD CONSTRAINT "fk_occassion_id_master_occassion" FOREIGN KEY ("occassion_id") REFERENCES "occassion"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "master_occassion" DROP CONSTRAINT "fk_occassion_id_master_occassion"`,
    );
    await queryRunner.query(`DROP TABLE "master_occassion"`);
  }
}
