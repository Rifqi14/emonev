import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateProcurementMethodTable1695312979618 implements MigrationInterface {
    name = 'CreateProcurementMethodTable1695312979618'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "procurement_method" ("id" BIGSERIAL NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_procurement_method" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "procurement_method"`);
    }

}
