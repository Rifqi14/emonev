import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRelationInProgramToOccassionTable1695218576411
  implements MigrationInterface
{
  name = 'AddRelationInProgramToOccassionTable1695218576411';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "program" ADD "occassion_id" bigint`);
    await queryRunner.query(
      `ALTER TABLE "program" ADD CONSTRAINT "fk_occassion_id_program" FOREIGN KEY ("occassion_id") REFERENCES "occassion"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "program" DROP CONSTRAINT "fk_occassion_id_program"`,
    );
    await queryRunner.query(`ALTER TABLE "program" DROP COLUMN "occassion_id"`);
  }
}
