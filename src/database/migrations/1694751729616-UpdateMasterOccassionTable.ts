import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateMasterOccassionTable1694751729616
  implements MigrationInterface
{
  name = 'UpdateMasterOccassionTable1694751729616';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "master_occassion" ADD CONSTRAINT "fk_master_id_master_occassion" FOREIGN KEY ("master_id") REFERENCES "master"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "master_occassion" DROP CONSTRAINT "fk_master_id_master_occassion"`,
    );
  }
}
