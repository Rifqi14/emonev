import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddCreatedByInUserTable1699997699212
  implements MigrationInterface
{
  name = 'AddCreatedByInUserTable1699997699212';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" ADD "created_by" bigint`);
    await queryRunner.query(
      `ALTER TABLE "user" ADD CONSTRAINT "fk_created_by_user" FOREIGN KEY ("created_by") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" DROP CONSTRAINT "fk_created_by_user"`,
    );
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "created_by"`);
  }
}
