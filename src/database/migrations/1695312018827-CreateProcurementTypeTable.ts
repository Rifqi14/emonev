import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateProcurementTypeTable1695312018827
  implements MigrationInterface
{
  name = 'CreateProcurementTypeTable1695312018827';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "procurement_type" ("id" BIGSERIAL NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_procurement_type" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "procurement_type"`);
  }
}
