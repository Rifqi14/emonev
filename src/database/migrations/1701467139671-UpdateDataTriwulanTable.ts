import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateDataTriwulanTable1701467139671
  implements MigrationInterface
{
  name = 'UpdateDataTriwulanTable1701467139671';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD "contract_date" date`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD "pic_name" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD "leader_name" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD "optional" character varying`,
    );
    await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "reason" text`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "data_triwulan" DROP COLUMN "reason"`);
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP COLUMN "optional"`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP COLUMN "leader_name"`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP COLUMN "pic_name"`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP COLUMN "contract_date"`,
    );
  }
}
