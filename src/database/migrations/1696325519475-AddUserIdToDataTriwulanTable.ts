import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddUserIdToDataTriwulanTable1696325519475
  implements MigrationInterface
{
  name = 'AddUserIdToDataTriwulanTable1696325519475';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "data_triwulan" ADD "user_id" bigint`);
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD CONSTRAINT "fk_user_id_data_triwulan" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP CONSTRAINT "fk_user_id_data_triwulan"`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP COLUMN "user_id"`,
    );
  }
}
