import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddSubActivityToActivityTable1695886981496
  implements MigrationInterface
{
  name = 'AddSubActivityToActivityTable1695886981496';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "activity" ADD "sub_activity" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "activity" DROP COLUMN "sub_activity"`,
    );
  }
}
