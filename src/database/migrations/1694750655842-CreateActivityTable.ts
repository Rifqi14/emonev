import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateActivityTable1694750655842 implements MigrationInterface {
  name = 'CreateActivityTable1694750655842';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "activity" ("id" BIGSERIAL NOT NULL, "title" character varying NOT NULL, "code" character varying NOT NULL, "program_id" bigint, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_activity" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "idx_code_unique_activity" ON "activity" ("code") `,
    );
    await queryRunner.query(
      `ALTER TABLE "activity" ADD CONSTRAINT "fk_program_id_activity" FOREIGN KEY ("program_id") REFERENCES "program"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "activity" DROP CONSTRAINT "fk_program_id_activity"`,
    );
    await queryRunner.query(`DROP INDEX "public"."idx_code_unique_activity"`);
    await queryRunner.query(`DROP TABLE "activity"`);
  }
}
