import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateProgramTable1694748593805 implements MigrationInterface {
  name = 'CreateProgramTable1694748593805';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "program" ("id" BIGSERIAL NOT NULL, "title" character varying NOT NULL, "code" character varying NOT NULL, "status_id" integer NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_program" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "program" ADD CONSTRAINT "fk_status_id_program" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "program" DROP CONSTRAINT "fk_status_id_program"`,
    );
    await queryRunner.query(`DROP TABLE "program"`);
  }
}
