import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateProgramTable1694749433597 implements MigrationInterface {
  name = 'UpdateProgramTable1694749433597';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "program" DROP CONSTRAINT "fk_status_id_program"`,
    );
    await queryRunner.query(
      `ALTER TABLE "program" ALTER COLUMN "status_id" DROP NOT NULL`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "idx_code_unique_program" ON "program" ("code") `,
    );
    await queryRunner.query(
      `ALTER TABLE "program" ADD CONSTRAINT "fk_status_id_program" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "program" DROP CONSTRAINT "fk_status_id_program"`,
    );
    await queryRunner.query(`DROP INDEX "public"."idx_code_unique_program"`);
    await queryRunner.query(
      `ALTER TABLE "program" ALTER COLUMN "status_id" SET NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "program" ADD CONSTRAINT "fk_status_id_program" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
