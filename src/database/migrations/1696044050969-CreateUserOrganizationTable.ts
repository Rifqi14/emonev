import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateUserOrganizationTable1696044050969
  implements MigrationInterface
{
  name = 'CreateUserOrganizationTable1696044050969';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "user_organization" ("user_id" bigint NOT NULL, "organization_id" integer NOT NULL, CONSTRAINT "pk_user_organization" PRIMARY KEY ("user_id", "organization_id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_organization" ADD CONSTRAINT "fk_organization_id_user_organization" FOREIGN KEY ("organization_id") REFERENCES "organization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_organization" ADD CONSTRAINT "fk_user_id_user_organization" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user_organization" DROP CONSTRAINT "fk_user_id_user_organization"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user_organization" DROP CONSTRAINT "fk_organization_id_user_organization"`,
    );
    await queryRunner.query(`DROP TABLE "user_organization"`);
  }
}
