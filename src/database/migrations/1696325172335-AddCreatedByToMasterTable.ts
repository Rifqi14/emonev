import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddCreatedByToMasterTable1696325172335
  implements MigrationInterface
{
  name = 'AddCreatedByToMasterTable1696325172335';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "master" ADD "user_id" bigint`);
    await queryRunner.query(
      `ALTER TABLE "master" ADD CONSTRAINT "fk_user_id_master" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "master" DROP CONSTRAINT "fk_user_id_master"`,
    );
    await queryRunner.query(`ALTER TABLE "master" DROP COLUMN "user_id"`);
  }
}
