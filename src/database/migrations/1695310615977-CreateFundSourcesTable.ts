import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateFundSourcesTable1695310615977 implements MigrationInterface {
  name = 'CreateFundSourcesTable1695310615977';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "fund_sources" ("id" BIGSERIAL NOT NULL, "name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_fund_source" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "fund_sources"`);
  }
}
