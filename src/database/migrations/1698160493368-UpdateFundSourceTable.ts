import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateFundSourceTable1698160493368 implements MigrationInterface {
  name = 'UpdateFundSourceTable1698160493368';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "fund_sources" ADD "fund_source_total" numeric(20,2) DEFAULT '0'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "fund_sources" DROP COLUMN "fund_source_total"`,
    );
  }
}
