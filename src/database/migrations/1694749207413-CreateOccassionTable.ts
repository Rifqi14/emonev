import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateOccassionTable1694749207413 implements MigrationInterface {
  name = 'CreateOccassionTable1694749207413';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "occassion" ("id" BIGSERIAL NOT NULL, "code" character varying NOT NULL, "title" character varying NOT NULL, "status_id" integer, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_occassion" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "idx_code_unique_occassion" ON "occassion" ("code") `,
    );
    await queryRunner.query(
      `ALTER TABLE "occassion" ADD CONSTRAINT "fk_status_id_occassion" FOREIGN KEY ("status_id") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "occassion" DROP CONSTRAINT "fk_status_id_occassion"`,
    );
    await queryRunner.query(`DROP INDEX "public"."idx_code_unique_occassion"`);
    await queryRunner.query(`DROP TABLE "occassion"`);
  }
}
