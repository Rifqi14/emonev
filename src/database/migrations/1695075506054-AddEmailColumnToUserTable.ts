import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddEmailColumnToUserTable1695075506054
  implements MigrationInterface
{
  name = 'AddEmailColumnToUserTable1695075506054';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" ADD "email" character varying`);
    await queryRunner.query(
      `CREATE UNIQUE INDEX "idx_email_unique_user" ON "user" ("email") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX "public"."idx_email_unique_user"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "email"`);
  }
}
