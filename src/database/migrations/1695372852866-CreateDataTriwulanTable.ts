import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDataTriwulanTable1695372852866
  implements MigrationInterface
{
  name = 'CreateDataTriwulanTable1695372852866';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "data_triwulan" ("id" BIGSERIAL NOT NULL, "activity_name" character varying NOT NULL, "activity_location" character varying NOT NULL, "fund_source_id" bigint, "fund_ceiling" numeric(16,2) NOT NULL DEFAULT '0', "management_organization" character varying, "pptk_name" character varying, "contract_number_date" character varying, "contractor_name" character varying, "implementation_period" character varying, "contract_value" numeric(16,2) NOT NULL DEFAULT '0', "physical_realization" numeric(16,2) NOT NULL DEFAULT '0', "fund_realization" numeric(16,2) NOT NULL DEFAULT '0', "activity_volume" character varying, "activity_output" character varying, "direct_target_group" character varying, "indirect_target_group" character varying, "local_workforce" numeric(16,2) NOT NULL DEFAULT '0', "non_local_workforce" numeric(16,2) NOT NULL DEFAULT '0', "problems" character varying, "solution" character varying, "procurement_type_id" bigint, "procurement_method_id" bigint, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "pk_data_triwulan" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD CONSTRAINT "fk_fund_source_id_data_triwulan" FOREIGN KEY ("fund_source_id") REFERENCES "fund_sources"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD CONSTRAINT "fk_procurement_method_id_data_triwulan" FOREIGN KEY ("procurement_method_id") REFERENCES "procurement_method"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" ADD CONSTRAINT "fk_procurement_type_id_data_triwulan" FOREIGN KEY ("procurement_type_id") REFERENCES "procurement_type"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP CONSTRAINT "fk_procurement_type_id_data_triwulan"`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP CONSTRAINT "fk_procurement_method_id_data_triwulan"`,
    );
    await queryRunner.query(
      `ALTER TABLE "data_triwulan" DROP CONSTRAINT "fk_fund_source_id_data_triwulan"`,
    );
    await queryRunner.query(`DROP TABLE "data_triwulan"`);
  }
}
