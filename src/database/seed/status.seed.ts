import { Logger } from '@nestjs/common';
import status from 'src/database/data/status.json';
import { DataSource } from 'typeorm';
import { Status } from '../models/status.entity';

export const statusSeed = async (ds: DataSource) => {
  if (status) {
    const manager = ds.manager;
    const logger = new Logger();

    status.forEach((status) => {
      manager
        .exists(Status, { where: { id: status.id } })
        .then(async (exist) => {
          if (!exist) {
            manager
              .create(Status, { id: status.id, name: status.name })
              .save()
              .then((res) =>
                logger.log(`Success seed status: ${res.name}`, 'SeedStatus'),
              )
              .catch((err) =>
                logger.error(
                  `Error seed status: ${status.name} with error: ${err}`,
                  'SeedStatus',
                ),
              );
          }
        });
    });
  }
};
