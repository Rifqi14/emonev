import { Logger } from '@nestjs/common';
import triwulans from 'src/database/data/triwulan.json';
import { DataSource } from 'typeorm';
import { Triwulan } from '../models/triwulan.entity';

export const triwulanSeed = async (ds: DataSource) => {
  if (triwulans) {
    const transaction = ds.createQueryRunner();

    await transaction.startTransaction();
    const logger = new Logger();

    try {
      triwulans.forEach(async (triwulan) => {
        await transaction.manager
          .exists(Triwulan, { where: { name: triwulan.name } })
          .then(async (exist) => {
            if (!exist) {
              await transaction.manager
                .create(Triwulan, { id: triwulan.id, name: triwulan.name })
                .save()
                .then((res) =>
                  logger.log(
                    `Success triwulan status: ${res.name}`,
                    'SeedTriwulan',
                  ),
                )
                .catch((err) =>
                  logger.error(
                    `Error triwulan status: ${triwulan.name} with error: ${err}`,
                    'SeedTriwulan',
                  ),
                );
            }
          });
      });
    } catch (error) {
      await transaction.rollbackTransaction();
      throw error;
    }
  }
};
