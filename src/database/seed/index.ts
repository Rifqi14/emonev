import { config } from 'dotenv';
import { join } from 'path';
import { DataSource } from 'typeorm';
import { roleSeed } from './role.seed';
import { settingSeed } from './setting.seed';
import { statusSeed } from './status.seed';
import { triwulanSeed } from './triwulan.seed';
import { userSeed } from './user.seed';
config();

export const seed = async (ds?: DataSource) => {
  const conn =
    ds ??
    (await new DataSource({
      type: 'postgres',
      host: process.env.DATABASE_HOST,
      port: process.env.DATABASE_PORT ? +process.env.DATABASE_PORT : 5432,
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASS,
      database: process.env.DATABASE_NAME,
      entities: [join(__dirname, '..', '..', '**', '*.entity{.ts,.js}')],
    }).initialize());
  await statusSeed(conn);
  await roleSeed(conn);
  await userSeed(conn);
  await triwulanSeed(conn);
  await settingSeed(conn);
};

seed();
