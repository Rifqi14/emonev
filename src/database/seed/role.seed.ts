import { Logger } from '@nestjs/common';
import roles from 'src/database/data/roles.json';
import { DataSource } from 'typeorm';
import { Role } from '../models/role.entity';

export const roleSeed = async (ds: DataSource) => {
  if (roles) {
    const manager = ds.manager;
    const logger = new Logger();

    roles.forEach(async (role) => {
      await manager
        .exists(Role, { where: { name: role.name } })
        .then(async (exist) => {
          if (!exist) {
            manager
              .create(Role, { id: role.id, name: role.name })
              .save()
              .then((res) =>
                logger.log(`Success seed role: ${res.name}`, 'SeedRole'),
              )
              .catch((err) =>
                logger.error(
                  `Error seed role: ${role.name} with error: ${err}`,
                  'SeedRole',
                ),
              );
          }
        });
    });
  }
};
