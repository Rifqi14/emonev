import { Logger } from '@nestjs/common';
import users from 'src/database/data/users.json';
import { DataSource } from 'typeorm';
import { Role } from '../models/role.entity';
import { Status } from '../models/status.entity';
import { User } from '../models/user.entity';

export const userSeed = async (ds: DataSource) => {
  if (users) {
    const manager = ds.manager;
    const logger = new Logger();

    users.forEach(async (user) => {
      await manager
        .exists(User, { where: { username: user.username } })
        .then(async (exist) => {
          if (!exist) {
            await manager
              .create(User, {
                id: user.id,
                name: user.name,
                username: user.username,
                password: user.username,
                role: await manager
                  .findOneByOrFail(Role, { name: user.role })
                  .catch(async (_) => {
                    const role = manager.create(Role, {
                      name: user.role,
                    });

                    await manager.insert(Role, role);
                    return role;
                  }),
                status: await manager.findOneByOrFail(Status, {
                  name: user.status,
                }),
              })
              .save()
              .then((res) =>
                logger.log(`Success seed user: ${res.name}`, 'SeedUser'),
              )
              .catch((err) =>
                logger.error(
                  `Error seed user: ${user.name} with error: ${err}`,
                  'SeedUser',
                ),
              );
          }
        });
    });
  }
};
