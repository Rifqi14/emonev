import { Logger } from '@nestjs/common';
import { SeedSetting } from 'src/utils/type';
import { DataSource } from 'typeorm';
import { Setting } from '../models/setting.entity';

export const settingSeed = async (ds: DataSource) => {
  if (SeedSetting) {
    const manager = ds.manager;
    const logger = new Logger();

    SeedSetting.forEach(async (setting) => {
      await manager
        .exists(Setting, { where: { id: setting.id } })
        .then(async (exist) => {
          if (!exist) {
            await manager
              .create(Setting, { ...setting })
              .save()
              .then((res) =>
                logger.log(`Success seed setting: ${res.key}`, 'SeedSetting'),
              )
              .catch((err) =>
                logger.error(
                  `Error seed setting: ${setting.key} with error: ${err}`,
                  'SeedSetting',
                ),
              );
          }
        });
    });
  }
};
