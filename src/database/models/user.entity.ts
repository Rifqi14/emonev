import { ApiProperty } from '@nestjs/swagger';
import { genSaltSync, hashSync } from 'bcrypt';
import { Exclude } from 'class-transformer';
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Organization } from './organization.entity';
import { Role } from './role.entity';
import { Status } from './status.entity';
import { UserOrganization } from './user-organization.entity';

@Entity()
@Index('idx_username_unique', ['username'], { unique: true })
@Index('idx_email_unique_user', ['email'], { unique: true })
export class User extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    type: 'int8',
    primaryKeyConstraintName: 'pk_user',
  })
  id: number;

  @ApiProperty()
  @Column('varchar')
  username: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  email?: string;

  @Exclude({ toPlainOnly: true })
  @Column('varchar', { select: false })
  password: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  name?: string;

  @ApiProperty()
  @Column('int', { nullable: true })
  organization_id?: number;

  @ApiProperty()
  @Column('int')
  role_id: number;

  @ApiProperty()
  @Column('int', { nullable: true })
  status_id?: number;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  created_by?: string;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;

  @ApiProperty()
  @ManyToOne(() => User, { nullable: true })
  @JoinColumn({
    name: 'created_by',
    foreignKeyConstraintName: 'fk_created_by_user',
    referencedColumnName: 'username',
  })
  createdBy?: User;

  @ApiProperty()
  @ManyToOne(() => Organization, { nullable: true })
  @JoinColumn({
    name: 'organization_id',
    foreignKeyConstraintName: 'fk_organization_id_user',
  })
  organization?: Organization;

  @ApiProperty()
  @ManyToOne(() => Role)
  @JoinColumn({ name: 'role_id', foreignKeyConstraintName: 'fk_role_id_user' })
  role: Role;

  @ApiProperty()
  @ManyToOne(() => Status, { nullable: true })
  @JoinColumn({
    name: 'status_id',
    foreignKeyConstraintName: 'fk_status_id_user',
  })
  status?: Status;

  @ApiProperty()
  @OneToMany(
    () => UserOrganization,
    (userOrganization) => userOrganization.user,
    { cascade: true },
  )
  userOrganization: UserOrganization[];

  @BeforeInsert()
  @BeforeUpdate()
  createPassword() {
    if (this.password) {
      const salt = genSaltSync(10);
      this.password = hashSync(this.password.replace(' ', ''), salt);
    }
  }
}
