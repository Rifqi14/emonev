import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Status } from './status.entity';

@Entity()
@Index('idx_code_unique_occassion', ['code'], { unique: true })
export class Occassion extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    type: 'int8',
    primaryKeyConstraintName: 'pk_occassion',
  })
  id: number;

  @ApiProperty()
  @Column('varchar')
  code: string;

  @ApiProperty()
  @Column('varchar')
  title: string;

  @ApiProperty()
  @Column('int', { nullable: true })
  status_id?: number;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;

  @ApiProperty({ type: Status })
  @ManyToOne(() => Status, { nullable: true })
  @JoinColumn({
    name: 'status_id',
    foreignKeyConstraintName: 'fk_status_id_occassion',
  })
  status?: Status;
}
