import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Activity } from './activity.entity';
import { FundSources } from './fund_sources.entity';
import { User } from './user.entity';

@Entity()
export class DataTriwulan extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    type: 'int8',
    primaryKeyConstraintName: 'pk_data_triwulan',
  })
  id: number;

  @ApiProperty()
  @Column('varchar')
  activity_name: string;

  @ApiProperty()
  @Column('varchar')
  activity_location: string;

  @ApiProperty()
  @Column('int8', { nullable: true })
  fund_source_id?: number;

  @ApiProperty()
  @Column({ type: 'numeric', scale: 2, precision: 16, default: 0 })
  fund_ceiling: number;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  management_organization?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  pptk_name?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  contract_number_date?: string;

  @ApiProperty()
  @Column('date', { nullable: true })
  contract_date?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  contractor_name?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  implementation_period?: string;

  @ApiProperty()
  @Column({ type: 'numeric', scale: 2, precision: 16, default: 0 })
  contract_value: number;

  @ApiProperty()
  @Column({ type: 'numeric', scale: 2, precision: 16, default: 0 })
  physical_realization: number;

  @ApiProperty()
  @Column({ type: 'numeric', scale: 2, precision: 16, default: 0 })
  fund_realization: number;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  activity_volume?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  activity_output?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  direct_target_group?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  indirect_target_group?: string;

  @ApiProperty()
  @Column({ type: 'numeric', scale: 2, precision: 16, default: 0 })
  local_workforce: number;

  @ApiProperty()
  @Column({ type: 'numeric', scale: 2, precision: 16, default: 0 })
  non_local_workforce: number;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  problems?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  solution?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  procurement_type?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  procurement_method?: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  file?: string;

  @ApiProperty()
  @Column('int8', { nullable: true })
  user_id?: number;

  @ApiProperty()
  @Column('int8', { nullable: true })
  activity_id?: number;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  activity_form?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  pic_name?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  leader_name?: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  optional?: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  reason?: string;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => FundSources, { nullable: true })
  @JoinColumn({
    name: 'fund_source_id',
    foreignKeyConstraintName: 'fk_fund_source_id_data_triwulan',
  })
  fundSource?: FundSources;

  @ManyToOne(() => User, { nullable: true })
  @JoinColumn({
    name: 'user_id',
    foreignKeyConstraintName: 'fk_user_id_data_triwulan',
  })
  createdBy?: User;

  @ManyToOne(() => Activity, { nullable: true })
  @JoinColumn({
    name: 'activity_id',
    foreignKeyConstraintName: 'fk_activity_id_data_triwulan',
  })
  activity?: Activity;
}
