import { ApiProperty } from '@nestjs/swagger';
import { SettingDataType } from 'src/utils/type';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Setting extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    primaryKeyConstraintName: 'pk_setting',
    type: 'int8',
  })
  id: number;

  @ApiProperty()
  @Column('varchar')
  key: string;

  @ApiProperty()
  @Column('text')
  value: string;

  @ApiProperty()
  @Column('varchar')
  data_type: SettingDataType;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;
}
