import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Occassion } from './occassion.entity';
import { Master } from './master.entity';

@Entity()
export class MasterOccassion extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    type: 'int8',
    primaryKeyConstraintName: 'pk_master_occassion',
  })
  id: number;

  @ApiProperty()
  @Column('int8', { nullable: true })
  master_id?: number;

  @ApiProperty()
  @Column('int8', { nullable: true })
  occassion_id?: number;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => Master, { nullable: true })
  @JoinColumn({
    name: 'master_id',
    foreignKeyConstraintName: 'fk_master_id_master_occassion',
  })
  master?: Master;

  @ApiProperty()
  @ManyToOne(() => Occassion, { nullable: true })
  @JoinColumn({
    name: 'occassion_id',
    foreignKeyConstraintName: 'fk_occassion_id_master_occassion',
  })
  occassion?: Occassion;
}
