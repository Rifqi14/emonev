import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Program } from './program.entity';

@Entity()
@Index('idx_code_unique_activity', ['code'], { unique: true })
export class Activity extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    type: 'int8',
    primaryKeyConstraintName: 'pk_activity',
  })
  id: number;

  @ApiProperty()
  @Column('varchar')
  title: string;

  @ApiProperty()
  @Column('varchar')
  code: string;

  @ApiProperty()
  @Column('varchar', { nullable: true })
  sub_activity?: string;

  @ApiProperty()
  @Column('int8', { nullable: true })
  program_id?: number;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;

  @ApiProperty()
  @ManyToOne(() => Program, { nullable: true })
  @JoinColumn({
    name: 'program_id',
    foreignKeyConstraintName: 'fk_program_id_activity',
  })
  program?: Program;
}
