import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Status } from './status.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Occassion } from './occassion.entity';

@Entity()
@Index('idx_code_unique_program', ['code'], { unique: true })
export class Program extends BaseEntity {
  @ApiProperty({ type: Number })
  @PrimaryGeneratedColumn('increment', {
    type: 'int8',
    primaryKeyConstraintName: 'pk_program',
  })
  id: number;

  @ApiProperty({ type: String })
  @Column('varchar')
  title: string;

  @ApiProperty({ type: String })
  @ApiProperty()
  @Column('varchar')
  code: string;

  @ApiProperty({ type: Number })
  @Column('int8', { nullable: true })
  occassion_id?: number;

  @ApiProperty({ type: Number })
  @Column('int', { nullable: true })
  status_id?: number;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;

  @ApiProperty()
  @ManyToOne(() => Occassion, { nullable: true })
  @JoinColumn({
    foreignKeyConstraintName: 'fk_occassion_id_program',
    name: 'occassion_id',
  })
  occassion?: Occassion;

  @ApiProperty({ type: Status })
  @ManyToOne(() => Status, { nullable: true })
  @JoinColumn({
    foreignKeyConstraintName: 'fk_status_id_program',
    name: 'status_id',
  })
  status?: Status;
}
