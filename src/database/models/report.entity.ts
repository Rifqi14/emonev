import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Triwulan } from './triwulan.entity';
import { Organization } from './organization.entity';
import { Occassion } from './occassion.entity';
import { Program } from './program.entity';

@Entity()
export class Report extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    type: 'int8',
    primaryKeyConstraintName: 'pk_report',
  })
  id: number;

  @ApiProperty()
  @Column('text', { nullable: true })
  description?: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  program_description?: string;

  @ApiProperty()
  @Column('int8', { nullable: true })
  triwulan_id?: number;

  @ApiProperty()
  @Column('int8', { nullable: true })
  organization_id?: number;

  @ApiProperty()
  @Column('int8', { nullable: true })
  occassion_id?: number;

  @ApiProperty()
  @Column('int8', { nullable: true })
  program_id?: number;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;

  @ApiProperty()
  @ManyToOne(() => Triwulan, { nullable: true })
  @JoinColumn({
    name: 'triwulan_id',
    foreignKeyConstraintName: 'fk_triwulan_id_report',
  })
  triwulan?: Triwulan;

  @ApiProperty()
  @ManyToOne(() => Organization, { nullable: true })
  @JoinColumn({
    name: 'organization_id',
    foreignKeyConstraintName: 'fk_organization_id_report',
  })
  organization?: Organization;

  @ApiProperty()
  @ManyToOne(() => Occassion, { nullable: true })
  @JoinColumn({
    name: 'occassion_id',
    foreignKeyConstraintName: 'fk_occassion_id_report',
  })
  occassion?: Occassion;

  @ApiProperty()
  @ManyToOne(() => Program, { nullable: true })
  @JoinColumn({
    name: 'program_id',
    foreignKeyConstraintName: 'fk_program_id_report',
  })
  program?: Program;
}
