import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Triwulan } from './triwulan.entity';
import { Purpose } from './purpose.entity';
import { Organization } from './organization.entity';
import { MasterOccassion } from './master_occassion.entity';
import { User } from './user.entity';

@Entity()
export class Master extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    type: 'int8',
    primaryKeyConstraintName: 'pk_master',
  })
  id: number;

  @ApiProperty()
  @Column('text', { nullable: true })
  description?: string;

  @ApiProperty()
  @Column('int8', { nullable: true })
  triwulan_id?: number;

  @ApiProperty()
  @Column('int8', { nullable: true })
  organization_id?: number;

  @ApiProperty()
  @Column('int8', { nullable: true })
  purpose_id?: number;

  @ApiProperty()
  @Column('int8', { nullable: true })
  user_id?: number;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;

  @ApiProperty()
  @ManyToOne(() => Triwulan, { nullable: true })
  @JoinColumn({
    name: 'triwulan_id',
    foreignKeyConstraintName: 'fk_triwulan_id_master',
  })
  triwulan?: Triwulan;

  @ApiProperty()
  @ManyToOne(() => Purpose, { nullable: true })
  @JoinColumn({
    name: 'purpose_id',
    foreignKeyConstraintName: 'fk_purpose_id_master',
  })
  purpose?: Purpose;

  @ApiProperty()
  @ManyToOne(() => Organization, { nullable: true })
  @JoinColumn({
    name: 'organization_id',
    foreignKeyConstraintName: 'fk_organization_id_master',
  })
  organization?: Organization;

  @ApiProperty({ type: [MasterOccassion] })
  @OneToMany(() => MasterOccassion, (masterOccasion) => masterOccasion.master, {
    eager: true,
    cascade: true,
  })
  masterOccassions: MasterOccassion[];

  @ApiProperty()
  @ManyToOne(() => User, { nullable: true })
  @JoinColumn({
    name: 'user_id',
    foreignKeyConstraintName: 'fk_user_id_master',
  })
  createdBy?: User;
}
