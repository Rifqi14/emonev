import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Status } from './status.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
@Index('idx_code_unique', ['code'], { unique: true })
export class Organization extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    primaryKeyConstraintName: 'pk_organization',
  })
  id: number;

  @ApiProperty()
  @Column('varchar')
  code: string;

  @ApiProperty()
  @Column('varchar')
  title: string;

  @ApiProperty()
  @Column('integer')
  status_id: number;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;

  @ApiProperty()
  @ManyToOne(() => Status)
  @JoinColumn({
    name: 'status_id',
    foreignKeyConstraintName: 'fk_status_id_organization',
  })
  status: Status;
}
