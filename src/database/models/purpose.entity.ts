import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Purpose extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('increment', {
    type: 'int8',
    primaryKeyConstraintName: 'pk_purpose',
  })
  id: number;

  @ApiProperty()
  @Column('varchar')
  title: string;

  @ApiProperty()
  @CreateDateColumn()
  created_at: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updated_at: Date;
}
