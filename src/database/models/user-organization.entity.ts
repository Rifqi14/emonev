import { ApiProperty } from '@nestjs/swagger';
import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Organization } from './organization.entity';
import { User } from './user.entity';

@Entity()
export class UserOrganization {
  @ApiProperty()
  @PrimaryColumn('int8', { primaryKeyConstraintName: 'pk_user_organization' })
  user_id: number;

  @ApiProperty()
  @PrimaryColumn('int8', { primaryKeyConstraintName: 'pk_user_organization' })
  organization_id: number;

  @ApiProperty()
  @ManyToOne(() => Organization, { eager: true })
  @JoinColumn({
    name: 'organization_id',
    foreignKeyConstraintName: 'fk_organization_id_user_organization',
  })
  organization: Organization;

  @ManyToOne(() => User)
  @JoinColumn({
    name: 'user_id',
    foreignKeyConstraintName: 'fk_user_id_user_organization',
  })
  user: User;
}
