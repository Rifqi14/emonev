import { DataSource } from 'typeorm';
import { connectionOptions } from './connection';

export default new DataSource(connectionOptions);
