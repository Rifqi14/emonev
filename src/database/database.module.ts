import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { connectionOptions } from './connection';

@Global()
@Module({
  imports: [TypeOrmModule.forRoot({ ...connectionOptions })],
  providers: [
    {
      provide: 'DATA_SOURCE',
      useFactory: async () => {
        const dataSource = new DataSource({ ...connectionOptions });
        const ds = dataSource.initialize();

        // ds.then(async (ds) => {
        //   await seed(ds);
        // });

        return ds;
      },
    },
  ],
  exports: [
    {
      provide: 'DATA_SOURCE',
      useExisting: true,
    },
  ],
})
export class DatabaseModule {}
