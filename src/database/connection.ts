import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv';
import { join } from 'path';
import { AppEnv, NodeEnvStageType } from 'src/common/configuration';
import { DataSourceOptions } from 'typeorm';

config();

const configService = new ConfigService<AppEnv>();

export const connectionOptions: DataSourceOptions = {
  type: 'postgres',
  host: configService.get<string>('DATABASE_HOST', 'localhost'),
  port: configService.get<number>('DATABASE_PORT', 5432),
  username: configService.get<string>('DATABASE_USER', 'postgres'),
  password: configService.get<string>('DATABASE_PASS', 'postgres'),
  database: configService.get<string>('DATABASE_NAME', 'postgres'),
  entities: [join(__dirname, '**', '*.entity{.ts,.js}')],
  migrations: [join(__dirname, 'migrations', '*{.ts,.js}')],
  logging:
    configService.get<NodeEnvStageType>('NODE_ENV', 'development') ===
    'development',
};
