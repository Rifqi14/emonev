import { Controller, Get, Inject } from '@nestjs/common';
import { WINSTON_MODULE_PROVIDER, WinstonLogger } from 'nest-winston';
import { AppService } from './app.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SwaggerOkResponse } from './common/openapi';
import {
  StaticRoleApiResponse,
  StaticStatusApiResponse,
  StaticTriwulanApiResponse,
} from './app.providers';
import { PublicAPI } from './modules/authentication/authentication.guard';

@ApiTags('Static')
@ApiBearerAuth()
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: WinstonLogger,
  ) {}

  @PublicAPI()
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @PublicAPI()
  @SwaggerOkResponse(StaticStatusApiResponse)
  @Get('static/status')
  async staticStatus(): Promise<StaticStatusApiResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.appService.getStatus(),
    };
  }

  @PublicAPI()
  @SwaggerOkResponse(StaticRoleApiResponse)
  @Get('static/admin-role')
  async staticRole(): Promise<StaticRoleApiResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.appService.getRole(),
    };
  }

  @PublicAPI()
  @SwaggerOkResponse(StaticTriwulanApiResponse)
  @Get('static/triwulan')
  async staticTriwulan(): Promise<StaticTriwulanApiResponse> {
    return {
      statusCode: 200,
      message: 'Pengambilan data berhasil',
      data: await this.appService.getTriwulan(),
    };
  }
}
