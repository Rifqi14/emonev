import { Provider } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { Triwulan } from './database/models/triwulan.entity';
import { Status } from './database/models/status.entity';
import { Role } from './database/models/role.entity';
import { BaseApiResponse } from './common/api-response';
import { ApiProperty } from '@nestjs/swagger';

export const appProviders: Array<Provider> = [
  {
    provide: 'TRIWULAN_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Triwulan),
    inject: ['DATA_SOURCE'],
  },
  {
    provide: 'STATUS_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Status),
    inject: ['DATA_SOURCE'],
  },
  {
    provide: 'ROLE_REPOSITORY',
    useFactory: (ds: DataSource) => ds.getRepository(Role),
    inject: ['DATA_SOURCE'],
  },
];

export class StaticStatusApiResponse extends BaseApiResponse {
  @ApiProperty({ type: [Status] })
  data: Array<Status>;
}

export class StaticTriwulanApiResponse extends BaseApiResponse {
  @ApiProperty({ type: [Triwulan] })
  data: Array<Triwulan>;
}

export class StaticRoleApiResponse extends BaseApiResponse {
  @ApiProperty({ type: [Role] })
  data: Array<Role>;
}
